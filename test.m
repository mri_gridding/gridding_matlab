% testX = zeros(768, 768, 768);
% for dim = 1:768
%     testX(dim, :, :) = grid_data_3D(:, :, dim);
% end
% clear grid_data_3D
% testY = zeros(768, 768, 768);
% for dim = 1:768
%     testY(:, dim, :) = testX(:, :, dim);
% end
% clear testX
% testZ = circshift(testY, [3 3 3]);
% clear testY
% nrms(testZ, python_griddat_3d_cube);
% done=1;

% for i = 0:x_tile_dim-1
%     for j = 0:y_tile_dim-1
%         for k = 0:z_tile_dim-1
%             x_min = i*x_accel_dim +1;
%             x_max = (i+1)*x_accel_dim;
%             y_min = j*y_accel_dim +1;
%             y_max = (j+1)*y_accel_dim;
%             z_min = i*z_accel_dim +1;
%             z_max = (i+1)*z_accel_dim;
%             tile_num = z_tile_dim*j*i+x_tile_dim*j+i +1;
%             grid_data_tiled_3D(x_min:x_max, y_min:y_max, z_min:z_max) = grid_data(:, :, :, tile_num);
%         end
%     end
% end



x_accel_dim    = 8;
y_accel_dim    = 8;
x_mem_dim      = 1024;
y_mem_dim      = 1024;
x_mem_tile_dim = x_mem_dim / x_accel_dim;
y_mem_tile_dim = y_mem_dim / y_accel_dim;
x_grid_dim     = 1024;
y_grid_dim     = 1024;
x_tile_dim     = x_grid_dim / x_accel_dim;
y_tile_dim     = y_grid_dim / y_accel_dim;
untiled_data      = zeros(x_grid_dim, y_grid_dim);
for i = 0:x_tile_dim-1
    for j = 0:y_tile_dim-1
        x_min = i*x_accel_dim +1;
        x_max = (i+1)*x_accel_dim;
        y_min = j*y_accel_dim +1;
        y_max = (j+1)*y_accel_dim;
        tile_num = x_mem_tile_dim*j+i +1;
        untiled_data(x_min:x_max, y_min:y_max) = test2(:,:,tile_num);
    end
end


% x_len = 2^nextpow2(size(grid_data, 1));
% y_len = 2^nextpow2(size(grid_data, 2));
% 
% grid_data = padarray(grid_data, [(x_len-size(grid_data, 1)) (y_len-size(grid_data, 2))], 0, 'post');
% 
% % Do an IFFT of each col
% for col = 1:1
%     x = grid_data(:, col);
%     p=nextpow2(length(x));                 % checking the size of the input array
%     x=swap_complex_components(x); % can also just take the conjugate; x=conj(x)
%     x=[x zeros(1,(2^p)-length(x))];        % complementing an array of zeros if necessary
%     N=length(x);                           % computing the array size
%     S=log2(N);                             % computing the number of conversion stages
%     Half=1;                                % Seting the initial "Half" value
%     x=bitrevorder(x);                      % Placing the data samples in bit-reversed order
%     for stage=1:S                          % stages of transformation
%         for index=0:(2^stage):(N-1)        % series of "butterflies" for each stage
%             for n=0:(Half-1)               % creating "butterfly" and saving the results
%                 pos=n+index+1;             % index of the data sample
%                 pow=(2^(S-stage))*n;       % part of power of the complex multiplier
%                 w=exp((-1i)*(2*pi)*pow/N); % complex multiplier
%                 a=x(pos)+x(pos+Half).*w;   % 1-st part of the "butterfly" creating operation
%                 b=x(pos)-x(pos+Half).*w;   % 2-nd part of the "butterfly" creating operation
%                 x(pos)=a;                  % saving computation of the 1-st part
%                 x(pos+Half)=b;             % saving computation of the 2-nd part
%             end
%         end
%     Half=2*Half;                           % computing the next "Half" value
%     end
%     x=swap_complex_components(x)./1024; % can also just take the conjugate; x=conj(x)
%     grid_data(:, col) = x;               % returning the result from function
% end