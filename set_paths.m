function paths = set_paths()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script sets sets the paths for all scripts. Please put
% all paths needed here and load this file.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath(genpath('data'));
addpath(genpath('comb_gridding'));
addpath(genpath('gridding'));
addpath(genpath('regridding'));
addpath(genpath('FFT2'));
addpath(genpath('helper_scripts'));
