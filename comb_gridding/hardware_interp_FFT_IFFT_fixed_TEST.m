function [ hardware_interp_result ] = hardware_interp_FFT_IFFT_fixed_TEST(input_data, nufft_type, fi_cfg, config)
%   Detailed explanation goes here
% kdat --- complex magnitude; one complex value for each tm coordinate
% dims --- dimensions of the output grid
% table_mat --- interpolation table; each dim (x or y) has one row
% numpoints --- size of the interpolation window
% Jlist --- interpolation window indices for numpoints*numpoints elements
% L --- table oversampling factor (L table points between every grid point)
% tm --- coordinates for the kdata samples
% griddat --- output grid arranged as a 1D vector

%%%%%%%%%% Assign Inputs %%%%%%%%%%
x_accel_dim    = config.hardware.x_accel_dim;
y_accel_dim    = config.hardware.y_accel_dim;
x_mem_dim      = config.hardware.x_mem_dim;
y_mem_dim      = config.hardware.y_mem_dim;
x_mem_tile_dim = config.hardware.x_mem_tile_dim;
y_mem_tile_dim = config.hardware.y_mem_tile_dim;

x_grid_dim     = config.image.x_grid_dim;
y_grid_dim     = config.image.y_grid_dim;
x_tile_dim     = config.image.x_tile_dim;
y_tile_dim     = config.image.y_tile_dim;
x_img_dim      = config.image.x_img_dim;
y_img_dim      = config.image.y_img_dim;
x_img_tile_dim = config.image.x_img_tile_dim;
y_img_tile_dim = config.image.y_img_tile_dim;

nonuniform_data  = input_data.nonuniform_data;
nonuniform_coord = quantize(fi_cfg.input_coord, input_data.nonuniform_coord);
interp_table     = quantize(fi_cfg.interp_table, input_data.table);
table_oversamp   = input_data.table_oversamp;
table_x_dim      = input_data.table_x_dim;
table_y_dim      = input_data.table_y_dim;
griddat          = input_data.griddat;

num_input_points = size(nonuniform_coord, 2);

if(x_grid_dim > x_mem_dim || y_grid_dim > y_mem_dim)
    disp('ERROR: Input dimensions don not fit inside memory!');
end

grid_data_tiled = zeros(x_accel_dim, y_accel_dim, ((x_mem_dim*y_mem_dim)/(x_accel_dim*y_accel_dim)));
grid_data       = reshape(griddat, x_grid_dim, y_grid_dim); % Reshape into a square and rotate to match hardware output

if(strcmp(nufft_type, 'forw'))
    input_data = complex(zeros(1, size(nonuniform_coord, 2)), 0);

    %%%%%%%%%% Pack Grid %%%%%%%%%%
    tic
    for i = 0:x_tile_dim-1
        for j = 0:y_tile_dim-1
            x_min = i*x_accel_dim +1;
            x_max = (i+1)*x_accel_dim;
            y_min = j*y_accel_dim +1;
            y_max = (j+1)*y_accel_dim;
            tile_num = x_mem_tile_dim*j+i +1;
            grid_data_tiled(:,:,tile_num) = grid_data(x_min:x_max, y_min:y_max);
        end
    end
    fprintf('3D Cube Pack Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
else
    input_data = quantize(fi_cfg.input_data, nonuniform_data);
end

% Pre-shift the data; this avoids the circshift previously required for grid_data (forw input, adj output)
nonuniform_coord(1, :) = rem(nonuniform_coord(1, :) + (table_x_dim/2), x_grid_dim);
nonuniform_coord(2, :) = rem(nonuniform_coord(2, :) + (table_y_dim/2), y_grid_dim);


%%%%%%%%%% Interpolate Data %%%%%%%%%%
output_file = fopen(sprintf('random.out'), 'w');
fileID1 = fopen(sprintf('random.fileID1.out'), 'w');
fileID2 = fopen(sprintf('random.fileID2.out'), 'w');
fileID3 = fopen(sprintf('random.fileID3.out'), 'w');
fileID4 = fopen(sprintf('random.fileID4.out'), 'w');
fileID5 = fopen(sprintf('random.fileID5.out'), 'w');
fileID6 = fopen(sprintf('random.fileID6.out'), 'w');
fileIDX = fopen(sprintf('random.fileIDX.out'), 'w');
tic
% Loop over accelerator pipelines to process input point
for x_accel_idx = 0:x_accel_dim-1
    for y_accel_idx = 0:y_accel_dim-1
        % Process the input array one point at a time, updating target grid points
        for input_point_idx = 1:num_input_points
            % Extract the current point being processed
            input_point_coord = nonuniform_coord(:, input_point_idx);

            % Get the quotient and remainder to determine grid and tile location
            x_base_tile_idx = fix(input_point_coord(1)/x_accel_dim); % x virtual tile index
            y_base_tile_idx = fix(input_point_coord(2)/y_accel_dim); % y virtual tile index

            % Extract the "relative" coordinates (within a tile)
            x_coord = rem(input_point_coord(1), x_accel_dim);
            y_coord = rem(input_point_coord(2), y_accel_dim);

            % Determine if this pipeline is affected by this source point
            % If x_coord is within [x_accel_idx,x_accel_idx+table_x_dim) of the x_accel_idx, it does
            x_distance = rem(x_accel_dim+x_coord-x_accel_idx, x_accel_dim); % rem is implemented as an upper truncation
            % If y_coord is within [y_accel_idx,y_accel_idx+table_y_dim) of the y_accel_idx, it does
            y_distance = rem(y_accel_dim+y_coord-y_accel_idx, y_accel_dim); % rem is implemented as an upper truncation


            % fprintf(fileID1,"x_base_tile_idx=%d\ty_base_tile_idx=%d\tx_full_coord=%.4f\ty_full_coord=%.4f\tx_coord=%.4f\ty_coord=%.4f\tx_distance=%.4f\ty_distance=%.4f\n", x_base_tile_idx, y_base_tile_idx, floor(10000*input_point_coord(1))/10000, floor(10000*input_point_coord(2))/10000, floor(10000*x_coord)/10000, floor(10000*y_coord)/10000, floor(10000*x_distance)/10000, floor(10000*y_distance)/10000);

            if(x_distance < table_x_dim)
                if(y_distance < table_y_dim)
                    % This point affects this pipeline!

                    % Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
                    if(x_coord < x_accel_idx)
                        % A wrap occured!
                        if(x_base_tile_idx == 0)
                            x_tile_idx = x_tile_dim - 1;
                        else
                            x_tile_idx = x_base_tile_idx - 1;
                        end
                    else
                        x_tile_idx = x_base_tile_idx;
                    end

                    % Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
                    if(y_coord < y_accel_idx)
                        % A wrap occured!
                        if(y_base_tile_idx == 0)
                            y_tile_idx = y_tile_dim - 1;
                        else
                            y_tile_idx = y_base_tile_idx - 1;
                        end
                    else
                        y_tile_idx = y_base_tile_idx;
                    end

                    % Calculate the tile index ("depth" in the pipeline's data array)
                    tile_idx = y_tile_idx * x_mem_tile_dim + x_tile_idx; % 1D global virtual tile index

                    % Calculate the table index
                    x_table_idx = floor((x_distance * table_oversamp(1)) + 0.5); % *L (a power of 2) is implemented as a left shift
                    y_table_idx = floor((y_distance * table_oversamp(2)) + 0.5); % *L (a power of 2) is implemented as a left shift

                    % fprintf(fileID2,"input_point_idx=%d\tx_accel_idx=%d\ty_accel_idx=%d\tx_tile_idx=%d\ty_tile_idx=%d\ttile_idx=%d\tx_table_idx=%d\ty_table_idx=%d\n", input_point_idx-1, x_accel_idx, y_accel_idx, x_tile_idx, y_tile_idx, tile_idx, x_table_idx, y_table_idx);

                    % Extract the interp table values
                    x_table_val = interp_table(1, x_table_idx+1); % +1 because Matlab indexing starts at 1
                    y_table_val = interp_table(2, y_table_idx+1); % +1 because Matlab indexing starts at 1

                    % Split the complex value into its real and imaginary components
                    x_table_val_real = real(x_table_val);
                    x_table_val_imag = imag(x_table_val);
                    y_table_val_real = real(y_table_val);
                    y_table_val_imag = imag(y_table_val);


                    % fprintf(output_file,"x_accel_idx=%d\ty_accel_idx=%d\tx_base_tile_idx=%d\ty_base_tile_idx=%d\tx_coord=%.4f\ty_coord=%.4f\tx_distance=%.4f\ty_distance=%.4f\tx_tile_idx=%d\ty_tile_idx=%d\ttile_idx=%d\tx_table_idx=%d\ty_table_idx=%d\n", x_accel_idx, y_accel_idx, x_base_tile_idx, y_base_tile_idx, x_coord, y_coord, x_distance, y_distance, x_tile_idx, y_tile_idx, tile_idx, x_table_idx, y_table_idx);
                    % fprintf(output_file,"x_table_val_real=%.4f\tx_table_val_imag=%.4f\ty_table_val_real=%.4f\ty_table_val_imag=%.4f\n", x_table_val_real, x_table_val_imag, y_table_val_real, x_table_val_imag);

                    % Calculate 2D table value (product of X and Y values)
                    k1 = x_table_val_real + x_table_val_imag;
                    k2 = y_table_val_imag - y_table_val_real;
                    k3 = y_table_val_real + y_table_val_imag;
                    k4 = y_table_val_real * k1;
                    k5 = x_table_val_real * k2;
                    k6 = x_table_val_imag * k3;
                    R1 = k4 - k6;
                    I1 = k4 + k5;

                    % fprintf(fileIDX,"R1_tmp=%.20f\tR1=%.20f\tI1_tmp=%.20f\tI1=%.20f\n", R1, R1, I1, I1);

                    fprintf(fileID5,"x_table_val_real=%.4f\tx_table_val_imag=%.4f\ty_table_val_real=%.4f\ty_table_val_imag=%.4f\tk1=%.4f\tk2=%.4f\tk3=%.4f\tk4=%.4f\tk5=%.4f\tk6=%.4f\tR1=%.4f\tI1=%.4f\n", floor(10000*x_table_val_real)/10000, floor(10000*x_table_val_imag)/10000, floor(10000*y_table_val_real)/10000, floor(10000*y_table_val_imag)/10000, floor(10000*k1)/10000, floor(10000*k2)/10000, floor(10000*k3)/10000, floor(10000*k4)/10000, floor(10000*k5)/10000, floor(10000*k6)/10000, floor(10000*R1)/10000, floor(10000*I1)/10000);

                    if(strcmp(nufft_type, 'forw'))
                        % Extract the grid point being processed
                        input_point_data = grid_data_tiled(x_accel_idx+1, y_accel_idx+1, tile_idx+1); % +1 because Matlab indexing starts at 1
                        
                        % Split the complex value into its real and imaginary components
                        input_point_data_real = real(input_point_data);
                        input_point_data_imag = imag(input_point_data);
                    else
                        % Get the conjugate of the table value (invert sign of I1)
                        I1 = -I1;

                        % Extract the grid point being processed
                        input_point_data = input_data(:, input_point_idx);
                        
                        % Split the complex value into its real and imaginary components
                        input_point_data_real = real(input_point_data);
                        input_point_data_imag = imag(input_point_data);
                    end

                    % fprintf(output_file,"x_accel_idx=%d\ty_accel_idx=%d\ttile_idx=%d\tinput_point_data_real=%.4f\tinput_point_data_imag=%.4f\tx_table_val_real=%.4f\tx_table_val_imag=%.4f\ty_table_val_real=%.4f\ty_table_val_imag=%.4f\n", x_accel_idx, y_accel_idx, tile_idx, input_point_data_real, input_point_data_imag, x_table_val_real, x_table_val_imag, y_table_val_real, x_table_val_imag);
                    % fprintf(output_file,"x_accel_idx=%d\ty_accel_idx=%d\ttile_idx=%d\tinput_point_data_real=%.4f\tinput_point_data_imag=%.4f\n", x_accel_idx, y_accel_idx, tile_idx, input_point_data_real, input_point_data_imag);

                    % Calculate interpolation result
                    k7  = R1 + I1;
                    k8  = input_point_data_imag - input_point_data_real;
                    k9  = input_point_data_real + input_point_data_imag;
                    k10 = input_point_data_real * k7;
                    k11 = R1 * k8;
                    k12 = I1 * k9;
                    R2  = k10 - k12;
                    I2  = k10 + k11;
                    interp_result = complex(R2, I2);

                    fprintf(fileID6,"R1=%.4f\tI1=%.4f\tinput_point_data_real=%.4f\tinput_point_data_imag=%.4f\tk11=%.4f\tk7=%.4f\tk8=%.4f\tk9=%.4f\tk10=%.4f\tk11=%.4f\tk12=%.4f\tR2=%.4f\tI2=%.4f\n", floor(10000*R1)/10000, floor(10000*I1)/10000, floor(10000*input_point_data_real)/10000, floor(10000*input_point_data_imag)/10000, floor(10000*k11)/10000, floor(10000*k7)/10000, floor(10000*k8)/10000, floor(10000*k9)/10000, floor(10000*k10)/10000, floor(10000*k11)/10000, floor(10000*k12)/10000, floor(10000*R2)/10000, floor(10000*I2)/10000);

                    % fprintf(fileID3,"x_table_val_real=%.4f\tx_table_val_imag=%.4f\ty_table_val_real=%.4f\ty_table_val_imag=%.4f\tR1=%.4f\tI1=%.4f\n", floor(10000*x_table_val_real)/10000, floor(10000*x_table_val_imag)/10000, floor(10000*y_table_val_real)/10000, floor(10000*y_table_val_imag)/10000, floor(10000*R1)/10000, floor(10000*I1)/10000);
                    % fprintf(fileID4,"uniform_point_data_real=%.4f\tuniform_point_data_imag=%.4f\tR2=%.4f\tI2=%.4f\tnonuniform_point_data_real=%.4f\tnonuniform_point_data_imag=%.4f\n", floor(10000*input_point_data_real)/10000, floor(10000*input_point_data_imag)/10000, floor(10000*R2)/10000, floor(10000*I2)/10000, floor(10000*real(input_data(:, input_point_idx)))/10000, floor(10000*imag(input_data(:, input_point_idx)))/10000);

                    % Accumulate the interpolation result with the current value (either kdat sample or griddat memory)
                    if(strcmp(nufft_type, 'forw'))
                        input_data(:, input_point_idx) = interp_result + input_data(:, input_point_idx);
                        fprintf(fileIDX,"input_point_idx=%d\tnonuniform_point_data_real=%.4f\tnonuniform_point_data_imag=%.4f\n", input_point_idx-1, real(input_data(:, input_point_idx)), imag(input_data(:, input_point_idx)));
                    else
                        grid_data_tiled(x_accel_idx+1, y_accel_idx+1, tile_idx+1) = interp_result + grid_data_tiled(x_accel_idx+1, y_accel_idx+1, tile_idx+1);
                    end
                end
            end
        end
    end
end
if(strcmp(nufft_type, 'forw'))
    fprintf('3D Cube Regridding Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
else
    fprintf('3D Cube Gridding Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
end
fclose(output_file);
fclose(fileID1);
fclose(fileID2);
fclose(fileID3);
fclose(fileID4);
fclose(fileID5);
fclose(fileID6);
fclose(fileIDX);


%%%%%%%%%% Output Data %%%%%%%%%%
if(strcmp(nufft_type, 'forw'))
    hardware_interp_result = input_data;
else
    hardware_interp_result = grid_data_tiled;
end

end
