function [ hardware_interp_result ] = hardware_interp_fixed_parfor(python_filename, nufft_type, fi_cfg)
%   Detailed explanation goes here
load(python_filename, 'griddat', 'dims', 'table_mat', 'numpoints', 'Jlist', 'L', 'tm', 'kdat');
% kdat --- complex magnitude; one complex value for each tm coordinate
% dims --- dimensions of the output grid
% table_mat --- interpolation table; each dim (x or y) has one row
% numpoints --- size of the interpolation window
% Jlist --- interpolation window indices for numpoints*numpoints elements
% L --- table oversampling factor (L table points between every grid point)
% tm --- coordinates for the kdata samples
% griddat --- output grid arranged as a 1D vector

%%%%%%%%%% Assign Inputs %%%%%%%%%%
input_coord  = quantize(fi_cfg.input_coord, flipud(tm)); % flip the coordinates to match Python output
interp_table = quantize(fi_cfg.interp_table, table_mat);

table_oversamp = L;

num_input_points = size(input_coord, 2);

x_grid_dim = int64(dims(1));
y_grid_dim = int64(dims(2));

x_accel_dim = 8;
y_accel_dim = 8;

x_tile_dim = x_grid_dim / x_accel_dim;
y_tile_dim = y_grid_dim / y_accel_dim;

table_x_dim = nthroot(size(Jlist, 2), size(Jlist, 1));
table_y_dim = nthroot(size(Jlist, 2), size(Jlist, 1));

grid_data       = zeros(x_accel_dim, y_accel_dim, ((x_grid_dim*y_grid_dim)/(x_accel_dim*y_accel_dim)));
grid_data_tiled = reshape(griddat, dims(1), dims(1)); % Reshape into a square and rotate to match hardware output

if(strcmp(nufft_type, 'forw'))
    input_data = complex(zeros(1, size(input_coord, 2)), 0);

    % Rotate data structure to match the hardware memory layout
    grid_data_tiled = circshift(grid_data_tiled, [-3 -3]);

    %%%%%%%%%% Pack Grid %%%%%%%%%%
    tic
    for i = 0:x_tile_dim-1
        for j = 0:y_tile_dim-1
            x_min = i*x_accel_dim +1;
            x_max = (i+1)*x_accel_dim;
            y_min = j*y_accel_dim +1;
            y_max = (j+1)*y_accel_dim;
            tile_num = x_tile_dim*j+i +1;
            grid_data(:,:,tile_num) = grid_data_tiled(x_min:x_max, y_min:y_max);
        end
    end
    grid_data = quantize(fi_cfg.input_data, grid_data);
    toc
else
    input_data = quantize(fi_cfg.input_data, kdat);
    grid_data = reshape(grid_data, x_accel_dim*y_accel_dim, ((x_grid_dim*y_grid_dim)/(x_accel_dim*y_accel_dim)));
end

clear griddat dims table_mat numpoints Jlist L tm kdat


%%%%%%%%%% Interpolate Data %%%%%%%%%%
tic
parfor accel_idx = 1:x_accel_dim*y_accel_dim
    %%%%%%%%%% Parfor-Specific Setup %%%%%%%%%%
    fi_cfg_parfor = fixed_point_cfg(fi_cfg);
    x_accel_idx = mod((accel_idx-1), x_accel_dim);
    y_accel_idx = fix((accel_idx-1)/y_accel_dim);
    grid_data_array = grid_data(accel_idx, :);

    % Process the input array one point at a time, updating target grid points
    for input_point_idx = 1:num_input_points
        % Extract the current point being processed
        input_point_coord = input_coord(:, input_point_idx);

        % Make sure that the input coords don't exceed grid dim; if yes, subtract grid (or take mod)
        if(input_point_coord(1) >= x_grid_dim)
            input_point_coord(1) = input_point_coord(1) - x_grid_dim;
        end
        if(input_point_coord(2) >= y_grid_dim)
            input_point_coord(2) = input_point_coord(2) - y_grid_dim;
        end

        % Get the quotient and remainder to determine grid and tile location
        x_base_tile_idx = fix(input_point_coord(1)/x_accel_dim); % x virtual tile index
        y_base_tile_idx = fix(input_point_coord(2)/y_accel_dim); % y virtual tile index

        % Extract the "relative" coordinates (within a tile)
        x_coord = rem(input_point_coord(1), x_accel_dim);
        y_coord = rem(input_point_coord(2), y_accel_dim);

        % Determine if this pipeline is affected by this source point
        % If x_coord is within [x_accel_idx,x_accel_idx+table_x_dim) of the x_accel_idx, it does
        x_distance = rem(x_accel_dim+x_coord-x_accel_idx, x_accel_dim); % rem is implemented as an upper truncation
        if(x_distance < table_x_dim)
            % If y_coord is within [y_accel_idx,y_accel_idx+table_y_dim) of the y_accel_idx, it does
            y_distance = rem(y_accel_dim+y_coord-y_accel_idx, y_accel_dim); % rem is implemented as an upper truncation
            if(y_distance < table_y_dim)
                % This point affects this pipeline!

                % Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
                if(x_coord < x_accel_idx)
                    % A wrap occured!
                    if(x_base_tile_idx == 0)
                        x_tile_idx = x_tile_dim - 1;
                    else
                        x_tile_idx = x_base_tile_idx - 1;
                    end
                else
                    x_tile_idx = x_base_tile_idx;
                end

                % Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
                if(y_coord < y_accel_idx)
                    % A wrap occured!
                    if(y_base_tile_idx == 0)
                        y_tile_idx = y_tile_dim - 1;
                    else
                        y_tile_idx = y_base_tile_idx - 1;
                    end
                else
                    y_tile_idx = y_base_tile_idx;
                end

                % Calculate the tile index ("depth" in the pipeline's data array)
                tile_idx = y_tile_idx * x_tile_dim + x_tile_idx; % 1D global virtual tile index

                % Calculate the table index
                x_table_idx = floor((x_distance * double(table_oversamp(1))) + 0.5); % *L (a power of 2) is implemented as a left shift
                y_table_idx = floor((y_distance * double(table_oversamp(2))) + 0.5); % *L (a power of 2) is implemented as a left shift

                % Extract the interp table values
                x_table_val = interp_table(1, x_table_idx+1); % +1 because Matlab indexing starts at 1
                y_table_val = interp_table(2, y_table_idx+1); % +1 because Matlab indexing starts at 1

                % Split the complex value into its real and imaginary components
                x_table_val_real = real(x_table_val);
                x_table_val_imag = imag(x_table_val);
                y_table_val_real = real(y_table_val);
                y_table_val_imag = imag(y_table_val);

                % Calculate 2D table value (product of X and Y values)
                k1 = quantize(fi_cfg_parfor.k1, x_table_val_real + x_table_val_imag);
                k2 = quantize(fi_cfg_parfor.k2, y_table_val_imag - y_table_val_real);
                k3 = quantize(fi_cfg_parfor.k3, y_table_val_real + y_table_val_imag);
                k4 = quantize(fi_cfg_parfor.k4, y_table_val_real * k1);
                k5 = quantize(fi_cfg_parfor.k5, x_table_val_real * k2);
                k6 = quantize(fi_cfg_parfor.k6, x_table_val_imag * k3);
                R1 = quantize(fi_cfg_parfor.R1, k4 - k6);
                I1 = quantize(fi_cfg_parfor.I1, k4 + k5);

                % Get the conjugate of the table value (invert sign of I1)
                I1 = quantize(fi_cfg_parfor.I1, -I1);

                % Extract the grid point being processed
                input_point_data = input_data(:, input_point_idx);
                
                % Split the complex value into its real and imaginary components
                input_point_data_real = real(input_point_data);
                input_point_data_imag = imag(input_point_data);

                % Calculate interpolation result
                k7  = quantize(fi_cfg_parfor.k7, R1 + I1);
                k8  = quantize(fi_cfg_parfor.k8, input_point_data_imag - input_point_data_real);
                k9  = quantize(fi_cfg_parfor.k9, input_point_data_real + input_point_data_imag);
                k10 = quantize(fi_cfg_parfor.k10, input_point_data_real * k7);
                k11 = quantize(fi_cfg_parfor.k11, R1 * k8);
                k12 = quantize(fi_cfg_parfor.k12, I1 * k9);
                R2  = quantize(fi_cfg_parfor.R2, k10 - k12);
                I2  = quantize(fi_cfg_parfor.I2, k10 + k11);
                interp_result = complex(R2, I2);

                % Accumulate the interpolation result with the current value (either kdat sample or griddat memory)
                grid_data_array(tile_idx+1) = quantize(fi_cfg_parfor.interp_accum, interp_result + grid_data_array(tile_idx+1));
            end
        end
    end
    %%%%%%%%%% Parfor-Specific Merging %%%%%%%%%%
    grid_data(accel_idx, :) = grid_data_array;
end
toc


%%%%%%%%%% Output Data %%%%%%%%%%
if(strcmp(nufft_type, 'forw'))
    hardware_interp_result = input_data;
else
    grid_data = reshape(grid_data, x_accel_dim, y_accel_dim, ((x_grid_dim*y_grid_dim)/(x_accel_dim*y_accel_dim)));
    %%%%%%%%%% Unpack Grid %%%%%%%%%%
    tic
    for i = 0:x_tile_dim-1
        for j = 0:y_tile_dim-1
            x_min = i*x_accel_dim +1;
            x_max = (i+1)*x_accel_dim;
            y_min = j*y_accel_dim +1;
            y_max = (j+1)*y_accel_dim;
            tile_num = x_tile_dim*j+i +1;
            grid_data_tiled(x_min:x_max, y_min:y_max) = grid_data(:,:,tile_num);
        end
    end
    toc
    hardware_interp_result = circshift(grid_data_tiled, [3 3]);
end

end
