function [ hardware_interp_result ] = hardware_interp_FFT_IFFT_fi(input_data, nufft_type, fi_cfg, config)
%   Detailed explanation goes here
% kdat --- complex magnitude; one complex value for each tm coordinate
% dims --- dimensions of the output grid
% table_mat --- interpolation table; each dim (x or y) has one row
% numpoints --- size of the interpolation window
% Jlist --- interpolation window indices for numpoints*numpoints elements
% L --- table oversampling factor (L table points between every grid point)
% tm --- coordinates for the kdata samples
% griddat --- output grid arranged as a 1D vector

%%%%%%%%%% Assign Inputs %%%%%%%%%%
x_accel_dim    = config.hardware.x_accel_dim;
y_accel_dim    = config.hardware.y_accel_dim;
x_mem_dim      = config.hardware.x_mem_dim;
y_mem_dim      = config.hardware.y_mem_dim;
x_mem_tile_dim = config.hardware.x_mem_tile_dim;
y_mem_tile_dim = config.hardware.y_mem_tile_dim;

x_grid_dim     = config.image.x_grid_dim;
y_grid_dim     = config.image.y_grid_dim;
x_tile_dim     = config.image.x_tile_dim;
y_tile_dim     = config.image.y_tile_dim;
x_img_dim      = config.image.x_img_dim;
y_img_dim      = config.image.y_img_dim;
x_img_tile_dim = config.image.x_img_tile_dim;
y_img_tile_dim = config.image.y_img_tile_dim;


NT_table = numerictype('WordLength', 16, 'FractionLength', 14, 'Signed', true);
NT_table2 = numerictype('WordLength', 32, 'FractionLength', 30, 'Signed', true);
if(strcmp(nufft_type, 'forw'))
    NT_data = numerictype('WordLength', 32, 'FractionLength', 10, 'Signed', true);
else
    NT_data = numerictype('WordLength', 32, 'FractionLength', 40, 'Signed', true);
end
FM_init = fimath('OverflowAction','Saturate','RoundingMethod','Nearest','ProductMode','KeepMSB','SumMode','KeepMSB');
FM_run = fimath('OverflowAction','Saturate','RoundingMethod','Floor','ProductMode','KeepMSB','SumMode','KeepMSB');
FM_table_init = fimath('OverflowAction','Saturate','RoundingMethod','Floor','ProductMode','KeepMSB','SumMode','KeepLSB','ProductWordLength',16,'SumWordLength',16);
FM_table_run = fimath('OverflowAction','Saturate','RoundingMethod','Floor','ProductMode','KeepMSB','SumMode','KeepLSB','ProductWordLength',16,'SumWordLength',16);


nonuniform_data  = input_data.nonuniform_data;
nonuniform_coord = quantize(fi_cfg.input_coord, input_data.nonuniform_coord);
% interp_table     = fi(input_data.table, NT_table, FM_table_init);
% interp_table     = fi(interp_table, NT_table, FM_table_run);
interp_table     = quantize(fi_cfg.interp_table, input_data.table);
interp_table2    = quantize(fi_cfg.interp_table, input_data.table);
table_oversamp   = input_data.table_oversamp;
table_x_dim      = input_data.table_x_dim;
table_y_dim      = input_data.table_y_dim;
griddat          = input_data.griddat;

num_input_points = size(nonuniform_coord, 2);

if(x_grid_dim > x_mem_dim || y_grid_dim > y_mem_dim)
    disp('ERROR: Input dimensions don not fit inside memory!');
end

grid_data_tiled = zeros(x_accel_dim, y_accel_dim, ((x_mem_dim*y_mem_dim)/(x_accel_dim*y_accel_dim)));
grid_data       = reshape(griddat, x_grid_dim, y_grid_dim); % Reshape into a square and rotate to match hardware output

if(strcmp(nufft_type, 'forw'))
    input_data = complex(zeros(1, size(nonuniform_coord, 2)), 0);
    
    %%%%%%%%%% Pack Grid %%%%%%%%%%
    tic
    for i = 0:x_tile_dim-1
        for j = 0:y_tile_dim-1
            x_min = i*x_accel_dim +1;
            x_max = (i+1)*x_accel_dim;
            y_min = j*y_accel_dim +1;
            y_max = (j+1)*y_accel_dim;
            tile_num = x_mem_tile_dim*j+i +1;
            grid_data_tiled(:,:,tile_num) = grid_data(x_min:x_max, y_min:y_max);
        end
    end
    fprintf('3D Cube Pack Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
    
    grid_data_tiled = fi(grid_data_tiled, NT_data, FM_init); % TODO: see if this is necessary for forw
    grid_data_tiled = fi(grid_data_tiled, NT_data, FM_run);
    input_data = fi(input_data, NT_data, FM_init); % this is just to make Matlab happy; it is automatic in hardware (all 0's)
    input_data = fi(input_data, NT_data, FM_run);
else
    input_data = fi(nonuniform_data, NT_data, FM_init);
    input_data = fi(input_data, NT_data, FM_run);
    input_data2 = quantize(fi_cfg.input_data, nonuniform_data);
end

% Pre-shift the data; this avoids the circshift previously required for grid_data (forw input, adj output)
nonuniform_coord(1, :) = rem(nonuniform_coord(1, :) + (table_x_dim/2), x_grid_dim);
nonuniform_coord(2, :) = rem(nonuniform_coord(2, :) + (table_y_dim/2), y_grid_dim);


%%%%%%%%%% Interpolate Data %%%%%%%%%%
tic
% Loop over accelerator pipelines to process input point
for x_accel_idx = 0:x_accel_dim-1
    for y_accel_idx = 0:y_accel_dim-1
        % Process the input array one point at a time, updating target grid points
        for input_point_idx = 1:num_input_points
%         for input_point_idx = 1:10000
            % Extract the current point being processed
            input_point_coord = nonuniform_coord(:, input_point_idx);

            % Get the quotient and remainder to determine grid and tile location
            x_base_tile_idx = fix(input_point_coord(1)/x_accel_dim); % x virtual tile index
            y_base_tile_idx = fix(input_point_coord(2)/y_accel_dim); % y virtual tile index

            % Extract the "relative" coordinates (within a tile)
            x_coord = rem(input_point_coord(1), x_accel_dim);
            y_coord = rem(input_point_coord(2), y_accel_dim);

            % Determine if this pipeline is affected by this source point
            % If x_coord is within [x_accel_idx,x_accel_idx+table_x_dim) of the x_accel_idx, it does
            x_distance = rem(x_accel_dim+x_coord-x_accel_idx, x_accel_dim); % rem is implemented as an upper truncation
            if(x_distance < table_x_dim)
                % If y_coord is within [y_accel_idx,y_accel_idx+table_y_dim) of the y_accel_idx, it does
                y_distance = rem(y_accel_dim+y_coord-y_accel_idx, y_accel_dim); % rem is implemented as an upper truncation
                if(y_distance < table_y_dim)
                    % This point affects this pipeline!

                    % Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
                    if(x_coord < x_accel_idx)
                        % A wrap occured!
                        if(x_base_tile_idx == 0)
                            x_tile_idx = x_tile_dim - 1;
                        else
                            x_tile_idx = x_base_tile_idx - 1;
                        end
                    else
                        x_tile_idx = x_base_tile_idx;
                    end

                    % Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
                    if(y_coord < y_accel_idx)
                        % A wrap occured!
                        if(y_base_tile_idx == 0)
                            y_tile_idx = y_tile_dim - 1;
                        else
                            y_tile_idx = y_base_tile_idx - 1;
                        end
                    else
                        y_tile_idx = y_base_tile_idx;
                    end

                    % Calculate the tile index ("depth" in the pipeline's data array)
                    tile_idx = y_tile_idx * x_mem_tile_dim + x_tile_idx; % 1D global virtual tile index

                    % Calculate the table index
                    x_table_idx = floor((x_distance * table_oversamp(1)) + 0.5); % *L (a power of 2) is implemented as a left shift
                    y_table_idx = floor((y_distance * table_oversamp(2)) + 0.5); % *L (a power of 2) is implemented as a left shift

                    % Extract the interp table values
                    x_table_val = interp_table(1, x_table_idx+1); % +1 because Matlab indexing starts at 1
                    y_table_val = interp_table(2, y_table_idx+1); % +1 because Matlab indexing starts at 1

                    % Split the complex value into its real and imaginary components
                    x_table_val_real = real(x_table_val);
                    x_table_val_imag = imag(x_table_val);
                    y_table_val_real = real(y_table_val);
                    y_table_val_imag = imag(y_table_val);

                    % Calculate 2D table value (product of X and Y values)
%                     k1 = x_table_val_real + x_table_val_imag;
%                     k2 = y_table_val_imag - y_table_val_real;
%                     k3 = y_table_val_real + y_table_val_imag;
%                     k4 = y_table_val_real * k1;
%                     k5 = x_table_val_real * k2;
%                     k6 = x_table_val_imag * k3;
%                     R1 = k4 - k6;
%                     I1 = k4 + k5;

                    k1 = quantize(fi_cfg.k1, x_table_val_real + x_table_val_imag);
                    k2 = quantize(fi_cfg.k2, y_table_val_imag - y_table_val_real);
                    k3 = quantize(fi_cfg.k3, y_table_val_real + y_table_val_imag);
                    k4 = quantize(fi_cfg.k4, y_table_val_real * k1);
                    k5 = quantize(fi_cfg.k5, x_table_val_real * k2);
                    k6 = quantize(fi_cfg.k6, x_table_val_imag * k3);
                    R1 = quantize(fi_cfg.R1, k4 - k6);
                    I1 = quantize(fi_cfg.I1, k4 + k5);
                    R1 = fi(R1, NT_table2, FM_run);
                    I1 = fi(I1, NT_table2, FM_run);

                    if(strcmp(nufft_type, 'forw'))
                        % Extract the grid point being processed
                        input_point_data = grid_data_tiled(x_accel_idx+1, y_accel_idx+1, tile_idx+1); % +1 because Matlab indexing starts at 1
                    else
                        % Get the conjugate of the table value (invert sign of I1)
                        I1 = -I1;

                        % Extract the grid point being processed
                        input_point_data = input_data(:, input_point_idx);
                    end
                        
                    % Split the complex value into its real and imaginary components
                    input_point_data_real = real(input_point_data);
                    input_point_data_imag = imag(input_point_data);

                    % Calculate interpolation result
                    k7  = R1 + I1;
                    k8  = input_point_data_imag - input_point_data_real;
                    k9  = input_point_data_real + input_point_data_imag;
                    k10 = input_point_data_real * k7;
                    k11 = R1 * k8;
                    k12 = I1 * k9;
                    R2  = k10 - k12;
                    I2  = k10 + k11;
                    interp_result = complex(R2, I2);

                    % Accumulate the interpolation result with the current value (either kdat sample or griddat memory)
                    if(strcmp(nufft_type, 'forw'))
                        input_data(:, input_point_idx) = interp_result + input_data(:, input_point_idx);
                    else
                        grid_data_tiled(x_accel_idx+1, y_accel_idx+1, tile_idx+1) = interp_result + grid_data_tiled(x_accel_idx+1, y_accel_idx+1, tile_idx+1);
%                         test = quantize(fi_cfg.interp_accum, double(interp_result) + double(grid_data_tiled(x_accel_idx+1, y_accel_idx+1, tile_idx+1)));
                    end
                end
            end
        end
    end
end
if(strcmp(nufft_type, 'forw'))
    fprintf('3D Cube Regridding Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
else
    fprintf('3D Cube Gridding Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
end


%%%%%%%%%% Output Data %%%%%%%%%%
if(strcmp(nufft_type, 'forw'))
    hardware_interp_result = input_data;
else
    hardware_interp_result = grid_data_tiled;
end

end
