function [ hardware_interp_result ] = hardware_interp_FFT_IFFT_fixed_parfor(input_data, nufft_type, fi_cfg, config)
%   Detailed explanation goes here
% kdat --- complex magnitude; one complex value for each tm coordinate
% dims --- dimensions of the output grid
% table_mat --- interpolation table; each dim (x or y) has one row
% numpoints --- size of the interpolation window
% Jlist --- interpolation window indices for numpoints*numpoints elements
% L --- table oversampling factor (L table points between every grid point)
% tm --- coordinates for the kdata samples
% griddat --- output grid arranged as a 1D vector

%%%%%%%%%% Assign Inputs %%%%%%%%%%
x_accel_dim    = config.hardware.x_accel_dim;
y_accel_dim    = config.hardware.y_accel_dim;
x_mem_dim      = config.hardware.x_mem_dim;
y_mem_dim      = config.hardware.y_mem_dim;
x_mem_tile_dim = config.hardware.x_mem_tile_dim;
y_mem_tile_dim = config.hardware.y_mem_tile_dim;

x_grid_dim     = config.image.x_grid_dim;
y_grid_dim     = config.image.y_grid_dim;
x_tile_dim     = config.image.x_tile_dim;
y_tile_dim     = config.image.y_tile_dim;
x_img_dim      = config.image.x_img_dim;
y_img_dim      = config.image.y_img_dim;
x_img_tile_dim = config.image.x_img_tile_dim;
y_img_tile_dim = config.image.y_img_tile_dim;

nonuniform_data  = input_data.nonuniform_data;
nonuniform_coord = quantize(fi_cfg.input_coord, input_data.nonuniform_coord);
interp_table     = quantize(fi_cfg.interp_table, input_data.table);
table_oversamp   = input_data.table_oversamp;
table_x_dim      = input_data.table_x_dim;
table_y_dim      = input_data.table_y_dim;
griddat          = input_data.griddat;

num_input_points = size(nonuniform_coord, 2);

if(x_grid_dim > x_mem_dim || y_grid_dim > y_mem_dim)
    disp('ERROR: Input dimensions don not fit inside memory!');
end

grid_data_tiled = zeros(x_accel_dim, y_accel_dim, ((x_mem_dim*y_mem_dim)/(x_accel_dim*y_accel_dim)));
grid_data       = reshape(griddat, x_grid_dim, y_grid_dim); % Reshape into a square and rotate to match hardware output

if(strcmp(nufft_type, 'forw'))
    input_data = complex(zeros(1, size(nonuniform_coord, 2)), 0);

    %%%%%%%%%% Pack Grid %%%%%%%%%%
    tic
    for i = 0:x_tile_dim-1
        for j = 0:y_tile_dim-1
            x_min = i*x_accel_dim +1;
            x_max = (i+1)*x_accel_dim;
            y_min = j*y_accel_dim +1;
            y_max = (j+1)*y_accel_dim;
            tile_num = x_mem_tile_dim*j+i +1;
            grid_data_tiled(:,:,tile_num) = grid_data(x_min:x_max, y_min:y_max);
        end
    end
    fprintf('3D Cube Pack Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
else
    input_data = quantize(fi_cfg.input_data, nonuniform_data);
    grid_data_tiled = reshape(grid_data_tiled, x_accel_dim*y_accel_dim, ((x_mem_dim*y_mem_dim)/(x_accel_dim*y_accel_dim)));
end

% Pre-shift the data; this avoids the circshift previously required for grid_data (forw input, adj output)
nonuniform_coord(1, :) = rem(nonuniform_coord(1, :) + (table_x_dim/2), x_grid_dim);
nonuniform_coord(2, :) = rem(nonuniform_coord(2, :) + (table_y_dim/2), y_grid_dim);


save(sprintf('parfor_data.mat'), 'fi_cfg');
%%%%%%%%%% Interpolate Data %%%%%%%%%%
if(strcmp(nufft_type, 'forw'))
    tic
    % Process the input array one point at a time, updating target grid points
    parfor input_point_idx = 1:num_input_points
        %%%%%%%%%% Parfor-Specific Setup %%%%%%%%%%
        res = load('parfor_data.mat');
        fi_cfg_parfor = res.fi_cfg;
        
        % Extract the current point being processed
        input_point_coord = nonuniform_coord(:, input_point_idx);

        % Get the quotient and remainder to determine grid and tile location
        x_base_tile_idx = fix(input_point_coord(1)/x_accel_dim); % x virtual tile index
        y_base_tile_idx = fix(input_point_coord(2)/y_accel_dim); % y virtual tile index

        % Extract the "relative" coordinates (within a tile)
        x_coord = rem(input_point_coord(1), x_accel_dim);
        y_coord = rem(input_point_coord(2), y_accel_dim);

        % Loop over accelerator pipelines to process input point
        for x_accel_idx = 0:x_accel_dim-1
            for y_accel_idx = 0:y_accel_dim-1
                % Determine if this pipeline is affected by this source point
                % If x_coord is within [x_accel_idx,x_accel_idx+table_x_dim) of the x_accel_idx, it does
                x_distance = rem(x_accel_dim+x_coord-x_accel_idx, x_accel_dim); % rem is implemented as an upper truncation

                if(x_distance < table_x_dim)
                    % If y_coord is within [y_accel_idx,y_accel_idx+table_y_dim) of the y_accel_idx, it does
                    y_distance = rem(y_accel_dim+y_coord-y_accel_idx, y_accel_dim); % rem is implemented as an upper truncation
                    
                    if(y_distance < table_y_dim)
                        % This point affects this pipeline!
                        % Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
                        if(x_coord < x_accel_idx)
                            % A wrap occured!
                            if(x_base_tile_idx == 0)
                                x_tile_idx = x_tile_dim - 1;
                            else
                                x_tile_idx = x_base_tile_idx - 1;
                            end
                        else
                            x_tile_idx = x_base_tile_idx;
                        end

                        % Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
                        if(y_coord < y_accel_idx)
                            % A wrap occured!
                            if(y_base_tile_idx == 0)
                                y_tile_idx = y_tile_dim - 1;
                            else
                                y_tile_idx = y_base_tile_idx - 1;
                            end
                        else
                            y_tile_idx = y_base_tile_idx;
                        end

                        % Calculate the tile index ("depth" in the pipeline's data array)
                        tile_idx = y_tile_idx * x_mem_tile_dim + x_tile_idx; % 1D global virtual tile index

                        % Calculate the table index
                        x_table_idx = floor((x_distance * table_oversamp(1)) + 0.5); % *L (a power of 2) is implemented as a left shift
                        y_table_idx = floor((y_distance * table_oversamp(2)) + 0.5); % *L (a power of 2) is implemented as a left shift

                        % Extract the interp table values
                        x_table_val = interp_table(1, x_table_idx+1); % +1 because Matlab indexing starts at 1
                        y_table_val = interp_table(2, y_table_idx+1); % +1 because Matlab indexing starts at 1

                        % Split the complex value into its real and imaginary components
                        x_table_val_real = real(x_table_val);
                        x_table_val_imag = imag(x_table_val);
                        y_table_val_real = real(y_table_val);
                        y_table_val_imag = imag(y_table_val);

                        % Calculate 2D table value (product of X and Y values)
                        k1 = quantize(fi_cfg_parfor.k1, x_table_val_real + x_table_val_imag);
                        k2 = quantize(fi_cfg_parfor.k2, y_table_val_imag - y_table_val_real);
                        k3 = quantize(fi_cfg_parfor.k3, y_table_val_real + y_table_val_imag);
                        k4 = quantize(fi_cfg_parfor.k4, y_table_val_real * k1);
                        k5 = quantize(fi_cfg_parfor.k5, x_table_val_real * k2);
                        k6 = quantize(fi_cfg_parfor.k6, x_table_val_imag * k3);
                        R1 = quantize(fi_cfg_parfor.R1, k4 - k6);
                        I1 = quantize(fi_cfg_parfor.I1, k4 + k5);

                        % Extract the grid point being processed
                        input_point_data = grid_data_tiled(x_accel_idx+1, y_accel_idx+1, tile_idx+1); % +1 because Matlab indexing starts at 1
                        
                        % Split the complex value into its real and imaginary components
                        input_point_data_real = real(input_point_data);
                        input_point_data_imag = imag(input_point_data);

                        % Calculate interpolation result
                        k7  = quantize(fi_cfg_parfor.k7, R1 + I1);
                        k8  = quantize(fi_cfg_parfor.k8, input_point_data_imag - input_point_data_real);
                        k9  = quantize(fi_cfg_parfor.k9, input_point_data_real + input_point_data_imag);
                        k10 = quantize(fi_cfg_parfor.k10, input_point_data_real * k7);
                        k11 = quantize(fi_cfg_parfor.k11, R1 * k8);
                        k12 = quantize(fi_cfg_parfor.k12, I1 * k9);
                        R2  = quantize(fi_cfg_parfor.R2, k10 - k12);
                        I2  = quantize(fi_cfg_parfor.I2, k10 + k11);
                        interp_result = complex(R2, I2);

                        % Accumulate the interpolation result with the current value (either kdat sample or griddat memory)
                        input_data(:, input_point_idx) = quantize(fi_cfg_parfor.interp_accum, interp_result + input_data(:, input_point_idx));
                    end
                end
            end
        end
    end
    fprintf('3D Cube Regridding Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
else
    tic
    parfor accel_idx = 1:x_accel_dim*y_accel_dim
        %%%%%%%%%% Parfor-Specific Setup %%%%%%%%%%
        res = load('parfor_data.mat');
        fi_cfg_parfor = res.fi_cfg;
        x_accel_idx = mod((accel_idx-1), x_accel_dim);
        y_accel_idx = fix((accel_idx-1)/y_accel_dim);
        grid_data_array = grid_data_tiled(accel_idx, :);

        % Process the input array one point at a time, updating target grid points
        for input_point_idx = 1:num_input_points
            % Extract the current point being processed
            input_point_coord = nonuniform_coord(:, input_point_idx);

            % Get the quotient and remainder to determine grid and tile location
            x_base_tile_idx = fix(input_point_coord(1)/x_accel_dim); % x virtual tile index
            y_base_tile_idx = fix(input_point_coord(2)/y_accel_dim); % y virtual tile index

            % Extract the "relative" coordinates (within a tile)
            x_coord = rem(input_point_coord(1), x_accel_dim);
            y_coord = rem(input_point_coord(2), y_accel_dim);

            % Determine if this pipeline is affected by this source point
            % If x_coord is within [x_accel_idx,x_accel_idx+table_x_dim) of the x_accel_idx, it does
            x_distance = rem(x_accel_dim+x_coord-x_accel_idx, x_accel_dim); % rem is implemented as an upper truncation
            if(x_distance < table_x_dim)
                % If y_coord is within [y_accel_idx,y_accel_idx+table_y_dim) of the y_accel_idx, it does
                y_distance = rem(y_accel_dim+y_coord-y_accel_idx, y_accel_dim); % rem is implemented as an upper truncation
                if(y_distance < table_y_dim)
                    % This point affects this pipeline!

                    % Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
                    if(x_coord < x_accel_idx)
                        % A wrap occured!
                        if(x_base_tile_idx == 0)
                            x_tile_idx = x_tile_dim - 1;
                        else
                            x_tile_idx = x_base_tile_idx - 1;
                        end
                    else
                        x_tile_idx = x_base_tile_idx;
                    end

                    % Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
                    if(y_coord < y_accel_idx)
                        % A wrap occured!
                        if(y_base_tile_idx == 0)
                            y_tile_idx = y_tile_dim - 1;
                        else
                            y_tile_idx = y_base_tile_idx - 1;
                        end
                    else
                        y_tile_idx = y_base_tile_idx;
                    end

                    % Calculate the tile index ("depth" in the pipeline's data array)
                    tile_idx = y_tile_idx * x_mem_tile_dim + x_tile_idx; % 1D global virtual tile index

                    % Calculate the table index
                    x_table_idx = floor((x_distance * table_oversamp(1)) + 0.5); % *L (a power of 2) is implemented as a left shift
                    y_table_idx = floor((y_distance * table_oversamp(2)) + 0.5); % *L (a power of 2) is implemented as a left shift

                    % Extract the interp table values
                    x_table_val = interp_table(1, x_table_idx+1); % +1 because Matlab indexing starts at 1
                    y_table_val = interp_table(2, y_table_idx+1); % +1 because Matlab indexing starts at 1

                    % Split the complex value into its real and imaginary components
                    x_table_val_real = real(x_table_val);
                    x_table_val_imag = imag(x_table_val);
                    y_table_val_real = real(y_table_val);
                    y_table_val_imag = imag(y_table_val);

                    % Calculate 2D table value (product of X and Y values)
                    k1 = quantize(fi_cfg_parfor.k1, x_table_val_real + x_table_val_imag);
                    k2 = quantize(fi_cfg_parfor.k2, y_table_val_imag - y_table_val_real);
                    k3 = quantize(fi_cfg_parfor.k3, y_table_val_real + y_table_val_imag);
                    k4 = quantize(fi_cfg_parfor.k4, y_table_val_real * k1);
                    k5 = quantize(fi_cfg_parfor.k5, x_table_val_real * k2);
                    k6 = quantize(fi_cfg_parfor.k6, x_table_val_imag * k3);
                    R1 = quantize(fi_cfg_parfor.R1, k4 - k6);
                    I1 = quantize(fi_cfg_parfor.I1, k4 + k5);

                    % Get the conjugate of the table value (invert sign of I1)
                    I1 = quantize(fi_cfg_parfor.I1, -I1);

                    % Extract the grid point being processed
                    input_point_data = input_data(:, input_point_idx);
                    
                    % Split the complex value into its real and imaginary components
                    input_point_data_real = real(input_point_data);
                    input_point_data_imag = imag(input_point_data);

                    % Calculate interpolation result
                    k7  = quantize(fi_cfg_parfor.k7, R1 + I1);
                    k8  = quantize(fi_cfg_parfor.k8, input_point_data_imag - input_point_data_real);
                    k9  = quantize(fi_cfg_parfor.k9, input_point_data_real + input_point_data_imag);
                    k10 = quantize(fi_cfg_parfor.k10, input_point_data_real * k7);
                    k11 = quantize(fi_cfg_parfor.k11, R1 * k8);
                    k12 = quantize(fi_cfg_parfor.k12, I1 * k9);
                    R2  = quantize(fi_cfg_parfor.R2, k10 - k12);
                    I2  = quantize(fi_cfg_parfor.I2, k10 + k11);
                    interp_result = complex(R2, I2);

                    % Accumulate the interpolation result with the current value (either kdat sample or griddat memory)
                    grid_data_array(tile_idx+1) = quantize(fi_cfg_parfor.interp_accum, interp_result + grid_data_array(tile_idx+1));
                end
            end
        end
        %%%%%%%%%% Parfor-Specific Merging %%%%%%%%%%
        grid_data_tiled(accel_idx, :) = grid_data_array;
    end
    fprintf('3D Cube Gridding Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
end
delete('parfor_data.mat');


%%%%%%%%%% Output Data %%%%%%%%%%
if(strcmp(nufft_type, 'forw'))
    hardware_interp_result = input_data;
else
    grid_data_tiled = reshape(grid_data_tiled, x_accel_dim, y_accel_dim, ((x_mem_dim*y_mem_dim)/(x_accel_dim*y_accel_dim)));
    hardware_interp_result = grid_data_tiled;
end

end
