#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <cuda.h>
#include <stdio.h>
#include <math.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <mm_malloc.h>

#define MAX_TABLE_DIM 1024
#define MEM_DIM 1024
#define GRID_DIM 768
#define WEIGHT_OVERSAMP_FACTOR 32
#define ACCEL_DIM 8
#define MEM_TILE_DIM (MEM_DIM/ACCEL_DIM)
#define TILE_DIM (GRID_DIM/ACCEL_DIM)
#define INTERP_WIN_DIM 6
// #define NUM_INPUT_POINTS 460800
#define NUM_TABLE_VALS 193
#define NUM_POINTS_PER_THREAD (MEM_DIM*MEM_DIM/(ACCEL_DIM*ACCEL_DIM))
#define NUM_BLOCKS_PER_DIM 64
#define NUM_INPUTS_PER_THREAD (NUM_INPUT_POINTS/(NUM_BLOCKS_PER_DIM * NUM_BLOCKS_PER_DIM))

struct complex_float {
    float real;
    float imag;
};

struct input_sample {
    float x_coord;
    float y_coord;
    complex_float val;
};

struct table_vals {
    complex_float table_arr[NUM_TABLE_VALS];
};


int num_weight_data_real_doubles;
int num_weight_data_imag_doubles;
int num_nonuniform_data_real_doubles;
int num_nonuniform_data_imag_doubles;
int num_nonuniform_x_coord_doubles;
int num_nonuniform_y_coord_doubles;
int num_uniform_data_tiled_real_doubles;
int num_uniform_data_tiled_imag_doubles;

// Declare variables for "constants" that require modification based on operation type
int grid_dim;
int tile_dim;

using namespace std;

// Declare the size of the grid and blocks
dim3 gridSize(NUM_BLOCKS_PER_DIM, NUM_BLOCKS_PER_DIM, 1);
dim3 blockSize(ACCEL_DIM, ACCEL_DIM, 1);

// Declare constant memory
__constant__ table_vals table_d;


//////////////////////////////////////////////
//                                          //
//                 Gridding                 //
//                                          //
//////////////////////////////////////////////
__global__ void gridding_cuda(input_sample *nonuniform_data_arr_d, complex_float *uniform_data_arr_d, int tile_dim_d, int num_nonuniform_points_d, int num_nonuniform_points_per_thread_d) {
    // Calculate the global thread indexes in each direction (relative indexes)
    int tidx = threadIdx.x;
    int tidy = threadIdx.y;
    int tidz = threadIdx.z;
    int tid  = tidz * blockDim.x * blockDim.y + tidy * blockDim.x + tidx;

    int num_points;
    if(num_nonuniform_points_d % (NUM_BLOCKS_PER_DIM^2) != 0)
        num_points = num_nonuniform_points_per_thread_d+1;
    else
        num_points = num_nonuniform_points_per_thread_d;

    int offset = tid * NUM_POINTS_PER_THREAD;
    int start  = (blockIdx.y * gridDim.x + blockIdx.x) * num_points;
    int end    = start + num_points;

    if(end > num_nonuniform_points_d)
        end = num_nonuniform_points_d;

    for(int input_point_idx = start; input_point_idx < end; ++input_point_idx) {
        // Extract the current point being processed
        input_sample current_sample = nonuniform_data_arr_d[input_point_idx];


        //////////////////////////////////////////////
        //                                          //
        //                  Select                  //
        //                                          //
        //////////////////////////////////////////////
        // Extract the "relative" coordinates (within a tile)
        float x_coord = fmod(current_sample.x_coord, (float)ACCEL_DIM);
        float y_coord = fmod(current_sample.y_coord, (float)ACCEL_DIM);

        // Determine if this pipeline is affected by this source point
        float x_distance = fmod(ACCEL_DIM+x_coord-tidx, (float)ACCEL_DIM); // rem is implemented as an upper truncation
        float y_distance = fmod(ACCEL_DIM+y_coord-tidy, (float)ACCEL_DIM); // rem is implemented as an upper truncation
        
        // Check if the point lies within the interpolation window
        if(x_distance < INTERP_WIN_DIM && y_distance < INTERP_WIN_DIM) {
            // Get the quotient and remainder to determine grid and tile location
            int x_base_tile_idx = current_sample.x_coord/ACCEL_DIM; // x virtual tile index
            int y_base_tile_idx = current_sample.y_coord/ACCEL_DIM; // y virtual tile index

            // This point affects this pipeline!
            // Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
            int x_tile_idx;
            if(x_coord < tidx) {
               // A wrap occured!
                if(x_base_tile_idx == 0) {
                    x_tile_idx = tile_dim_d - 1;
                } else {
                    x_tile_idx = x_base_tile_idx - 1;
                }
            } else {
                x_tile_idx = x_base_tile_idx;
            }

            // Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
            int y_tile_idx;
            if(y_coord < tidy) {
               // A wrap occured!
                if(y_base_tile_idx == 0) {
                    y_tile_idx = tile_dim_d - 1;
                } else {
                    y_tile_idx = y_base_tile_idx - 1;
                }
            } else {
                y_tile_idx = y_base_tile_idx;
            }

            // Calculate the tile index ("depth" in the pipeline's data array)
            int tile_idx = y_tile_idx * MEM_TILE_DIM + x_tile_idx; // 1D global virtual tile index

            // Calculate the table index
            int x_table_idx = ((x_distance * float(WEIGHT_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift
            int y_table_idx = ((y_distance * float(WEIGHT_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift


            //////////////////////////////////////////////
            //                                          //
            //               Interp Table               //
            //                                          //
            //////////////////////////////////////////////
            // Extract the interp table values
            complex_float x_table_val = table_d.table_arr[x_table_idx];
            complex_float y_table_val = table_d.table_arr[y_table_idx];

            // Calculate 2D table value (product of X and Y values)
            float k1 = x_table_val.real + x_table_val.imag;
            float k2 = y_table_val.imag - y_table_val.real;
            float k3 = y_table_val.real + y_table_val.imag;
            float k4 = y_table_val.real * k1;
            float k5 = x_table_val.real * k2;
            float k6 = x_table_val.imag * k3;
            float R1 = k4 - k6;
            float I1 = k4 + k5;

            // Get the conjugate of the table value (invert sign of I1) - required for gridding (not regridding)
            I1 = I1 * (-1);


            //////////////////////////////////////////////
            //                                          //
            //               Interpolation              //
            //                                          //
            //////////////////////////////////////////////
            // Calculate interpolation result; conjugate of interp table value incorporated into this calculation directly by flipping sign for I1
            float k7 = R1 + I1;
            float k8 = current_sample.val.imag - current_sample.val.real;
            float k9 = current_sample.val.real + current_sample.val.imag;
            float k10 = current_sample.val.real * k7;
            float k11 = R1 * k8;
            float k12 = I1 * k9;
            float R2 = k10 - k12;
            float I2 = k10 + k11;


            //////////////////////////////////////////////
            //                                          //
            //               Accumulation               //
            //                                          //
            //////////////////////////////////////////////
            atomicAdd(&uniform_data_arr_d[offset+tile_idx].real, R2);
            atomicAdd(&uniform_data_arr_d[offset+tile_idx].imag, I2);
        }
    }
}


//////////////////////////////////////////////
//                                          //
//                 Regridding               //
//                                          //
//////////////////////////////////////////////
__global__ void regridding_cuda(input_sample *nonuniform_data_arr_d, complex_float *uniform_data_arr_d, int tile_dim_d, int num_nonuniform_points_d) {
    // Calculate the global thread indexes in each direction (relative indexes)
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int tidy = blockIdx.y * blockDim.y + threadIdx.y;
    int tidz = blockIdx.z * blockDim.z + threadIdx.z;

    //Calculate the global thread index, used for accessing linear arrays
    int tid = tidz * (blockDim.x * gridDim.x) * (blockDim.y * gridDim.y) + tidy * (blockDim.x * gridDim.x) + tidx;

    // if (tid < 100) {
    //     printf("Hello from thread %d\n", tid);
    // }
    // printf("Hello from thread (%d, %d, %d); tid %d\n", tidz, tidy, tidx, tid);

    // TODO: map one input point to each thread?
    if(tid < num_nonuniform_points_d) {
        // printf("tid=%d\n", tid);
        // Extract the current point being processed
        input_sample current_sample = nonuniform_data_arr_d[tid];

        // if(current_sample.val.real != 0 || current_sample.val.imag != 0)
        //     printf("Thread (%d, %d, %d); tid %d; current sample -> real=%.14e, imag=%.14e\n", tidz, tidy, tidx, tid, current_sample.val.real, current_sample.val.imag);
        // printf("Thread (%d, %d, %d); tid %d; current sample -> real=%.14e, imag=%.14e, x_coord=%f, y_coord=%f\n", tidz, tidy, tidx, tid, current_sample.val.real, current_sample.val.imag, current_sample.x_coord, current_sample.y_coord);

        for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; ++x_accel_idx) {
            for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; ++y_accel_idx) {
                // if(x_accel_idx == 2 && y_accel_idx == 3) {
                // // if(x_accel_idx == 3 && y_accel_idx == 2) {
                //     int offset = ((x_accel_idx * ACCEL_DIM) + y_accel_idx) * NUM_POINTS_PER_THREAD;
                //     // for(int z = 0; z < 16384; ++z) {
                //     for(int z = 0; z < 10; ++z) {
                //         complex_float uniform_point = uniform_data_arr_d[offset+z];

                //         printf("Uniform point: real=%f, imag=%f\n", uniform_point.real, uniform_point.imag);
                //     }
                // }

                // printf("tile_dim_d: %d\n", tile_dim_d);

                //////////////////////////////////////////////
                //                                          //
                //                  Select                  //
                //                                          //
                //////////////////////////////////////////////
                // Extract the "relative" coordinates (within a tile)
                float x_coord = fmod(current_sample.x_coord, (float)ACCEL_DIM);
                float y_coord = fmod(current_sample.y_coord, (float)ACCEL_DIM);

                // Determine if this pipeline is affected by this source point
                float x_distance = fmod(ACCEL_DIM+x_coord-x_accel_idx, (float)ACCEL_DIM); // rem is implemented as an upper truncation
                float y_distance = fmod(ACCEL_DIM+y_coord-y_accel_idx, (float)ACCEL_DIM); // rem is implemented as an upper truncation
                
                // Check if the point lies within the interpolation window
                if(x_distance < INTERP_WIN_DIM && y_distance < INTERP_WIN_DIM) {
                    // Get the quotient and remainder to determine grid and tile location
                    int x_base_tile_idx = current_sample.x_coord/ACCEL_DIM; // x virtual tile index
                    int y_base_tile_idx = current_sample.y_coord/ACCEL_DIM; // y virtual tile index

                    // This point affects this pipeline!
                    // Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
                    int x_tile_idx;
                    if(x_coord < x_accel_idx) {
                       // A wrap occured!
                        if(x_base_tile_idx == 0) {
                            x_tile_idx = tile_dim_d - 1;
                        } else {
                            x_tile_idx = x_base_tile_idx - 1;
                        }
                    } else {
                        x_tile_idx = x_base_tile_idx;
                    }

                    // Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
                    int y_tile_idx;
                    if(y_coord < y_accel_idx) {
                       // A wrap occured!
                        if(y_base_tile_idx == 0) {
                            y_tile_idx = tile_dim_d - 1;
                        } else {
                            y_tile_idx = y_base_tile_idx - 1;
                        }
                    } else {
                        y_tile_idx = y_base_tile_idx;
                    }

                    // Calculate the tile index ("depth" in the pipeline's data array)
                    int tile_idx = y_tile_idx * MEM_TILE_DIM + x_tile_idx; // 1D global virtual tile index

                    // Calculate the table index
                    int x_table_idx = ((x_distance * float(WEIGHT_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift
                    int y_table_idx = ((y_distance * float(WEIGHT_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift


                    //////////////////////////////////////////////
                    //                                          //
                    //               Interp Table               //
                    //                                          //
                    //////////////////////////////////////////////
                    // Extract the interp table values
                    complex_float x_table_val = table_d.table_arr[x_table_idx];
                    complex_float y_table_val = table_d.table_arr[y_table_idx];

                    // printf("weight: x=%d, real=%e, imag=%e\n", x_table_idx, x_table_val.real, x_table_val.imag);
                    // printf("weight: y=%d, real=%e, imag=%e\n", y_table_idx, y_table_val.real, y_table_val.imag);

                    // Calculate 2D table value (product of X and Y values)
                    float k1 = x_table_val.real + x_table_val.imag;
                    float k2 = y_table_val.imag - y_table_val.real;
                    float k3 = y_table_val.real + y_table_val.imag;
                    float k4 = y_table_val.real * k1;
                    float k5 = x_table_val.real * k2;
                    float k6 = x_table_val.imag * k3;
                    float R1 = k4 - k6;
                    float I1 = k4 + k5;

                    // Get the uniform data point for interpolation
                    int offset = ((x_accel_idx * ACCEL_DIM) + y_accel_idx) * NUM_POINTS_PER_THREAD;
                    // int offset = (y_accel_idx * ACCEL_DIM * NUM_POINTS_PER_THREAD) + (x_accel_idx * NUM_POINTS_PER_THREAD);
                    complex_float uniform_point = uniform_data_arr_d[offset+tile_idx];

                    // printf("Uniform point: real=%e, imag=%e\n", uniform_point.real, uniform_point.imag);


                    //////////////////////////////////////////////
                    //                                          //
                    //               Interpolation              //
                    //                                          //
                    //////////////////////////////////////////////
                    // Calculate interpolation result; conjugate of interp table value incorporated into this calculation directly by flipping sign for I1
                    float k7 = R1 + I1;
                    float k8 = uniform_point.imag - uniform_point.real;
                    float k9 = uniform_point.real + uniform_point.imag;
                    float k10 = uniform_point.real * k7;
                    float k11 = R1 * k8;
                    float k12 = I1 * k9;
                    float R2 = k10 - k12;
                    float I2 = k10 + k11;


                    //////////////////////////////////////////////
                    //                                          //
                    //               Accumulation               //
                    //                                          //
                    //////////////////////////////////////////////
                    // printf("BEFORE: R2=%.20f, I2=%.20f, original -> real=%.20f, imag=%.20f\n", R2, I2, nonuniform_data_arr_d[tid].val.real, nonuniform_data_arr_d[tid].val.imag);
                    atomicAdd(&nonuniform_data_arr_d[tid].val.real, R2);
                    atomicAdd(&nonuniform_data_arr_d[tid].val.imag, I2);

                    // if(R2 != 0.0 || I2 != 0.0)
                    //     printf("Thread (%d, %d, %d); tid %d was affected by pipeline (%d, %d)\n", tidz, tidy, tidx, tid, x_accel_idx, y_accel_idx);
                    // printf("Thread (%d, %d, %d); tid %d was affected by pipeline (%d, %d) in tile %d, offset=%d; point real=%.14e, imag=%.14e; R1=%.14e, I1=%.14e, R2=%.14e, I2=%.14e\n", tidz, tidy, tidx, tid, x_accel_idx, y_accel_idx, tile_idx, offset, uniform_point.real, uniform_point.imag, R1, I1, R2, I2);
                    // printf("AFTER:  R2=%.20f, I2=%.20f, result   -> real=%.20f, imag=%.20f\n", R2, I2, nonuniform_data_arr_d[tid].val.real, nonuniform_data_arr_d[tid].val.imag);
                }
            }
        }
    }
}


//////////////////////////////////////////////
//                                          //
//               Regridding V2!             //
//                                          //
//////////////////////////////////////////////
__global__ void regridding_v2_cuda(input_sample *nonuniform_data_arr_d, complex_float *uniform_data_arr_d, int tile_dim_d, int num_nonuniform_points_d, int num_nonuniform_points_per_thread_d) {
    // Calculate the global thread indexes in each direction (relative indexes)
    int tidx = threadIdx.x;
    int tidy = threadIdx.y;
    int tidz = threadIdx.z;
    int tid  = tidz * blockDim.x * blockDim.y + tidy * blockDim.x + tidx;

    int num_points;
    if(num_nonuniform_points_d % (NUM_BLOCKS_PER_DIM^2) != 0)
        num_points = num_nonuniform_points_per_thread_d+1;
    else
        num_points = num_nonuniform_points_per_thread_d;

    int offset = tid * NUM_POINTS_PER_THREAD;
    int start  = (blockIdx.y * gridDim.x + blockIdx.x) * num_points;
    int end    = start + num_points;

    if(end > num_nonuniform_points_d)
        end = num_nonuniform_points_d;

    for(int input_point_idx = start; input_point_idx < end; ++input_point_idx) {
        // Extract the current point being processed
        input_sample current_sample = nonuniform_data_arr_d[input_point_idx];


        //////////////////////////////////////////////
        //                                          //
        //                  Select                  //
        //                                          //
        //////////////////////////////////////////////
        // Extract the "relative" coordinates (within a tile)
        float x_coord = fmod(current_sample.x_coord, (float)ACCEL_DIM);
        float y_coord = fmod(current_sample.y_coord, (float)ACCEL_DIM);

        // Determine if this pipeline is affected by this source point
        float x_distance = fmod(ACCEL_DIM+x_coord-tidx, (float)ACCEL_DIM); // rem is implemented as an upper truncation
        float y_distance = fmod(ACCEL_DIM+y_coord-tidy, (float)ACCEL_DIM); // rem is implemented as an upper truncation
        
        // Check if the point lies within the interpolation window
        if(x_distance < INTERP_WIN_DIM && y_distance < INTERP_WIN_DIM) {
            // Get the quotient and remainder to determine grid and tile location
            int x_base_tile_idx = current_sample.x_coord/ACCEL_DIM; // x virtual tile index
            int y_base_tile_idx = current_sample.y_coord/ACCEL_DIM; // y virtual tile index

            // This point affects this pipeline!
            // Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
            int x_tile_idx;
            if(x_coord < tidx) {
               // A wrap occured!
                if(x_base_tile_idx == 0) {
                    x_tile_idx = tile_dim_d - 1;
                } else {
                    x_tile_idx = x_base_tile_idx - 1;
                }
            } else {
                x_tile_idx = x_base_tile_idx;
            }

            // Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
            int y_tile_idx;
            if(y_coord < tidy) {
               // A wrap occured!
                if(y_base_tile_idx == 0) {
                    y_tile_idx = tile_dim_d - 1;
                } else {
                    y_tile_idx = y_base_tile_idx - 1;
                }
            } else {
                y_tile_idx = y_base_tile_idx;
            }

            // Calculate the tile index ("depth" in the pipeline's data array)
            int tile_idx = y_tile_idx * MEM_TILE_DIM + x_tile_idx; // 1D global virtual tile index

            // Calculate the table index
            int x_table_idx = ((x_distance * float(WEIGHT_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift
            int y_table_idx = ((y_distance * float(WEIGHT_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift


            //////////////////////////////////////////////
            //                                          //
            //               Interp Table               //
            //                                          //
            //////////////////////////////////////////////
            // Extract the interp table values
            complex_float x_table_val = table_d.table_arr[x_table_idx];
            complex_float y_table_val = table_d.table_arr[y_table_idx];

            // Calculate 2D table value (product of X and Y values)
            float k1 = x_table_val.real + x_table_val.imag;
            float k2 = y_table_val.imag - y_table_val.real;
            float k3 = y_table_val.real + y_table_val.imag;
            float k4 = y_table_val.real * k1;
            float k5 = x_table_val.real * k2;
            float k6 = x_table_val.imag * k3;
            float R1 = k4 - k6;
            float I1 = k4 + k5;

            // Get the uniform data point for interpolation
            complex_float uniform_point = uniform_data_arr_d[offset+tile_idx];


            //////////////////////////////////////////////
            //                                          //
            //               Interpolation              //
            //                                          //
            //////////////////////////////////////////////
            // Calculate interpolation result; conjugate of interp table value incorporated into this calculation directly by flipping sign for I1
            float k7 = R1 + I1;
            float k8 = uniform_point.imag - uniform_point.real;
            float k9 = uniform_point.real + uniform_point.imag;
            float k10 = uniform_point.real * k7;
            float k11 = R1 * k8;
            float k12 = I1 * k9;
            float R2 = k10 - k12;
            float I2 = k10 + k11;


            //////////////////////////////////////////////
            //                                          //
            //               Accumulation               //
            //                                          //
            //////////////////////////////////////////////
            atomicAdd(&nonuniform_data_arr_d[input_point_idx].val.real, R2);
            atomicAdd(&nonuniform_data_arr_d[input_point_idx].val.imag, I2);
        }
    }
}


//////////////////////////////////////////////
//                                          //
//                   Main                   //
//                                          //
//////////////////////////////////////////////
int main(int argc, char *argv[]) {
    // Check if the correct numbe of arguments are present; argc should be 2 for correct execution
    if(argc != 10) {
        // If there are the wrong number of arguments, print usage
        printf("Error: %d arguments given, but expected 9.\n", argc-1);
        printf("usage: %s operation_type weight_data_real weight_data_imag nonuniform_data_real nonuniform_data_imag nonuniform_x_coord nonuniform_y_coord uniform_data_tiled_real uniform_data_tiled_imag\n", argv[0]);
    } else {
        // If the number of arguments is correct, save operation type and open files
        string operation_type = argv[1];

        ifstream weight_data_real_File(argv[2], ios::in | ios::binary | ios::ate);
        ifstream weight_data_imag_File(argv[3], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_data_real_File(argv[4], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_data_imag_File(argv[5], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_x_coord_File(argv[6], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_y_coord_File(argv[7], ios::in | ios::binary | ios::ate);
        ifstream uniform_data_tiled_real_File(argv[8], ios::in | ios::binary | ios::ate);
        ifstream uniform_data_tiled_imag_File(argv[9], ios::in | ios::binary | ios::ate);

        if(!weight_data_real_File.good() || !weight_data_imag_File.good() || !nonuniform_data_real_File.good() || !nonuniform_data_imag_File.good() || !nonuniform_x_coord_File.good() || !nonuniform_y_coord_File.good() || !uniform_data_tiled_real_File.good() || !uniform_data_tiled_imag_File.good()) {
            // If file fails to open, notify user
            if(!weight_data_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[2]);
            } else {
                weight_data_real_File.close();
            }
            if(!weight_data_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[3]);
            } else {
                weight_data_imag_File.close();
            }
            if(!nonuniform_data_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[4]);
            } else {
                nonuniform_data_real_File.close();
            }
            if(!nonuniform_data_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[5]);
            } else {
                nonuniform_data_imag_File.close();
            }
            if(!nonuniform_x_coord_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[6]);
            } else {
                nonuniform_x_coord_File.close();
            }
            if(!nonuniform_y_coord_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[7]);
            } else {
                nonuniform_y_coord_File.close();
            }
            if(!uniform_data_tiled_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[8]);
            } else {
                uniform_data_tiled_real_File.close();
            }
            if(!uniform_data_tiled_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[9]);
            } else {
                uniform_data_tiled_imag_File.close();
            }
        } else {
            // Get the size of the file in bytes
            streampos weight_data_real = weight_data_real_File.tellg();
            streampos weight_data_imag = weight_data_imag_File.tellg();
            streampos nonuniform_x_coord = nonuniform_x_coord_File.tellg();
            streampos nonuniform_y_coord = nonuniform_y_coord_File.tellg();
            streampos nonuniform_data_real = nonuniform_data_real_File.tellg();
            streampos nonuniform_data_imag = nonuniform_data_imag_File.tellg();
            streampos uniform_data_tiled_real = uniform_data_tiled_real_File.tellg();
            streampos uniform_data_tiled_imag = uniform_data_tiled_imag_File.tellg();


            cout << "sizeof(complex_float) = " << sizeof(complex_float) << endl;
            cout << "output points = " << MEM_DIM * MEM_DIM << endl;
            cout << "threads = " << ACCEL_DIM * ACCEL_DIM << endl;
            cout << "points per thread = " << NUM_POINTS_PER_THREAD << endl;
            

            int num_weight_data_real_bytes = (int)weight_data_real;
            // cout << "num_weight_data_real_bytes=" << num_weight_data_real_bytes << endl;
            num_weight_data_real_doubles = (int)(weight_data_real / sizeof(double));
            cout << "num_weight_data_real_doubles=" << num_weight_data_real_doubles << endl;

            int num_weight_data_imag_bytes = (int)weight_data_imag;
            // cout << "num_weight_data_imag_bytes=" << num_weight_data_imag_bytes << endl;
            num_weight_data_imag_doubles = (int)(weight_data_imag / sizeof(double));
            cout << "num_weight_data_imag_doubles=" << num_weight_data_imag_doubles << endl;

            int num_nonuniform_data_real_bytes = (int)nonuniform_data_real;
            // cout << "num_nonuniform_data_real_bytes=" << num_nonuniform_data_real_bytes << endl;
            num_nonuniform_data_real_doubles = (int)(nonuniform_data_real / sizeof(double));
            cout << "num_nonuniform_data_real_doubles=" << num_nonuniform_data_real_doubles << endl;

            int num_nonuniform_data_imag_bytes = (int)nonuniform_data_imag;
            // cout << "num_nonuniform_data_imag_bytes=" << num_nonuniform_data_imag_bytes << endl;
            num_nonuniform_data_imag_doubles = (int)(nonuniform_data_imag / sizeof(double));
            cout << "num_nonuniform_data_imag_doubles=" << num_nonuniform_data_imag_doubles << endl;

            int num_nonuniform_x_coord_bytes = (int)nonuniform_x_coord;
            // cout << "num_nonuniform_x_coord_bytes=" << num_nonuniform_x_coord_bytes << endl;
            num_nonuniform_x_coord_doubles = (int)(nonuniform_x_coord / sizeof(double));
            cout << "num_nonuniform_x_coord_doubles=" << num_nonuniform_x_coord_doubles << endl;

            int num_nonuniform_y_coord_bytes = (int)nonuniform_y_coord;
            // cout << "num_nonuniform_y_coord_bytes=" << num_nonuniform_y_coord_bytes << endl;
            num_nonuniform_y_coord_doubles = (int)(nonuniform_y_coord / sizeof(double));
            cout << "num_nonuniform_y_coord_doubles=" << num_nonuniform_y_coord_doubles << endl;

            int num_uniform_data_tiled_real_bytes = (int)uniform_data_tiled_real;
            // cout << "num_uniform_data_tiled_real_bytes=" << num_uniform_data_tiled_real_bytes << endl;
            num_uniform_data_tiled_real_doubles = (int)(uniform_data_tiled_real / sizeof(double));
            cout << "num_uniform_data_tiled_real_doubles=" << num_uniform_data_tiled_real_doubles << endl;

            int num_uniform_data_tiled_imag_bytes = (int)uniform_data_tiled_imag;
            // cout << "num_uniform_data_tiled_imag_bytes=" << num_uniform_data_tiled_imag_bytes << endl;
            num_uniform_data_tiled_imag_doubles = (int)(uniform_data_tiled_imag / sizeof(double));
            cout << "num_uniform_data_tiled_imag_doubles=" << num_uniform_data_tiled_imag_doubles << endl;

            // Declare pointers for input data arrays
            double *weight_data_real_arr;
            double *weight_data_imag_arr;
            double *nonuniform_x_coord_arr;
            double *nonuniform_y_coord_arr;
            double *nonuniform_data_real_arr;
            double *nonuniform_data_imag_arr;
            double *uniform_data_tiled_real_arr;
            double *uniform_data_tiled_imag_arr;

            input_sample *nonuniform_data_arr, *nonuniform_data_arr_d;
            table_vals table;
            

            // Allocate space for the input data arrays
            weight_data_real_arr = (double*)_mm_malloc(num_weight_data_real_doubles * sizeof(double), 64);
            if(weight_data_real_arr == NULL) fprintf(stderr, "Bad malloc on weight_data_real_arr\n");
            weight_data_imag_arr = (double*)_mm_malloc(num_weight_data_imag_doubles * sizeof(double), 64);
            if(weight_data_imag_arr == NULL) fprintf(stderr, "Bad malloc on weight_data_imag_arr\n");

            nonuniform_data_real_arr = (double*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(double), 64);
            if(nonuniform_data_real_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_data_real_arr\n");
            nonuniform_data_imag_arr = (double*)_mm_malloc(num_nonuniform_data_imag_doubles * sizeof(double), 64);
            if(nonuniform_data_imag_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_data_imag_arr\n");

            nonuniform_x_coord_arr = (double*)_mm_malloc(num_nonuniform_x_coord_doubles * sizeof(double), 64);
            if(nonuniform_x_coord_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_x_coord_arr\n");
            nonuniform_y_coord_arr = (double*)_mm_malloc(num_nonuniform_y_coord_doubles * sizeof(double), 64);
            if(nonuniform_y_coord_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_y_coord_arr\n");

            uniform_data_tiled_real_arr = (double*)_mm_malloc(num_uniform_data_tiled_real_doubles * sizeof(double), 64);
            if(uniform_data_tiled_real_arr == NULL) fprintf(stderr, "Bad malloc on uniform_data_tiled_real_arr\n");
            uniform_data_tiled_imag_arr = (double*)_mm_malloc(num_uniform_data_tiled_imag_doubles * sizeof(double), 64);
            if(uniform_data_tiled_imag_arr == NULL) fprintf(stderr, "Bad malloc on uniform_data_tiled_imag_arr\n");

            nonuniform_data_arr = (input_sample*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(input_sample), 64);
            if(nonuniform_data_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_data_arr\n");


            // Declare pointers for output data arrays
            complex_float *uniform_data_arr, *uniform_data_arr_d;

            // Allocate space for the GPU arrays
            cudaError_t t1 = cudaMalloc((void**)&nonuniform_data_arr_d, num_nonuniform_data_real_doubles * sizeof(input_sample));
            if(nonuniform_data_arr_d == NULL) fprintf(stderr, "Bad malloc on nonuniform_data_arr_d\n");

            cudaError_t t2 = cudaMalloc((void**)&uniform_data_arr_d, MEM_DIM * MEM_DIM * sizeof(complex_float));
            if(uniform_data_arr_d == NULL) fprintf(stderr, "Bad malloc on uniform_data_arr_d\n");


            // Allocate space for the output data arrays
            uniform_data_arr = (complex_float*)malloc(MEM_DIM * MEM_DIM * sizeof(complex_float));
            if(uniform_data_arr == NULL) fprintf(stderr, "Bad malloc on uniform_data_arr\n");


            // Stream the file into the input data arrays
            weight_data_real_File.seekg(0, ios::beg);
            weight_data_real_File.read((char*)weight_data_real_arr, num_weight_data_real_bytes);
            weight_data_real_File.close();

            weight_data_imag_File.seekg(0, ios::beg);
            weight_data_imag_File.read((char*)weight_data_imag_arr, num_weight_data_imag_bytes);
            weight_data_imag_File.close();

            nonuniform_data_real_File.seekg(0, ios::beg);
            nonuniform_data_real_File.read((char*)nonuniform_data_real_arr, num_nonuniform_data_real_bytes);
            nonuniform_data_real_File.close();

            nonuniform_data_imag_File.seekg(0, ios::beg);
            nonuniform_data_imag_File.read((char*)nonuniform_data_imag_arr, num_nonuniform_data_imag_bytes);
            nonuniform_data_imag_File.close();

            nonuniform_x_coord_File.seekg(0, ios::beg);
            nonuniform_x_coord_File.read((char*)nonuniform_x_coord_arr, num_nonuniform_x_coord_bytes);
            nonuniform_x_coord_File.close();

            nonuniform_y_coord_File.seekg(0, ios::beg);
            nonuniform_y_coord_File.read((char*)nonuniform_y_coord_arr, num_nonuniform_y_coord_bytes);
            nonuniform_y_coord_File.close();

            uniform_data_tiled_real_File.seekg(0, ios::beg);
            uniform_data_tiled_real_File.read((char*)uniform_data_tiled_real_arr, num_uniform_data_tiled_real_bytes);
            uniform_data_tiled_real_File.close();

            uniform_data_tiled_imag_File.seekg(0, ios::beg);
            uniform_data_tiled_imag_File.read((char*)uniform_data_tiled_imag_arr, num_uniform_data_tiled_imag_bytes);
            uniform_data_tiled_imag_File.close();
            
            cout << "number of nonuniform samples: " << num_nonuniform_data_real_doubles << endl;
            double tmp;
            for(int i = 0; i < num_nonuniform_data_real_doubles; ++i) {
                tmp = static_cast<double>(nonuniform_data_real_arr[i]);
                nonuniform_data_arr[i].val.real = (float)tmp;
                tmp = static_cast<double>(nonuniform_data_imag_arr[i]);
                nonuniform_data_arr[i].val.imag = (float)tmp;
                tmp = static_cast<double>(nonuniform_x_coord_arr[i]);
                nonuniform_data_arr[i].x_coord = (float)tmp;
                tmp = static_cast<double>(nonuniform_y_coord_arr[i]);
                nonuniform_data_arr[i].y_coord = (float)tmp;
                // cout << "real=" << nonuniform_data_arr[i].val.real << ", imag=" << nonuniform_data_arr[i].val.imag << ", x_coord=" << nonuniform_data_arr[i].x_coord << ", y_coord=" << nonuniform_data_arr[i].y_coord << endl;
            }

            cout << "number of weights: " << num_weight_data_real_doubles << endl;
            for(int i = 0; i < num_weight_data_real_doubles; ++i) {
                tmp = static_cast<double>(weight_data_real_arr[i]);
                table.table_arr[i].real = (float)tmp;
                tmp = static_cast<double>(weight_data_imag_arr[i]);
                table.table_arr[i].imag = (float)tmp;
                // cout << "real=" << table.table_arr[i].real << ", imag=" << table.table_arr[i].imag << endl;
            }

            cout << "number of uniform values: " << num_uniform_data_tiled_real_doubles << endl;
            for(int x = 0; x < ACCEL_DIM; ++x) {
                for(int y = 0; y < ACCEL_DIM; ++y) {
                    // for(int z = 0; z < ((MAX_TABLE_DIM * MAX_TABLE_DIM) / (ACCEL_DIM * ACCEL_DIM)); ++z) {
                    for(int z = 0; z < (num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM); ++z) {
                        int tid = ((y*ACCEL_DIM*(num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM)) + (x*(num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM))) + z;
                        tmp = static_cast<double>(uniform_data_tiled_real_arr[tid]);
                        uniform_data_arr[tid].real = (float)tmp;
                        tmp = static_cast<double>(uniform_data_tiled_imag_arr[tid]);
                        uniform_data_arr[tid].imag = (float)tmp;
                        // cout << "real=" << uniform_data_arr[i].real << ", imag=" << uniform_data_arr[i].imag << endl;
                    }
                }
            }

            cout << "All files have been loaded into memory." << endl;


            //Copy the constant table struct and the input data array to the device
            cudaMemcpyToSymbol(table_d, &table, sizeof(table_vals));
            cudaMemcpy(nonuniform_data_arr_d, nonuniform_data_arr, num_nonuniform_data_real_doubles * sizeof(input_sample), cudaMemcpyHostToDevice);
            cudaMemcpy(uniform_data_arr_d, uniform_data_arr, MEM_DIM * MEM_DIM * sizeof(complex_float), cudaMemcpyHostToDevice);


            cout << "***Beginning GPU Computation***" << endl;

            // Start recording the elapsed time
            cudaEvent_t start, stop;
            cudaEventCreate(&start);
            cudaEventCreate(&stop);
            cudaEventRecord(start);

            // Run kernel to interpolate values
            if(operation_type == "gridding") {
                grid_dim = 768;
                tile_dim = grid_dim / ACCEL_DIM;
                gridding_cuda<<<gridSize, blockSize>>>(nonuniform_data_arr_d, uniform_data_arr_d, tile_dim, num_nonuniform_data_real_doubles, (num_nonuniform_data_real_doubles/(NUM_BLOCKS_PER_DIM * NUM_BLOCKS_PER_DIM)));
            } else if(operation_type == "regridding") {
                dim3 customGridSize(64, 32, 1);
                dim3 customblockSize(ACCEL_DIM, ACCEL_DIM, 1);
                grid_dim = 512;
                tile_dim = grid_dim / ACCEL_DIM;
                // regridding_cuda<<<gridSize, blockSize>>>(nonuniform_data_arr_d, uniform_data_arr_d, tile_dim, num_nonuniform_data_real_doubles);
                regridding_cuda<<<customGridSize, customblockSize>>>(nonuniform_data_arr_d, uniform_data_arr_d, tile_dim, num_nonuniform_data_real_doubles);
                // regridding_v2_cuda<<<gridSize, blockSize>>>(nonuniform_data_arr_d, uniform_data_arr_d, tile_dim, num_nonuniform_data_real_doubles, (num_nonuniform_data_real_doubles/(NUM_BLOCKS_PER_DIM * NUM_BLOCKS_PER_DIM)));
            } else {
                printf("Operation type %s not supported!\n", operation_type.c_str());
            }
            
            // Stop recording the elapsed time
            cudaEventRecord(stop);
            cudaEventSynchronize(stop);
            float milliseconds = 0;
            cudaEventElapsedTime(&milliseconds, start, stop);
            cout << "Compute Time (milliseconds): " << milliseconds << endl;

            cout << "***Copying Data from GPU***" << endl;

            // Copy the output data arrays to the host
            cudaError_t t3 = cudaMemcpy(nonuniform_data_arr, nonuniform_data_arr_d, num_nonuniform_data_real_doubles * sizeof(input_sample), cudaMemcpyDeviceToHost);
            cudaError_t t4 = cudaMemcpy(uniform_data_arr, uniform_data_arr_d, MEM_DIM * MEM_DIM * sizeof(complex_float), cudaMemcpyDeviceToHost);

            cout << "***Writing GPU Output***" << endl;

            // // DEBUG
            // // Print GPU output for debugging
            // for(int i = 0; i < num_nonuniform_data_real_doubles; ++i) {
            //     if(i<50) {
            //         if(nonuniform_data_arr[i].val.real != 0 || nonuniform_data_arr[i].val.imag != 0) {
            //             printf("[%d], real=%.14e, imag=%.14e\n", i, nonuniform_data_arr[i].val.real, nonuniform_data_arr[i].val.imag);
            //         }
            //     }
            // }
            // for(int k = 0; k < NUM_POINTS_PER_THREAD; ++k) {
            //     for(int j = 0; j < ACCEL_DIM; ++j) {
            //         for(int i = 0; i < ACCEL_DIM; ++i) {
            //             int offset = j*(ACCEL_DIM*NUM_POINTS_PER_THREAD) + i*NUM_POINTS_PER_THREAD;
            //             if(i==0 && j==0 && k<100) {
            //                 if(uniform_data_arr[offset+k].real != 0 || uniform_data_arr[offset+k].imag != 0) {
            //                     printf("[%d][%d][%d], real=%.14e, imag=%.14e\n", i, j, k, uniform_data_arr[offset+k].real, uniform_data_arr[offset+k].imag);
            //                 }
            //             }
            //         }
            //     }
            // }
            // // DEBUG

            // Dump GPU output to binary files
            std::ofstream nonuniform_real_out("data/tmp/gpu_nonuniform_data_arr_real.bin",std::ios_base::binary);
            std::ofstream nonuniform_imag_out("data/tmp/gpu_nonuniform_data_arr_imag.bin",std::ios_base::binary);
            if(nonuniform_real_out.good() && nonuniform_imag_out.good()) {
                for(int idx = 0; idx < num_nonuniform_data_real_doubles; ++idx) {
                    input_sample tmp = nonuniform_data_arr[idx];
                    double real = tmp.val.real;
                    double imag = tmp.val.imag;
                    nonuniform_real_out.write((char *)&real, sizeof(double));
                    nonuniform_imag_out.write((char *)&imag, sizeof(double));
                }
            }
            nonuniform_real_out.close();
            nonuniform_imag_out.close();

            std::ofstream uniform_real_out("data/tmp/gpu_uniform_data_arr_real.bin",std::ios_base::binary);
            std::ofstream uniform_imag_out("data/tmp/gpu_uniform_data_arr_imag.bin",std::ios_base::binary);
            if(uniform_real_out.good() && uniform_imag_out.good()) {
                for(int x = 0; x < ACCEL_DIM; ++x) {
                    for(int y = 0; y < ACCEL_DIM; ++y) {
                        // for(int z = 0; z < ((MAX_TABLE_DIM * MAX_TABLE_DIM) / (ACCEL_DIM * ACCEL_DIM)); ++z) {
                        for(int z = 0; z < (num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM); ++z) {
                            int tid = ((y*ACCEL_DIM*(num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM)) + (x*(num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM))) + z;
                            complex_float tmp = uniform_data_arr[tid];
                            double real = tmp.real;
                            double imag = tmp.imag;
                            uniform_real_out.write((char *)&real, sizeof(double));
                            uniform_imag_out.write((char *)&imag, sizeof(double));
                        }
                    }
                }
            }
            uniform_real_out.close();
            uniform_imag_out.close();


            // Free up allocated memory
            free(nonuniform_x_coord_arr);
            free(nonuniform_y_coord_arr);
            free(nonuniform_data_real_arr);
            free(nonuniform_data_imag_arr);
            free(weight_data_real_arr);
            free(weight_data_imag_arr);
            free(nonuniform_data_arr);
            free(uniform_data_arr);
            cudaFree(nonuniform_data_arr_d);

            // Reset device for proper profiler function
            cudaDeviceReset();
        }
    }
}
