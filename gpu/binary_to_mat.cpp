#include <stdio.h>
#include <math.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <chrono>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>
#include <unistd.h>
#include <mm_malloc.h>
#include <mat.h>
#include <cstdio>
#include <vector>
#include <bitset>

#define N 32 // This can't be changed (possibly implement variable support later?)
#define E 8 // This can't be changed (possibly implement variable support later?)
#define S 1 // This can't be changed (must always be 1 for FP)
#define MEM_DIM 1024
#define TABLE_VALUES_OVERSAMP_FACTOR 32
#define ACCEL_DIM 8
#define MEM_TILE_DIM (MEM_DIM/ACCEL_DIM)
#define INTERP_WIN_DIM 6
#define NUM_POINTS_PER_THREAD (MEM_DIM*MEM_DIM/(ACCEL_DIM*ACCEL_DIM))



using namespace std;

typedef union {
    float f;
    struct
    {
        // Order is important.
        // Here the members of the union data structure
        // use the same memory (32 bits).
        // The ordering is taken
        // from the LSB to the MSB.
        unsigned int mantissa : (N-E-S);
        unsigned int exponent : E;
        unsigned int sign : S;
    } raw;
} RawFloat;

struct complex_float {
    RawFloat real;
    RawFloat imag;
};

// Global Variables
// Declare pointers for input data arrays
vector<complex_float> nonuniform_data_arr;

// Declare pointers for output data arrays
vector<vector<vector<complex_float>>> uniform_data_arr;

// Declare variables for storing the number of elements per file
int num_nonuniform_data_real_doubles;
int num_nonuniform_data_imag_doubles;
int num_uniform_data_tiled_real_doubles;
int num_uniform_data_tiled_imag_doubles;


void write_mat_files() {
    // Write the uniform data to a matfile
    double *reals = (double*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(double), 64);
    double *imags = (double*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(double), 64);
    for(int idx = 0; idx < num_nonuniform_data_real_doubles; ++idx) {
        reals[idx] = (double)nonuniform_data_arr.at(idx).real.f;
        imags[idx] = (double)nonuniform_data_arr.at(idx).imag.f;
    }
    MATFile *pmat;
    mxArray *paReal;
    mxArray *paImag;
    int status1;
    int status2;
    string filename = "data/gpu/gpu_nonuniform_data.mat";
    printf("Creating file %s...\n\n", filename.c_str());
    pmat = matOpen(filename.c_str(), "w");
    if (pmat == NULL) {
        printf("Error creating file %s\n", filename.c_str());
        printf("(Do you have write permission in this directory?)\n");
    } else {
        paReal = mxCreateDoubleMatrix(num_nonuniform_data_real_doubles, 1, mxREAL);
        paImag = mxCreateDoubleMatrix(num_nonuniform_data_real_doubles, 1, mxREAL);
        if (paReal == NULL || paImag == NULL) {
            printf("%s : Out of memory on line %d\n", __FILE__, __LINE__);
            printf("Unable to create mxArray.\n");
        } else {
            memcpy((void *)(mxGetPr(paReal)), (double*)reals, num_nonuniform_data_real_doubles * 8);
            memcpy((void *)(mxGetPr(paImag)), (double*)imags, num_nonuniform_data_real_doubles * 8);
            status1 = matPutVariableAsGlobal(pmat, "nonuniform_data_real", paReal);
            status2 = matPutVariableAsGlobal(pmat, "nonuniform_data_imag", paImag);
            if (status1 != 0 || status2 != 0) {
                printf("Error using matPutVariableAsGlobal\n");
            }
        }
        mxDestroyArray(paReal);
        mxDestroyArray(paImag);
    }
    _mm_free(reals);
    _mm_free(imags);


    // Write the uniform data to a matfile
    reals = (double*)_mm_malloc(num_uniform_data_tiled_real_doubles * sizeof(double), 64);
    imags = (double*)_mm_malloc(num_uniform_data_tiled_real_doubles * sizeof(double), 64);
    for(int x = 0; x < ACCEL_DIM; ++x) {
        for(int y = 0; y < ACCEL_DIM; ++y) {
            for(int z = 0; z < (num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM); ++z) {
                int tid = ((y*ACCEL_DIM*(num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM)) + (x*(num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM))) + z;
                reals[tid] = (double)uniform_data_arr.at(x).at(y).at(z).real.f;
                imags[tid] = (double)uniform_data_arr.at(x).at(y).at(z).imag.f;
            }
        }
    }
    pmat;
    paReal;
    paImag;
    status1;
    status2;
    filename = "data/gpu/gpu_uniform_data.mat";
    printf("Creating file %s...\n\n", filename.c_str());
    pmat = matOpen(filename.c_str(), "w");
    if (pmat == NULL) {
        printf("Error creating file %s\n", filename.c_str());
        printf("(Do you have write permission in this directory?)\n");
    } else {
        paReal = mxCreateDoubleMatrix(num_uniform_data_tiled_real_doubles, 1, mxREAL);
        paImag = mxCreateDoubleMatrix(num_uniform_data_tiled_real_doubles, 1, mxREAL);
        if (paReal == NULL || paImag == NULL) {
            printf("%s : Out of memory on line %d\n", __FILE__, __LINE__);
            printf("Unable to create mxArray.\n");
        } else {
            memcpy((void *)(mxGetPr(paReal)), (double*)reals, num_uniform_data_tiled_real_doubles * 8);
            memcpy((void *)(mxGetPr(paImag)), (double*)imags, num_uniform_data_tiled_real_doubles * 8);
            status1 = matPutVariableAsGlobal(pmat, "uniform_data_tiled_real", paReal);
            status2 = matPutVariableAsGlobal(pmat, "uniform_data_tiled_imag", paImag);
            if (status1 != 0 || status2 != 0) {
                printf("Error using matPutVariableAsGlobal\n");
            }
        }
        mxDestroyArray(paReal);
        mxDestroyArray(paImag);
    }
    _mm_free(reals);
    _mm_free(imags);
}


//////////////////////////////////////////////
//                                          //
//                   Main                   //
//                                          //
//////////////////////////////////////////////
int main(int argc, char *argv[]) {
    // Check if the correct numbe of arguments are present; argc should be 2 for correct execution
    if(argc != 5) {
        // If there are the wrong number of arguments, print usage
        printf("Error: %d arguments given, but expected 4.\n", argc-1);
        printf("usage: %s nonuniform_data_real_file nonuniform_data_imag_file uniform_data_tiled_real_file uniform_data_tiled_imag_file\n", argv[0]);
    } else {

        // If the number of arguments is correct, save operation type and open files
        ifstream nonuniform_data_real_File(argv[1], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_data_imag_File(argv[2], ios::in | ios::binary | ios::ate);

        ifstream uniform_data_tiled_real_File(argv[3], ios::in | ios::binary | ios::ate);
        ifstream uniform_data_tiled_imag_File(argv[4], ios::in | ios::binary | ios::ate);

        if(!nonuniform_data_real_File.good() || !nonuniform_data_imag_File.good() || !uniform_data_tiled_real_File.good() || !uniform_data_tiled_imag_File.good()) {
            if(!nonuniform_data_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[1]);
            } else {
                nonuniform_data_real_File.close();
            }
            if(!nonuniform_data_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[2]);
            } else {
                nonuniform_data_imag_File.close();
            }
            if(!uniform_data_tiled_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[3]);
            } else {
                uniform_data_tiled_real_File.close();
            }
            if(!uniform_data_tiled_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[4]);
            } else {
                uniform_data_tiled_imag_File.close();
            }
        } else {
            // Get the size of the file in bytes
            streampos nonuniform_data_real = nonuniform_data_real_File.tellg();
            streampos nonuniform_data_imag = nonuniform_data_imag_File.tellg();
            streampos uniform_data_tiled_real = uniform_data_tiled_real_File.tellg();
            streampos uniform_data_tiled_imag = uniform_data_tiled_imag_File.tellg();


            cout << "customfloat info:\nN= " << N << "; E= " << E << "\n" << endl;
            cout << "sizeof(complex_float) = " << sizeof(complex_float) << endl;
            cout << "output points = " << MEM_DIM * MEM_DIM << endl;
            cout << "threads = " << ACCEL_DIM * ACCEL_DIM << endl;
            cout << "points per thread = " << NUM_POINTS_PER_THREAD << endl;


            int num_nonuniform_data_real_bytes = (int)nonuniform_data_real;
            // cout << "num_nonuniform_data_real_bytes=" << num_nonuniform_data_real_bytes << endl;
            num_nonuniform_data_real_doubles = (int)(nonuniform_data_real / sizeof(double));
            cout << "num_nonuniform_data_real_doubles=" << num_nonuniform_data_real_doubles << endl;

            int num_nonuniform_data_imag_bytes = (int)nonuniform_data_imag;
            // cout << "num_nonuniform_data_imag_bytes=" << num_nonuniform_data_imag_bytes << endl;
            num_nonuniform_data_imag_doubles = (int)(nonuniform_data_imag / sizeof(double));
            cout << "num_nonuniform_data_imag_doubles=" << num_nonuniform_data_imag_doubles << endl;

            int num_uniform_data_tiled_real_bytes = (int)uniform_data_tiled_real;
            // cout << "num_uniform_data_tiled_real_bytes=" << num_uniform_data_tiled_real_bytes << endl;
            num_uniform_data_tiled_real_doubles = (int)(uniform_data_tiled_real / sizeof(double));
            cout << "num_uniform_data_tiled_real_doubles=" << num_uniform_data_tiled_real_doubles << endl;

            int num_uniform_data_tiled_imag_bytes = (int)uniform_data_tiled_imag;
            // cout << "num_uniform_data_tiled_imag_bytes=" << num_uniform_data_tiled_imag_bytes << endl;
            num_uniform_data_tiled_imag_doubles = (int)(uniform_data_tiled_imag / sizeof(double));
            cout << "num_uniform_data_tiled_imag_doubles=" << num_uniform_data_tiled_imag_doubles << endl;


            // Declare pointers for input data arrays
            double *nonuniform_data_real_arr;
            double *nonuniform_data_imag_arr;
            double *uniform_data_tiled_real_arr;
            double *uniform_data_tiled_imag_arr;


            // Allocate space for the input data arrays
            nonuniform_data_real_arr = (double*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(double), 64);
            if(nonuniform_data_real_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_data_real_arr\n");
            nonuniform_data_imag_arr = (double*)_mm_malloc(num_nonuniform_data_imag_doubles * sizeof(double), 64);
            if(nonuniform_data_imag_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_data_imag_arr\n");

            uniform_data_tiled_real_arr = (double*)_mm_malloc(num_uniform_data_tiled_real_doubles * sizeof(double), 64);
            if(uniform_data_tiled_real_arr == NULL) fprintf(stderr, "Bad malloc on uniform_data_tiled_real_arr\n");
            uniform_data_tiled_imag_arr = (double*)_mm_malloc(num_uniform_data_tiled_imag_doubles * sizeof(double), 64);
            if(uniform_data_tiled_imag_arr == NULL) fprintf(stderr, "Bad malloc on uniform_data_tiled_imag_arr\n");


            // Stream the file into the input data arrays
            nonuniform_data_real_File.seekg(0, ios::beg);
            nonuniform_data_real_File.read((char*)nonuniform_data_real_arr, num_nonuniform_data_real_bytes);
            nonuniform_data_real_File.close();

            nonuniform_data_imag_File.seekg(0, ios::beg);
            nonuniform_data_imag_File.read((char*)nonuniform_data_imag_arr, num_nonuniform_data_imag_bytes);
            nonuniform_data_imag_File.close();

            uniform_data_tiled_real_File.seekg(0, ios::beg);
            uniform_data_tiled_real_File.read((char*)uniform_data_tiled_real_arr, num_uniform_data_tiled_real_bytes);
            uniform_data_tiled_real_File.close();

            uniform_data_tiled_imag_File.seekg(0, ios::beg);
            uniform_data_tiled_imag_File.read((char*)uniform_data_tiled_imag_arr, num_uniform_data_tiled_imag_bytes);
            uniform_data_tiled_imag_File.close();


            for(int i = 0; i < num_nonuniform_data_real_doubles; ++i) {
                complex_float tmp;
                double val1, val2;
                val1 = (static_cast<double>(nonuniform_data_real_arr[i]));
                val2 = (static_cast<double>(nonuniform_data_imag_arr[i]));
                tmp.real.f = (float)val1;
                tmp.imag.f = (float)val2;

                nonuniform_data_arr.push_back(tmp);
            }

            int idx = 0;
            for(int x = 0; x < ACCEL_DIM; ++x) {
                uniform_data_arr.push_back(vector<vector<complex_float>>{ACCEL_DIM});
                for(int y = 0; y < ACCEL_DIM; ++y) {
                    uniform_data_arr.at(x).push_back(vector<complex_float>{ACCEL_DIM});
                    for(int z = 0; z < (num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM); ++z) {
                        complex_float tmp;
                        double val1, val2;
                        val1 = (static_cast<double>(uniform_data_tiled_real_arr[idx]));
                        val2 = (static_cast<double>(uniform_data_tiled_imag_arr[idx]));
                        tmp.real.f = (float)val1;
                        tmp.imag.f = (float)val2;

                        uniform_data_arr.at(x).at(y).push_back(tmp);

                        idx++;
                    }
                }
            }

            write_mat_files();

            // Free up allocated memory
            _mm_free(nonuniform_data_real_arr);
            _mm_free(nonuniform_data_imag_arr);
            _mm_free(uniform_data_tiled_real_arr);
            _mm_free(uniform_data_tiled_imag_arr);
            usleep(50000);
        }
    }
}
