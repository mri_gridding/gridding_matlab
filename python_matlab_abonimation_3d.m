%%%%%%%%%% Initial Setup  %%%%%%%%%%
clear
run('set_paths.m')
run('irt/setup.m')

%%%%%%%%%% Python Interpolation %%%%%%%%%%
% tic
% if(strcmp(computer('arch'), 'win64'))
% 	system(['"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\python.exe" kbnufft_adj_interp_3d.py']);
% elseif(strcmp(computer('arch'), 'glnxa64'))
% 	system(['python3 kbnufft_adj_interp_3d.py']);
% else
%     fprintf('Error: only Windows and Linux are supported.\n');
%     return;
% end
% fprintf('\n\nPython Gridding Time: %s', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))


%%%%%%%%%% Load Python Interpolation %%%%%%%%%%
load(sprintf('python_griddat_3d.mat'));
python_griddat_3d = reshape(python_griddat_3d, 768, 768, 768); % Reshape into a cube to match hardware output

%%%%%%%%%% Matlab Interpolation %%%%%%%%%%
% tic
input_filename = 'run_interp_adj_var_3d.mat';
% matlab_griddat_3d = pythonish_interp_adj_3d(input_filename, 'adj', 'double', 16);
% matlab_griddat_3d = reshape(matlab_griddat_3d, 768, 768, 768); % Reshape into a cube to match hardware output

% matlab_griddat_3d = hardware_sim_adj_3d(input_filename);
matlab_griddat_3d = hardware_sim_adj_3d_cube(input_filename);
% matlab_griddat_3d = hardware_sim_adj_3d_TEST(input_filename, 'single_fixed_input_coord_interp_table');
% matlab_griddat_3d_fixed = hardware_sim_adj_3d_fixed(input_filename, 'adj', 'fixed_3d', 32);
% matlab_griddat_3d_fixed = hardware_sim_adj_3d_fixed_TEST(input_filename, 'adj', 'fixed_3d', 32);

% save(sprintf('matlab_griddat_3d.mat'), 'matlab_griddat_3d', '-v7.3');
% fprintf('Matlab Gridding Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))

%%%%%%%%%% Compare Interpolation Results %%%%%%%%%%
% nrms_error = nrms(matlab_griddat_3d_fixed, matlab_griddat_3d)*100;
% max_perc_diff = (max(abs(matlab_griddat_3d(:)-matlab_griddat_3d_fixed(:))) / max(abs(matlab_griddat_3d_fixed(:))))*100;
% fprintf('\n\nNormalized Root Mean Square Error: %f%%\n', nrms_error)
% fprintf('Maximum [Pixel] Percent Difference: %f%%\n\n\n', max_perc_diff)
% nrms(python_griddat, matlab_griddat)
% nrms(matlab_griddat_3d, matlab_griddat_3d_fixed)

%%%%%%%%%% Print Error Percentages %%%%%%%%%%
nrms_error = nrms(python_griddat_3d, matlab_griddat_3d)*100;
max_perc_diff = (max(abs(matlab_griddat_3d(:)-python_griddat_3d(:))) / max(abs(python_griddat_3d(:))))*100;

% nrms_error = nrms(python_griddat_3d, matlab_griddat_3d_fixed)*100;
% max_perc_diff = (max(abs(matlab_griddat_3d_fixed(:)-python_griddat_3d(:))) / max(abs(python_griddat_3d(:))))*100;

fprintf('\n\nNormalized Root Mean Square Error: %f%%\n', nrms_error)
fprintf('Maximum [Pixel] Percent Difference: %f%%\n\n\n', max_perc_diff)
