#include <stdio.h>
#include <math.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <chrono>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>
#include <unistd.h>
#include <mm_malloc.h>
#include <mat.h>
#include <cstdio>
#include <vector>
#include <bitset>

#define MEM_DIM 1024
#define TABLE_VALUES_OVERSAMP_FACTOR 32
#define ACCEL_DIM 8
#define MEM_TILE_DIM (MEM_DIM/ACCEL_DIM)
#define INTERP_WIN_DIM 6
#define NUM_POINTS_PER_THREAD (MEM_DIM*MEM_DIM/(ACCEL_DIM*ACCEL_DIM))


#define NUM_THREADS (ACCEL_DIM*ACCEL_DIM) // Number of threads

using namespace std;

bool nufft_type_adj; // true for adj, false for forw

struct complex_float {
    float real;
    float imag;
};

struct input_sample {
    complex_float val;
    float x_coord;
    float y_coord;
};

struct PointArgs {
    bool nufft_type_adj;
    uint tidx;
    uint tidy;
};

pthread_mutex_t ThreadLock; /* mutex */
volatile int numThreads;

// Global Variables
int numThreadLoops; // Number of threads for each calculation

// Declare pointers for input data arrays
vector<input_sample> nonuniform_data_arr;
vector<complex_float> weight_arr;

// Declare pointers for output data arrays
vector<vector<vector<complex_float>>> uniform_data_arr;

// Declare variables for storing the number of elements per file
int num_weight_data_real_doubles;
int num_weight_data_imag_doubles;
int num_nonuniform_data_real_doubles;
int num_nonuniform_data_imag_doubles;
int num_nonuniform_x_coord_doubles;
int num_nonuniform_y_coord_doubles;
int num_uniform_data_tiled_real_doubles;
int num_uniform_data_tiled_imag_doubles;

// Declare variables for "constants" that require modification based on operation type
int grid_dim;
int tile_dim;
int fft_len;


complex_float complex_fmul(complex_float a, complex_float b) {
    float k1;
    k1 = a.real + a.imag;
    float k2;
    k2 = b.imag - b.real;
    float k3;
    k3 = b.real + b.imag;
    float k4;
    k4 = b.real * k1;
    float k5;
    k5 = a.real * k2;
    float k6;
    k6 = a.imag * k3;
    float R;
    R = k4 - k6;
    float I;
    I = k4 + k5;

    complex_float result;
    result.real = R;
    result.imag = I;

    return result;
}


complex_float complex_fadd(complex_float a, complex_float b) {
    complex_float result;
    result.real = a.real + b.real;
    result.imag = a.imag + b.imag;

    return result;
}


complex_float complex_fsub(complex_float a, complex_float b) {
    complex_float result;
    result.real = a.real - b.real;
    result.imag = a.imag - b.imag;

    return result;
}


complex_float swap_complex_components(complex_float datum) {
    complex_float tmp;
    tmp.real = datum.imag;
    tmp.imag = datum.real;

    return tmp;
}


void complex_butterfly(bool isIFFT, int x_accel_idx, int y_accel_idx, int tile_idx_1, int tile_idx_2, int w_addr, int x_comb_pipe_offset, int y_comb_pipe_offset) {
    complex_float w = weight_arr.at(w_addr);

    complex_float val1 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1);
    complex_float val2 = uniform_data_arr.at(x_accel_idx-x_comb_pipe_offset).at(y_accel_idx-y_comb_pipe_offset).at(tile_idx_2);
    if(nufft_type_adj) {
        val1 = swap_complex_components(val1);
        val2 = swap_complex_components(val2);
    }

    complex_float result = complex_fmul(w, val1);

    complex_float a = complex_fsub(val2, result); // 1-st part of the "butterfly" creating operation
    complex_float b = complex_fadd(val2, result); // 2-nd part of the "butterfly" creating operation

    if(nufft_type_adj) {
        a = swap_complex_components(a);
        b = swap_complex_components(b);
    }
    uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1) = a;
    uniform_data_arr.at(x_accel_idx-x_comb_pipe_offset).at(y_accel_idx-y_comb_pipe_offset).at(tile_idx_2) = b;
}


void write_mat_files() {
    // Write the uniform data to a matfile
    double *reals = (double*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(double), 64);
    double *imags = (double*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(double), 64);
    for(int idx = 0; idx < num_nonuniform_data_real_doubles; ++idx) {
        reals[idx] = (double)nonuniform_data_arr.at(idx).val.real;
        imags[idx] = (double)nonuniform_data_arr.at(idx).val.imag;
    }
    MATFile *pmat;
    mxArray *paReal;
    mxArray *paImag;
    int status1;
    int status2;
    string filename = "data/cpp/tmp/cpu_nonuniform_data.mat";
    printf("Creating file %s...\n\n", filename.c_str());
    pmat = matOpen(filename.c_str(), "w");
    if (pmat == NULL) {
        printf("Error creating file %s\n", filename.c_str());
        printf("(Do you have write permission in this directory?)\n");
    } else {
        paReal = mxCreateDoubleMatrix(num_nonuniform_data_real_doubles, 1, mxREAL);
        paImag = mxCreateDoubleMatrix(num_nonuniform_data_real_doubles, 1, mxREAL);
        if (paReal == NULL || paImag == NULL) {
            printf("%s : Out of memory on line %d\n", __FILE__, __LINE__);
            printf("Unable to create mxArray.\n");
        } else {
            memcpy((void *)(mxGetPr(paReal)), (double*)reals, num_nonuniform_data_real_doubles * 8);
            memcpy((void *)(mxGetPr(paImag)), (double*)imags, num_nonuniform_data_real_doubles * 8);
            status1 = matPutVariableAsGlobal(pmat, "nonuniform_data_real", paReal);
            status2 = matPutVariableAsGlobal(pmat, "nonuniform_data_imag", paImag);
            if (status1 != 0 || status2 != 0) {
                printf("Error using matPutVariableAsGlobal\n");
            }
        }
        mxDestroyArray(paReal);
        mxDestroyArray(paImag);
    }
    _mm_free(reals);
    _mm_free(imags);


    // Write the uniform data to a matfile
    reals = (double*)_mm_malloc(num_uniform_data_tiled_real_doubles * sizeof(double), 64);
    imags = (double*)_mm_malloc(num_uniform_data_tiled_real_doubles * sizeof(double), 64);
    for(int x = 0; x < ACCEL_DIM; ++x) {
        for(int y = 0; y < ACCEL_DIM; ++y) {
            for(int z = 0; z < (num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM); ++z) {
                int tid = ((y*ACCEL_DIM*(num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM)) + (x*(num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM))) + z;
                reals[tid] = (double)uniform_data_arr.at(x).at(y).at(z).real;
                imags[tid] = (double)uniform_data_arr.at(x).at(y).at(z).imag;
            }
        }
    }
    pmat;
    paReal;
    paImag;
    status1;
    status2;
    filename = "data/cpp/tmp/cpu_uniform_data.mat";
    printf("Creating file %s...\n\n", filename.c_str());
    pmat = matOpen(filename.c_str(), "w");
    if (pmat == NULL) {
        printf("Error creating file %s\n", filename.c_str());
        printf("(Do you have write permission in this directory?)\n");
    } else {
        paReal = mxCreateDoubleMatrix(num_uniform_data_tiled_real_doubles, 1, mxREAL);
        paImag = mxCreateDoubleMatrix(num_uniform_data_tiled_real_doubles, 1, mxREAL);
        if (paReal == NULL || paImag == NULL) {
            printf("%s : Out of memory on line %d\n", __FILE__, __LINE__);
            printf("Unable to create mxArray.\n");
        } else {
            memcpy((void *)(mxGetPr(paReal)), (double*)reals, num_uniform_data_tiled_real_doubles * 8);
            memcpy((void *)(mxGetPr(paImag)), (double*)imags, num_uniform_data_tiled_real_doubles * 8);
            status1 = matPutVariableAsGlobal(pmat, "uniform_data_tiled_real", paReal);
            status2 = matPutVariableAsGlobal(pmat, "uniform_data_tiled_imag", paImag);
            if (status1 != 0 || status2 != 0) {
                printf("Error using matPutVariableAsGlobal\n");
            }
        }
        mxDestroyArray(paReal);
        mxDestroyArray(paImag);
    }
    _mm_free(reals);
    _mm_free(imags);
}


//////////////////////////////////////////////
//                                          //
//                 Gridding                 //
//                                          //
//////////////////////////////////////////////
void *gridding(void *pointarg) {
    // Calculate the global thread indexes in each direction (relative indexes)
    int tidx = ((struct PointArgs*)pointarg)->tidx;
    int tidy = ((struct PointArgs*)pointarg)->tidy;
    int tid = tidy * ACCEL_DIM + tidx;

    bool nufft_type_adj = ((struct PointArgs*)pointarg)->nufft_type_adj;

    int num_points = num_nonuniform_data_real_doubles;

    int start = 0;
    int end = num_nonuniform_data_real_doubles; //start + num_points;

    // printf("Hello from pipeline (%d, %d); start=%d, end=%d\n", tidx, tidy, start, end);

    #pragma vector aligned
    for(int input_point_idx = start; input_point_idx < end; ++input_point_idx) {
        // Extract the current point being processed
        input_sample current_sample = nonuniform_data_arr.at(input_point_idx);


        //////////////////////////////////////////////
        //                                          //
        //                  Select                  //
        //                                          //
        //////////////////////////////////////////////
        // Extract the "relative" coordinates (within a tile)
        double x_coord = fmod(current_sample.x_coord, (double)ACCEL_DIM);
        double y_coord = fmod(current_sample.y_coord, (double)ACCEL_DIM);

        // Determine if this pipeline is affected by this source point
        double x_distance = fmod(ACCEL_DIM+x_coord-tidx, (double)ACCEL_DIM); // rem is implemented as an upper truncation
        double y_distance = fmod(ACCEL_DIM+y_coord-tidy, (double)ACCEL_DIM); // rem is implemented as an upper truncation

        // Check if the point lies within the interpolation window
        if(x_distance < INTERP_WIN_DIM && y_distance < INTERP_WIN_DIM) {
            // Get the quotient and remainder to determine grid and tile location
            int x_base_tile_idx = current_sample.x_coord/ACCEL_DIM; // x virtual tile index
            int y_base_tile_idx = current_sample.y_coord/ACCEL_DIM; // y virtual tile index

            // This point affects this pipeline!
            // Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
            int x_tile_idx;
            if(x_coord < tidx) {
               // A wrap occured!
                if(x_base_tile_idx == 0) {
                    x_tile_idx = tile_dim - 1;
                } else {
                    x_tile_idx = x_base_tile_idx - 1;
                }
            } else {
                x_tile_idx = x_base_tile_idx;
            }

            // Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
            int y_tile_idx;
            if(y_coord < tidy) {
               // A wrap occured!
                if(y_base_tile_idx == 0) {
                    y_tile_idx = tile_dim - 1;
                } else {
                    y_tile_idx = y_base_tile_idx - 1;
                }
            } else {
                y_tile_idx = y_base_tile_idx;
            }

            // Calculate the tile index ("depth" in the pipeline's data array)
            int tile_idx = y_tile_idx * MEM_TILE_DIM + x_tile_idx; // 1D global virtual tile index
            
            // Calculate the table index
            int x_table_idx = ((x_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift
            int y_table_idx = ((y_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift


            //////////////////////////////////////////////
            //                                          //
            //               Interp Table               //
            //                                          //
            //////////////////////////////////////////////
            // Extract the interp table values
            complex_float x_table_val = weight_arr.at(x_table_idx);
            complex_float y_table_val = weight_arr.at(y_table_idx);

            // Calculate 2D table value (product of X and Y values)
            complex_float combined_weight_float = complex_fmul(x_table_val, y_table_val);

            // Get the conjugate of the table value (invert sign of I1) - required for gridding (not regridding)
            combined_weight_float.imag = combined_weight_float.imag * (-1.0);

            // Extract the grid point being processed
            complex_float input_point_data = current_sample.val;


            //////////////////////////////////////////////
            //                                          //
            //               Interpolation              //
            //                                          //
            //////////////////////////////////////////////
            // Calculate interpolation result; conjugate of interp table value incorporated into this calculation directly by flipping sign for I1
            complex_float interp_result = complex_fmul(combined_weight_float, input_point_data);


            //////////////////////////////////////////////
            //                                          //
            //               Accumulation               //
            //                                          //
            //////////////////////////////////////////////
            uniform_data_arr.at(tidx).at(tidy).at(tile_idx) = complex_fadd(uniform_data_arr.at(tidx).at(tidy).at(tile_idx), interp_result);
        }
    }

    pthread_mutex_lock(&ThreadLock); /* Get the thread lock */
    numThreads--;
    pthread_mutex_unlock(&ThreadLock); /* Release the lock */

    return nullptr;
}


//////////////////////////////////////////////
//                                          //
//                FFT2/IFFT2                //
//                                          //
//////////////////////////////////////////////
void fft2_ifft2(bool nufft_type_adj) {
    // Do an FFT/IFFT of each "col" (cols are stacked)
    int step_val = 1;
    int end_val = fft_len/ACCEL_DIM;
    int offset = fft_len/2/ACCEL_DIM;
    int x_comb_pipe_offset = 0;
    int y_comb_pipe_offset = 0;
    uint w_mask = 0;
    int bit_shift_offset = log2(MEM_DIM)-log2(fft_len);
    int num_stages = log2(fft_len);

    for(int stage_idx = 0; stage_idx < num_stages-3; ++stage_idx) { // stages of transformation
        // Loop over all columns before going to next stage
        for(int set_idx = 0; set_idx < (fft_len/ACCEL_DIM)*MEM_TILE_DIM; set_idx += MEM_TILE_DIM) {
            int iter1 = offset;
            int iter2 = 0;
            int saved_val = iter1;

            while(iter1 < end_val) {
                int tile_idx_1 = iter1+set_idx;
                int tile_idx_2 = iter2+set_idx;

                if(iter2+step_val == saved_val) {
                    iter1 = iter1+step_val+offset;
                    iter2 = iter2+step_val+offset;
                    saved_val = iter1;
                } else {
                    iter1 = iter1+step_val;
                    iter2 = iter2+step_val;
                }

                // Loop over accelerator pipelines to process input point
                for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; ++x_accel_idx) {
                    for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; ++y_accel_idx) {
                        // tile_idx_2[6:(7-stage)] for stage > 0; else 0 (6:7 is zero bits)
                        int w_addr = (tile_idx_2 & w_mask) >> (7-(stage_idx+bit_shift_offset));

                        complex_butterfly(nufft_type_adj, x_accel_idx, y_accel_idx, tile_idx_1, tile_idx_2, w_addr, x_comb_pipe_offset, y_comb_pipe_offset);
                    }
                }
            }
        }
        offset = (int)(offset / 2);
        w_mask |= 1UL << max(7-(stage_idx+bit_shift_offset)-1, 0);
    }
    x_comb_pipe_offset = 4;
    offset = 0;
    for(int stage_idx = num_stages-3; stage_idx < num_stages; ++stage_idx) { // stages of transformation
        // Loop over all columns before going to next stage
        for(int set_idx = 0; set_idx < (fft_len/ACCEL_DIM)*MEM_TILE_DIM; set_idx += MEM_TILE_DIM) {
            int iter1 = offset;
            int iter2 = 0;
            int saved_val = iter1;

            while(iter1 < end_val) {
                int tile_idx_1 = iter1+set_idx;
                int tile_idx_2 = iter2+set_idx;

                if(iter2+step_val == saved_val) {
                    iter1 = iter1+step_val+offset;
                    iter2 = iter2+step_val+offset;
                    saved_val = iter1;
                } else {
                    iter1 = iter1+step_val;
                    iter2 = iter2+step_val;
                }

                // Loop over accelerator pipelines to process input point
                for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; ++x_accel_idx) {
                    for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; ++y_accel_idx) {
                        if(((x_accel_idx >> ((num_stages-1)-stage_idx)) & 1U) == 1) { // num_stages instead of num_stages-1 because the first index is 1 in Matlab, not 0
                            // Stage 7: tile_idx_2[6:(7-stage)]. Stage 8: Concatenate tile_idx_2[6:0] and x_accel_idx[2]. Stage 9: Concatenate tile_idx_2[6:0] and x_accel_idx[2:1]
                            int w_addr = (tile_idx_2 & w_mask) << ((stage_idx+bit_shift_offset)-7); // tile_idx_2[6:0] shifted to the left (for stage > 7)
                            int append_bits = (x_accel_idx & 6) >> x_comb_pipe_offset; // x_accel_idx[2:1] shifted to the right
                            w_addr = (w_addr | append_bits); // [tile_idx_2[6:0] x_accel_idx[2:1]]

                            complex_butterfly(nufft_type_adj, x_accel_idx, y_accel_idx, tile_idx_1, tile_idx_2, w_addr, x_comb_pipe_offset, y_comb_pipe_offset);
                        }
                    }
                }
            }
        }
        x_comb_pipe_offset = (int)(x_comb_pipe_offset / 2);
        w_mask |= 1UL << max(7-(stage_idx+bit_shift_offset)-1, 0);
    }

    // Do an FFT/IFFT of each "row" (rows are stacked)
    step_val = 128;
    end_val = fft_len*2*ACCEL_DIM;
    offset = fft_len*ACCEL_DIM;
    y_comb_pipe_offset = 0;
    w_mask = 0;
    bit_shift_offset = log2(MEM_DIM)-log2(fft_len);
    num_stages = log2(fft_len);

    for(int stage_idx = 0; stage_idx < num_stages-3; ++stage_idx) { // stages of transformation
        // Loop over all columns before going to next stage
        for(int set_idx = 0; set_idx < fft_len/ACCEL_DIM; ++set_idx) {
            int iter1 = offset;
            int iter2 = 0;
            int saved_val = iter1;

            while(iter1 < end_val) {
                int tile_idx_1 = iter1+set_idx;
                int tile_idx_2 = iter2+set_idx;

                if(iter2+step_val == saved_val) {
                    iter1 = iter1+step_val+offset;
                    iter2 = iter2+step_val+offset;
                    saved_val = iter1;
                } else {
                    iter1 = iter1+step_val;
                    iter2 = iter2+step_val;
                }

                // Loop over accelerator pipelines to process input point
                for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; ++x_accel_idx) {
                    for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; ++y_accel_idx) {
                        // tile_idx_2[13:(14-stage)] for stage > 0; else 0 (13:14 is zero bits)
                        int w_addr = (tile_idx_2 & w_mask) >> (14-(stage_idx+bit_shift_offset));

                        complex_butterfly(nufft_type_adj, x_accel_idx, y_accel_idx, tile_idx_1, tile_idx_2, w_addr, x_comb_pipe_offset, y_comb_pipe_offset);
                    }
                }
            }
        }
        offset = (int)(offset / 2);
        w_mask |= 1UL << max(14-(stage_idx+bit_shift_offset)-1, 0);
    }
    y_comb_pipe_offset = 4;
    offset = 0;
    for(int stage_idx = num_stages-3; stage_idx < num_stages; ++stage_idx) { // stages of transformation
        // Loop over all columns before going to next stage
        for(int set_idx = 0; set_idx < fft_len/ACCEL_DIM; ++set_idx) {
            int iter1 = offset;
            int iter2 = 0;
            int saved_val = iter1;

            while(iter1 < end_val) {
                int tile_idx_1 = iter1+set_idx;
                int tile_idx_2 = iter2+set_idx;

                if(iter2+step_val == saved_val) {
                    iter1 = iter1+step_val+offset;
                    iter2 = iter2+step_val+offset;
                    saved_val = iter1;
                } else {
                    iter1 = iter1+step_val;
                    iter2 = iter2+step_val;
                }

                // Loop over accelerator pipelines to process input point
                for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; ++x_accel_idx) {
                    for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; ++y_accel_idx) {
                        if(((y_accel_idx >> ((num_stages-1)-stage_idx)) & 1U) == 1) { // num_stages instead of num_stages-1 because the first index is 1 in Matlab, not 0
                            // Stage 7: tile_idx_2[13:(14-stage)]. Stage 8: Concatenate tile_idx_2[13:7] and y_accel_idx[2]. Stage 9: Concatenate tile_idx_2[13:7] and y_accel_idx[2:1]
                            int w_addr = ((tile_idx_2 & w_mask) >> max(14-(stage_idx+bit_shift_offset), 7)) << ((stage_idx+bit_shift_offset)-7); // tile_idx_2[13:7] shifted to the left (for stage > 7)
                            int append_bits = ((y_accel_idx & 6) >> y_comb_pipe_offset); // y_accel_idx[2:1] shifted to the right
                            w_addr = (w_addr | append_bits); // [tile_idx_2[13:7] y_accel_idx[2:1]]

                            complex_butterfly(nufft_type_adj, x_accel_idx, y_accel_idx, tile_idx_1, tile_idx_2, w_addr, x_comb_pipe_offset, y_comb_pipe_offset);
                        }
                    }
                }
            }
        }
        y_comb_pipe_offset = (int)(y_comb_pipe_offset / 2);
        w_mask |= 1UL << max(14-(stage_idx+bit_shift_offset)-1, 0);
    }
}


//////////////////////////////////////////////
//                                          //
//                Regridding                //
//                                          //
//////////////////////////////////////////////
void *regridding(void *pointarg) {
    // Calculate the global thread indexes in each direction (relative indexes)
    int threadIdx_x = ((struct PointArgs*)pointarg)->tidx;
    int threadIdx_y = ((struct PointArgs*)pointarg)->tidy;
    int tid = threadIdx_y * ACCEL_DIM + threadIdx_x;

    bool nufft_type_adj = ((struct PointArgs*)pointarg)->nufft_type_adj;

    int num_points = ceil(num_nonuniform_data_real_doubles / NUM_THREADS);
    int start = tid * num_points;
    int end = start + num_points; //start + num_points;
    if(end > num_nonuniform_data_real_doubles)
        end = num_nonuniform_data_real_doubles;

    // printf("Hello from pipeline (%d, %d); start=%d, end=%d; tid=%d, num_points=%d\n", threadIdx_x, threadIdx_y, start, end, tid, num_points);

    #pragma vector aligned
    for(int input_point_idx = start; input_point_idx < end; ++input_point_idx) {
        for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; ++x_accel_idx) {
            for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; ++y_accel_idx) {
                //////////////////////////////////////////////
                //                                          //
                //                  Select                  //
                //                                          //
                //////////////////////////////////////////////
                // Extract the current point being processed
                input_sample current_sample = nonuniform_data_arr.at(input_point_idx);

                // Get the quotient and remainder to determine grid and tile location
                int x_base_tile_idx = current_sample.x_coord/ACCEL_DIM; // x virtual tile index
                int y_base_tile_idx = current_sample.y_coord/ACCEL_DIM; // y virtual tile index

                // Extract the "relative" coordinates (within a tile)
                double x_coord = fmod(current_sample.x_coord, (double)ACCEL_DIM);
                double y_coord = fmod(current_sample.y_coord, (double)ACCEL_DIM);

                // Determine if this pipeline is affected by this source point
                double x_distance = fmod(ACCEL_DIM+x_coord-x_accel_idx, (double)ACCEL_DIM); // rem is implemented as an upper truncation
                double y_distance = fmod(ACCEL_DIM+y_coord-y_accel_idx, (double)ACCEL_DIM); // rem is implemented as an upper truncation

                // Check if the point lies within the interpolation window
                if(x_distance < INTERP_WIN_DIM && y_distance < INTERP_WIN_DIM) {
                    // This point affects this pipeline!
                    // Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
                    int x_tile_idx;
                    if(x_coord < x_accel_idx) {
                       // A wrap occured!
                        if(x_base_tile_idx == 0) {
                            x_tile_idx = tile_dim - 1;
                        } else {
                            x_tile_idx = x_base_tile_idx - 1;
                        }
                    } else {
                        x_tile_idx = x_base_tile_idx;
                    }

                    // Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
                    int y_tile_idx;
                    if(y_coord < y_accel_idx) {
                       // A wrap occured!
                        if(y_base_tile_idx == 0) {
                            y_tile_idx = tile_dim - 1;
                        } else {
                            y_tile_idx = y_base_tile_idx - 1;
                        }
                    } else {
                        y_tile_idx = y_base_tile_idx;
                    }

                    // Calculate the tile index ("depth" in the pipeline's data array)
                    int tile_idx = y_tile_idx * MEM_TILE_DIM + x_tile_idx; // 1D global virtual tile index

                    // Calculate the table index
                    int x_table_idx = ((x_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift
                    int y_table_idx = ((y_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift


                    //////////////////////////////////////////////
                    //                                          //
                    //               Interp Table               //
                    //                                          //
                    //////////////////////////////////////////////
                    // Extract the interp table values
                    complex_float x_table_val = weight_arr.at(x_table_idx);
                    complex_float y_table_val = weight_arr.at(y_table_idx);

                    // Calculate 2D table value (product of X and Y values)
                    complex_float combined_weight_float = complex_fmul(x_table_val, y_table_val);

                    // Extract the grid point being processed
                    complex_float input_point_data = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx);


                    //////////////////////////////////////////////
                    //                                          //
                    //               Interpolation              //
                    //                                          //
                    //////////////////////////////////////////////
                    // Calculate interpolation result; conjugate of interp table value incorporated into this calculation directly by flipping sign for I1
                    complex_float interp_result = complex_fmul(combined_weight_float, input_point_data);


                    //////////////////////////////////////////////
                    //                                          //
                    //               Accumulation               //
                    //                                          //
                    //////////////////////////////////////////////
                    nonuniform_data_arr.at(input_point_idx).val = complex_fadd(nonuniform_data_arr.at(input_point_idx).val, interp_result);
                }
            }
        }
    }

    pthread_mutex_lock(&ThreadLock); /* Get the thread lock */
    numThreads--;
    pthread_mutex_unlock(&ThreadLock); /* Release the lock */

    return nullptr;
}


//////////////////////////////////////////////
//                                          //
//                Regridding_1T             //
//                                          //
//////////////////////////////////////////////
void *regridding_1t(void *pointarg) {
    // Calculate the global thread indexes in each direction (relative indexes)
    bool nufft_type_adj = ((struct PointArgs*)pointarg)->nufft_type_adj;

    int num_points = num_nonuniform_data_real_doubles;
    int start = 0;
    int end = num_points; //start + num_points;

    // printf("Hello from pipeline (%d, %d); start=%d, end=%d; tid=%d, num_points=%d\n", threadIdx_x, threadIdx_y, start, end, tid, num_points);

    #pragma vector aligned
    for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; ++x_accel_idx) {
        for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; ++y_accel_idx) {
            for(int input_point_idx = start; input_point_idx < end; ++input_point_idx) {
                //////////////////////////////////////////////
                //                                          //
                //                  Select                  //
                //                                          //
                //////////////////////////////////////////////
                // Extract the current point being processed
                input_sample current_sample = nonuniform_data_arr.at(input_point_idx);

                // Get the quotient and remainder to determine grid and tile location
                int x_base_tile_idx = current_sample.x_coord/ACCEL_DIM; // x virtual tile index
                int y_base_tile_idx = current_sample.y_coord/ACCEL_DIM; // y virtual tile index

                // Extract the "relative" coordinates (within a tile)
                double x_coord = fmod(current_sample.x_coord, (double)ACCEL_DIM);
                double y_coord = fmod(current_sample.y_coord, (double)ACCEL_DIM);

                // Determine if this pipeline is affected by this source point
                double x_distance = fmod(ACCEL_DIM+x_coord-x_accel_idx, (double)ACCEL_DIM); // rem is implemented as an upper truncation
                double y_distance = fmod(ACCEL_DIM+y_coord-y_accel_idx, (double)ACCEL_DIM); // rem is implemented as an upper truncation

                // Check if the point lies within the interpolation window
                if(x_distance < INTERP_WIN_DIM && y_distance < INTERP_WIN_DIM) {
                    // This point affects this pipeline!
                    // Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
                    int x_tile_idx;
                    if(x_coord < x_accel_idx) {
                       // A wrap occured!
                        if(x_base_tile_idx == 0) {
                            x_tile_idx = tile_dim - 1;
                        } else {
                            x_tile_idx = x_base_tile_idx - 1;
                        }
                    } else {
                        x_tile_idx = x_base_tile_idx;
                    }

                    // Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
                    int y_tile_idx;
                    if(y_coord < y_accel_idx) {
                       // A wrap occured!
                        if(y_base_tile_idx == 0) {
                            y_tile_idx = tile_dim - 1;
                        } else {
                            y_tile_idx = y_base_tile_idx - 1;
                        }
                    } else {
                        y_tile_idx = y_base_tile_idx;
                    }

                    // Calculate the tile index ("depth" in the pipeline's data array)
                    int tile_idx = y_tile_idx * MEM_TILE_DIM + x_tile_idx; // 1D global virtual tile index

                    // Calculate the table index
                    int x_table_idx = ((x_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift
                    int y_table_idx = ((y_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift


                    //////////////////////////////////////////////
                    //                                          //
                    //               Interp Table               //
                    //                                          //
                    //////////////////////////////////////////////
                    // Extract the interp table values
                    complex_float x_table_val = weight_arr.at(x_table_idx);
                    complex_float y_table_val = weight_arr.at(y_table_idx);

                    // Calculate 2D table value (product of X and Y values)
                    complex_float combined_weight_float = complex_fmul(x_table_val, y_table_val);

                    // Extract the grid point being processed
                    complex_float input_point_data = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx);


                    //////////////////////////////////////////////
                    //                                          //
                    //               Interpolation              //
                    //                                          //
                    //////////////////////////////////////////////
                    // Calculate interpolation result; conjugate of interp table value incorporated into this calculation directly by flipping sign for I1
                    complex_float interp_result = complex_fmul(combined_weight_float, input_point_data);


                    //////////////////////////////////////////////
                    //                                          //
                    //               Accumulation               //
                    //                                          //
                    //////////////////////////////////////////////
                    nonuniform_data_arr.at(input_point_idx).val = complex_fadd(nonuniform_data_arr.at(input_point_idx).val, interp_result);
                }
            }
        }
    }

    pthread_mutex_lock(&ThreadLock); /* Get the thread lock */
    numThreads--;
    pthread_mutex_unlock(&ThreadLock); /* Release the lock */

    return nullptr;
}


//////////////////////////////////////////////
//                                          //
//                   Main                   //
//                                          //
//////////////////////////////////////////////
int main(int argc, char *argv[]) {
    // Check if the correct numbe of arguments are present; argc should be 2 for correct execution
    if(argc != 10) {
        // If there are the wrong number of arguments, print usage
        printf("Error: %d arguments given, but expected 9.\n", argc);
        printf("usage: %s operation_type weight_data_real weight_data_imag nonuniform_data_real nonuniform_data_imag nonuniform_x_coord nonuniform_y_coord uniform_data_tiled_real uniform_data_tiled_imag\n", argv[0]);
    } else {
        // If the number of arguments is correct, save operation type and open files
        string operation_type = argv[1];

        ifstream weight_data_real_File(argv[2], ios::in | ios::binary | ios::ate);
        ifstream weight_data_imag_File(argv[3], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_data_real_File(argv[4], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_data_imag_File(argv[5], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_x_coord_File(argv[6], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_y_coord_File(argv[7], ios::in | ios::binary | ios::ate);
        ifstream uniform_data_tiled_real_File(argv[8], ios::in | ios::binary | ios::ate);
        ifstream uniform_data_tiled_imag_File(argv[9], ios::in | ios::binary | ios::ate);

        if(!weight_data_real_File.good() || !weight_data_imag_File.good() || !nonuniform_data_real_File.good() || !nonuniform_data_imag_File.good() || !nonuniform_x_coord_File.good() || !nonuniform_y_coord_File.good() || !uniform_data_tiled_real_File.good() || !uniform_data_tiled_imag_File.good()) {
            // If file fails to open, notify user
            if(!weight_data_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[2]);
            } else {
                weight_data_real_File.close();
            }
            if(!weight_data_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[3]);
            } else {
                weight_data_imag_File.close();
            }
            if(!nonuniform_data_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[4]);
            } else {
                nonuniform_data_real_File.close();
            }
            if(!nonuniform_data_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[5]);
            } else {
                nonuniform_data_imag_File.close();
            }
            if(!nonuniform_x_coord_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[6]);
            } else {
                nonuniform_x_coord_File.close();
            }
            if(!nonuniform_y_coord_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[7]);
            } else {
                nonuniform_y_coord_File.close();
            }
            if(!uniform_data_tiled_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[8]);
            } else {
                uniform_data_tiled_real_File.close();
            }
            if(!uniform_data_tiled_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[9]);
            } else {
                uniform_data_tiled_imag_File.close();
            }
        } else {
            // Get the size of the file in bytes
            streampos weight_data_real = weight_data_real_File.tellg();
            streampos weight_data_imag = weight_data_imag_File.tellg();
            streampos nonuniform_data_real = nonuniform_data_real_File.tellg();
            streampos nonuniform_data_imag = nonuniform_data_imag_File.tellg();
            streampos nonuniform_x_coord = nonuniform_x_coord_File.tellg();
            streampos nonuniform_y_coord = nonuniform_y_coord_File.tellg();
            streampos uniform_data_tiled_real = uniform_data_tiled_real_File.tellg();
            streampos uniform_data_tiled_imag = uniform_data_tiled_imag_File.tellg();


            cout << "sizeof(complex_float) = " << sizeof(complex_float) << endl;
            cout << "output points = " << MEM_DIM * MEM_DIM << endl;
            cout << "threads = " << ACCEL_DIM * ACCEL_DIM << endl;
            cout << "points per thread = " << NUM_POINTS_PER_THREAD << endl;


            int num_weight_data_real_bytes = (int)weight_data_real;
            // cout << "num_weight_data_real_bytes=" << num_weight_data_real_bytes << endl;
            num_weight_data_real_doubles = (int)(weight_data_real / sizeof(double));
            cout << "num_weight_data_real_doubles=" << num_weight_data_real_doubles << endl;

            int num_weight_data_imag_bytes = (int)weight_data_imag;
            // cout << "num_weight_data_imag_bytes=" << num_weight_data_imag_bytes << endl;
            num_weight_data_imag_doubles = (int)(weight_data_imag / sizeof(double));
            cout << "num_weight_data_imag_doubles=" << num_weight_data_imag_doubles << endl;

            int num_nonuniform_data_real_bytes = (int)nonuniform_data_real;
            // cout << "num_nonuniform_data_real_bytes=" << num_nonuniform_data_real_bytes << endl;
            num_nonuniform_data_real_doubles = (int)(nonuniform_data_real / sizeof(double));
            cout << "num_nonuniform_data_real_doubles=" << num_nonuniform_data_real_doubles << endl;

            int num_nonuniform_data_imag_bytes = (int)nonuniform_data_imag;
            // cout << "num_nonuniform_data_imag_bytes=" << num_nonuniform_data_imag_bytes << endl;
            num_nonuniform_data_imag_doubles = (int)(nonuniform_data_imag / sizeof(double));
            cout << "num_nonuniform_data_imag_doubles=" << num_nonuniform_data_imag_doubles << endl;

            int num_nonuniform_x_coord_bytes = (int)nonuniform_x_coord;
            // cout << "num_nonuniform_x_coord_bytes=" << num_nonuniform_x_coord_bytes << endl;
            num_nonuniform_x_coord_doubles = (int)(nonuniform_x_coord / sizeof(double));
            cout << "num_nonuniform_x_coord_doubles=" << num_nonuniform_x_coord_doubles << endl;

            int num_nonuniform_y_coord_bytes = (int)nonuniform_y_coord;
            // cout << "num_nonuniform_y_coord_bytes=" << num_nonuniform_y_coord_bytes << endl;
            num_nonuniform_y_coord_doubles = (int)(nonuniform_y_coord / sizeof(double));
            cout << "num_nonuniform_y_coord_doubles=" << num_nonuniform_y_coord_doubles << endl;

            int num_uniform_data_tiled_real_bytes = (int)uniform_data_tiled_real;
            // cout << "num_uniform_data_tiled_real_bytes=" << num_uniform_data_tiled_real_bytes << endl;
            num_uniform_data_tiled_real_doubles = (int)(uniform_data_tiled_real / sizeof(double));
            cout << "num_uniform_data_tiled_real_doubles=" << num_uniform_data_tiled_real_doubles << endl;

            int num_uniform_data_tiled_imag_bytes = (int)uniform_data_tiled_imag;
            // cout << "num_uniform_data_tiled_imag_bytes=" << num_uniform_data_tiled_imag_bytes << endl;
            num_uniform_data_tiled_imag_doubles = (int)(uniform_data_tiled_imag / sizeof(double));
            cout << "num_uniform_data_tiled_imag_doubles=" << num_uniform_data_tiled_imag_doubles << endl;


            // Declare pointers for input data arrays
            double *weight_data_real_arr;
            double *weight_data_imag_arr;
            double *nonuniform_data_real_arr;
            double *nonuniform_data_imag_arr;
            double *nonuniform_x_coord_arr;
            double *nonuniform_y_coord_arr;
            double *uniform_data_tiled_real_arr;
            double *uniform_data_tiled_imag_arr;


            // Allocate space for the input data arrays
            weight_data_real_arr = (double*)_mm_malloc(num_weight_data_real_doubles * sizeof(double), 64);
            if(weight_data_real_arr == NULL) fprintf(stderr, "Bad malloc on weight_data_real_arr\n");
            weight_data_imag_arr = (double*)_mm_malloc(num_weight_data_imag_doubles * sizeof(double), 64);
            if(weight_data_imag_arr == NULL) fprintf(stderr, "Bad malloc on weight_data_imag_arr\n");

            nonuniform_data_real_arr = (double*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(double), 64);
            if(nonuniform_data_real_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_data_real_arr\n");
            nonuniform_data_imag_arr = (double*)_mm_malloc(num_nonuniform_data_imag_doubles * sizeof(double), 64);
            if(nonuniform_data_imag_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_data_imag_arr\n");

            nonuniform_x_coord_arr = (double*)_mm_malloc(num_nonuniform_x_coord_doubles * sizeof(double), 64);
            if(nonuniform_x_coord_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_x_coord_arr\n");
            nonuniform_y_coord_arr = (double*)_mm_malloc(num_nonuniform_y_coord_doubles * sizeof(double), 64);
            if(nonuniform_y_coord_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_y_coord_arr\n");

            uniform_data_tiled_real_arr = (double*)_mm_malloc(num_uniform_data_tiled_real_doubles * sizeof(double), 64);
            if(uniform_data_tiled_real_arr == NULL) fprintf(stderr, "Bad malloc on uniform_data_tiled_real_arr\n");
            uniform_data_tiled_imag_arr = (double*)_mm_malloc(num_uniform_data_tiled_imag_doubles * sizeof(double), 64);
            if(uniform_data_tiled_imag_arr == NULL) fprintf(stderr, "Bad malloc on uniform_data_tiled_imag_arr\n");


            // Stream the file into the input data arrays
            weight_data_real_File.seekg(0, ios::beg);
            weight_data_real_File.read((char*)weight_data_real_arr, num_weight_data_real_bytes);
            weight_data_real_File.close();

            weight_data_imag_File.seekg(0, ios::beg);
            weight_data_imag_File.read((char*)weight_data_imag_arr, num_weight_data_imag_bytes);
            weight_data_imag_File.close();

            nonuniform_data_real_File.seekg(0, ios::beg);
            nonuniform_data_real_File.read((char*)nonuniform_data_real_arr, num_nonuniform_data_real_bytes);
            nonuniform_data_real_File.close();

            nonuniform_data_imag_File.seekg(0, ios::beg);
            nonuniform_data_imag_File.read((char*)nonuniform_data_imag_arr, num_nonuniform_data_imag_bytes);
            nonuniform_data_imag_File.close();

            nonuniform_x_coord_File.seekg(0, ios::beg);
            nonuniform_x_coord_File.read((char*)nonuniform_x_coord_arr, num_nonuniform_x_coord_bytes);
            nonuniform_x_coord_File.close();

            nonuniform_y_coord_File.seekg(0, ios::beg);
            nonuniform_y_coord_File.read((char*)nonuniform_y_coord_arr, num_nonuniform_y_coord_bytes);
            nonuniform_y_coord_File.close();

            uniform_data_tiled_real_File.seekg(0, ios::beg);
            uniform_data_tiled_real_File.read((char*)uniform_data_tiled_real_arr, num_uniform_data_tiled_real_bytes);
            uniform_data_tiled_real_File.close();

            uniform_data_tiled_imag_File.seekg(0, ios::beg);
            uniform_data_tiled_imag_File.read((char*)uniform_data_tiled_imag_arr, num_uniform_data_tiled_imag_bytes);
            uniform_data_tiled_imag_File.close();


            for(int i = 0; i < num_weight_data_real_doubles; ++i) {
                complex_float tmp;
                double val1, val2;
                val1 = static_cast<double>(weight_data_real_arr[i]);
                val2 = static_cast<double>(weight_data_imag_arr[i]);
                tmp.real = (float)val1;
                tmp.imag = (float)val2;

                weight_arr.push_back(tmp);
            }

            for(int i = 0; i < num_nonuniform_data_real_doubles; ++i) {
                input_sample tmp;
                double val1, val2, x_coord, y_coord;
                val1 = static_cast<double>(nonuniform_data_real_arr[i]);
                val2 = static_cast<double>(nonuniform_data_imag_arr[i]);
                x_coord = static_cast<double>(nonuniform_x_coord_arr[i]);
                y_coord = static_cast<double>(nonuniform_y_coord_arr[i]);
                tmp.val.real = (float)val1;
                tmp.val.imag = (float)val2;
                tmp.x_coord = (float)x_coord;
                tmp.y_coord = (float)y_coord;

                nonuniform_data_arr.push_back(tmp);
            }

            int idx = 0;
            for(int x = 0; x < ACCEL_DIM; ++x) {
                uniform_data_arr.push_back(vector<vector<complex_float>>{ACCEL_DIM});
                for(int y = 0; y < ACCEL_DIM; ++y) {
                    uniform_data_arr.at(x).push_back(vector<complex_float>{ACCEL_DIM});
                    for(int z = 0; z < (num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM); ++z) {
                        complex_float tmp;
                        double val1, val2;
                        val1 = static_cast<double>(uniform_data_tiled_real_arr[idx]);
                        val2 = static_cast<double>(uniform_data_tiled_imag_arr[idx]);
                        tmp.real = (float)val1;
                        tmp.imag = (float)val2;

                        uniform_data_arr.at(x).at(y).push_back(tmp);

                        idx++;
                    }
                }
            }


            /* Thread Variables */
            int ret; // Return result for pthread functions
            pthread_t* threads = (pthread_t *)_mm_malloc(NUM_THREADS * sizeof(pthread_t), 64); // Allocate threads
            assert(threads != NULL); // Check that the malloc was successful
            ret = pthread_mutex_init(&ThreadLock, NULL); // Initialize the mutex
            assert(ret == 0); // Check to see that the initialization was successful

            PointArgs *threadIdx;


            if(operation_type == "fft2") {
                nufft_type_adj = false;
                grid_dim = 512;
                tile_dim = (grid_dim/ACCEL_DIM);
                fft_len = pow(2, ceil(log2(grid_dim)));

                /* --------------------------------------------------------------------- */

                cout << "***Beginning FFT2 Computation***" << endl;

                // Start recording the elapsed time
                auto start = chrono::steady_clock::now();

                /* ---------------------------- COMPUTATION ---------------------------- */

                fft2_ifft2(nufft_type_adj);

                /* --------------------------------------------------------------------- */

                // Stop recording the elapsed time
                auto end = chrono::steady_clock::now();
                cout << "FFT2 Compute Time (milliseconds): " << (double)chrono::duration_cast<chrono::microseconds>(end-start).count()/1000 << endl;

                /* ----------------------------- DATA DUMP ----------------------------- */

                // Print output
                // write_data_file("fft2");
                write_mat_files();

                /* --------------------------------------------------------------------- */
            } else if(operation_type == "regridding") {
                nufft_type_adj = false;
                grid_dim = 512;
                tile_dim = (grid_dim/ACCEL_DIM);
                fft_len = pow(2, ceil(log2(grid_dim)));

                /* --------------------------------------------------------------------- */

                cout << "***Beginning Regridding Computation***" << endl;

                // Start recording the elapsed time
                auto start = chrono::steady_clock::now();

                /* ---------------------------- COMPUTATION ---------------------------- */

                numThreads = NUM_THREADS;

                // // DEBUG
                // numThreads = 1;
                // for(int x = 0; x < 1; ++x) {
                //     for(int y = 0; y < 1; ++y) {
                //         threadIdx = (PointArgs*)malloc(sizeof(PointArgs));
                //         threadIdx->tidx = x;
                //         threadIdx->tidy = y;
                //         threadIdx->nufft_type_adj = nufft_type_adj;
                //         ret = pthread_create(&threads[x*y+y], NULL, regridding_1t, (void*)threadIdx);
                //         assert(ret == 0);
                //     }
                // }
                // // DEBUG

                // Run kernel to interpolate values
                #pragma vector aligned
                for(int x = 0; x < ACCEL_DIM; ++x) {
                    for(int y = 0; y < ACCEL_DIM; ++y) {
                        threadIdx = (PointArgs*)malloc(sizeof(PointArgs));
                        threadIdx->tidx = x;
                        threadIdx->tidy = y;
                        threadIdx->nufft_type_adj = nufft_type_adj;
                        ret = pthread_create(&threads[x*y+y], NULL, regridding, (void*)threadIdx);
                        assert(ret == 0);
                    }
                }

                // Wait for threads to complete
                while(numThreads > 0);

                /* --------------------------------------------------------------------- */

                // Stop recording the elapsed time
                auto end = chrono::steady_clock::now();
                cout << "Regridding Compute Time (milliseconds): " << (double)chrono::duration_cast<chrono::microseconds>(end-start).count()/1000 << endl;

                /* ----------------------------- DATA DUMP ----------------------------- */

                // Print output
                // write_data_file("regridding");
                write_mat_files();

                /* --------------------------------------------------------------------- */
            } else if(operation_type == "gridding") {
                nufft_type_adj = true;
                grid_dim = 768;
                tile_dim = (grid_dim/ACCEL_DIM);
                fft_len = pow(2, ceil(log2(grid_dim)));

                /* --------------------------------------------------------------------- */
                cout << "***Beginning Gridding Computation***" << endl;

                // Start recording the elapsed time
                auto start = chrono::steady_clock::now();

                /* ---------------------------- COMPUTATION ---------------------------- */

                numThreads = NUM_THREADS;

                // Run kernel to interpolate values
                #pragma vector aligned
                for(int x = 0; x < ACCEL_DIM; ++x) {
                    for(int y = 0; y < ACCEL_DIM; ++y) {
                        threadIdx = (PointArgs*)malloc(sizeof(PointArgs));
                        threadIdx->tidx = x;
                        threadIdx->tidy = y;
                        threadIdx->nufft_type_adj = nufft_type_adj;
                        ret = pthread_create(&threads[x*y+y], NULL, gridding, (void*)threadIdx);
                        assert(ret == 0);
                    }
                }

                // Wait for threads to complete
                while(numThreads > 0);

                /* --------------------------------------------------------------------- */

                // Stop recording the elapsed time
                auto end = chrono::steady_clock::now();
                cout << "Gridding Compute Time (milliseconds): " << (double)chrono::duration_cast<chrono::microseconds>(end-start).count()/1000 << endl;

                /* ----------------------------- DATA DUMP ----------------------------- */

                // Print output
                // write_data_file("gridding");
                write_mat_files();

                /* --------------------------------------------------------------------- */
            } else if(operation_type == "ifft2") {
                nufft_type_adj = true;
                grid_dim = 768;
                tile_dim = (grid_dim/ACCEL_DIM);
                fft_len = pow(2, ceil(log2(grid_dim)));

                /* --------------------------------------------------------------------- */

                cout << "***Beginning IFFT2 Computation***" << endl;

                // Start recording the elapsed time
                auto start = chrono::steady_clock::now();

                /* ---------------------------- COMPUTATION ---------------------------- */

                fft2_ifft2(nufft_type_adj);

                /* --------------------------------------------------------------------- */

                // Stop recording the elapsed time
                auto end = chrono::steady_clock::now();
                cout << "IFFT2 Compute Time (milliseconds): " << (double)chrono::duration_cast<chrono::microseconds>(end-start).count()/1000 << endl;

                /* ----------------------------- DATA DUMP ----------------------------- */

                // Print output
                // write_data_file("ifft2");
                write_mat_files();

                /* --------------------------------------------------------------------- */
            } else {
                cout << "Invalid Operation Type" << endl;
            }


            // Free up allocated memory
            pthread_mutex_destroy(&ThreadLock);
            _mm_free(weight_data_real_arr);
            _mm_free(weight_data_imag_arr);
            _mm_free(nonuniform_data_real_arr);
            _mm_free(nonuniform_data_imag_arr);
            _mm_free(nonuniform_x_coord_arr);
            _mm_free(nonuniform_y_coord_arr);
            _mm_free(uniform_data_tiled_real_arr);
            _mm_free(uniform_data_tiled_imag_arr);
            usleep(50000);

            cout << operation_type << " complete!" << endl;
        }
    }
}
