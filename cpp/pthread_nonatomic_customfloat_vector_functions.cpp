#include <stdio.h>
#include <math.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <chrono>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>
#include <unistd.h>
#include <mm_malloc.h>
#include <mat.h>
#include <cstdio>
#include <vector>
#include <bitset>

#define N 32
#define E 8
#define S 1
#define MEM_DIM 1024
#define TABLE_VALUES_OVERSAMP_FACTOR 32
#define ACCEL_DIM 8
#define MEM_TILE_DIM (MEM_DIM/ACCEL_DIM)
#define INTERP_WIN_DIM 6
#define NUM_POINTS_PER_THREAD (MEM_DIM*MEM_DIM/(ACCEL_DIM*ACCEL_DIM))
#define NUM_INPUTS_PER_THREAD num_nonuniform_data_real_doubles


#define NUM_THREADS (ACCEL_DIM*ACCEL_DIM) // Number of threads

// #define WRITE_HEX 1

using namespace std;

bool nufft_type_adj; // true for adj, false for forw

typedef union {
    float f;
    struct
    {
        // Order is important.
        // Here the members of the union data structure
        // use the same memory (32 bits).
        // The ordering is taken
        // from the LSB to the MSB.
        unsigned int mantissa : (N-E-S);
        unsigned int exponent : E;
        unsigned int sign : S;
    } raw;
} CustomFloat;

struct complex_double {
    double real;
    double imag;
};

struct complex_customfloat {
    CustomFloat real;
    CustomFloat imag;
};

struct complex_customfloat_table {
    CustomFloat real;
    CustomFloat imag;
};

struct input_sample {
    complex_customfloat val;
    double x_coord;
    double y_coord;
};

struct PointArgs {
    bool nufft_type_adj;
    uint tidx;
    uint tidy;
};


CustomFloat fmul(CustomFloat a, CustomFloat b) {
    CustomFloat result;

    if((a.raw.exponent == 0 && a.raw.mantissa == 0) || (b.raw.exponent == 0 && b.raw.mantissa == 0)) {
        result.raw.sign = 0;
        result.raw.exponent = 0;
        result.raw.mantissa = 0;
    } else {
        result.raw.sign = a.raw.sign ^ b.raw.sign;

        result.raw.exponent = a.raw.exponent + b.raw.exponent - 0x7F;

        u_int64_t aMant = (u_int64_t)(a.raw.mantissa | 0b0100000000000000000000000);
        u_int64_t bMant = (u_int64_t)(b.raw.mantissa | 0b0100000000000000000000000);
        u_int64_t cMantissaPreNorm = ((u_int64_t)a.raw.mantissa | 0b0100000000000000000000000) * ((u_int64_t)b.raw.mantissa | 0b0100000000000000000000000); // add implicit 1's and multiply

        if((cMantissaPreNorm & 0b0100000000000000000000000000000000000000000000000) > 0) {
            result.raw.exponent = a.raw.exponent + b.raw.exponent - 0x7F + 1;
            result.raw.mantissa = (cMantissaPreNorm >> 24) & 0b00000000011111111111111111111111;
        } else {
            result.raw.mantissa = (cMantissaPreNorm >> 23) & 0b00000000011111111111111111111111;
        }
    }

    // result.f = a.f * b.f;

    return result;
}


CustomFloat fadd(CustomFloat a, CustomFloat b) {
    CustomFloat larger, smaller, result;
    u_int64_t tmpResult;
    bool zero = false;

    if((a.raw.exponent == 0 && a.raw.mantissa == 0) || (b.raw.exponent == 0 && b.raw.mantissa == 0)) {
        zero = true;
    }

    // Make sure the larger [absolute] value is the first operand
    larger = a;
    smaller = b;
    if(b.raw.exponent > a.raw.exponent || (a.raw.exponent == b.raw.exponent && b.raw.mantissa > a.raw.mantissa)) {
        larger = b;
        smaller = a;
    }

    uint expDiff = larger.raw.exponent - smaller.raw.exponent;

    u_int64_t larger_mant = ((u_int64_t)larger.raw.mantissa | 0x00800000);
    u_int64_t smaller_mant = ((u_int64_t)smaller.raw.mantissa | 0x00800000) >> expDiff;

    // If the signs are the same, add, else subtract
    if(zero) {
        tmpResult = ((u_int64_t)larger.raw.mantissa | 0x00800000);
    } else if(larger.raw.sign == smaller.raw.sign) {
        tmpResult = ((u_int64_t)larger.raw.mantissa | 0x00800000) + (((u_int64_t)smaller.raw.mantissa | 0x00800000) >> expDiff);
    } else {
        tmpResult = ((u_int64_t)larger.raw.mantissa | 0x00800000) - (((u_int64_t)smaller.raw.mantissa | 0x00800000) >> expDiff);
    }

    result.raw.sign = 0;
    result.raw.exponent = 0;
    result.raw.mantissa = 0;
    if(tmpResult != 0)
        result.raw.sign = larger.raw.sign;

    if(0x01000000 & tmpResult) {
        // If bit 25 is set, take bits 24:1 and increment the exponent by 1
        result.raw.exponent = larger.raw.exponent + 1;
        result.raw.mantissa = (tmpResult >> 1) & 0x007FFFFF;
    } else {
        // Count the number of shifts required to find the first 1 in the remaining 24 bits (23:0, MSB to LSB)
        uint mask = 0x00800000;
        for(int i = 0; i < (32-8-1); ++i) {
            if(((mask & tmpResult) > 0)) {
                // Check for underflow
                if(i > larger.raw.exponent) {
                    result.raw.exponent = 1;
                    result.raw.mantissa = 0;
                } else {
                    result.raw.sign = larger.raw.sign;
                    result.raw.exponent = larger.raw.exponent - i;

                    // shift the result
                    result.raw.mantissa = (tmpResult << i) & 0x007FFFFF;
                }

                break;
            }
            mask = mask >> 1;
        }
    }

    // result.f = a.f + b.f;

    return result;
}


CustomFloat fsub(CustomFloat a, CustomFloat b) {
    CustomFloat result;

    b.raw.sign = b.raw.sign ^ 0b1;
    result = fadd(a, b);

    // result.f = a.f - b.f;

    return result;
}

pthread_mutex_t ThreadLock; /* mutex */
volatile int numThreads;

// Global Variables
int numThreadLoops; // Number of threads for each calculation

// Declare pointers for input data arrays
vector<input_sample> nonuniform_data_arr;
vector<complex_customfloat_table> weight_arr;

// Declare pointers for output data arrays
vector<vector<vector<complex_customfloat>>> uniform_data_arr;

// Declare variables for storing the number of elements per file
int num_weight_data_real_doubles;
int num_weight_data_imag_doubles;
int num_nonuniform_data_real_doubles;
int num_nonuniform_data_imag_doubles;
int num_nonuniform_x_coord_doubles;
int num_nonuniform_y_coord_doubles;
int num_uniform_data_tiled_real_doubles;
int num_uniform_data_tiled_imag_doubles;

// Declare variables for "constants" that require modification based on operation type
int grid_dim;
int tile_dim;
int fft_len;


void write_data_file(string operation_type) {
    string filename = "cpu_" + operation_type + "_customfloats.data";
    FILE *output_file = fopen(filename.c_str(), "w");

    if(operation_type == "regridding") {
        for(int idx = 0; idx < num_nonuniform_data_real_doubles; ++idx) {
            if(nonuniform_data_arr.at(idx).val.real.f == 0)
                fprintf(output_file, "%.14f", nonuniform_data_arr.at(idx).val.real.f);
            else
                fprintf(output_file, "%.14e", nonuniform_data_arr.at(idx).val.real.f);
            if(nonuniform_data_arr.at(idx).val.imag.f == 0)
                fprintf(output_file, " + %.14fi\n", nonuniform_data_arr.at(idx).val.imag.f);
            else if(nonuniform_data_arr.at(idx).val.imag.f > 0)
                fprintf(output_file, " + %.14ei\n", nonuniform_data_arr.at(idx).val.imag.f);
            else
                fprintf(output_file, " - %.14ei\n", abs(nonuniform_data_arr.at(idx).val.imag.f));
            // if((nonuniform_data_arr.at(idx).val.real.f != 0 || nonuniform_data_arr.at(idx).val.imag.f != 0) && z<100) {
            //     // fwrite(&nonuniform_data_arr.at(idx).val, sizeof(complex_double), 1, output_file);
            //     printf("[%d][%d][%d], real=%.14e, imag=%.14e\n", x, y, z, nonuniform_data_arr.at(idx).val.real.f, nonuniform_data_arr.at(idx).val.imag.f);
            //     // fprintf(output_file, "[%d][%d][%d], real=%.14e, imag=%.14e\n", x, y, z, nonuniform_data_arr.at(idx).val.real, nonuniform_data_arr.at(idx).val.imag); // this is so the value "0" doesn't get represented as "0.00000000000000e-000"
            // }
        }
    } else {
        int CHECK_X = 0;
        int CHECK_Y = 0;
        for(int x = 0; x < ACCEL_DIM; ++x) {
            for(int y = 0; y < ACCEL_DIM; ++y) {
                for(int z = 0; z < NUM_POINTS_PER_THREAD; ++z) {
                    int tid = x*ACCEL_DIM*ACCEL_DIM + y * ACCEL_DIM + z;
                    if(x==CHECK_X && y==CHECK_Y) {
                        if(uniform_data_arr.at(x).at(y).at(z).real.f == 0)
                            fprintf(output_file, "%.14f", uniform_data_arr.at(x).at(y).at(z).real.f);
                        else
                            fprintf(output_file, "%.14e", uniform_data_arr.at(x).at(y).at(z).real.f);
                        if(uniform_data_arr.at(x).at(y).at(z).imag.f == 0)
                            fprintf(output_file, " + %.14fi\n", uniform_data_arr.at(x).at(y).at(z).imag.f);
                        else if(uniform_data_arr.at(x).at(y).at(z).imag.f > 0)
                            fprintf(output_file, " + %.14ei\n", uniform_data_arr.at(x).at(y).at(z).imag.f);
                        else
                            fprintf(output_file, " - %.14ei\n", abs(uniform_data_arr.at(x).at(y).at(z).imag.f));
                        // if((uniform_data_arr.at(x).at(y).at(z).real.f != 0 || uniform_data_arr.at(x).at(y).at(z).imag.f != 0) && z<100) {
                        //     // fwrite(&uniform_data_arr.at(x).at(y).at(z), sizeof(complex_double), 1, output_file);
                        //     printf("[%d][%d][%d], real=%.14e, imag=%.14e\n", x, y, z, uniform_data_arr.at(x).at(y).at(z).real.f, uniform_data_arr.at(x).at(y).at(z).imag.f);
                        //     // fprintf(output_file, "[%d][%d][%d], real=%.14e, imag=%.14e\n", x, y, z, uniform_data_arr.at(x).at(y).at(z).real, uniform_data_arr.at(x).at(y).at(z).imag); // this is so the value "0" doesn't get represented as "0.00000000000000e-000"
                        // }
                    }
                }
            }
        }
    }
    fclose(output_file);
}


void write_hex_file(string operation_type, bool isOutputData, int start_x = 0, int end_x = 0, int start_y = 0, int end_y = 0) {
    if(operation_type == "regridding" || operation_type == "gridding") {
        string filename = "data/rtl/input/customfloats_" + operation_type + "_nonuniform.bin";
        if(isOutputData)
            filename = "data/rtl/output/customfloats_" + operation_type + "_nonuniform.bin";
        FILE *output_file = fopen(filename.c_str(), "wb");

        for(int idx = 0; idx < num_nonuniform_data_real_doubles; ++idx) {
            fwrite(&nonuniform_data_arr.at(idx).val.real.f, sizeof(float), 1, output_file);
            fwrite(&nonuniform_data_arr.at(idx).val.imag.f, sizeof(float), 1, output_file);
        }

        fclose(output_file);
    }

    for(int x = start_x; x < end_x; ++x) {
        for(int y = start_y; y < end_y; ++y) {
            string filename = "data/rtl/input/customfloats_" + operation_type + "_nonuniform_" + to_string(x) + "_" + to_string(y) + ".bin";
            if(isOutputData)
                filename = "data/rtl/output/customfloats_" + operation_type + "_nonuniform_" + to_string(x) + "_" + to_string(y) + ".bin";
            FILE *output_file = fopen(filename.c_str(), "wb");

            for(int z = 0; z < NUM_POINTS_PER_THREAD; ++z) {
                fwrite(&uniform_data_arr.at(x).at(y).at(z).real.f, sizeof(float), 1, output_file);
                fwrite(&uniform_data_arr.at(x).at(y).at(z).imag.f, sizeof(float), 1, output_file);
                // if(x == 0 && y == 0 && z < 2) {
                //     cout << std::bitset<1>(uniform_data_arr.at(x).at(y).at(z).real.raw.sign) << std::bitset<8>(uniform_data_arr.at(x).at(y).at(z).real.raw.exponent) << std::bitset<23>(uniform_data_arr.at(x).at(y).at(z).real.raw.mantissa) << endl;
                //     cout << std::bitset<1>(uniform_data_arr.at(x).at(y).at(z).imag.raw.sign) << std::bitset<8>(uniform_data_arr.at(x).at(y).at(z).imag.raw.exponent) << std::bitset<23>(uniform_data_arr.at(x).at(y).at(z).imag.raw.mantissa) << endl;
                // }
            }
            
            fclose(output_file);
        }
    }
}


void write_mat_files() {
    // Write the uniform data to a matfile
    double *reals = (double*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(double), 64);
    double *imags = (double*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(double), 64);
    for(int idx = 0; idx < num_nonuniform_data_real_doubles; ++idx) {
        reals[idx] = (double)nonuniform_data_arr.at(idx).val.real.f;
        imags[idx] = (double)nonuniform_data_arr.at(idx).val.imag.f;
    }
    MATFile *pmat;
    mxArray *paReal;
    mxArray *paImag;
    int status1;
    int status2;
    string filename = "data/cpp/tmp/customfloat_nonuniform_data.mat";
    printf("Creating file %s...\n\n", filename.c_str());
    pmat = matOpen(filename.c_str(), "w");
    if (pmat == NULL) {
        printf("Error creating file %s\n", filename.c_str());
        printf("(Do you have write permission in this directory?)\n");
    } else {
        paReal = mxCreateDoubleMatrix(num_nonuniform_data_real_doubles, 1, mxREAL);
        paImag = mxCreateDoubleMatrix(num_nonuniform_data_real_doubles, 1, mxREAL);
        if (paReal == NULL || paImag == NULL) {
            printf("%s : Out of memory on line %d\n", __FILE__, __LINE__);
            printf("Unable to create mxArray.\n");
        } else {
            memcpy((void *)(mxGetPr(paReal)), (double*)reals, num_nonuniform_data_real_doubles * 8);
            memcpy((void *)(mxGetPr(paImag)), (double*)imags, num_nonuniform_data_real_doubles * 8);
            status1 = matPutVariableAsGlobal(pmat, "nonuniform_data_real", paReal);
            status2 = matPutVariableAsGlobal(pmat, "nonuniform_data_imag", paImag);
            if (status1 != 0 || status2 != 0) {
                printf("Error using matPutVariableAsGlobal\n");
            }
        }
        mxDestroyArray(paReal);
        mxDestroyArray(paImag);
    }
    _mm_free(reals);
    _mm_free(imags);


    // Write the uniform data to a matfile
    reals = (double*)_mm_malloc(num_uniform_data_tiled_real_doubles * sizeof(double), 64);
    imags = (double*)_mm_malloc(num_uniform_data_tiled_real_doubles * sizeof(double), 64);
    for(int x = 0; x < ACCEL_DIM; ++x) {
        for(int y = 0; y < ACCEL_DIM; ++y) {
            for(int z = 0; z < (num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM); ++z) {
                int tid = ((y*ACCEL_DIM*(num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM)) + (x*(num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM))) + z;
                reals[tid] = (double)uniform_data_arr.at(x).at(y).at(z).real.f;
                imags[tid] = (double)uniform_data_arr.at(x).at(y).at(z).imag.f;
            }
        }
    }
    pmat;
    paReal;
    paImag;
    status1;
    status2;
    filename = "data/cpp/tmp/customfloat_uniform_data.mat";
    printf("Creating file %s...\n\n", filename.c_str());
    pmat = matOpen(filename.c_str(), "w");
    if (pmat == NULL) {
        printf("Error creating file %s\n", filename.c_str());
        printf("(Do you have write permission in this directory?)\n");
    } else {
        paReal = mxCreateDoubleMatrix(num_uniform_data_tiled_real_doubles, 1, mxREAL);
        paImag = mxCreateDoubleMatrix(num_uniform_data_tiled_real_doubles, 1, mxREAL);
        if (paReal == NULL || paImag == NULL) {
            printf("%s : Out of memory on line %d\n", __FILE__, __LINE__);
            printf("Unable to create mxArray.\n");
        } else {
            memcpy((void *)(mxGetPr(paReal)), (double*)reals, num_uniform_data_tiled_real_doubles * 8);
            memcpy((void *)(mxGetPr(paImag)), (double*)imags, num_uniform_data_tiled_real_doubles * 8);
            status1 = matPutVariableAsGlobal(pmat, "uniform_data_tiled_real", paReal);
            status2 = matPutVariableAsGlobal(pmat, "uniform_data_tiled_imag", paImag);
            if (status1 != 0 || status2 != 0) {
                printf("Error using matPutVariableAsGlobal\n");
            }
        }
        mxDestroyArray(paReal);
        mxDestroyArray(paImag);
    }
    _mm_free(reals);
    _mm_free(imags);
}


complex_customfloat swap_complex_components(complex_customfloat datum) {
    complex_customfloat tmp;
    tmp.real = datum.imag;
    tmp.imag = datum.real;

    return tmp;
}


//////////////////////////////////////////////
//                                          //
//                 Gridding                 //
//                                          //
//////////////////////////////////////////////
void *gridding(void *pointarg) {
    // Calculate the global thread indexes in each direction (relative indexes)
    int tidx = ((struct PointArgs*)pointarg)->tidx;
    int tidy = ((struct PointArgs*)pointarg)->tidy;
    int tid = tidy * ACCEL_DIM + tidx;

    bool nufft_type_adj = ((struct PointArgs*)pointarg)->nufft_type_adj;

    int num_points = num_nonuniform_data_real_doubles;

    int start = 0;
    int end = num_nonuniform_data_real_doubles; //start + num_points;

    // printf("Hello from pipeline (%d, %d); start=%d, end=%d\n", tidx, tidy, start, end);

    #pragma vector aligned
    for(int input_point_idx = start; input_point_idx < end; ++input_point_idx) {
        // Extract the current point being processed
        input_sample current_sample = nonuniform_data_arr.at(input_point_idx);


        //////////////////////////////////////////////
        //                                          //
        //                  Select                  //
        //                                          //
        //////////////////////////////////////////////
        // Extract the "relative" coordinates (within a tile)
        double x_coord = fmod(current_sample.x_coord, (double)ACCEL_DIM);
        double y_coord = fmod(current_sample.y_coord, (double)ACCEL_DIM);

        // Determine if this pipeline is affected by this source point
        double x_distance = fmod(ACCEL_DIM+x_coord-tidx, (double)ACCEL_DIM); // rem is implemented as an upper truncation
        double y_distance = fmod(ACCEL_DIM+y_coord-tidy, (double)ACCEL_DIM); // rem is implemented as an upper truncation

        // Check if the point lies within the interpolation window
        if(x_distance < INTERP_WIN_DIM && y_distance < INTERP_WIN_DIM) {
            // Get the quotient and remainder to determine grid and tile location
            int x_base_tile_idx = current_sample.x_coord/ACCEL_DIM; // x virtual tile index
            int y_base_tile_idx = current_sample.y_coord/ACCEL_DIM; // y virtual tile index

            // This point affects this pipeline!
            // Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
            int x_tile_idx;
            if(x_coord < tidx) {
               // A wrap occured!
                if(x_base_tile_idx == 0) {
                    x_tile_idx = tile_dim - 1;
                } else {
                    x_tile_idx = x_base_tile_idx - 1;
                }
            } else {
                x_tile_idx = x_base_tile_idx;
            }

            // Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
            int y_tile_idx;
            if(y_coord < tidy) {
               // A wrap occured!
                if(y_base_tile_idx == 0) {
                    y_tile_idx = tile_dim - 1;
                } else {
                    y_tile_idx = y_base_tile_idx - 1;
                }
            } else {
                y_tile_idx = y_base_tile_idx;
            }

            // Calculate the tile index ("depth" in the pipeline's data array)
            int tile_idx = y_tile_idx * MEM_TILE_DIM + x_tile_idx; // 1D global virtual tile index

            // Calculate the table index
            int x_table_idx = ((x_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift
            int y_table_idx = ((y_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift


            //////////////////////////////////////////////
            //      ****TESTING PURPOSES ONLY****       //
            //     Precomputed Interp Table Values      //
            //      ****TESTING PURPOSES ONLY****       //
            //////////////////////////////////////////////
            int table_tid = (x_table_idx * 193) + y_table_idx;
            CustomFloat R1 = weight_arr.at(table_tid).real;
            CustomFloat I1 = weight_arr.at(table_tid).imag;

            // //////////////////////////////////////////////
            // //                                          //
            // //               Interp Table               //
            // //                                          //
            // //////////////////////////////////////////////
            // // Extract the interp table values
            // complex_customfloat_table x_table_val = weight_arr.at(x_table_idx);
            // complex_customfloat_table y_table_val = weight_arr.at(y_table_idx);

            // // Calculate 2D table value (product of X and Y values)
            // CustomFloat k1;
            // k1.f = fadd(x_table_val.real, x_table_val.imag);
            // CustomFloat k2;
            // k2.f = fsub(y_table_val.imag, y_table_val.real);
            // CustomFloat k3;
            // k3.f = fadd(y_table_val.real, y_table_val.imag);
            // CustomFloat k4;
            // k4.f = fmul(y_table_val.real, k1);
            // CustomFloat k5;
            // k5.f = fmul(x_table_val.real, k2);
            // CustomFloat k6;
            // k6.f = fmul(x_table_val.imag, k3);
            // CustomFloat R1;
            // R1.f = fsub(k4, k6);
            // CustomFloat I1;
            // I1.f = fadd(k4, k5);


            // Get the conjugate of the table value (invert sign of I1)
            // I1 = -I1;
            I1.raw.sign = I1.raw.sign ^ 1;

            // Extract the grid point being processed
            complex_customfloat input_point_data = current_sample.val;


            //////////////////////////////////////////////
            //                                          //
            //               Interpolation              //
            //                                          //
            //////////////////////////////////////////////
            // Calculate interpolation result; conjugate of interp table value incorporated into this calculation directly by flipping sign for I1
            CustomFloat k7;
            k7 = fadd(R1, I1);
            CustomFloat k8;
            k8 = fsub(input_point_data.imag, input_point_data.real);
            CustomFloat k9;
            k9 = fadd(input_point_data.real, input_point_data.imag);
            CustomFloat k10;
            k10 = fmul(current_sample.val.real, k7);
            CustomFloat k11;
            k11 = fmul(R1, k8);
            CustomFloat k12;
            k12 = fmul(I1, k9);
            CustomFloat R2;
            R2 = fsub(k10, k12);
            CustomFloat I2;
            I2 = fadd(k10, k11);


            //////////////////////////////////////////////
            //                                          //
            //               Accumulation               //
            //                                          //
            //////////////////////////////////////////////
            uniform_data_arr.at(tidx).at(tidy).at(tile_idx).real = fadd(uniform_data_arr.at(tidx).at(tidy).at(tile_idx).real, R2);
            uniform_data_arr.at(tidx).at(tidy).at(tile_idx).imag = fadd(uniform_data_arr.at(tidx).at(tidy).at(tile_idx).imag, I2);
        }
    }

    pthread_mutex_lock(&ThreadLock); /* Get the thread lock */
    numThreads--;
    pthread_mutex_unlock(&ThreadLock); /* Release the lock */

    return nullptr;
}


//////////////////////////////////////////////
//                                          //
//                FFT2/IFFT2                //
//                                          //
//////////////////////////////////////////////
void fft2_ifft2(bool nufft_type_adj) {
    int tid, tid_offset;

    // Do an FFT/IFFT of each "col" (cols are stacked)
    int start_val = 0;
    int step_val = 1;
    int end_val = fft_len/ACCEL_DIM;
    int offset = fft_len/2/ACCEL_DIM;
    int comb_pipe_offset = fft_len/2;
    uint w_mask = 0;
    int bit_shift_offset = log2(MEM_DIM)-log2(fft_len);
    int num_stages = log2(fft_len);

    for(int stage = 0; stage < num_stages; ++stage) { // stages of transformation
        // Loop over accelerator pipelines to process input point
        for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; ++x_accel_idx) {
            for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; ++y_accel_idx) {
                tid = ((y_accel_idx * ACCEL_DIM + x_accel_idx) * NUM_POINTS_PER_THREAD);
                tid_offset = tid * NUM_POINTS_PER_THREAD;

                if(stage < num_stages-3) {
                    // Loop over all columns before going to next stage
                    for(int set_idx = 0; set_idx < fft_len/ACCEL_DIM; ++set_idx) {
                        int set_offset = set_idx * MEM_TILE_DIM;

                        int iter1 = start_val+offset;
                        int iter2 = start_val;
                        int saved_val = iter1;

                        while(iter1 < end_val) {
                            int tile_idx_1 = iter1+set_offset;
                            int tile_idx_2 = iter2+set_offset;

                            if(iter2+step_val == saved_val) {
                                iter1 = iter1+step_val+offset;
                                iter2 = iter2+step_val+offset;
                                saved_val = iter1;
                            } else {
                                iter1 = iter1+step_val;
                                iter2 = iter2+step_val;
                            }

                            // tile_idx_2[6:(7-stage)] for stage > 0; else 0 (6:7 is zero bits)
                            // bitshift(bitand(tile_idx_2, w_mask), -(7-(stage+bit_shift_offset)));
                            int w_addr = (tile_idx_2 & w_mask) >> (7-(stage+bit_shift_offset));

                            complex_customfloat_table w = weight_arr.at(w_addr);

                            complex_customfloat val1 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1);
                            complex_customfloat val2 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_2);
                            if(nufft_type_adj) {
                                val1 = swap_complex_components(val1);
                                val2 = swap_complex_components(val2);
                            }

                            CustomFloat k13;
                            k13 = fadd(w.real, w.imag);
                            CustomFloat k14;
                            k14 = fsub(val1.imag, val1.real);
                            CustomFloat k15;
                            k15 = fadd(val1.real, val1.imag);
                            CustomFloat k16;
                            k16 = fmul(val1.real, k13);
                            CustomFloat k17;
                            k17 = fmul(w.real, k14);
                            CustomFloat k18;
                            k18 = fmul(w.imag, k15);
                            CustomFloat R3;
                            R3 = fsub(k16, k18);
                            CustomFloat I3;
                            I3 = fadd(k16, k17);

                            complex_customfloat a;
                            a.real = fsub(val2.real, R3); // 1-st part of the "butterfly" creating operation
                            a.imag = fsub(val2.imag, I3); // 1-st part of the "butterfly" creating operation
                            complex_customfloat b;
                            b.real = fadd(val2.real, R3); // 2-nd part of the "butterfly" creating operation
                            b.imag = fadd(val2.imag, I3); // 2-nd part of the "butterfly" creating operation

                            if(nufft_type_adj) {
                                a = swap_complex_components(a);
                                b = swap_complex_components(b);
                            }
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1) = a;
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_2) = b;
                        }
                    }
                } else if(((x_accel_idx >> ((num_stages-1)-stage)) & 1U) == 1) { // num_stages instead of num_stages-1 because the first index is 1 in Matlab, not 0
                    // Loop over all columns before going to next stage
                    for(int set_idx = 0; set_idx < fft_len/ACCEL_DIM; ++set_idx) {
                        int set_offset = set_idx * MEM_TILE_DIM;

                        int iter1 = start_val+offset;
                        int iter2 = start_val;
                        int saved_val = iter1;

                        while(iter1 < end_val) {
                            int tile_idx_1 = iter1+set_offset;
                            int tile_idx_2 = iter2+set_offset;

                            if(iter2+step_val == saved_val) {
                                iter1 = iter1+step_val+offset;
                                iter2 = iter2+step_val+offset;
                                saved_val = iter1;
                            } else {
                                iter1 = iter1+step_val;
                                iter2 = iter2+step_val;
                            }

                            // Stage 7: tile_idx_2[6:(7-stage)]. Stage 8: Concatenate tile_idx_2[6:0] and x_accel_idx[2]. Stage 9: Concatenate tile_idx_2[6:0] and x_accel_idx[2:1]
                            // w_addr = bitshift(bitand(tile_idx_2, w_mask), -(7-(stage+bit_shift_offset))); % tile_idx_2[6:0] shifted to the left (for stage > 7)
                            // append_bits = bitshift(bitand(int32(x_accel_idx), 6), -comb_pipe_offset); % x_accel_idx[2:1] shifted to the right
                            // w_addr = bitor(w_addr, append_bits); % [tile_idx_2[6:0] x_accel_idx[2:1]]
                            int w_addr = (tile_idx_2 & w_mask) << ((stage+bit_shift_offset)-7); // tile_idx_2[6:0] shifted to the left (for stage > 7)
                            int append_bits = (x_accel_idx & 6) >> comb_pipe_offset; // x_accel_idx[2:1] shifted to the right
                            w_addr = (w_addr | append_bits); // [tile_idx_2[6:0] x_accel_idx[2:1]]

                            complex_customfloat_table w = weight_arr.at(w_addr);

                            complex_customfloat val1 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1);
                            complex_customfloat val2 = uniform_data_arr.at(x_accel_idx-comb_pipe_offset).at(y_accel_idx).at(tile_idx_2);
                            if(nufft_type_adj) {
                                val1 = swap_complex_components(val1);
                                val2 = swap_complex_components(val2);
                            }

                            CustomFloat k13;
                            k13 = fadd(w.real, w.imag);
                            CustomFloat k14;
                            k14 = fsub(val1.imag, val1.real);
                            CustomFloat k15;
                            k15 = fadd(val1.real, val1.imag);
                            CustomFloat k16;
                            k16 = fmul(val1.real, k13);
                            CustomFloat k17;
                            k17 = fmul(w.real, k14);
                            CustomFloat k18;
                            k18 = fmul(w.imag, k15);
                            CustomFloat R3;
                            R3 = fsub(k16, k18);
                            CustomFloat I3;
                            I3 = fadd(k16, k17);

                            complex_customfloat a;
                            a.real = fsub(val2.real, R3); // 1-st part of the "butterfly" creating operation
                            a.imag = fsub(val2.imag, I3); // 1-st part of the "butterfly" creating operation
                            complex_customfloat b;
                            b.real = fadd(val2.real, R3); // 2-nd part of the "butterfly" creating operation
                            b.imag = fadd(val2.imag, I3); // 2-nd part of the "butterfly" creating operation

                            if(nufft_type_adj) {
                                a = swap_complex_components(a);
                                b = swap_complex_components(b);
                            }
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1) = a;
                            uniform_data_arr.at(x_accel_idx-comb_pipe_offset).at(y_accel_idx).at(tile_idx_2) = b;
                        }
                    }
                }
            }
        }
        offset = (int)(offset / 2);
        comb_pipe_offset = (int)(comb_pipe_offset / 2);
        // w_mask = bitset(w_mask, max(7-(stage+bit_shift_offset), 1));
        w_mask |= 1UL << max(7-(stage+bit_shift_offset)-1, 0);
    }

    // Do an FFT/IFFT of each "row" (rows are stacked)
    start_val = 0;
    step_val = 128;
    end_val = fft_len*2*ACCEL_DIM;
    offset = fft_len*ACCEL_DIM;
    comb_pipe_offset = fft_len/2;
    w_mask = 0;
    bit_shift_offset = log2(MEM_DIM)-log2(fft_len);
    num_stages = log2(fft_len);

    for(int stage = 0; stage < num_stages; ++stage) { // stages of transformation
        // Loop over accelerator pipelines to process input point
        for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; ++x_accel_idx) {
            for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; ++y_accel_idx) {
                tid = y_accel_idx * ACCEL_DIM + x_accel_idx;
                tid_offset = tid * NUM_POINTS_PER_THREAD;

                if(stage < num_stages-3) {
                    // Loop over all columns before going to next stage
                    for(int set_idx = 0; set_idx < fft_len/ACCEL_DIM; ++set_idx) {
                        int iter1 = start_val+offset;
                        int iter2 = start_val;
                        int saved_val = iter1;

                        while(iter1 < end_val) {
                            int tile_idx_1 = iter1+set_idx;
                            int tile_idx_2 = iter2+set_idx;

                            if(iter2+step_val == saved_val) {
                                iter1 = iter1+step_val+offset;
                                iter2 = iter2+step_val+offset;
                                saved_val = iter1;
                            } else {
                                iter1 = iter1+step_val;
                                iter2 = iter2+step_val;
                            }

                            // tile_idx_2[13:(14-stage)] for stage > 0; else 0 (13:14 is zero bits)
                            // w_addr = bitshift(bitand(tile_idx_2, w_mask), -(14-(stage+bit_shift_offset)));
                            int w_addr = (tile_idx_2 & w_mask) >> (14-(stage+bit_shift_offset));

                            complex_customfloat_table w = weight_arr.at(w_addr);

                            complex_customfloat val1 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1);
                            complex_customfloat val2 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_2);
                            if(nufft_type_adj) {
                                val1 = swap_complex_components(val1);
                                val2 = swap_complex_components(val2);
                            }

                            CustomFloat k13;
                            k13 = fadd(w.real, w.imag);
                            CustomFloat k14;
                            k14 = fsub(val1.imag, val1.real);
                            CustomFloat k15;
                            k15 = fadd(val1.real, val1.imag);
                            CustomFloat k16;
                            k16 = fmul(val1.real, k13);
                            CustomFloat k17;
                            k17 = fmul(w.real, k14);
                            CustomFloat k18;
                            k18 = fmul(w.imag, k15);
                            CustomFloat R3;
                            R3 = fsub(k16, k18);
                            CustomFloat I3;
                            I3 = fadd(k16, k17);

                            complex_customfloat a;
                            a.real = fsub(val2.real, R3); // 1-st part of the "butterfly" creating operation
                            a.imag = fsub(val2.imag, I3); // 1-st part of the "butterfly" creating operation
                            complex_customfloat b;
                            b.real = fadd(val2.real, R3); // 2-nd part of the "butterfly" creating operation
                            b.imag = fadd(val2.imag, I3); // 2-nd part of the "butterfly" creating operation

                            if(nufft_type_adj) {
                                a = swap_complex_components(a);
                                b = swap_complex_components(b);
                            }
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1) = a;
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_2) = b;
                        }
                    }
                } else if(((y_accel_idx >> ((num_stages-1)-stage)) & 1U) == 1) { // num_stages instead of num_stages-1 because the first index is 1 in Matlab, not 0
                    // Loop over all columns before going to next stage
                    for(int set_idx = 0; set_idx < fft_len/ACCEL_DIM; ++set_idx) {
                        int iter1 = start_val;
                        int iter2 = start_val;
                        int saved_val = iter1;

                        while(iter1 < end_val) {
                            int tile_idx_1 = iter1+set_idx;
                            int tile_idx_2 = iter2+set_idx;

                            if(iter2+step_val == saved_val) {
                                iter1 = iter1+step_val;
                                iter2 = iter2+step_val;
                                saved_val = iter1;
                            } else {
                                iter1 = iter1+step_val;
                                iter2 = iter2+step_val;
                            }

                            // Stage 7: tile_idx_2[13:(14-stage)]. Stage 8: Concatenate tile_idx_2[13:7] and y_accel_idx[2]. Stage 9: Concatenate tile_idx_2[13:7] and y_accel_idx[2:1]
                            // w_addr = bitshift(bitshift(bitand(tile_idx_2, w_mask), -max(14-(stage+bit_shift_offset), 7)), mod(-(14-(stage+bit_shift_offset)),7)); % tile_idx_2[13:7] shifted to the left (for stage > 7)
                            // append_bits = bitshift(bitand(int32(y_accel_idx), 6), -comb_pipe_offset); % y_accel_idx[2:1] shifted to the right
                            // w_addr = bitor(w_addr, append_bits); % [tile_idx_2[13:7] y_accel_idx[2:1]]
                            int w_addr = ((tile_idx_2 & w_mask) >> max(14-(stage+bit_shift_offset), 7)) << ((stage+bit_shift_offset)-7); // tile_idx_2[13:7] shifted to the left (for stage > 7)
                            int append_bits = ((y_accel_idx & 6) >> comb_pipe_offset); // y_accel_idx[2:1] shifted to the right
                            w_addr = (w_addr | append_bits); // [tile_idx_2[13:7] y_accel_idx[2:1]]

                            complex_customfloat_table w = weight_arr.at(w_addr);

                            complex_customfloat val1 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1);
                            complex_customfloat val2 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx-comb_pipe_offset).at(tile_idx_2);
                            if(nufft_type_adj) {
                                val1 = swap_complex_components(val1);
                                val2 = swap_complex_components(val2);
                            }

                            CustomFloat k13;
                            k13 = fadd(w.real, w.imag);
                            CustomFloat k14;
                            k14 = fsub(val1.imag, val1.real);
                            CustomFloat k15;
                            k15 = fadd(val1.real, val1.imag);
                            CustomFloat k16;
                            k16 = fmul(val1.real, k13);
                            CustomFloat k17;
                            k17 = fmul(w.real, k14);
                            CustomFloat k18;
                            k18 = fmul(w.imag, k15);
                            CustomFloat R3;
                            R3 = fsub(k16, k18);
                            CustomFloat I3;
                            I3 = fadd(k16, k17);

                            complex_customfloat a;
                            a.real = fsub(val2.real, R3); // 1-st part of the "butterfly" creating operation
                            a.imag = fsub(val2.imag, I3); // 1-st part of the "butterfly" creating operation
                            complex_customfloat b;
                            b.real = fadd(val2.real, R3); // 2-nd part of the "butterfly" creating operation
                            b.imag = fadd(val2.imag, I3); // 2-nd part of the "butterfly" creating operation

                            if(nufft_type_adj) {
                                a = swap_complex_components(a);
                                b = swap_complex_components(b);
                            }
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1) = a;
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx-comb_pipe_offset).at(tile_idx_2) = b;
                        }
                    }
                }
            }
        }
        offset = (int)(offset / 2);
        comb_pipe_offset = (int)(comb_pipe_offset / 2);
        // w_mask = bitset(w_mask, max(14-(stage+bit_shift_offset), 1));
        w_mask |= 1UL << max(14-(stage+bit_shift_offset)-1, 0);
    }
}


//////////////////////////////////////////////
//                                          //
//                Regridding                //
//                                          //
//////////////////////////////////////////////
void *regridding(void *pointarg) {
    // Calculate the global thread indexes in each direction (relative indexes)
    int threadIdx_x = ((struct PointArgs*)pointarg)->tidx;
    int threadIdx_y = ((struct PointArgs*)pointarg)->tidy;
    int tid = threadIdx_y * ACCEL_DIM + threadIdx_x;

    bool nufft_type_adj = ((struct PointArgs*)pointarg)->nufft_type_adj;

    int num_points = ceil(num_nonuniform_data_real_doubles / NUM_THREADS);
    int start = tid * num_points;
    int end = start + num_points; //start + num_points;
    if(end > num_nonuniform_data_real_doubles)
        end = num_nonuniform_data_real_doubles;

    // printf("Hello from slice %d, pipeline (%d, %d); start=%d, end=%d; tid=%d, num_points=%d\n", threadIdx_z, threadIdx_x, threadIdx_y, start, end, tid, num_points);
    // printf("Hello from pipeline (%d, %d); start=%d, end=%d; tid=%d, num_points=%d\n", threadIdx_x, threadIdx_y, start, end, tid, num_points);

    #pragma vector aligned
    for(int input_point_idx = start; input_point_idx < end; ++input_point_idx) {
        for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; ++x_accel_idx) {
            for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; ++y_accel_idx) {
                //////////////////////////////////////////////
                //                                          //
                //                  Select                  //
                //                                          //
                //////////////////////////////////////////////
                // Extract the current point being processed
                input_sample current_sample = nonuniform_data_arr.at(input_point_idx);

                // Get the quotient and remainder to determine grid and tile location
                int x_base_tile_idx = current_sample.x_coord/ACCEL_DIM; // x virtual tile index
                int y_base_tile_idx = current_sample.y_coord/ACCEL_DIM; // y virtual tile index

                // Extract the "relative" coordinates (within a tile)
                double x_coord = fmod(current_sample.x_coord, (double)ACCEL_DIM);
                double y_coord = fmod(current_sample.y_coord, (double)ACCEL_DIM);

                // Determine if this pipeline is affected by this source point
                double x_distance = fmod(ACCEL_DIM+x_coord-x_accel_idx, (double)ACCEL_DIM); // rem is implemented as an upper truncation
                double y_distance = fmod(ACCEL_DIM+y_coord-y_accel_idx, (double)ACCEL_DIM); // rem is implemented as an upper truncation

                // Check if the point lies within the interpolation window
                if(x_distance < INTERP_WIN_DIM && y_distance < INTERP_WIN_DIM) {
                    // This point affects this pipeline!
                    // Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
                    int x_tile_idx;
                    if(x_coord < x_accel_idx) {
                       // A wrap occured!
                        if(x_base_tile_idx == 0) {
                            x_tile_idx = tile_dim - 1;
                        } else {
                            x_tile_idx = x_base_tile_idx - 1;
                        }
                    } else {
                        x_tile_idx = x_base_tile_idx;
                    }

                    // Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
                    int y_tile_idx;
                    if(y_coord < y_accel_idx) {
                       // A wrap occured!
                        if(y_base_tile_idx == 0) {
                            y_tile_idx = tile_dim - 1;
                        } else {
                            y_tile_idx = y_base_tile_idx - 1;
                        }
                    } else {
                        y_tile_idx = y_base_tile_idx;
                    }

                    // Calculate the tile index ("depth" in the pipeline's data array)
                    int tile_idx = y_tile_idx * MEM_TILE_DIM + x_tile_idx; // 1D global virtual tile index

                    // Calculate the table index
                    int x_table_idx = ((x_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift
                    int y_table_idx = ((y_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift


                    //////////////////////////////////////////////
                    //      ****TESTING PURPOSES ONLY****       //
                    //     Precomputed Interp Table Values      //
                    //      ****TESTING PURPOSES ONLY****       //
                    //////////////////////////////////////////////
                    int table_tid = (x_table_idx * 193) + y_table_idx;
                    CustomFloat R1 = weight_arr.at(table_tid).real;
                    CustomFloat I1 = weight_arr.at(table_tid).imag;

                    //////////////////////////////////////////////
                    //                                          //
                    //               Interp Table               //
                    //                                          //
                    //////////////////////////////////////////////
                    // Extract the interp table values
                    // complex_customfloat_table x_table_val = weight_arr.at(x_table_idx);
                    // complex_customfloat_table y_table_val = weight_arr.at(y_table_idx);

                    // // Calculate 2D table value (product of X and Y values)
                    // CustomFloat k1;
                    // k1.f = fadd(x_table_val.real, x_table_val.imag);
                    // CustomFloat k2;
                    // k2.f = fsub(y_table_val.imag, y_table_val.real);
                    // CustomFloat k3;
                    // k3.f = fadd(y_table_val.real, y_table_val.imag);
                    // CustomFloat k4;
                    // k4.f = fmul(y_table_val.real, k1);
                    // CustomFloat k5;
                    // k5.f = fmul(x_table_val.real, k2);
                    // CustomFloat k6;
                    // k6.f = fmul(x_table_val.imag, k3);
                    // CustomFloat R1;
                    // R1.f = fsub(k4, k6);
                    // CustomFloat I1;
                    // I1.f = fadd(k4, k5);

                    // Extract the grid point being processed
                    complex_customfloat input_point_data = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx);


                    //////////////////////////////////////////////
                    //                                          //
                    //               Interpolation              //
                    //                                          //
                    //////////////////////////////////////////////
                    // Calculate interpolation result; conjugate of interp table value incorporated into this calculation directly by flipping sign for I1
                    CustomFloat k7;
                    k7 = fadd(R1, I1);
                    CustomFloat k8;
                    k8 = fsub(input_point_data.imag, input_point_data.real);
                    CustomFloat k9;
                    k9 = fadd(input_point_data.real, input_point_data.imag);
                    CustomFloat k10;
                    k10 = fmul(input_point_data.real, k7);
                    CustomFloat k11;
                    k11 = fmul(R1, k8);
                    CustomFloat k12;
                    k12 = fmul(I1, k9);
                    CustomFloat R2;
                    R2 = fsub(k10, k12);
                    CustomFloat I2;
                    I2 = fadd(k10, k11);


                    //////////////////////////////////////////////
                    //                                          //
                    //               Accumulation               //
                    //                                          //
                    //////////////////////////////////////////////
                    nonuniform_data_arr.at(input_point_idx).val.real = fadd(nonuniform_data_arr.at(input_point_idx).val.real, R2);
                    nonuniform_data_arr.at(input_point_idx).val.imag = fadd(nonuniform_data_arr.at(input_point_idx).val.imag, I2);
                }
            }
        }
    }

    pthread_mutex_lock(&ThreadLock); /* Get the thread lock */
    numThreads--;
    pthread_mutex_unlock(&ThreadLock); /* Release the lock */

    return nullptr;
}


//////////////////////////////////////////////
//                                          //
//                   Main                   //
//                                          //
//////////////////////////////////////////////
int main(int argc, char *argv[]) {
    // Check if the correct numbe of arguments are present; argc should be 2 for correct execution
    if(argc != 10) {
        // If there are the wrong number of arguments, print usage
        printf("Error: %d arguments given, but expected 9.\n", argc);
        printf("usage: %s operation_type weight_data_real weight_data_imag nonuniform_data_real nonuniform_data_imag nonuniform_x_coord nonuniform_y_coord uniform_data_tiled_real uniform_data_tiled_imag\n", argv[0]);
    } else {
        // If the number of arguments is correct, save operation type and open files
        string operation_type = argv[1];

        ifstream weight_data_real_File(argv[2], ios::in | ios::binary | ios::ate);
        ifstream weight_data_imag_File(argv[3], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_data_real_File(argv[4], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_data_imag_File(argv[5], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_x_coord_File(argv[6], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_y_coord_File(argv[7], ios::in | ios::binary | ios::ate);
        ifstream uniform_data_tiled_real_File(argv[8], ios::in | ios::binary | ios::ate);
        ifstream uniform_data_tiled_imag_File(argv[9], ios::in | ios::binary | ios::ate);

        if(!weight_data_real_File.good() || !weight_data_imag_File.good() || !nonuniform_data_real_File.good() || !nonuniform_data_imag_File.good() || !nonuniform_x_coord_File.good() || !nonuniform_y_coord_File.good() || !uniform_data_tiled_real_File.good() || !uniform_data_tiled_imag_File.good()) {
            // If file fails to open, notify user
            if(!weight_data_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[2]);
            } else {
                weight_data_real_File.close();
            }
            if(!weight_data_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[3]);
            } else {
                weight_data_imag_File.close();
            }
            if(!nonuniform_data_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[4]);
            } else {
                nonuniform_data_real_File.close();
            }
            if(!nonuniform_data_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[5]);
            } else {
                nonuniform_data_imag_File.close();
            }
            if(!nonuniform_x_coord_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[6]);
            } else {
                nonuniform_x_coord_File.close();
            }
            if(!nonuniform_y_coord_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[7]);
            } else {
                nonuniform_y_coord_File.close();
            }
            if(!uniform_data_tiled_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[8]);
            } else {
                uniform_data_tiled_real_File.close();
            }
            if(!uniform_data_tiled_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[9]);
            } else {
                uniform_data_tiled_imag_File.close();
            }
        } else {
            // Get the size of the file in bytes
            streampos weight_data_real = weight_data_real_File.tellg();
            streampos weight_data_imag = weight_data_imag_File.tellg();
            streampos nonuniform_data_real = nonuniform_data_real_File.tellg();
            streampos nonuniform_data_imag = nonuniform_data_imag_File.tellg();
            streampos nonuniform_x_coord = nonuniform_x_coord_File.tellg();
            streampos nonuniform_y_coord = nonuniform_y_coord_File.tellg();
            streampos uniform_data_tiled_real = uniform_data_tiled_real_File.tellg();
            streampos uniform_data_tiled_imag = uniform_data_tiled_imag_File.tellg();


            cout << "customfloat info:\nN= " << N << "; E= " << E << "\n" << endl;
            cout << "sizeof(complex_double) = " << sizeof(complex_double) << endl;
            cout << "output points = " << MEM_DIM * MEM_DIM << endl;
            cout << "threads = " << ACCEL_DIM * ACCEL_DIM << endl;
            cout << "points per thread = " << NUM_POINTS_PER_THREAD << endl;


            int num_weight_data_real_bytes = (int)weight_data_real;
            // cout << "num_weight_data_real_bytes=" << num_weight_data_real_bytes << endl;
            num_weight_data_real_doubles = (int)(weight_data_real / sizeof(double));
            cout << "num_weight_data_real_doubles=" << num_weight_data_real_doubles << endl;

            int num_weight_data_imag_bytes = (int)weight_data_imag;
            // cout << "num_weight_data_imag_bytes=" << num_weight_data_imag_bytes << endl;
            num_weight_data_imag_doubles = (int)(weight_data_imag / sizeof(double));
            cout << "num_weight_data_imag_doubles=" << num_weight_data_imag_doubles << endl;

            int num_nonuniform_data_real_bytes = (int)nonuniform_data_real;
            // cout << "num_nonuniform_data_real_bytes=" << num_nonuniform_data_real_bytes << endl;
            num_nonuniform_data_real_doubles = (int)(nonuniform_data_real / sizeof(double));
            cout << "num_nonuniform_data_real_doubles=" << num_nonuniform_data_real_doubles << endl;

            int num_nonuniform_data_imag_bytes = (int)nonuniform_data_imag;
            // cout << "num_nonuniform_data_imag_bytes=" << num_nonuniform_data_imag_bytes << endl;
            num_nonuniform_data_imag_doubles = (int)(nonuniform_data_imag / sizeof(double));
            cout << "num_nonuniform_data_imag_doubles=" << num_nonuniform_data_imag_doubles << endl;

            int num_nonuniform_x_coord_bytes = (int)nonuniform_x_coord;
            // cout << "num_nonuniform_x_coord_bytes=" << num_nonuniform_x_coord_bytes << endl;
            num_nonuniform_x_coord_doubles = (int)(nonuniform_x_coord / sizeof(double));
            cout << "num_nonuniform_x_coord_doubles=" << num_nonuniform_x_coord_doubles << endl;

            int num_nonuniform_y_coord_bytes = (int)nonuniform_y_coord;
            // cout << "num_nonuniform_y_coord_bytes=" << num_nonuniform_y_coord_bytes << endl;
            num_nonuniform_y_coord_doubles = (int)(nonuniform_y_coord / sizeof(double));
            cout << "num_nonuniform_y_coord_doubles=" << num_nonuniform_y_coord_doubles << endl;

            int num_uniform_data_tiled_real_bytes = (int)uniform_data_tiled_real;
            // cout << "num_uniform_data_tiled_real_bytes=" << num_uniform_data_tiled_real_bytes << endl;
            num_uniform_data_tiled_real_doubles = (int)(uniform_data_tiled_real / sizeof(double));
            cout << "num_uniform_data_tiled_real_doubles=" << num_uniform_data_tiled_real_doubles << endl;

            int num_uniform_data_tiled_imag_bytes = (int)uniform_data_tiled_imag;
            // cout << "num_uniform_data_tiled_imag_bytes=" << num_uniform_data_tiled_imag_bytes << endl;
            num_uniform_data_tiled_imag_doubles = (int)(uniform_data_tiled_imag / sizeof(double));
            cout << "num_uniform_data_tiled_imag_doubles=" << num_uniform_data_tiled_imag_doubles << endl;


            // Declare pointers for input data arrays
            double *weight_data_real_arr;
            double *weight_data_imag_arr;
            double *nonuniform_data_real_arr;
            double *nonuniform_data_imag_arr;
            double *nonuniform_x_coord_arr;
            double *nonuniform_y_coord_arr;
            double *uniform_data_tiled_real_arr;
            double *uniform_data_tiled_imag_arr;


            // Allocate space for the input data arrays
            weight_data_real_arr = (double*)_mm_malloc(num_weight_data_real_doubles * sizeof(double), 64);
            if(weight_data_real_arr == NULL) fprintf(stderr, "Bad malloc on weight_data_real_arr\n");
            weight_data_imag_arr = (double*)_mm_malloc(num_weight_data_imag_doubles * sizeof(double), 64);
            if(weight_data_imag_arr == NULL) fprintf(stderr, "Bad malloc on weight_data_imag_arr\n");

            nonuniform_data_real_arr = (double*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(double), 64);
            if(nonuniform_data_real_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_data_real_arr\n");
            nonuniform_data_imag_arr = (double*)_mm_malloc(num_nonuniform_data_imag_doubles * sizeof(double), 64);
            if(nonuniform_data_imag_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_data_imag_arr\n");

            nonuniform_x_coord_arr = (double*)_mm_malloc(num_nonuniform_x_coord_doubles * sizeof(double), 64);
            if(nonuniform_x_coord_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_x_coord_arr\n");
            nonuniform_y_coord_arr = (double*)_mm_malloc(num_nonuniform_y_coord_doubles * sizeof(double), 64);
            if(nonuniform_y_coord_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_y_coord_arr\n");

            uniform_data_tiled_real_arr = (double*)_mm_malloc(num_uniform_data_tiled_real_doubles * sizeof(double), 64);
            if(uniform_data_tiled_real_arr == NULL) fprintf(stderr, "Bad malloc on uniform_data_tiled_real_arr\n");
            uniform_data_tiled_imag_arr = (double*)_mm_malloc(num_uniform_data_tiled_imag_doubles * sizeof(double), 64);
            if(uniform_data_tiled_imag_arr == NULL) fprintf(stderr, "Bad malloc on uniform_data_tiled_imag_arr\n");


            // Stream the file into the input data arrays
            weight_data_real_File.seekg(0, ios::beg);
            weight_data_real_File.read((char*)weight_data_real_arr, num_weight_data_real_bytes);
            weight_data_real_File.close();

            weight_data_imag_File.seekg(0, ios::beg);
            weight_data_imag_File.read((char*)weight_data_imag_arr, num_weight_data_imag_bytes);
            weight_data_imag_File.close();

            nonuniform_data_real_File.seekg(0, ios::beg);
            nonuniform_data_real_File.read((char*)nonuniform_data_real_arr, num_nonuniform_data_real_bytes);
            nonuniform_data_real_File.close();

            nonuniform_data_imag_File.seekg(0, ios::beg);
            nonuniform_data_imag_File.read((char*)nonuniform_data_imag_arr, num_nonuniform_data_imag_bytes);
            nonuniform_data_imag_File.close();

            nonuniform_x_coord_File.seekg(0, ios::beg);
            nonuniform_x_coord_File.read((char*)nonuniform_x_coord_arr, num_nonuniform_x_coord_bytes);
            nonuniform_x_coord_File.close();

            nonuniform_y_coord_File.seekg(0, ios::beg);
            nonuniform_y_coord_File.read((char*)nonuniform_y_coord_arr, num_nonuniform_y_coord_bytes);
            nonuniform_y_coord_File.close();

            uniform_data_tiled_real_File.seekg(0, ios::beg);
            uniform_data_tiled_real_File.read((char*)uniform_data_tiled_real_arr, num_uniform_data_tiled_real_bytes);
            uniform_data_tiled_real_File.close();

            uniform_data_tiled_imag_File.seekg(0, ios::beg);
            uniform_data_tiled_imag_File.read((char*)uniform_data_tiled_imag_arr, num_uniform_data_tiled_imag_bytes);
            uniform_data_tiled_imag_File.close();


            for(int i = 0; i < num_weight_data_real_doubles; ++i) {
                complex_customfloat_table tmp;
                double val1, val2;
                val1 = (static_cast<double>(weight_data_real_arr[i]));
                val2 = (static_cast<double>(weight_data_imag_arr[i]));
                tmp.real.f = (float)val1;
                tmp.imag.f = (float)val2;

                if(tmp.real.f != tmp.real.f) {
                    cout << "weight_data_real_arr ERROR" << endl;
                }
                if(tmp.imag.f != tmp.imag.f) {
                    cout << "weight_data_imag_arr ERROR" << endl;
                }

                weight_arr.push_back(tmp);
            }

            for(int i = 0; i < num_nonuniform_data_real_doubles; ++i) {
                input_sample tmp;
                double val1, val2;
                val1 = (static_cast<double>(nonuniform_data_real_arr[i]));
                val2 = (static_cast<double>(nonuniform_data_imag_arr[i]));
                tmp.val.real.f = (float)val1;
                tmp.val.imag.f = (float)val2;
                tmp.x_coord = static_cast<double>(nonuniform_x_coord_arr[i]);
                tmp.y_coord = static_cast<double>(nonuniform_y_coord_arr[i]);

                if(tmp.val.real.f != tmp.val.real.f) {
                    cout << "nonuniform_data_real_arr ERROR" << endl;
                }
                if(tmp.val.imag.f != tmp.val.imag.f) {
                    cout << "nonuniform_data_imag_arr ERROR" << endl;
                }

                nonuniform_data_arr.push_back(tmp);
            }

            int idx = 0;
            for(int x = 0; x < ACCEL_DIM; ++x) {
                uniform_data_arr.push_back(vector<vector<complex_customfloat>>{ACCEL_DIM});
                for(int y = 0; y < ACCEL_DIM; ++y) {
                    uniform_data_arr.at(x).push_back(vector<complex_customfloat>{ACCEL_DIM});
                    for(int z = 0; z < (num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM); ++z) {
                        complex_customfloat tmp;
                        double val1, val2;
                        val1 = (static_cast<double>(uniform_data_tiled_real_arr[idx]));
                        val2 = (static_cast<double>(uniform_data_tiled_imag_arr[idx]));
                        tmp.real.f = (float)val1;
                        tmp.imag.f = (float)val2;

                        if(tmp.real.f != tmp.real.f) {
                            cout << "uniform_data_tiled_real_arr ERROR" << endl;
                        }
                        if(tmp.imag.f != tmp.imag.f) {
                            cout << "uniform_data_tiled_imag_arr ERROR" << endl;
                        }

                        uniform_data_arr.at(x).at(y).push_back(tmp);

                        idx++;
                    }
                }
            }


            /* Thread Variables */
            int ret; // Return result for pthread functions
            pthread_t* threads = (pthread_t *)_mm_malloc(NUM_THREADS * sizeof(pthread_t), 64); // Allocate threads
            assert(threads != NULL); // Check that the malloc was successful
            ret = pthread_mutex_init(&ThreadLock, NULL); // Initialize the mutex
            assert(ret == 0); // Check to see that the initialization was successful

            PointArgs *threadIdx;


            if(operation_type == "fft2") {
                nufft_type_adj = false;
                grid_dim = 512;
                tile_dim = (grid_dim/ACCEL_DIM);
                fft_len = pow(2, ceil(log2(grid_dim)));

                /* --------------------------------------------------------------------- */

                cout << "***Beginning FFT2 Computation***" << endl;

                // Start recording the elapsed time
                auto start = chrono::steady_clock::now();

                /* ---------------------------- COMPUTATION ---------------------------- */

                #ifdef WRITE_HEX
                    write_hex_file(operation_type, false, 0, 8, 0, 8);
                #endif

                fft2_ifft2(nufft_type_adj);

                /* --------------------------------------------------------------------- */

                // Stop recording the elapsed time
                auto end = chrono::steady_clock::now();
                cout << "FFT2 Compute Time (milliseconds): " << (double)chrono::duration_cast<chrono::microseconds>(end-start).count()/1000 << endl;

                /* ----------------------------- DATA DUMP ----------------------------- */

                // Print output
                // write_data_file("fft2");
                #ifdef WRITE_HEX
                    write_hex_file(operation_type, true, 0, 8, 0, 8);
                #endif
                write_mat_files();

                /* --------------------------------------------------------------------- */
            } else if(operation_type == "regridding") {
                nufft_type_adj = false;
                grid_dim = 512;
                tile_dim = (grid_dim/ACCEL_DIM);
                fft_len = pow(2, ceil(log2(grid_dim)));

                /* --------------------------------------------------------------------- */

                cout << "***Beginning Regridding Computation***" << endl;

                // Start recording the elapsed time
                auto start = chrono::steady_clock::now();

                /* ---------------------------- COMPUTATION ---------------------------- */

                #ifdef WRITE_HEX
                    write_hex_file(operation_type, false, 0, 8, 0, 8);
                #endif

                numThreads = NUM_THREADS;

                // Run kernel to interpolate values
                #pragma vector aligned
                for(int x = 0; x < ACCEL_DIM; ++x) {
                    for(int y = 0; y < ACCEL_DIM; ++y) {
                        threadIdx = (PointArgs*)malloc(sizeof(PointArgs));
                        threadIdx->tidx = x;
                        threadIdx->tidy = y;
                        threadIdx->nufft_type_adj = nufft_type_adj;
                        ret = pthread_create(&threads[x*y+y], NULL, regridding, (void*)threadIdx);
                        assert(ret == 0);
                    }
                }
                // threadIdx = (PointArgs*)malloc(sizeof(PointArgs));
                // threadIdx->tidx = 0;
                // threadIdx->tidy = 0;
                // threadIdx->nufft_type_adj = nufft_type_adj;
                // ret = pthread_create(&threads[0*0+0], NULL, regridding, (void*)threadIdx);
                // assert(ret == 0);

                // Wait for threads to complete
                while(numThreads > 0);

                /* --------------------------------------------------------------------- */

                // Stop recording the elapsed time
                auto end = chrono::steady_clock::now();
                cout << "Regridding Compute Time (milliseconds): " << (double)chrono::duration_cast<chrono::microseconds>(end-start).count()/1000 << endl;

                /* ----------------------------- DATA DUMP ----------------------------- */

                // Print output
                // write_data_file("regridding");
                #ifdef WRITE_HEX
                    write_hex_file(operation_type, true, 0, 8, 0, 8);
                #endif
                write_mat_files();

                /* --------------------------------------------------------------------- */
            } else if(operation_type == "gridding") {
                nufft_type_adj = true;
                grid_dim = 768;
                tile_dim = (grid_dim/ACCEL_DIM);
                fft_len = pow(2, ceil(log2(grid_dim)));

                /* --------------------------------------------------------------------- */
                cout << "***Beginning Gridding Computation***" << endl;

                // Start recording the elapsed time
                auto start = chrono::steady_clock::now();

                /* ---------------------------- COMPUTATION ---------------------------- */

                #ifdef WRITE_HEX
                    write_hex_file(operation_type, false, 0, 8, 0, 8);
                #endif

                numThreads = NUM_THREADS;

                // Run kernel to interpolate values
                #pragma vector aligned
                for(int x = 0; x < ACCEL_DIM; ++x) {
                    for(int y = 0; y < ACCEL_DIM; ++y) {
                        threadIdx = (PointArgs*)malloc(sizeof(PointArgs));
                        threadIdx->tidx = x;
                        threadIdx->tidy = y;
                        threadIdx->nufft_type_adj = nufft_type_adj;
                        ret = pthread_create(&threads[x*y+y], NULL, gridding, (void*)threadIdx);
                        assert(ret == 0);
                    }
                }

                // Wait for threads to complete
                while(numThreads > 0);

                /* --------------------------------------------------------------------- */

                // Stop recording the elapsed time
                auto end = chrono::steady_clock::now();
                cout << "Gridding Compute Time (milliseconds): " << (double)chrono::duration_cast<chrono::microseconds>(end-start).count()/1000 << endl;

                /* ----------------------------- DATA DUMP ----------------------------- */

                // Print output
                // write_data_file("gridding");
                #ifdef WRITE_HEX
                    write_hex_file(operation_type, true, 0, 8, 0, 8);
                #endif
                write_mat_files();

                /* --------------------------------------------------------------------- */
            } else if(operation_type == "ifft2") {
                nufft_type_adj = true;
                grid_dim = 768;
                tile_dim = (grid_dim/ACCEL_DIM);
                fft_len = pow(2, ceil(log2(grid_dim)));

                /* --------------------------------------------------------------------- */

                cout << "***Beginning IFFT2 Computation***" << endl;

                // Start recording the elapsed time
                auto start = chrono::steady_clock::now();

                /* ---------------------------- COMPUTATION ---------------------------- */

                #ifdef WRITE_HEX
                    write_hex_file(operation_type, false, 0, 8, 0, 8);
                #endif

                fft2_ifft2(nufft_type_adj);

                /* --------------------------------------------------------------------- */

                // Stop recording the elapsed time
                auto end = chrono::steady_clock::now();
                cout << "IFFT2 Compute Time (milliseconds): " << (double)chrono::duration_cast<chrono::microseconds>(end-start).count()/1000 << endl;

                /* ----------------------------- DATA DUMP ----------------------------- */

                // Print output
                // write_data_file("ifft2");
                #ifdef WRITE_HEX
                    write_hex_file(operation_type, true, 0, 8, 0, 8);
                #endif
                write_mat_files();

                /* --------------------------------------------------------------------- */
            } else {
                cout << "Invalid Operation Type" << endl;
            }


            // Free up allocated memory
            pthread_mutex_destroy(&ThreadLock);
            _mm_free(weight_data_real_arr);
            _mm_free(weight_data_imag_arr);
            _mm_free(nonuniform_data_real_arr);
            _mm_free(nonuniform_data_imag_arr);
            _mm_free(nonuniform_x_coord_arr);
            _mm_free(nonuniform_y_coord_arr);
            _mm_free(uniform_data_tiled_real_arr);
            _mm_free(uniform_data_tiled_imag_arr);
            usleep(50000);

            cout << operation_type << " complete!" << endl;
        }
    }
}
