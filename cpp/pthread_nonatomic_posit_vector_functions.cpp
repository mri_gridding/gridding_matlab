#include <stdio.h>
#include <math.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <chrono>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>
#include <unistd.h>
#include <mm_malloc.h>
#include <mat.h>
#include <cstdio>
#include <vector>
#include <bitset>
#include "posit.h"

#define N_data 32
#define ES_data 3
#define N_table 32
#define ES_table 3
#define MEM_DIM 1024
#define TABLE_VALUES_OVERSAMP_FACTOR 32
#define ACCEL_DIM 8
#define MEM_TILE_DIM (MEM_DIM/ACCEL_DIM)
#define INTERP_WIN_DIM 6
#define NUM_POINTS_PER_THREAD (MEM_DIM*MEM_DIM/(ACCEL_DIM*ACCEL_DIM))
#define NUM_BLOCKS 1 //must be >= 1
#define NUM_INPUTS_PER_THREAD num_nonuniform_data_real_doubles/NUM_BLOCKS


#define NUM_THREADS ((ACCEL_DIM*ACCEL_DIM)*NUM_BLOCKS) // Number of threads

using namespace std;

bool nufft_type_adj; // true for adj, false for forw

struct complex_double {
    double real;
    double imag;
};

struct complex_posit {
    Posit real = Posit(N_data, ES_data);
    Posit imag = Posit(N_data, ES_data);
};

struct complex_posit_table {
    Posit real = Posit(N_table, ES_table);
    Posit imag = Posit(N_table, ES_table);
};

struct input_sample {
    complex_posit val;
    double x_coord;
    double y_coord;
};

struct PointArgs {
    bool nufft_type_adj;
    uint tidx;
    uint tidy;
    uint tidz;
};

pthread_mutex_t ThreadLock; /* mutex */
volatile int numThreads;

// Global Variables
int numThreadLoops; // Number of threads for each calculation

// Declare pointers for input data arrays
vector<input_sample> nonuniform_data_arr;
vector<complex_posit_table> weight_arr;

// Declare pointers for output data arrays
vector<vector<vector<complex_posit>>> uniform_data_arr;

// Declare variables for storing the number of elements per file
int num_weight_data_real_doubles;
int num_weight_data_imag_doubles;
int num_nonuniform_data_real_doubles;
int num_nonuniform_data_imag_doubles;
int num_nonuniform_x_coord_doubles;
int num_nonuniform_y_coord_doubles;
int num_uniform_data_tiled_real_doubles;
int num_uniform_data_tiled_imag_doubles;

// Declare variables for "constants" that require modification based on operation type
int grid_dim;
int tile_dim;
int fft_len;


// void write_data_file(string operation_type) {
//     string filename = "cpu_" + operation_type + "_posit.data";
//     FILE *output_file = fopen(filename.c_str(), "w");

//     if(operation_type == "regridding") {
//         for(int idx = 0; idx < num_nonuniform_data_real_doubles; ++idx) {
//             if(nonuniform_data_arr.at(idx).val.real.getDouble() == 0)
//                 fprintf(output_file, "%.14f", nonuniform_data_arr.at(idx).val.real.getDouble());
//             else
//                 fprintf(output_file, "%.14e", nonuniform_data_arr.at(idx).val.real.getDouble());
//             if(nonuniform_data_arr.at(idx).val.imag.getDouble() == 0)
//                 fprintf(output_file, " + %.14fi\n", nonuniform_data_arr.at(idx).val.imag.getDouble());
//             else if(nonuniform_data_arr.at(idx).val.imag.getDouble() > 0)
//                 fprintf(output_file, " + %.14ei\n", nonuniform_data_arr.at(idx).val.imag.getDouble());
//             else
//                 fprintf(output_file, " - %.14ei\n", abs(nonuniform_data_arr.at(idx).val.imag.getDouble()));
//             // if((nonuniform_data_arr.at(idx).val.real.getDouble() != 0 || nonuniform_data_arr.at(idx).val.imag.getDouble() != 0) && z<100) {
//             //     // fwrite(&nonuniform_data_arr.at(idx).val, sizeof(complex_double), 1, output_file);
//             //     printf("[%d][%d][%d], real=%.14e, imag=%.14e\n", x, y, z, nonuniform_data_arr.at(idx).val.real.getDouble(), nonuniform_data_arr.at(idx).val.imag.getDouble());
//             //     // fprintf(output_file, "[%d][%d][%d], real=%.14e, imag=%.14e\n", x, y, z, nonuniform_data_arr.at(idx).val.real, nonuniform_data_arr.at(idx).val.imag); // this is so the value "0" doesn't get represented as "0.00000000000000e-000"
//             // }
//         }
//     } else {
//         int CHECK_X = 0;
//         int CHECK_Y = 0;
//         for(int x = 0; x < ACCEL_DIM; ++x) {
//             for(int y = 0; y < ACCEL_DIM; ++y) {
//                 for(int z = 0; z < NUM_POINTS_PER_THREAD; ++z) {
//                     int tid = x*ACCEL_DIM*ACCEL_DIM + y * ACCEL_DIM + z;
//                     if(x==CHECK_X && y==CHECK_Y) {
//                         if(uniform_data_arr.at(x).at(y).at(z).real.getDouble() == 0)
//                             fprintf(output_file, "%.14f", uniform_data_arr.at(x).at(y).at(z).real.getDouble());
//                         else
//                             fprintf(output_file, "%.14e", uniform_data_arr.at(x).at(y).at(z).real.getDouble());
//                         if(uniform_data_arr.at(x).at(y).at(z).imag.getDouble() == 0)
//                             fprintf(output_file, " + %.14fi\n", uniform_data_arr.at(x).at(y).at(z).imag.getDouble());
//                         else if(uniform_data_arr.at(x).at(y).at(z).imag.getDouble() > 0)
//                             fprintf(output_file, " + %.14ei\n", uniform_data_arr.at(x).at(y).at(z).imag.getDouble());
//                         else
//                             fprintf(output_file, " - %.14ei\n", abs(uniform_data_arr.at(x).at(y).at(z).imag.getDouble()));
//                         // if((uniform_data_arr.at(x).at(y).at(z).real.getDouble() != 0 || uniform_data_arr.at(x).at(y).at(z).imag.getDouble() != 0) && z<100) {
//                         //     // fwrite(&uniform_data_arr.at(x).at(y).at(z), sizeof(complex_double), 1, output_file);
//                         //     printf("[%d][%d][%d], real=%.14e, imag=%.14e\n", x, y, z, uniform_data_arr.at(x).at(y).at(z).real.getDouble(), uniform_data_arr.at(x).at(y).at(z).imag.getDouble());
//                         //     // fprintf(output_file, "[%d][%d][%d], real=%.14e, imag=%.14e\n", x, y, z, uniform_data_arr.at(x).at(y).at(z).real, uniform_data_arr.at(x).at(y).at(z).imag); // this is so the value "0" doesn't get represented as "0.00000000000000e-000"
//                         // }
//                     }
//                 }
//             }
//         }
//     }
//     fclose(output_file);
// }


void write_mat_files() {
    // Write the uniform data to a matfile
    double *reals = (double*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(double), 64);
    double *imags = (double*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(double), 64);
    for(int idx = 0; idx < num_nonuniform_data_real_doubles; ++idx) {
        reals[idx] = nonuniform_data_arr.at(idx).val.real.getDouble();
        imags[idx] = nonuniform_data_arr.at(idx).val.imag.getDouble();
    }
    MATFile *pmat;
    mxArray *paReal;
    mxArray *paImag;
    int status1;
    int status2;
    string filename = "data/cpp/tmp/posit_nonuniform_data.mat";
    printf("Creating file %s...\n\n", filename.c_str());
    pmat = matOpen(filename.c_str(), "w");
    if (pmat == NULL) {
        printf("Error creating file %s\n", filename.c_str());
        printf("(Do you have write permission in this directory?)\n");
    } else {
        paReal = mxCreateDoubleMatrix(num_nonuniform_data_real_doubles, 1, mxREAL);
        paImag = mxCreateDoubleMatrix(num_nonuniform_data_real_doubles, 1, mxREAL);
        if (paReal == NULL || paImag == NULL) {
            printf("%s : Out of memory on line %d\n", __FILE__, __LINE__);
            printf("Unable to create mxArray.\n");
        } else {
            memcpy((void *)(mxGetPr(paReal)), (double*)reals, num_nonuniform_data_real_doubles * 8);
            memcpy((void *)(mxGetPr(paImag)), (double*)imags, num_nonuniform_data_real_doubles * 8);
            status1 = matPutVariableAsGlobal(pmat, "nonuniform_data_real", paReal);
            status2 = matPutVariableAsGlobal(pmat, "nonuniform_data_imag", paImag);
            if (status1 != 0 || status2 != 0) {
                printf("Error using matPutVariableAsGlobal\n");
            }
        }
        mxDestroyArray(paReal);
        mxDestroyArray(paImag);
    }
    _mm_free(reals);
    _mm_free(imags);


    // Write the uniform data to a matfile
    reals = (double*)_mm_malloc(num_uniform_data_tiled_real_doubles * sizeof(double), 64);
    imags = (double*)_mm_malloc(num_uniform_data_tiled_real_doubles * sizeof(double), 64);
    for(int x = 0; x < ACCEL_DIM; ++x) {
        for(int y = 0; y < ACCEL_DIM; ++y) {
            for(int z = 0; z < (num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM); ++z) {
                int tid = ((y*ACCEL_DIM*(num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM)) + (x*(num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM))) + z;
                reals[tid] = uniform_data_arr.at(x).at(y).at(z).real.getDouble();
                imags[tid] = uniform_data_arr.at(x).at(y).at(z).imag.getDouble();
            }
        }
    }
    pmat;
    paReal;
    paImag;
    status1;
    status2;
    filename = "data/cpp/tmp/posit_uniform_data.mat";
    printf("Creating file %s...\n\n", filename.c_str());
    pmat = matOpen(filename.c_str(), "w");
    if (pmat == NULL) {
        printf("Error creating file %s\n", filename.c_str());
        printf("(Do you have write permission in this directory?)\n");
    } else {
        paReal = mxCreateDoubleMatrix(num_uniform_data_tiled_real_doubles, 1, mxREAL);
        paImag = mxCreateDoubleMatrix(num_uniform_data_tiled_real_doubles, 1, mxREAL);
        if (paReal == NULL || paImag == NULL) {
            printf("%s : Out of memory on line %d\n", __FILE__, __LINE__);
            printf("Unable to create mxArray.\n");
        } else {
            memcpy((void *)(mxGetPr(paReal)), (double*)reals, num_uniform_data_tiled_real_doubles * 8);
            memcpy((void *)(mxGetPr(paImag)), (double*)imags, num_uniform_data_tiled_real_doubles * 8);
            status1 = matPutVariableAsGlobal(pmat, "uniform_data_tiled_real", paReal);
            status2 = matPutVariableAsGlobal(pmat, "uniform_data_tiled_imag", paImag);
            if (status1 != 0 || status2 != 0) {
                printf("Error using matPutVariableAsGlobal\n");
            }
        }
        mxDestroyArray(paReal);
        mxDestroyArray(paImag);
    }
    _mm_free(reals);
    _mm_free(imags);
}


complex_posit swap_complex_components(complex_posit datum) {
    complex_posit tmp;
    tmp.real = datum.imag;
    tmp.imag = datum.real;

    return tmp;
}


//////////////////////////////////////////////
//                                          //
//                 Gridding                 //
//                                          //
//////////////////////////////////////////////
void *gridding(void *pointarg) {
    // Calculate the global thread indexes in each direction (relative indexes)
    int tidx = ((struct PointArgs*)pointarg)->tidx;
    int tidy = ((struct PointArgs*)pointarg)->tidy;
    int tidz = ((struct PointArgs*)pointarg)->tidz;
    int tid = tidz*ACCEL_DIM*ACCEL_DIM + tidy * ACCEL_DIM + tidx;

    bool nufft_type_adj = ((struct PointArgs*)pointarg)->nufft_type_adj;

    int num_points = num_points = NUM_INPUTS_PER_THREAD;
    if(num_nonuniform_data_real_doubles % NUM_BLOCKS != 0)
        num_points = NUM_INPUTS_PER_THREAD+1;

    int start = tidz * num_points;
    int end = num_nonuniform_data_real_doubles; //start + num_points;

    // printf("Hello from slice %d, pipeline (%d, %d); start=%d, end=%d\n", tidz, tidx, tidy, start, end);

    #pragma vector aligned
    for(int input_point_idx = start; input_point_idx < end; ++input_point_idx) {
        // Extract the current point being processed
        input_sample current_sample = nonuniform_data_arr.at(input_point_idx);


        //////////////////////////////////////////////
        //                                          //
        //                  Select                  //
        //                                          //
        //////////////////////////////////////////////
        // Extract the "relative" coordinates (within a tile)
        double x_coord = fmod(current_sample.x_coord, (double)ACCEL_DIM);
        double y_coord = fmod(current_sample.y_coord, (double)ACCEL_DIM);

        // Determine if this pipeline is affected by this source point
        double x_distance = fmod(ACCEL_DIM+x_coord-tidx, (double)ACCEL_DIM); // rem is implemented as an upper truncation
        double y_distance = fmod(ACCEL_DIM+y_coord-tidy, (double)ACCEL_DIM); // rem is implemented as an upper truncation

        // Check if the point lies within the interpolation window
        if(x_distance < INTERP_WIN_DIM && y_distance < INTERP_WIN_DIM) {
            // Get the quotient and remainder to determine grid and tile location
            int x_base_tile_idx = current_sample.x_coord/ACCEL_DIM; // x virtual tile index
            int y_base_tile_idx = current_sample.y_coord/ACCEL_DIM; // y virtual tile index

            // This point affects this pipeline!
            // Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
            int x_tile_idx;
            if(x_coord < tidx) {
               // A wrap occured!
                if(x_base_tile_idx == 0) {
                    x_tile_idx = tile_dim - 1;
                } else {
                    x_tile_idx = x_base_tile_idx - 1;
                }
            } else {
                x_tile_idx = x_base_tile_idx;
            }

            // Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
            int y_tile_idx;
            if(y_coord < tidy) {
               // A wrap occured!
                if(y_base_tile_idx == 0) {
                    y_tile_idx = tile_dim - 1;
                } else {
                    y_tile_idx = y_base_tile_idx - 1;
                }
            } else {
                y_tile_idx = y_base_tile_idx;
            }

            // Calculate the tile index ("depth" in the pipeline's data array)
            int tile_idx = y_tile_idx * MEM_TILE_DIM + x_tile_idx; // 1D global virtual tile index

            // Calculate the table index
            int x_table_idx = ((x_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift
            int y_table_idx = ((y_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift


            //////////////////////////////////////////////
            //      ****TESTING PURPOSES ONLY****       //
            //     Precomputed Interp Table Values      //
            //      ****TESTING PURPOSES ONLY****       //
            //////////////////////////////////////////////
            int table_tid = (x_table_idx * 193) + y_table_idx;
            Posit R1 = weight_arr.at(table_tid).real;
            Posit I1 = weight_arr.at(table_tid).imag;

            // //////////////////////////////////////////////
            // //                                          //
            // //               Interp Table               //
            // //                                          //
            // //////////////////////////////////////////////
            // // Extract the interp table values
            // complex_posit_table x_table_val = weight_arr.at(x_table_idx);
            // complex_posit_table y_table_val = weight_arr.at(y_table_idx);

            // // Calculate 2D table value (product of X and Y values)
            // Posit k1 = Posit(N_table, ES_table);
            // k1.set(x_table_val.real + x_table_val.imag);
            // Posit k2 = Posit(N_table, ES_table);
            // k2.set(y_table_val.imag - y_table_val.real);
            // Posit k3 = Posit(N_table, ES_table);
            // k3.set(y_table_val.real + y_table_val.imag);
            // Posit k4 = Posit(N_table, ES_table);
            // k4.set(y_table_val.real * k1);
            // Posit k5 = Posit(N_table, ES_table);
            // k5.set(x_table_val.real * k2);
            // Posit k6 = Posit(N_table, ES_table);
            // k6.set(x_table_val.imag * k3);
            // Posit R1 = Posit(N_table, ES_table);
            // R1.set(k4 - k6);
            // Posit I1 = Posit(N_table, ES_table);
            // I1.set(k4 + k5);


            // Get the conjugate of the table value (invert sign of I1)
            I1 = -I1;

            // Extract the grid point being processed
            complex_posit input_point_data = current_sample.val;


            //////////////////////////////////////////////
            //                                          //
            //               Interpolation              //
            //                                          //
            //////////////////////////////////////////////
            // Calculate interpolation result; conjugate of interp table value incorporated into this calculation directly by flipping sign for I1
            Posit k7 = Posit(N_data, ES_data);
            k7 = R1 + I1;
            Posit k8 = Posit(N_data, ES_data);
            k8 = input_point_data.imag - input_point_data.real;
            Posit k9 = Posit(N_data, ES_data);
            k9 = input_point_data.real + input_point_data.imag;
            Posit k10 = Posit(N_data, ES_data);
            k10 = current_sample.val.real * k7;
            Posit k11 = Posit(N_data, ES_data);
            k11 = R1 * k8;
            Posit k12 = Posit(N_data, ES_data);
            k12 = I1 * k9;
            Posit R2 = Posit(N_data, ES_data);
            R2 = k10 - k12;
            Posit I2 = Posit(N_data, ES_data);
            I2 = k10 + k11;


            //////////////////////////////////////////////
            //                                          //
            //               Accumulation               //
            //                                          //
            //////////////////////////////////////////////
            uniform_data_arr.at(tidx).at(tidy).at(tile_idx).real = uniform_data_arr.at(tidx).at(tidy).at(tile_idx).real + R2;
            uniform_data_arr.at(tidx).at(tidy).at(tile_idx).imag = uniform_data_arr.at(tidx).at(tidy).at(tile_idx).imag + I2;
        }
    }

    pthread_mutex_lock(&ThreadLock); /* Get the thread lock */
    numThreads--;
    pthread_mutex_unlock(&ThreadLock); /* Release the lock */

    return nullptr;
}


//////////////////////////////////////////////
//                                          //
//                FFT2/IFFT2                //
//                                          //
//////////////////////////////////////////////
void fft2_ifft2(bool nufft_type_adj) {
    // FILE *output_file = fopen("random_cpu.out", "w");
    int tid, tid_offset;

    // Do an FFT/IFFT of each "col" (cols are stacked)
    int start_val = 0;
    int step_val = 1;
    int end_val = fft_len/ACCEL_DIM;
    int offset = fft_len/2/ACCEL_DIM;
    int comb_pipe_offset = fft_len/2;
    uint w_mask = 0;
    int bit_shift_offset = log2(MEM_DIM)-log2(fft_len);
    int num_stages = log2(fft_len);

    for(int stage = 0; stage < num_stages; ++stage) { // stages of transformation
        // Loop over accelerator pipelines to process input point
        for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; ++x_accel_idx) {
            for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; ++y_accel_idx) {
                tid = ((y_accel_idx * ACCEL_DIM + x_accel_idx) * NUM_POINTS_PER_THREAD);
                tid_offset = tid * NUM_POINTS_PER_THREAD;

                if(stage < num_stages-3) {
                    // Loop over all columns before going to next stage
                    for(int set_idx = 0; set_idx < fft_len/ACCEL_DIM; ++set_idx) {
                        int set_offset = set_idx * MEM_TILE_DIM;

                        int iter1 = start_val+offset;
                        int iter2 = start_val;
                        int saved_val = iter1;

                        while(iter1 < end_val) {
                            int tile_idx_1 = iter1+set_offset;
                            int tile_idx_2 = iter2+set_offset;

                            if(iter2+step_val == saved_val) {
                                iter1 = iter1+step_val+offset;
                                iter2 = iter2+step_val+offset;
                                saved_val = iter1;
                            } else {
                                iter1 = iter1+step_val;
                                iter2 = iter2+step_val;
                            }

                            // tile_idx_2[6:(7-stage)] for stage > 0; else 0 (6:7 is zero bits)
                            // bitshift(bitand(tile_idx_2, w_mask), -(7-(stage+bit_shift_offset)));
                            int w_addr = (tile_idx_2 & w_mask) >> (7-(stage+bit_shift_offset));

                            complex_posit_table w = weight_arr.at(w_addr);

                            complex_posit val1 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1);
                            complex_posit val2 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_2);
                            if(nufft_type_adj) {
                                val1 = swap_complex_components(val1);
                                val2 = swap_complex_components(val2);
                            }

                            Posit k13 = Posit(N_data, ES_data);
                            k13 = w.real + w.imag;
                            Posit k14 = Posit(N_data, ES_data);
                            k14 = val1.imag - val1.real;
                            Posit k15 = Posit(N_data, ES_data);
                            k15 = val1.real + val1.imag;
                            Posit k16 = Posit(N_data, ES_data);
                            k16 = val1.real * k13;
                            Posit k17 = Posit(N_data, ES_data);
                            k17 = w.real * k14;
                            Posit k18 = Posit(N_data, ES_data);
                            k18 = w.imag * k15;
                            Posit R3 = Posit(N_data, ES_data);
                            R3 = k16 - k18;
                            Posit I3 = Posit(N_data, ES_data);
                            I3 = k16 + k17;

                            complex_posit a;
                            a.real = val2.real-R3; // 1-st part of the "butterfly" creating operation
                            a.imag = val2.imag-I3; // 1-st part of the "butterfly" creating operation
                            complex_posit b;
                            b.real = val2.real+R3; // 2-nd part of the "butterfly" creating operation
                            b.imag = val2.imag+I3; // 2-nd part of the "butterfly" creating operation

                            // fprintf(output_file,"a_real=%f\ta_imag=%f\tb_real=%f\tb_imag=%f\n", a.real.getDouble(), a.imag.getDouble(), b.real.getDouble(), b.imag.getDouble());
                            // fprintf(output_file,"start_val=%d\tstep_val=%d\tend_val=%d\toffset=%d\tcomb_pipe_offset=%d\tw_mask=%d\tbit_shift_offset=%d\tnum_stages=%d\tstage=%d\tx_accel_idx=%d\ty_accel_idx=%d\tset_idx=%d\tset_offset=%d\titer1=%d\titer2=%d\tsaved_val=%d\ttile_idx_1=%d\ttile_idx_2=%d\tw_addr=%d\n", start_val, step_val, end_val, offset, comb_pipe_offset, w_mask, bit_shift_offset, num_stages, stage, x_accel_idx, y_accel_idx, set_idx, set_offset, iter1, iter2, saved_val, tile_idx_1, tile_idx_2, w_addr);
                            // fprintf(output_file,"w_real=%.5f\tw_imag=%.5f\n", w.real.getDouble(), w.imag.getDouble());
                            // fprintf(output_file,"stage=%d\tval1_real=%.10f\tval1_imag=%.10f\n", stage, val1.real.getDouble(), val1.imag.getDouble());
                            // if(stage == 0) {
                            //     fprintf(output_file,"stage=%d\tval1_real=%.10f\tval1_imag=%.10f\tval2_real=%.10f\tval2_imag=%.10f\ta_real=%.10f\ta_imag=%.10f\tb_real=%.10f\tb_imag=%.10f\n", stage, val1.real.getDouble(), val1.imag.getDouble(), val2.real.getDouble(), val2.imag.getDouble(), a.real.getDouble(), a.imag.getDouble(), b.real.getDouble(), b.imag.getDouble());
                            // }

                            if(nufft_type_adj) {
                                a = swap_complex_components(a);
                                b = swap_complex_components(b);
                            }
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1) = a;
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_2) = b;
                        }
                    }
                } else if(((x_accel_idx >> ((num_stages-1)-stage)) & 1U) == 1) { // num_stages instead of num_stages-1 because the first index is 1 in Matlab, not 0
                    // Loop over all columns before going to next stage
                    for(int set_idx = 0; set_idx < fft_len/ACCEL_DIM; ++set_idx) {
                        int set_offset = set_idx * MEM_TILE_DIM;

                        int iter1 = start_val+offset;
                        int iter2 = start_val;
                        int saved_val = iter1;

                        while(iter1 < end_val) {
                            int tile_idx_1 = iter1+set_offset;
                            int tile_idx_2 = iter2+set_offset;

                            if(iter2+step_val == saved_val) {
                                iter1 = iter1+step_val+offset;
                                iter2 = iter2+step_val+offset;
                                saved_val = iter1;
                            } else {
                                iter1 = iter1+step_val;
                                iter2 = iter2+step_val;
                            }

                            // Stage 7: tile_idx_2[6:(7-stage)]. Stage 8: Concatenate tile_idx_2[6:0] and x_accel_idx[2]. Stage 9: Concatenate tile_idx_2[6:0] and x_accel_idx[2:1]
                            // w_addr = bitshift(bitand(tile_idx_2, w_mask), -(7-(stage+bit_shift_offset))); % tile_idx_2[6:0] shifted to the left (for stage > 7)
                            // append_bits = bitshift(bitand(int32(x_accel_idx), 6), -comb_pipe_offset); % x_accel_idx[2:1] shifted to the right
                            // w_addr = bitor(w_addr, append_bits); % [tile_idx_2[6:0] x_accel_idx[2:1]]
                            int w_addr = (tile_idx_2 & w_mask) << ((stage+bit_shift_offset)-7); // tile_idx_2[6:0] shifted to the left (for stage > 7)
                            int append_bits = (x_accel_idx & 6) >> comb_pipe_offset; // x_accel_idx[2:1] shifted to the right
                            w_addr = (w_addr | append_bits); // [tile_idx_2[6:0] x_accel_idx[2:1]]

                            complex_posit_table w = weight_arr.at(w_addr);

                            complex_posit val1 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1);
                            complex_posit val2 = uniform_data_arr.at(x_accel_idx-comb_pipe_offset).at(y_accel_idx).at(tile_idx_2);
                            if(nufft_type_adj) {
                                val1 = swap_complex_components(val1);
                                val2 = swap_complex_components(val2);
                            }

                            Posit k13 = Posit(N_data, ES_data);
                            k13 = w.real + w.imag;
                            Posit k14 = Posit(N_data, ES_data);
                            k14 = val1.imag - val1.real;
                            Posit k15 = Posit(N_data, ES_data);
                            k15 = val1.real + val1.imag;
                            Posit k16 = Posit(N_data, ES_data);
                            k16 = val1.real * k13;
                            Posit k17 = Posit(N_data, ES_data);
                            k17 = w.real * k14;
                            Posit k18 = Posit(N_data, ES_data);
                            k18 = w.imag * k15;
                            Posit R3 = Posit(N_data, ES_data);
                            R3 = k16 - k18;
                            Posit I3 = Posit(N_data, ES_data);
                            I3 = k16 + k17;

                            complex_posit a;
                            a.real = val2.real-R3; // 1-st part of the "butterfly" creating operation
                            a.imag = val2.imag-I3; // 1-st part of the "butterfly" creating operation
                            complex_posit b;
                            b.real = val2.real+R3; // 2-nd part of the "butterfly" creating operation
                            b.imag = val2.imag+I3; // 2-nd part of the "butterfly" creating operation

                            // fprintf(output_file,"a_real=%f\ta_imag=%f\tb_real=%f\tb_imag=%f\n", a.real.getDouble(), a.imag.getDouble(), b.real.getDouble(), b.imag.getDouble());
                            // fprintf(output_file,"start_val=%d\tstep_val=%d\tend_val=%d\toffset=%d\tcomb_pipe_offset=%d\tw_mask=%d\tbit_shift_offset=%d\tnum_stages=%d\tstage=%d\tx_accel_idx=%d\ty_accel_idx=%d\tset_idx=%d\tset_offset=%d\titer1=%d\titer2=%d\tsaved_val=%d\ttile_idx_1=%d\ttile_idx_2=%d\tw_addr=%d\n", start_val, step_val, end_val, offset, comb_pipe_offset, w_mask, bit_shift_offset, num_stages, stage, x_accel_idx, y_accel_idx, set_idx, set_offset, iter1, iter2, saved_val, tile_idx_1, tile_idx_2, w_addr);
                            // fprintf(output_file,"w_real=%.5f\tw_imag=%.5f\n", w.real.getDouble(), w.imag.getDouble());
                            // fprintf(output_file,"stage=%d\tval1_real=%.10f\tval1_imag=%.10f\n", stage, val1.real.getDouble(), val1.imag.getDouble());

                            if(nufft_type_adj) {
                                a = swap_complex_components(a);
                                b = swap_complex_components(b);
                            }
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1) = a;
                            uniform_data_arr.at(x_accel_idx-comb_pipe_offset).at(y_accel_idx).at(tile_idx_2) = b;
                        }
                    }
                }
            }
        }
        offset = (int)(offset / 2);
        comb_pipe_offset = (int)(comb_pipe_offset / 2);
        // w_mask = bitset(w_mask, max(7-(stage+bit_shift_offset), 1));
        w_mask |= 1UL << max(7-(stage+bit_shift_offset)-1, 0);
    }

    // Do an FFT/IFFT of each "row" (rows are stacked)
    start_val = 0;
    step_val = 128;
    end_val = fft_len*2*ACCEL_DIM;
    offset = fft_len*ACCEL_DIM;
    comb_pipe_offset = fft_len/2;
    w_mask = 0;
    bit_shift_offset = log2(MEM_DIM)-log2(fft_len);
    num_stages = log2(fft_len);

    for(int stage = 0; stage < num_stages; ++stage) { // stages of transformation
        // Loop over accelerator pipelines to process input point
        for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; ++x_accel_idx) {
            for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; ++y_accel_idx) {
                tid = y_accel_idx * ACCEL_DIM + x_accel_idx;
                tid_offset = tid * NUM_POINTS_PER_THREAD;

                if(stage < num_stages-3) {
                    // Loop over all columns before going to next stage
                    for(int set_idx = 0; set_idx < fft_len/ACCEL_DIM; ++set_idx) {
                        int iter1 = start_val+offset;
                        int iter2 = start_val;
                        int saved_val = iter1;

                        while(iter1 < end_val) {
                            int tile_idx_1 = iter1+set_idx;
                            int tile_idx_2 = iter2+set_idx;

                            if(iter2+step_val == saved_val) {
                                iter1 = iter1+step_val+offset;
                                iter2 = iter2+step_val+offset;
                                saved_val = iter1;
                            } else {
                                iter1 = iter1+step_val;
                                iter2 = iter2+step_val;
                            }

                            // tile_idx_2[13:(14-stage)] for stage > 0; else 0 (13:14 is zero bits)
                            // w_addr = bitshift(bitand(tile_idx_2, w_mask), -(14-(stage+bit_shift_offset)));
                            int w_addr = (tile_idx_2 & w_mask) >> (14-(stage+bit_shift_offset));

                            complex_posit_table w = weight_arr.at(w_addr);

                            complex_posit val1 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1);
                            complex_posit val2 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_2);
                            if(nufft_type_adj) {
                                val1 = swap_complex_components(val1);
                                val2 = swap_complex_components(val2);
                            }

                            Posit k13 = Posit(N_data, ES_data);
                            k13 = w.real + w.imag;
                            Posit k14 = Posit(N_data, ES_data);
                            k14 = val1.imag - val1.real;
                            Posit k15 = Posit(N_data, ES_data);
                            k15 = val1.real + val1.imag;
                            Posit k16 = Posit(N_data, ES_data);
                            k16 = val1.real * k13;
                            Posit k17 = Posit(N_data, ES_data);
                            k17 = w.real * k14;
                            Posit k18 = Posit(N_data, ES_data);
                            k18 = w.imag * k15;
                            Posit R3 = Posit(N_data, ES_data);
                            R3 = k16 - k18;
                            Posit I3 = Posit(N_data, ES_data);
                            I3 = k16 + k17;

                            complex_posit a;
                            a.real = val2.real-R3; // 1-st part of the "butterfly" creating operation
                            a.imag = val2.imag-I3; // 1-st part of the "butterfly" creating operation
                            complex_posit b;
                            b.real = val2.real+R3; // 2-nd part of the "butterfly" creating operation
                            b.imag = val2.imag+I3; // 2-nd part of the "butterfly" creating operation

                            // fprintf(output_file,"a_real=%f\ta_imag=%f\tb_real=%f\tb_imag=%f\n", a.real.getDouble(), a.imag.getDouble(), b.real.getDouble(), b.imag.getDouble());
                            // fprintf(output_file,"start_val=%d\tstep_val=%d\tend_val=%d\toffset=%d\tcomb_pipe_offset=%d\tw_mask=%d\tbit_shift_offset=%d\tnum_stages=%d\tstage=%d\tx_accel_idx=%d\ty_accel_idx=%d\tset_idx=%d\titer1=%d\titer2=%d\tsaved_val=%d\ttile_idx_1=%d\ttile_idx_2=%d\tw_addr=%d\n", start_val, step_val, end_val, offset, comb_pipe_offset, w_mask, bit_shift_offset, num_stages, stage, x_accel_idx, y_accel_idx, set_idx, iter1, iter2, saved_val, tile_idx_1, tile_idx_2, w_addr);
                            // fprintf(output_file,"w_real=%.5f\tw_imag=%.5f\n", w.real.getDouble(), w.imag.getDouble());
                            // fprintf(output_file,"stage=%d\tval1_real=%.10f\tval1_imag=%.10f\n", stage, val1.real.getDouble(), val1.imag.getDouble());

                            if(nufft_type_adj) {
                                a = swap_complex_components(a);
                                b = swap_complex_components(b);
                            }
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1) = a;
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_2) = b;
                        }
                    }
                } else if(((y_accel_idx >> ((num_stages-1)-stage)) & 1U) == 1) { // num_stages instead of num_stages-1 because the first index is 1 in Matlab, not 0
                    // Loop over all columns before going to next stage
                    for(int set_idx = 0; set_idx < fft_len/ACCEL_DIM; ++set_idx) {
                        int iter1 = start_val;
                        int iter2 = start_val;
                        int saved_val = iter1;

                        while(iter1 < end_val) {
                            int tile_idx_1 = iter1+set_idx;
                            int tile_idx_2 = iter2+set_idx;

                            if(iter2+step_val == saved_val) {
                                iter1 = iter1+step_val;
                                iter2 = iter2+step_val;
                                saved_val = iter1;
                            } else {
                                iter1 = iter1+step_val;
                                iter2 = iter2+step_val;
                            }

                            // Stage 7: tile_idx_2[13:(14-stage)]. Stage 8: Concatenate tile_idx_2[13:7] and y_accel_idx[2]. Stage 9: Concatenate tile_idx_2[13:7] and y_accel_idx[2:1]
                            // w_addr = bitshift(bitshift(bitand(tile_idx_2, w_mask), -max(14-(stage+bit_shift_offset), 7)), mod(-(14-(stage+bit_shift_offset)),7)); % tile_idx_2[13:7] shifted to the left (for stage > 7)
                            // append_bits = bitshift(bitand(int32(y_accel_idx), 6), -comb_pipe_offset); % y_accel_idx[2:1] shifted to the right
                            // w_addr = bitor(w_addr, append_bits); % [tile_idx_2[13:7] y_accel_idx[2:1]]
                            int w_addr = ((tile_idx_2 & w_mask) >> max(14-(stage+bit_shift_offset), 7)) << ((stage+bit_shift_offset)-7); // tile_idx_2[13:7] shifted to the left (for stage > 7)
                            int append_bits = ((y_accel_idx & 6) >> comb_pipe_offset); // y_accel_idx[2:1] shifted to the right
                            w_addr = (w_addr | append_bits); // [tile_idx_2[13:7] y_accel_idx[2:1]]

                            complex_posit_table w = weight_arr.at(w_addr);

                            complex_posit val1 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1);
                            complex_posit val2 = uniform_data_arr.at(x_accel_idx).at(y_accel_idx-comb_pipe_offset).at(tile_idx_2);
                            if(nufft_type_adj) {
                                val1 = swap_complex_components(val1);
                                val2 = swap_complex_components(val2);
                            }

                            Posit k13 = Posit(N_data, ES_data);
                            k13 = w.real + w.imag;
                            Posit k14 = Posit(N_data, ES_data);
                            k14 = val1.imag - val1.real;
                            Posit k15 = Posit(N_data, ES_data);
                            k15 = val1.real + val1.imag;
                            Posit k16 = Posit(N_data, ES_data);
                            k16 = val1.real * k13;
                            Posit k17 = Posit(N_data, ES_data);
                            k17 = w.real * k14;
                            Posit k18 = Posit(N_data, ES_data);
                            k18 = w.imag * k15;
                            Posit R3 = Posit(N_data, ES_data);
                            R3 = k16 - k18;
                            Posit I3 = Posit(N_data, ES_data);
                            I3 = k16 + k17;

                            complex_posit a;
                            a.real = val2.real-R3; // 1-st part of the "butterfly" creating operation
                            a.imag = val2.imag-I3; // 1-st part of the "butterfly" creating operation
                            complex_posit b;
                            b.real = val2.real+R3; // 2-nd part of the "butterfly" creating operation
                            b.imag = val2.imag+I3; // 2-nd part of the "butterfly" creating operation

                            // fprintf(output_file,"a_real=%f\ta_imag=%f\tb_real=%f\tb_imag=%f\n", a.real.getDouble(), a.imag.getDouble(), b.real.getDouble(), b.imag.getDouble());
                            // fprintf(output_file,"start_val=%d\tstep_val=%d\tend_val=%d\toffset=%d\tcomb_pipe_offset=%d\tw_mask=%d\tbit_shift_offset=%d\tnum_stages=%d\tstage=%d\tx_accel_idx=%d\ty_accel_idx=%d\tset_idx=%d\titer1=%d\titer2=%d\tsaved_val=%d\ttile_idx_1=%d\ttile_idx_2=%d\tw_addr=%d\n", start_val, step_val, end_val, offset, comb_pipe_offset, w_mask, bit_shift_offset, num_stages, stage, x_accel_idx, y_accel_idx, set_idx, iter1, iter2, saved_val, tile_idx_1, tile_idx_2, w_addr);
                            // fprintf(output_file,"w_real=%.5f\tw_imag=%.5f\n", w.real.getDouble(), w.imag.getDouble());
                            // fprintf(output_file,"stage=%d\tval1_real=%.10f\tval1_imag=%.10f\n", stage, val1.real.getDouble(), val1.imag.getDouble());

                            if(nufft_type_adj) {
                                a = swap_complex_components(a);
                                b = swap_complex_components(b);
                            }
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx_1) = a;
                            uniform_data_arr.at(x_accel_idx).at(y_accel_idx-comb_pipe_offset).at(tile_idx_2) = b;
                        }
                    }
                }
            }
        }
        offset = (int)(offset / 2);
        comb_pipe_offset = (int)(comb_pipe_offset / 2);
        // w_mask = bitset(w_mask, max(14-(stage+bit_shift_offset), 1));
        w_mask |= 1UL << max(14-(stage+bit_shift_offset)-1, 0);
    }
    // fclose(output_file);
}


//////////////////////////////////////////////
//                                          //
//                Regridding                //
//                                          //
//////////////////////////////////////////////
void *regridding(void *pointarg) {
    // Calculate the global thread indexes in each direction (relative indexes)
    int threadIdx_x = ((struct PointArgs*)pointarg)->tidx;
    int threadIdx_y = ((struct PointArgs*)pointarg)->tidy;
    int threadIdx_z = ((struct PointArgs*)pointarg)->tidz;
    int tid = threadIdx_z*ACCEL_DIM*ACCEL_DIM + threadIdx_y * ACCEL_DIM + threadIdx_x;

    bool nufft_type_adj = ((struct PointArgs*)pointarg)->nufft_type_adj;

    int num_points = ceil(num_nonuniform_data_real_doubles / NUM_THREADS);
    int start = tid * num_points;
    int end = start + num_points; //start + num_points;
    if(end > num_nonuniform_data_real_doubles)
        end = num_nonuniform_data_real_doubles;

    printf("Hello from slice %d, pipeline (%d, %d); start=%d, end=%d; tid=%d, num_points=%d\n", threadIdx_z, threadIdx_x, threadIdx_y, start, end, tid, num_points);
    // printf("Hello from pipeline (%d, %d); start=%d, end=%d; tid=%d, num_points=%d\n", threadIdx_x, threadIdx_y, start, end, tid, num_points);

    #pragma vector aligned
    for(int input_point_idx = start; input_point_idx < end; ++input_point_idx) {
        for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; ++x_accel_idx) {
            for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; ++y_accel_idx) {
                //////////////////////////////////////////////
                //                                          //
                //                  Select                  //
                //                                          //
                //////////////////////////////////////////////
                // Extract the current point being processed
                input_sample current_sample = nonuniform_data_arr.at(input_point_idx);

                // Get the quotient and remainder to determine grid and tile location
                int x_base_tile_idx = current_sample.x_coord/ACCEL_DIM; // x virtual tile index
                int y_base_tile_idx = current_sample.y_coord/ACCEL_DIM; // y virtual tile index

                // Extract the "relative" coordinates (within a tile)
                double x_coord = fmod(current_sample.x_coord, (double)ACCEL_DIM);
                double y_coord = fmod(current_sample.y_coord, (double)ACCEL_DIM);

                // Determine if this pipeline is affected by this source point
                double x_distance = fmod(ACCEL_DIM+x_coord-x_accel_idx, (double)ACCEL_DIM); // rem is implemented as an upper truncation
                double y_distance = fmod(ACCEL_DIM+y_coord-y_accel_idx, (double)ACCEL_DIM); // rem is implemented as an upper truncation

                // Check if the point lies within the interpolation window
                if(x_distance < INTERP_WIN_DIM && y_distance < INTERP_WIN_DIM) {
                    // This point affects this pipeline!
                    // Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
                    int x_tile_idx;
                    if(x_coord < x_accel_idx) {
                       // A wrap occured!
                        if(x_base_tile_idx == 0) {
                            x_tile_idx = tile_dim - 1;
                        } else {
                            x_tile_idx = x_base_tile_idx - 1;
                        }
                    } else {
                        x_tile_idx = x_base_tile_idx;
                    }

                    // Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
                    int y_tile_idx;
                    if(y_coord < y_accel_idx) {
                       // A wrap occured!
                        if(y_base_tile_idx == 0) {
                            y_tile_idx = tile_dim - 1;
                        } else {
                            y_tile_idx = y_base_tile_idx - 1;
                        }
                    } else {
                        y_tile_idx = y_base_tile_idx;
                    }

                    // Calculate the tile index ("depth" in the pipeline's data array)
                    int tile_idx = y_tile_idx * MEM_TILE_DIM + x_tile_idx; // 1D global virtual tile index

                    // Calculate the table index
                    int x_table_idx = ((x_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift
                    int y_table_idx = ((y_distance * double(TABLE_VALUES_OVERSAMP_FACTOR)) + 0.5); // *L (a power of 2) is implemented as a left shift


                    //////////////////////////////////////////////
                    //      ****TESTING PURPOSES ONLY****       //
                    //     Precomputed Interp Table Values      //
                    //      ****TESTING PURPOSES ONLY****       //
                    //////////////////////////////////////////////
                    int table_tid = (x_table_idx * 193) + y_table_idx;
                    Posit R1 = weight_arr.at(table_tid).real;
                    Posit I1 = weight_arr.at(table_tid).imag;

                    //////////////////////////////////////////////
                    //                                          //
                    //               Interp Table               //
                    //                                          //
                    //////////////////////////////////////////////
                    // Extract the interp table values
                    // complex_posit_table x_table_val = weight_arr.at(x_table_idx);
                    // complex_posit_table y_table_val = weight_arr.at(y_table_idx);

                    // // Calculate 2D table value (product of X and Y values)
                    // Posit k1 = Posit(N_table, ES_table);
                    // k1.set(x_table_val.real + x_table_val.imag);
                    // Posit k2 = Posit(N_table, ES_table);
                    // k2.set(y_table_val.imag - y_table_val.real);
                    // Posit k3 = Posit(N_table, ES_table);
                    // k3.set(y_table_val.real + y_table_val.imag);
                    // Posit k4 = Posit(N_table, ES_table);
                    // k4.set(y_table_val.real * k1);
                    // Posit k5 = Posit(N_table, ES_table);
                    // k5.set(x_table_val.real * k2);
                    // Posit k6 = Posit(N_table, ES_table);
                    // k6.set(x_table_val.imag * k3);
                    // Posit R1 = Posit(N_table, ES_table);
                    // R1.set(k4 - k6);
                    // Posit I1 = Posit(N_table, ES_table);
                    // I1.set(k4 + k5);

                    // Extract the grid point being processed
                    complex_posit input_point_data = uniform_data_arr.at(x_accel_idx).at(y_accel_idx).at(tile_idx);


                    //////////////////////////////////////////////
                    //                                          //
                    //               Interpolation              //
                    //                                          //
                    //////////////////////////////////////////////
                    // Calculate interpolation result; conjugate of interp table value incorporated into this calculation directly by flipping sign for I1
                    Posit k7 = Posit(N_data, ES_data);
                    k7 = R1 + I1;
                    Posit k8 = Posit(N_data, ES_data);
                    k8 = input_point_data.imag - input_point_data.real;
                    Posit k9 = Posit(N_data, ES_data);
                    k9 = input_point_data.real + input_point_data.imag;
                    Posit k10 = Posit(N_data, ES_data);
                    k10 = input_point_data.real * k7;
                    Posit k11 = Posit(N_data, ES_data);
                    k11 = R1 * k8;
                    Posit k12 = Posit(N_data, ES_data);
                    k12 = I1 * k9;
                    Posit R2 = Posit(N_data, ES_data);
                    R2 = k10 - k12;
                    Posit I2 = Posit(N_data, ES_data);
                    I2 = k10 + k11;


                    //////////////////////////////////////////////
                    //                                          //
                    //               Accumulation               //
                    //                                          //
                    //////////////////////////////////////////////
                    nonuniform_data_arr.at(input_point_idx).val.real = nonuniform_data_arr.at(input_point_idx).val.real + R2;
                    nonuniform_data_arr.at(input_point_idx).val.imag = nonuniform_data_arr.at(input_point_idx).val.imag + I2;
                }
            }
        }
    }

    pthread_mutex_lock(&ThreadLock); /* Get the thread lock */
    numThreads--;
    pthread_mutex_unlock(&ThreadLock); /* Release the lock */

    return nullptr;
}


//////////////////////////////////////////////
//                                          //
//                   Main                   //
//                                          //
//////////////////////////////////////////////
int main(int argc, char *argv[]) {
    // Check if the correct numbe of arguments are present; argc should be 2 for correct execution
    if(argc != 10) {
        // If there are the wrong number of arguments, print usage
        printf("Error: %d arguments given, but expected 9.\n", argc);
        printf("usage: %s operation_type weight_data_real weight_data_imag nonuniform_data_real nonuniform_data_imag nonuniform_x_coord nonuniform_y_coord uniform_data_tiled_real uniform_data_tiled_imag\n", argv[0]);
    } else {
        // If the number of arguments is correct, save operation type and open files
        string operation_type = argv[1];

        ifstream weight_data_real_File(argv[2], ios::in | ios::binary | ios::ate);
        ifstream weight_data_imag_File(argv[3], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_data_real_File(argv[4], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_data_imag_File(argv[5], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_x_coord_File(argv[6], ios::in | ios::binary | ios::ate);
        ifstream nonuniform_y_coord_File(argv[7], ios::in | ios::binary | ios::ate);
        ifstream uniform_data_tiled_real_File(argv[8], ios::in | ios::binary | ios::ate);
        ifstream uniform_data_tiled_imag_File(argv[9], ios::in | ios::binary | ios::ate);

        if(!weight_data_real_File.good() || !weight_data_imag_File.good() || !nonuniform_data_real_File.good() || !nonuniform_data_imag_File.good() || !nonuniform_x_coord_File.good() || !nonuniform_y_coord_File.good() || !uniform_data_tiled_real_File.good() || !uniform_data_tiled_imag_File.good()) {
            // If file fails to open, notify user
            if(!weight_data_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[2]);
            } else {
                weight_data_real_File.close();
            }
            if(!weight_data_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[3]);
            } else {
                weight_data_imag_File.close();
            }
            if(!nonuniform_data_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[4]);
            } else {
                nonuniform_data_real_File.close();
            }
            if(!nonuniform_data_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[5]);
            } else {
                nonuniform_data_imag_File.close();
            }
            if(!nonuniform_x_coord_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[6]);
            } else {
                nonuniform_x_coord_File.close();
            }
            if(!nonuniform_y_coord_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[7]);
            } else {
                nonuniform_y_coord_File.close();
            }
            if(!uniform_data_tiled_real_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[8]);
            } else {
                uniform_data_tiled_real_File.close();
            }
            if(!uniform_data_tiled_imag_File.good()) {
                // If file fails to open, notify user
                printf("Could not open %s\n", argv[9]);
            } else {
                uniform_data_tiled_imag_File.close();
            }
        } else {
            // Get the size of the file in bytes
            streampos weight_data_real = weight_data_real_File.tellg();
            streampos weight_data_imag = weight_data_imag_File.tellg();
            streampos nonuniform_data_real = nonuniform_data_real_File.tellg();
            streampos nonuniform_data_imag = nonuniform_data_imag_File.tellg();
            streampos nonuniform_x_coord = nonuniform_x_coord_File.tellg();
            streampos nonuniform_y_coord = nonuniform_y_coord_File.tellg();
            streampos uniform_data_tiled_real = uniform_data_tiled_real_File.tellg();
            streampos uniform_data_tiled_imag = uniform_data_tiled_imag_File.tellg();


            cout << "posit info:\nN_data= " << N_data << "; ES_data= " << ES_data << "\nN_table= " << N_table << "; ES_table= " << ES_table << "\n" << endl;
            cout << "sizeof(complex_double) = " << sizeof(complex_double) << endl;
            cout << "output points = " << MEM_DIM * MEM_DIM << endl;
            cout << "threads = " << ACCEL_DIM * ACCEL_DIM << endl;
            cout << "points per thread = " << NUM_POINTS_PER_THREAD << endl;


            int num_weight_data_real_bytes = (int)weight_data_real;
            // cout << "num_weight_data_real_bytes=" << num_weight_data_real_bytes << endl;
            num_weight_data_real_doubles = (int)(weight_data_real / sizeof(double));
            cout << "num_weight_data_real_doubles=" << num_weight_data_real_doubles << endl;

            int num_weight_data_imag_bytes = (int)weight_data_imag;
            // cout << "num_weight_data_imag_bytes=" << num_weight_data_imag_bytes << endl;
            num_weight_data_imag_doubles = (int)(weight_data_imag / sizeof(double));
            cout << "num_weight_data_imag_doubles=" << num_weight_data_imag_doubles << endl;

            int num_nonuniform_data_real_bytes = (int)nonuniform_data_real;
            // cout << "num_nonuniform_data_real_bytes=" << num_nonuniform_data_real_bytes << endl;
            num_nonuniform_data_real_doubles = (int)(nonuniform_data_real / sizeof(double));
            cout << "num_nonuniform_data_real_doubles=" << num_nonuniform_data_real_doubles << endl;

            int num_nonuniform_data_imag_bytes = (int)nonuniform_data_imag;
            // cout << "num_nonuniform_data_imag_bytes=" << num_nonuniform_data_imag_bytes << endl;
            num_nonuniform_data_imag_doubles = (int)(nonuniform_data_imag / sizeof(double));
            cout << "num_nonuniform_data_imag_doubles=" << num_nonuniform_data_imag_doubles << endl;

            int num_nonuniform_x_coord_bytes = (int)nonuniform_x_coord;
            // cout << "num_nonuniform_x_coord_bytes=" << num_nonuniform_x_coord_bytes << endl;
            num_nonuniform_x_coord_doubles = (int)(nonuniform_x_coord / sizeof(double));
            cout << "num_nonuniform_x_coord_doubles=" << num_nonuniform_x_coord_doubles << endl;

            int num_nonuniform_y_coord_bytes = (int)nonuniform_y_coord;
            // cout << "num_nonuniform_y_coord_bytes=" << num_nonuniform_y_coord_bytes << endl;
            num_nonuniform_y_coord_doubles = (int)(nonuniform_y_coord / sizeof(double));
            cout << "num_nonuniform_y_coord_doubles=" << num_nonuniform_y_coord_doubles << endl;

            int num_uniform_data_tiled_real_bytes = (int)uniform_data_tiled_real;
            // cout << "num_uniform_data_tiled_real_bytes=" << num_uniform_data_tiled_real_bytes << endl;
            num_uniform_data_tiled_real_doubles = (int)(uniform_data_tiled_real / sizeof(double));
            cout << "num_uniform_data_tiled_real_doubles=" << num_uniform_data_tiled_real_doubles << endl;

            int num_uniform_data_tiled_imag_bytes = (int)uniform_data_tiled_imag;
            // cout << "num_uniform_data_tiled_imag_bytes=" << num_uniform_data_tiled_imag_bytes << endl;
            num_uniform_data_tiled_imag_doubles = (int)(uniform_data_tiled_imag / sizeof(double));
            cout << "num_uniform_data_tiled_imag_doubles=" << num_uniform_data_tiled_imag_doubles << endl;


            // Declare pointers for input data arrays
            double *weight_data_real_arr;
            double *weight_data_imag_arr;
            double *nonuniform_data_real_arr;
            double *nonuniform_data_imag_arr;
            double *nonuniform_x_coord_arr;
            double *nonuniform_y_coord_arr;
            double *uniform_data_tiled_real_arr;
            double *uniform_data_tiled_imag_arr;


            // Allocate space for the input data arrays
            weight_data_real_arr = (double*)_mm_malloc(num_weight_data_real_doubles * sizeof(double), 64);
            if(weight_data_real_arr == NULL) fprintf(stderr, "Bad malloc on weight_data_real_arr\n");
            weight_data_imag_arr = (double*)_mm_malloc(num_weight_data_imag_doubles * sizeof(double), 64);
            if(weight_data_imag_arr == NULL) fprintf(stderr, "Bad malloc on weight_data_imag_arr\n");

            nonuniform_data_real_arr = (double*)_mm_malloc(num_nonuniform_data_real_doubles * sizeof(double), 64);
            if(nonuniform_data_real_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_data_real_arr\n");
            nonuniform_data_imag_arr = (double*)_mm_malloc(num_nonuniform_data_imag_doubles * sizeof(double), 64);
            if(nonuniform_data_imag_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_data_imag_arr\n");

            nonuniform_x_coord_arr = (double*)_mm_malloc(num_nonuniform_x_coord_doubles * sizeof(double), 64);
            if(nonuniform_x_coord_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_x_coord_arr\n");
            nonuniform_y_coord_arr = (double*)_mm_malloc(num_nonuniform_y_coord_doubles * sizeof(double), 64);
            if(nonuniform_y_coord_arr == NULL) fprintf(stderr, "Bad malloc on nonuniform_y_coord_arr\n");

            uniform_data_tiled_real_arr = (double*)_mm_malloc(num_uniform_data_tiled_real_doubles * sizeof(double), 64);
            if(uniform_data_tiled_real_arr == NULL) fprintf(stderr, "Bad malloc on uniform_data_tiled_real_arr\n");
            uniform_data_tiled_imag_arr = (double*)_mm_malloc(num_uniform_data_tiled_imag_doubles * sizeof(double), 64);
            if(uniform_data_tiled_imag_arr == NULL) fprintf(stderr, "Bad malloc on uniform_data_tiled_imag_arr\n");


            // Stream the file into the input data arrays
            weight_data_real_File.seekg(0, ios::beg);
            weight_data_real_File.read((char*)weight_data_real_arr, num_weight_data_real_bytes);
            weight_data_real_File.close();

            weight_data_imag_File.seekg(0, ios::beg);
            weight_data_imag_File.read((char*)weight_data_imag_arr, num_weight_data_imag_bytes);
            weight_data_imag_File.close();

            nonuniform_data_real_File.seekg(0, ios::beg);
            nonuniform_data_real_File.read((char*)nonuniform_data_real_arr, num_nonuniform_data_real_bytes);
            nonuniform_data_real_File.close();

            nonuniform_data_imag_File.seekg(0, ios::beg);
            nonuniform_data_imag_File.read((char*)nonuniform_data_imag_arr, num_nonuniform_data_imag_bytes);
            nonuniform_data_imag_File.close();

            nonuniform_x_coord_File.seekg(0, ios::beg);
            nonuniform_x_coord_File.read((char*)nonuniform_x_coord_arr, num_nonuniform_x_coord_bytes);
            nonuniform_x_coord_File.close();

            nonuniform_y_coord_File.seekg(0, ios::beg);
            nonuniform_y_coord_File.read((char*)nonuniform_y_coord_arr, num_nonuniform_y_coord_bytes);
            nonuniform_y_coord_File.close();

            uniform_data_tiled_real_File.seekg(0, ios::beg);
            uniform_data_tiled_real_File.read((char*)uniform_data_tiled_real_arr, num_uniform_data_tiled_real_bytes);
            uniform_data_tiled_real_File.close();

            uniform_data_tiled_imag_File.seekg(0, ios::beg);
            uniform_data_tiled_imag_File.read((char*)uniform_data_tiled_imag_arr, num_uniform_data_tiled_imag_bytes);
            uniform_data_tiled_imag_File.close();


            for(int i = 0; i < num_weight_data_real_doubles; ++i) {
                complex_posit_table tmp;
                tmp.real.set(static_cast<double>(weight_data_real_arr[i]));
                tmp.imag.set(static_cast<double>(weight_data_imag_arr[i]));

                weight_arr.push_back(tmp);
            }

            for(int i = 0; i < num_nonuniform_data_real_doubles; ++i) {
                input_sample tmp;
                tmp.val.real.set(static_cast<double>(nonuniform_data_real_arr[i]));
                tmp.val.imag.set(static_cast<double>(nonuniform_data_imag_arr[i]));
                tmp.x_coord = static_cast<double>(nonuniform_x_coord_arr[i]);
                tmp.y_coord = static_cast<double>(nonuniform_y_coord_arr[i]);

                nonuniform_data_arr.push_back(tmp);
            }

            int idx = 0;
            for(int x = 0; x < ACCEL_DIM; ++x) {
                uniform_data_arr.push_back(vector<vector<complex_posit>>{ACCEL_DIM});
                for(int y = 0; y < ACCEL_DIM; ++y) {
                    uniform_data_arr.at(x).push_back(vector<complex_posit>{ACCEL_DIM});
                    for(int z = 0; z < (num_uniform_data_tiled_real_doubles / ACCEL_DIM / ACCEL_DIM); ++z) {
                        complex_posit tmp;
                        tmp.real.set(static_cast<double>(uniform_data_tiled_real_arr[idx]));
                        tmp.imag.set(static_cast<double>(uniform_data_tiled_imag_arr[idx]));

                        uniform_data_arr.at(x).at(y).push_back(tmp);

                        idx++;
                    }
                }
            }


            /* Thread Variables */
            int ret; // Return result for pthread functions
            pthread_t* threads = (pthread_t *)_mm_malloc(NUM_THREADS * sizeof(pthread_t), 64); // Allocate threads
            assert(threads != NULL); // Check that the malloc was successful
            ret = pthread_mutex_init(&ThreadLock, NULL); // Initialize the mutex
            assert(ret == 0); // Check to see that the initialization was successful

            PointArgs *threadIdx;


            if(operation_type == "fft2") {
                nufft_type_adj = false;
                grid_dim = 512;
                tile_dim = (grid_dim/ACCEL_DIM);
                fft_len = pow(2, ceil(log2(grid_dim)));

                /* --------------------------------------------------------------------- */

                cout << "***Beginning FFT2 Computation***" << endl;

                // Start recording the elapsed time
                auto start = chrono::steady_clock::now();

                /* ---------------------------- COMPUTATION ---------------------------- */

                fft2_ifft2(nufft_type_adj);

                /* --------------------------------------------------------------------- */

                // Stop recording the elapsed time
                auto end = chrono::steady_clock::now();
                cout << "FFT2 Compute Time (milliseconds): " << (double)chrono::duration_cast<chrono::microseconds>(end-start).count()/1000 << endl;

                /* ----------------------------- DATA DUMP ----------------------------- */

                // Print output
                // write_data_file("fft2");
                write_mat_files();

                /* --------------------------------------------------------------------- */
            } else if(operation_type == "regridding") {
                nufft_type_adj = false;
                grid_dim = 512;
                tile_dim = (grid_dim/ACCEL_DIM);
                fft_len = pow(2, ceil(log2(grid_dim)));

                /* --------------------------------------------------------------------- */

                cout << "***Beginning Regridding Computation***" << endl;

                // Start recording the elapsed time
                auto start = chrono::steady_clock::now();

                /* ---------------------------- COMPUTATION ---------------------------- */

                numThreads = NUM_THREADS;

                // Run kernel to interpolate values
                #pragma vector aligned
                for(int z = 0; z < NUM_BLOCKS; ++z) {
                    for(int x = 0; x < ACCEL_DIM; ++x) {
                        for(int y = 0; y < ACCEL_DIM; ++y) {
                            threadIdx = (PointArgs*)malloc(sizeof(PointArgs));
                            threadIdx->tidx = x;
                            threadIdx->tidy = y;
                            threadIdx->tidz = z;
                            threadIdx->nufft_type_adj = nufft_type_adj;
                            ret = pthread_create(&threads[x*y+y], NULL, regridding, (void*)threadIdx);
                            assert(ret == 0);
                        }
                    }
                }
                // threadIdx = (PointArgs*)malloc(sizeof(PointArgs));
                // threadIdx->tidx = 0;
                // threadIdx->tidy = 0;
                // threadIdx->tidz = 0;
                // threadIdx->nufft_type_adj = nufft_type_adj;
                // ret = pthread_create(&threads[0*0+0], NULL, regridding, (void*)threadIdx);
                // assert(ret == 0);

                // Wait for threads to complete
                while(numThreads > 0);

                /* --------------------------------------------------------------------- */

                // Stop recording the elapsed time
                auto end = chrono::steady_clock::now();
                cout << "Regridding Compute Time (milliseconds): " << (double)chrono::duration_cast<chrono::microseconds>(end-start).count()/1000 << endl;

                /* ----------------------------- DATA DUMP ----------------------------- */

                // Print output
                // write_data_file("regridding");
                write_mat_files();

                /* --------------------------------------------------------------------- */
            } else if(operation_type == "gridding") {
                nufft_type_adj = true;
                grid_dim = 768;
                tile_dim = (grid_dim/ACCEL_DIM);
                fft_len = pow(2, ceil(log2(grid_dim)));

                /* --------------------------------------------------------------------- */
                cout << "***Beginning Gridding Computation***" << endl;

                // Start recording the elapsed time
                auto start = chrono::steady_clock::now();

                /* ---------------------------- COMPUTATION ---------------------------- */

                numThreads = NUM_THREADS;

                // Run kernel to interpolate values
                #pragma vector aligned
                for(int z = 0; z < NUM_BLOCKS; ++z) {
                    for(int x = 0; x < ACCEL_DIM; ++x) {
                        for(int y = 0; y < ACCEL_DIM; ++y) {
                            threadIdx = (PointArgs*)malloc(sizeof(PointArgs));
                            threadIdx->tidx = x;
                            threadIdx->tidy = y;
                            threadIdx->tidz = z;
                            threadIdx->nufft_type_adj = nufft_type_adj;
                            ret = pthread_create(&threads[x*y+y], NULL, gridding, (void*)threadIdx);
                            assert(ret == 0);
                        }
                    }
                }

                // Wait for threads to complete
                while(numThreads > 0);

                /* --------------------------------------------------------------------- */

                // Stop recording the elapsed time
                auto end = chrono::steady_clock::now();
                cout << "Gridding Compute Time (milliseconds): " << (double)chrono::duration_cast<chrono::microseconds>(end-start).count()/1000 << endl;

                /* ----------------------------- DATA DUMP ----------------------------- */

                // Print output
                // write_data_file("gridding");
                write_mat_files();

                /* --------------------------------------------------------------------- */
            } else if(operation_type == "ifft2") {
                nufft_type_adj = true;
                grid_dim = 768;
                tile_dim = (grid_dim/ACCEL_DIM);
                fft_len = pow(2, ceil(log2(grid_dim)));

                /* --------------------------------------------------------------------- */

                cout << "***Beginning IFFT2 Computation***" << endl;

                // Start recording the elapsed time
                auto start = chrono::steady_clock::now();

                /* ---------------------------- COMPUTATION ---------------------------- */

                fft2_ifft2(nufft_type_adj);

                /* --------------------------------------------------------------------- */

                // Stop recording the elapsed time
                auto end = chrono::steady_clock::now();
                cout << "IFFT2 Compute Time (milliseconds): " << (double)chrono::duration_cast<chrono::microseconds>(end-start).count()/1000 << endl;

                /* ----------------------------- DATA DUMP ----------------------------- */

                // Print output
                // write_data_file("ifft2");
                write_mat_files();

                /* --------------------------------------------------------------------- */
            } else {
                cout << "Invalid Operation Type" << endl;
            }


            // Free up allocated memory
            pthread_mutex_destroy(&ThreadLock);
            _mm_free(weight_data_real_arr);
            _mm_free(weight_data_imag_arr);
            _mm_free(nonuniform_data_real_arr);
            _mm_free(nonuniform_data_imag_arr);
            _mm_free(nonuniform_x_coord_arr);
            _mm_free(nonuniform_y_coord_arr);
            _mm_free(uniform_data_tiled_real_arr);
            _mm_free(uniform_data_tiled_imag_arr);
            usleep(50000);

            cout << operation_type << " complete!" << endl;
        }
    }
}
