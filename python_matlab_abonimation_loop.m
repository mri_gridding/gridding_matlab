%%%%%%%%%% Initial Setup  %%%%%%%%%%
clear
run('set_paths.m')
run('irt/setup.m')

%%%%%%%%%% Python Interpolation %%%%%%%%%%
system(['"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\python.exe" kbnufft_generate_griddata_for_matlab.py']);

%%%%%%%%%% Load Python Interpolation %%%%%%%%%%
load(sprintf('python_griddat.mat'));
python_griddat = rot90(reshape(python_griddat, 768, 768));

parfor i = 6:10
    fileID = fopen(sprintf('i_%d',i),'w');
    for j = 1:3
        %%%%%%%%%% Matlab Interpolation %%%%%%%%%%
        tic
        % matlab_griddat = run_interp_adj('run_interp_adj_var.mat', 'adj', 'fixed', 16);
        % matlab_griddat = run_hardware_interp('run_interp_adj_var.mat', 'adj', 'double', 16);
        matlab_griddat = hardware_sim('run_interp_adj_var.mat', 'adj', 'fixed', 999, i, j);
        matlab_griddat = reshape(matlab_griddat, 768, 768);
        % save(sprintf('matlab_griddat.mat'), 'matlab_griddat');
        % python_griddat = reshape(python_griddat, 768, 768);
        datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF')

        %%%%%%%%%% Compare Interpolation Results %%%%%%%%%%
        % nrms(python_griddat, matlab_griddat)
        % nrms(griddat, matlab_griddat)

        %%%%%%%%%% Print Error Percentages %%%%%%%%%%
        % minmax(python_griddat)
        % minmax(matlab_griddat)
        % python_griddat_square = reshape(python_griddat, 768, 768);
        % matlab_griddat_square = reshape(matlab_griddat, 768, 768);
        % find(reshape(python_griddat_square, 768, 768)~=0)
        % python_griddat_square(find(reshape(python_griddat_square, 768, 768)~=0))'
        % find(reshape(matlab_griddat_square, 768, 768))
        % matlab_griddat_square(find(reshape(matlab_griddat_square, 768, 768)~=0))'
        nrms_error = nrms(python_griddat, matlab_griddat)*100;
        max_perc_diff = (max(abs(matlab_griddat(:)-python_griddat(:))) / max(abs(python_griddat(:))))*100;
        % isequal(python_griddat, matlab_griddat)
        % nrms(python_griddat, matlab_griddat)
        fprintf(fileID,'\n\ntest_val1: %d, test_val2: %d, Normalized Root Mean Square Error: %f%%\n', i, j, nrms_error)
        fprintf(fileID,'test_val1: %d, test_val2: %d, Maximum [Pixel] Percent Difference: %f%%\n\n\n', i, j, max_perc_diff)
    end
end
