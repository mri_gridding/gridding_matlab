function [  ] = cuda_data_dump(operation_type, input_data, nufft_type, fi_cfg, config)
%   Detailed explanation goes here
% kdat --- complex magnitude; one complex value for each tm coordinate
% dims --- dimensions of the output grid
% table_mat --- interpolation table; each dim (x or y) has one row
% numpoints --- size of the interpolation window
% Jlist --- interpolation window indices for numpoints*numpoints elements
% L --- table oversampling factor (L table points between every grid point)
% tm --- coordinates for the kdata samples
% griddat --- output grid arranged as a 1D vector

%%%%%%%%%% Assign Inputs %%%%%%%%%%
x_accel_dim    = config.hardware.x_accel_dim;
y_accel_dim    = config.hardware.y_accel_dim;
x_mem_dim      = config.hardware.x_mem_dim;
y_mem_dim      = config.hardware.y_mem_dim;
x_mem_tile_dim = config.hardware.x_mem_tile_dim;
y_mem_tile_dim = config.hardware.y_mem_tile_dim;

x_grid_dim     = config.image.x_grid_dim;
y_grid_dim     = config.image.y_grid_dim;
x_tile_dim     = config.image.x_tile_dim;
y_tile_dim     = config.image.y_tile_dim;
x_img_dim      = config.image.x_img_dim;
y_img_dim      = config.image.y_img_dim;
x_img_tile_dim = config.image.x_img_tile_dim;
y_img_tile_dim = config.image.y_img_tile_dim;
x_fft_len      = config.image.x_fft_len;
y_fft_len      = config.image.y_fft_len;

nonuniform_data  = input_data.nonuniform_data;
nonuniform_coord = input_data.nonuniform_coord;
interp_table     = input_data.table;
table_oversamp   = input_data.table_oversamp;
table_x_dim      = input_data.table_x_dim;
table_y_dim      = input_data.table_y_dim;
griddat          = input_data.griddat;

num_input_points = size(nonuniform_coord, 2);

if(x_grid_dim > x_mem_dim || y_grid_dim > y_mem_dim)
    disp('ERROR: Input dimensions don not fit inside memory!');
end

grid_data_tiled = zeros(x_accel_dim, y_accel_dim, ((x_mem_dim*y_mem_dim)/(x_accel_dim*y_accel_dim)));
grid_data       = reshape(griddat, x_grid_dim, y_grid_dim); % Reshape into a square and rotate to match hardware output

if(strcmp(nufft_type, 'forw'))
    nonuniform_data = complex(zeros(1, size(nonuniform_coord, 2)), 0);

    %%%%%%%%%% Pack Grid %%%%%%%%%%
    tic
    for i = 0:x_tile_dim-1
        for j = 0:y_tile_dim-1
            x_min = i*x_accel_dim +1;
            x_max = (i+1)*x_accel_dim;
            y_min = j*y_accel_dim +1;
            y_max = (j+1)*y_accel_dim;
            tile_num = x_mem_tile_dim*j+i +1;
            grid_data_tiled(:,:,tile_num) = grid_data(x_min:x_max, y_min:y_max);
        end
    end
else
    nonuniform_data = nonuniform_data;
end

% Pre-shift the data; this avoids the circshift previously required for grid_data (forw input, adj output)
nonuniform_coord(1, :) = rem(nonuniform_coord(1, :) + (table_x_dim/2), x_grid_dim);
nonuniform_coord(2, :) = rem(nonuniform_coord(2, :) + (table_y_dim/2), y_grid_dim);

switch operation_type
    case 'regridding'
        tic
        % Save the weights
        table_data_real = real(interp_table(1, :));
        fid = fopen(sprintf('data/gpu/regridding/weight_data_real.%d.data', size(interp_table, 2)), 'w');
        fwrite(fid, table_data_real, 'double');
        fclose(fid);
        table_data_imag = imag(interp_table(1, :));
        fid = fopen(sprintf('data/gpu/regridding/weight_data_imag.%d.data', size(interp_table, 2)), 'w');
        fwrite(fid, table_data_imag, 'double');
        fclose(fid);

        
        % Save the nonuniform data
        nonuniform_data_real = real(nonuniform_data);
        fid = fopen(sprintf('data/gpu/regridding/nonuniform_data_real.%d.data', size(nonuniform_data_real, 2)), 'w');
        fwrite(fid, (nonuniform_data_real), 'double');
        fclose(fid);
        nonuniform_data_imag = imag(nonuniform_data);
        fid = fopen(sprintf('data/gpu/regridding/nonuniform_data_imag.%d.data', size(nonuniform_data_imag, 2)), 'w');
        fwrite(fid, (nonuniform_data_imag), 'double');
        fclose(fid);

        
        % Save the nonuniform coordinates
        nonuniform_x_coord = nonuniform_coord(1, :);
        fid = fopen(sprintf('data/gpu/regridding/nonuniform_x_coord.%d.data', size(nonuniform_x_coord, 2)), 'w');
        fwrite(fid, (nonuniform_x_coord), 'double');
        fclose(fid);
        nonuniform_y_coord = nonuniform_coord(2, :);
        fid = fopen(sprintf('data/gpu/regridding/nonuniform_y_coord.%d.data', size(nonuniform_y_coord, 2)), 'w');
        fwrite(fid, (nonuniform_y_coord), 'double');
        fclose(fid);

        
        % Save the uniform grid; don't quantize because this is the output of fft2
        uniform_data_tiled_real = real(grid_data_tiled);
        fid = fopen(sprintf('data/gpu/regridding/uniform_data_tiled_real.%d.data', size(uniform_data_tiled_real, 1) * size(uniform_data_tiled_real, 2) * size(uniform_data_tiled_real, 3)), 'w');
        for x = 1:size(uniform_data_tiled_real, 1)
            for y = 1:size(uniform_data_tiled_real, 2)
                for z = 1:size(uniform_data_tiled_real, 3)
%                     if(x == 3 && y == 4)
                        fwrite(fid, uniform_data_tiled_real(x, y, z), 'double');
%                     else
%                         fwrite(fid, 0.0, 'double');
%                     end
                end
            end
        end
        fclose(fid);
        uniform_data_tiled_imag = imag(grid_data_tiled);
        fid = fopen(sprintf('data/gpu/regridding/uniform_data_tiled_imag.%d.data', size(uniform_data_tiled_imag, 1) * size(uniform_data_tiled_imag, 2) * size(uniform_data_tiled_imag, 3)), 'w');
        for x = 1:size(uniform_data_tiled_imag, 1)
            for y = 1:size(uniform_data_tiled_imag, 2)
                for z = 1:size(uniform_data_tiled_imag, 3)
%                     if(x == 3 && y == 4)
                        fwrite(fid, uniform_data_tiled_imag(x, y, z), 'double');
%                     else
%                         fwrite(fid, 0.0, 'double');
%                     end
                end
            end
        end
        fclose(fid);
        fprintf('Regridding Data Dump Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
    case 'gridding'
        tic
        % Save the weights
        table_data_real = real(interp_table(1, :));
        fid = fopen(sprintf('data/gpu/gridding/weight_data_real.%d.data', size(interp_table, 2)), 'w');
        fwrite(fid, table_data_real, 'double');
        fclose(fid);
        table_data_imag = imag(interp_table(1, :));
        fid = fopen(sprintf('data/gpu/gridding/weight_data_imag.%d.data', size(interp_table, 2)), 'w');
        fwrite(fid, table_data_imag, 'double');
        fclose(fid);

        
        % Save the nonuniform data
        nonuniform_data_real = real(nonuniform_data);
        fid = fopen(sprintf('data/gpu/gridding/nonuniform_data_real.%d.data', size(nonuniform_data_real, 2)), 'w');
        fwrite(fid, (nonuniform_data_real), 'double');
        fclose(fid);
        nonuniform_data_imag = imag(nonuniform_data);
        fid = fopen(sprintf('data/gpu/gridding/nonuniform_data_imag.%d.data', size(nonuniform_data_imag, 2)), 'w');
        fwrite(fid, (nonuniform_data_imag), 'double');
        fclose(fid);

        
        % Save the nonuniform coordinates
        nonuniform_x_coord = nonuniform_coord(1, :);
        fid = fopen(sprintf('data/gpu/gridding/nonuniform_x_coord.%d.data', size(nonuniform_x_coord, 2)), 'w');
        fwrite(fid, (nonuniform_x_coord), 'double');
        fclose(fid);
        nonuniform_y_coord = nonuniform_coord(2, :);
        fid = fopen(sprintf('data/gpu/gridding/nonuniform_y_coord.%d.data', size(nonuniform_y_coord, 2)), 'w');
        fwrite(fid, (nonuniform_y_coord), 'double');
        fclose(fid);

        
        % Save the uniform grid; don't quantize because this is all zeros
        uniform_data_tiled_real = real(grid_data_tiled);
        fid = fopen(sprintf('data/gpu/gridding/uniform_data_tiled_real.%d.data', size(uniform_data_tiled_real, 1) * size(uniform_data_tiled_real, 2) * size(uniform_data_tiled_real, 3)), 'w');
        for x = 1:size(uniform_data_tiled_real, 1)
            for y = 1:size(uniform_data_tiled_real, 2)
                for z = 1:size(uniform_data_tiled_real, 3)
                    fwrite(fid, uniform_data_tiled_real(x, y, z), 'double');
                end
            end
        end
        fclose(fid);
        uniform_data_tiled_imag = imag(grid_data_tiled);
        fid = fopen(sprintf('data/gpu/gridding/uniform_data_tiled_imag.%d.data', size(uniform_data_tiled_imag, 1) * size(uniform_data_tiled_imag, 2) * size(uniform_data_tiled_imag, 3)), 'w');
        for x = 1:size(uniform_data_tiled_imag, 1)
            for y = 1:size(uniform_data_tiled_imag, 2)
                for z = 1:size(uniform_data_tiled_imag, 3)
                    fwrite(fid, uniform_data_tiled_imag(x, y, z), 'double');
                end
            end
        end
        fclose(fid);
        fprintf('Gridding Data Dump Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
    otherwise
        error('Unidentified fi mode');
end

end
