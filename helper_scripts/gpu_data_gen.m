element_range_start = 1;
element_range_end = 103680;
% input_x_coord = input_coord(1, element_range_start:element_range_end);
% fid = fopen(sprintf('data/gpu/input_x_coord_%d_%d.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_x_coord), 'single');
% fclose(fid);
% input_y_coord = input_coord(2, element_range_start:element_range_end);
% fid = fopen(sprintf('data/gpu/input_y_coord_%d_%d.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_y_coord), 'single');
% fclose(fid);
% input_data_real = real(input_data(element_range_start:element_range_end));
% fid = fopen(sprintf('data/gpu/input_data_real_%d_%d.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_data_real), 'single');
% fclose(fid);
% input_data_imag = imag(input_data(element_range_start:element_range_end));
% fid = fopen(sprintf('data/gpu/input_data_imag_%d_%d.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_data_imag), 'single');
% fclose(fid);
% 
% 
% table_data_real = real(table_mat(1, :));
% fid = fopen(sprintf('data/gpu/table_data_real_%d.data', size(table_mat, 2)), 'w');
% fwrite(fid, (table_data_real), 'single');
% fclose(fid);
% table_data_imag = imag(table_mat(1, :));
% fid = fopen(sprintf('data/gpu/table_data_imag_%d.data', size(table_mat, 2)), 'w');
% fwrite(fid, (table_data_imag), 'single');
% fclose(fid);


% input_x_coord = tm(1, element_range_start:element_range_end);
% fid = fopen(sprintf('data/gpu/input_x_coord_%d_%d.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_x_coord), 'single');
% fclose(fid);
% input_y_coord = tm(2, element_range_start:element_range_end);
% fid = fopen(sprintf('data/gpu/input_y_coord_%d_%d.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_y_coord), 'single');
% fclose(fid);
% input_data_real = real(kdat(element_range_start:element_range_end));
% fid = fopen(sprintf('data/gpu/input_data_real_%d_%d.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_data_real), 'single');
% fclose(fid);
% input_data_imag = imag(kdat(element_range_start:element_range_end));
% fid = fopen(sprintf('data/gpu/input_data_imag_%d_%d.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_data_imag), 'single');
% fclose(fid);
% 
% 
% table_data_real = real(table_mat(1, :));
% fid = fopen(sprintf('data/gpu/table_data_real_%d.data', size(table_mat, 2)), 'w');
% fwrite(fid, (table_data_real), 'single');
% fclose(fid);
% table_data_imag = imag(table_mat(1, :));
% fid = fopen(sprintf('data/gpu/table_data_imag_%d.data', size(table_mat, 2)), 'w');
% fwrite(fid, (table_data_imag), 'single');
% fclose(fid);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%                     Fixed-Point                     %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fi_cfg.mode = 'fixed';
% fi_cfg.type = 32; % set bit width
% fi_cfg.nufft_type = 'forw'; % set interp mode ('forw' or 'adj')
% fi_cfg = fixed_point_cfg(fi_cfg);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%                     Regridding                      %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% input_coord_fixed = quantize(fi_cfg.input_coord, nonuniform_coord);
% input_coord_fixed = nonuniform_coord;
% input_x_coord = input_coord_fixed(1, element_range_start:element_range_end);
% fid = fopen(sprintf('data/gpu/input_x_coord_%d_%d_fixed.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_x_coord), 'double');
% fclose(fid);
% input_y_coord = input_coord_fixed(2, element_range_start:element_range_end);
% fid = fopen(sprintf('data/gpu/input_y_coord_%d_%d_fixed.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_y_coord), 'double');
% fclose(fid);

% table_data_real = real(interp_table(1, :));
% fid = fopen(sprintf('data/gpu/regridding_table_data_real_%d_fixed.data', size(table_data_real, 2)), 'w');
% fwrite(fid, (table_data_real), 'double');
% fclose(fid);
% table_data_imag = imag(interp_table(1, :));
% fid = fopen(sprintf('data/gpu/regridding_table_data_imag_%d_fixed.data', size(table_data_real, 2)), 'w');
% fwrite(fid, (table_data_imag), 'double');
% fclose(fid);

% % interp_table_fixed = quantize(fi_cfg.interp_table, interp_table);
% interp_table_fixed = interp_table;
% interp_table_fixed_combined = zeros(1, size(interp_table_fixed, 2) * size(interp_table_fixed, 2));
% for val1 = 0:size(interp_table_fixed, 2)-1
%     for val2 = 0:size(interp_table_fixed, 2)-1
%         % Extract the interp table values
%         x_table_val = interp_table_fixed(1, val1+1);
%         y_table_val = interp_table_fixed(2, val2+1);
% 
%         % Split the complex value into its real and imaginary components
%         x_table_val_real = real(x_table_val);
%         x_table_val_imag = imag(x_table_val);
%         y_table_val_real = real(y_table_val);
%         y_table_val_imag = imag(y_table_val);
% 
%         % Calculate 2D table value (product of X and Y values)
%         k1 = quantize(fi_cfg.k1, x_table_val_real + x_table_val_imag);
%         k2 = quantize(fi_cfg.k2, y_table_val_imag - y_table_val_real);
%         k3 = quantize(fi_cfg.k3, y_table_val_real + y_table_val_imag);
%         k4 = quantize(fi_cfg.k4, y_table_val_real * k1);
%         k5 = quantize(fi_cfg.k5, x_table_val_real * k2);
%         k6 = quantize(fi_cfg.k6, x_table_val_imag * k3);
%         R1 = quantize(fi_cfg.R1, k4 - k6);
%         I1 = quantize(fi_cfg.I1, k4 + k5);
%         
%         tid = (val1 * size(interp_table_fixed, 2)) + val2;
%         interp_table_fixed_combined(tid+1) = complex(R1, I1);
%     end
% end
% table_data_combined_real = real(interp_table_fixed_combined(1, :));
% fid = fopen(sprintf('data/gpu/regridding_table_data_real_%d_fixed.data', size(interp_table_fixed_combined, 2)), 'w');
% fwrite(fid, table_data_combined_real, 'double');
% fclose(fid);
% table_data_combined_imag = imag(interp_table_fixed_combined(1, :));
% fid = fopen(sprintf('data/gpu/regridding_table_data_imag_%d_fixed.data', size(interp_table_fixed_combined, 2)), 'w');
% fwrite(fid, table_data_combined_imag, 'double');
% fclose(fid);



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%                      Gridding                       %%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% input_data_fixed = quantize(fi_cfg.input_data, input_data);
% input_data_real = real(input_data_fixed(element_range_start:element_range_end));
% fid = fopen(sprintf('data/gpu/input_data_real_%d_%d_fixed.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_data_real), 'double');
% fclose(fid);
% input_data_imag = imag(input_data_fixed(element_range_start:element_range_end));
% fid = fopen(sprintf('data/gpu/input_data_imag_%d_%d_fixed.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_data_imag), 'double');
% fclose(fid);
% 
% 
% input_coord_fixed = quantize(fi_cfg.input_coord, nonuniform_coord);
% input_x_coord = input_coord_fixed(1, element_range_start:element_range_end);
% fid = fopen(sprintf('data/gpu/input_x_coord_%d_%d_fixed.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_x_coord), 'double');
% fclose(fid);
% input_y_coord = input_coord_fixed(2, element_range_start:element_range_end);
% fid = fopen(sprintf('data/gpu/input_y_coord_%d_%d_fixed.data', element_range_start, element_range_end), 'w');
% fwrite(fid, (input_y_coord), 'double');
% fclose(fid);
% 
% 
% interp_table_fixed = quantize(fi_cfg.interp_table, interp_table);
% table_data_real = real(interp_table_fixed(1, :));
% fid = fopen(sprintf('data/gpu/table_data_real_%d_fixed.data', size(interp_table_fixed, 2)), 'w');
% fwrite(fid, table_data_real, 'double');
% fclose(fid);
% table_data_imag = imag(interp_table_fixed(1, :));
% fid = fopen(sprintf('data/gpu/table_data_imag_%d_fixed.data', size(interp_table_fixed, 2)), 'w');
% fwrite(fid, table_data_imag, 'double');
% fclose(fid);
% 
% 
% interp_table_fixed = quantize(fi_cfg.interp_table, interp_table);
% interp_table_fixed_combined = zeros(1, size(interp_table_fixed, 2) * size(interp_table_fixed, 2));
% for val1 = 0:size(interp_table_fixed, 2)-1
%     for val2 = 0:size(interp_table_fixed, 2)-1
%         % Extract the interp table values
%         x_table_val = interp_table_fixed(1, val1+1);
%         y_table_val = interp_table_fixed(2, val2+1);
% 
%         % Split the complex value into its real and imaginary components
%         x_table_val_real = real(x_table_val);
%         x_table_val_imag = imag(x_table_val);
%         y_table_val_real = real(y_table_val);
%         y_table_val_imag = imag(y_table_val);
% 
%         % Calculate 2D table value (product of X and Y values)
%         k1 = quantize(fi_cfg.k1, x_table_val_real + x_table_val_imag);
%         k2 = quantize(fi_cfg.k2, y_table_val_imag - y_table_val_real);
%         k3 = quantize(fi_cfg.k3, y_table_val_real + y_table_val_imag);
%         k4 = quantize(fi_cfg.k4, y_table_val_real * k1);
%         k5 = quantize(fi_cfg.k5, x_table_val_real * k2);
%         k6 = quantize(fi_cfg.k6, x_table_val_imag * k3);
%         R1 = quantize(fi_cfg.R1, k4 - k6);
%         I1 = quantize(fi_cfg.I1, k4 + k5);
%         
%         tid = (val1 * size(interp_table_fixed, 2)) + val2;
%         interp_table_fixed_combined(tid+1) = complex(R1, I1);
%     end
% end
% table_data_combined_real = real(interp_table_fixed_combined(1, :));
% fid = fopen(sprintf('data/gpu/table_data_real_%d_fixed.data', size(interp_table_fixed_combined, 2)), 'w');
% fwrite(fid, table_data_combined_real, 'double');
% fclose(fid);
% table_data_combined_imag = imag(interp_table_fixed_combined(1, :));
% fid = fopen(sprintf('data/gpu/table_data_imag_%d_fixed.data', size(interp_table_fixed_combined, 2)), 'w');
% fwrite(fid, table_data_combined_imag, 'double');
% fclose(fid);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%            FFT2/IFFT2 Fixed-Point Setup             %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fi_cfg.mode = 'fixedfft';
fi_cfg.type = 32; % set bit width
fi_cfg.nufft_type = 'forw'; % set interp mode ('forw' or 'adj')
fi_cfg = fixed_point_cfg(fi_cfg);



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%                        IFFT2                        %%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% w_arr_fixed = quantize(fi_cfg.interp_table, w_arr);
% table_data_real = real(w_arr_fixed(1, :));
% fid = fopen(sprintf('data/gpu/twiddle_data_real_%d_fixed.data', size(w_arr_fixed, 2)), 'w');
% fwrite(fid, table_data_real, 'double');
% fclose(fid);
% table_data_imag = imag(w_arr_fixed(1, :));
% fid = fopen(sprintf('data/gpu/twiddle_data_imag_%d_fixed.data', size(w_arr_fixed, 2)), 'w');
% fwrite(fid, table_data_imag, 'double');
% fclose(fid);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%                        FFT2                         %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% w_arr_fixed = quantize(fi_cfg.interp_table, w_arr);
% table_data_real = real(w_arr_fixed(1, :));
% fid = fopen(sprintf('data/gpu/twiddle_data_real_%d_fixed.data', size(w_arr_fixed, 2)), 'w');
% fwrite(fid, table_data_real, 'double');
% fclose(fid);
% table_data_imag = imag(w_arr_fixed(1, :));
% fid = fopen(sprintf('data/gpu/twiddle_data_imag_%d_fixed.data', size(w_arr_fixed, 2)), 'w');
% fwrite(fid, table_data_imag, 'double');
% fclose(fid);
% 
% input_data_fixed = quantize(fi_cfg.ifft_input, data_matrix);
% input_data_real = real(input_data_fixed);
% fid = fopen(sprintf('data/gpu/fft_input_data_real_%d_%d_%d_fixed.data', size(input_data_real, 1), size(input_data_real, 2), size(input_data_real, 3)), 'w');
% for x = 1:size(input_data_real, 1)
%     for y = 1:size(input_data_real, 2)
%         for z = 1:size(input_data_real, 3)
%             fwrite(fid, input_data_real(x, y, z), 'double');
%         end
%     end
% end
% fclose(fid);
% input_data_imag = imag(input_data_fixed);
% fid = fopen(sprintf('data/gpu/fft_input_data_imag_%d_%d_%d_fixed.data', size(input_data_imag, 1), size(input_data_imag, 2), size(input_data_imag, 3)), 'w');
% for x = 1:size(input_data_imag, 1)
%     for y = 1:size(input_data_imag, 2)
%         for z = 1:size(input_data_imag, 3)
%             fwrite(fid, input_data_imag(x, y, z), 'double');
%         end
%     end
% end
% fclose(fid);


% load('cpu_fft2_posits.mat')
% fft2 = complex(fft2_real, fft2_imag);
% fft2 = reshape(fft2, [16384 8 8]);
% posit_fft2 = zeros(8, 8, 16384);
% for x = 1:8
%     for y = 1:8
%         posit_fft2(x,y,:) = fft2(:,x,y);
%     end
% end
% posit_fft2 = tile_to_matrix(posit_fft2, config);
% posit_fft2 = posit_fft2(1:config.image.x_grid_dim, 1:config.image.x_grid_dim);
% posit_fft2 = bitrevorder(bitrevorder(posit_fft2).').';
% posit_fft2 = matrix_to_tile(posit_fft2, config);
posit_fft2 = grid_data_tiled;
input_data_real = real(posit_fft2);
fid = fopen(sprintf('data/gpu/regridding_input_data_real_%d_%d_%d_fixed2.data', size(input_data_real, 1), size(input_data_real, 2), size(input_data_real, 3)), 'w');
for x = 1:size(input_data_real, 1)
    for y = 1:size(input_data_real, 2)
        for z = 1:size(input_data_real, 3)
            fwrite(fid, input_data_real(x, y, z), 'double');
        end
    end
end
fclose(fid);
input_data_imag = imag(posit_fft2);
fid = fopen(sprintf('data/gpu/regridding_input_data_imag_%d_%d_%d_fixed2.data', size(input_data_imag, 1), size(input_data_imag, 2), size(input_data_imag, 3)), 'w');
for x = 1:size(input_data_imag, 1)
    for y = 1:size(input_data_imag, 2)
        for z = 1:size(input_data_imag, 3)
            fwrite(fid, input_data_imag(x, y, z), 'double');
        end
    end
end
fclose(fid);
