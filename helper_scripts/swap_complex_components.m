function [ output ] = swap_complex_components(x)

% This function swaps the real and imaginary components of the input
output = conj(x)*1i;

end
