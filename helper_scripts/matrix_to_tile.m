% 2D Radix 2 Decimation in Time; for any length of N*M (RADIX 2 DIT)
% Input         : Normal Order
% Output        : Bit-reversed
% Unpack/resort : unpack(data_matrix) -> bitrevorder(bitrevorder(data_matrix')')
% Author        : NinjaNife

function [ data_tiles ] = matrix_to_tile( data_matrix, config )

x_accel_dim    = config.hardware.x_accel_dim;
y_accel_dim    = config.hardware.y_accel_dim;
x_mem_dim      = config.hardware.x_mem_dim;
y_mem_dim      = config.hardware.y_mem_dim;
x_mem_tile_dim = config.hardware.x_mem_tile_dim;
y_mem_tile_dim = config.hardware.y_mem_tile_dim;

x_grid_dim     = config.image.x_grid_dim;
y_grid_dim     = config.image.y_grid_dim;
x_tile_dim     = config.image.x_tile_dim;
y_tile_dim     = config.image.y_tile_dim;
x_img_dim      = config.image.x_img_dim;
y_img_dim      = config.image.y_img_dim;
x_img_tile_dim = config.image.x_img_tile_dim;
y_img_tile_dim = config.image.y_img_tile_dim;

data_matrix = padarray(data_matrix, [(x_mem_dim-size(data_matrix, 1)) (y_mem_dim-size(data_matrix, 2))], 0, 'post');

data_tiles = zeros(x_accel_dim, y_accel_dim, ((x_mem_dim*y_mem_dim)/(x_accel_dim*y_accel_dim))); % resize to fit output

tic
for i = 0:x_mem_tile_dim-1
    for j = 0:y_mem_tile_dim-1
        x_min = i*x_accel_dim +1;
        x_max = (i+1)*x_accel_dim;
        y_min = j*y_accel_dim +1;
        y_max = (j+1)*y_accel_dim;
        tile_num = x_mem_tile_dim*j+i +1;
        data_tiles(:,:,tile_num) = data_matrix(x_min:x_max, y_min:y_max);
    end
end
fprintf('Matrix to Tile Pack Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))

end
