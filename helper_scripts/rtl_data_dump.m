function [  ] = rtl_data_dump(operation_type, input_data, nufft_type, fi_cfg, config)
%   Detailed explanation goes here
% kdat --- complex magnitude; one complex value for each tm coordinate
% dims --- dimensions of the output grid
% table_mat --- interpolation table; each dim (x or y) has one row
% numpoints --- size of the interpolation window
% Jlist --- interpolation window indices for numpoints*numpoints elements
% L --- table oversampling factor (L table points between every grid point)
% tm --- coordinates for the kdata samples
% griddat --- output grid arranged as a 1D vector

%%%%%%%%%% Assign Inputs %%%%%%%%%%
x_accel_dim    = config.hardware.x_accel_dim;
y_accel_dim    = config.hardware.y_accel_dim;
x_mem_dim      = config.hardware.x_mem_dim;
y_mem_dim      = config.hardware.y_mem_dim;
x_mem_tile_dim = config.hardware.x_mem_tile_dim;
y_mem_tile_dim = config.hardware.y_mem_tile_dim;

x_grid_dim     = config.image.x_grid_dim;
y_grid_dim     = config.image.y_grid_dim;
x_tile_dim     = config.image.x_tile_dim;
y_tile_dim     = config.image.y_tile_dim;
x_img_dim      = config.image.x_img_dim;
y_img_dim      = config.image.y_img_dim;
x_img_tile_dim = config.image.x_img_tile_dim;
y_img_tile_dim = config.image.y_img_tile_dim;
x_fft_len      = config.image.x_fft_len;
y_fft_len      = config.image.y_fft_len;

nonuniform_data  = input_data.nonuniform_data;
nonuniform_coord = input_data.nonuniform_coord;
interp_table     = input_data.table;
table_oversamp   = input_data.table_oversamp;
table_x_dim      = input_data.table_x_dim;
table_y_dim      = input_data.table_y_dim;
griddat          = input_data.griddat;

num_input_points = size(nonuniform_coord, 2);

if(x_grid_dim > x_mem_dim || y_grid_dim > y_mem_dim)
    disp('ERROR: Input dimensions don not fit inside memory!');
end

grid_data_tiled = zeros(x_accel_dim, y_accel_dim, ((x_mem_dim*y_mem_dim)/(x_accel_dim*y_accel_dim)));
grid_data       = reshape(griddat, x_grid_dim, y_grid_dim); % Reshape into a square and rotate to match hardware output

if(strcmp(nufft_type, 'forw'))
    nonuniform_data = complex(zeros(1, size(nonuniform_coord, 2)), 0);

    %%%%%%%%%% Pack Grid %%%%%%%%%%
    tic
    for i = 0:x_tile_dim-1
        for j = 0:y_tile_dim-1
            x_min = i*x_accel_dim +1;
            x_max = (i+1)*x_accel_dim;
            y_min = j*y_accel_dim +1;
            y_max = (j+1)*y_accel_dim;
            tile_num = x_mem_tile_dim*j+i +1;
            grid_data_tiled(:,:,tile_num) = grid_data(x_min:x_max, y_min:y_max);
        end
    end
else
    nonuniform_data = nonuniform_data;
end

% Pre-shift the data; this avoids the circshift previously required for grid_data (forw input, adj output)
nonuniform_coord(1, :) = rem(nonuniform_coord(1, :) + (table_x_dim/2), x_grid_dim);
nonuniform_coord(2, :) = rem(nonuniform_coord(2, :) + (table_y_dim/2), y_grid_dim);

switch operation_type
    case 'fft2'
        tic
        % Save the weights
        load(sprintf('w_arr_descending_bitrevorder_N=%d.mat', x_fft_len))
        table_data_real_fixed = int16(reinterpretcast(fi(real(w_arr), true, 16, 14), numerictype(true, 16, 0)));
        table_data_imag_fixed = int16(reinterpretcast(fi(imag(w_arr), true, 16, 14), numerictype(true, 16, 0)));
        fid = fopen(sprintf('data/rtl/input/%s_weight_data_fixed.%d.bin', operation_type, size(table_data_real_fixed, 2)), 'wb');
        for idx = 1:size(table_data_real_fixed, 2)
            fwrite(fid, table_data_real_fixed(idx), 'int16');
            fwrite(fid, table_data_imag_fixed(idx), 'int16');
        end
        fclose(fid);


        % Save the uniform grid
        uniform_data_tiled_real = single(real(input_data.matlab_gridded_data));
        uniform_data_tiled_imag = single(imag(input_data.matlab_gridded_data));
        fid = fopen(sprintf('data/rtl/input/%s_uniform_data_tiled.%d.bin', operation_type, size(uniform_data_tiled_real, 1) * size(uniform_data_tiled_real, 2) * size(uniform_data_tiled_real, 3)), 'wb');
        for x = 1:size(uniform_data_tiled_real, 1)
            for y = 1:size(uniform_data_tiled_real, 2)
                for z = 1:size(uniform_data_tiled_real, 3)
                    fwrite(fid, uniform_data_tiled_real(x, y, z), 'single');
                    fwrite(fid, uniform_data_tiled_imag(x, y, z), 'single');
                end
            end
        end
        fclose(fid);

        fprintf('FFT2 Data Dump Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
    case 'regridding'
        tic
        % Save the weights
        table_data_real_fixed = int16(reinterpretcast(fi(real(interp_table(1, :)), true, 16, 14), numerictype(true, 16, 0)));
        table_data_imag_fixed = int16(reinterpretcast(fi(imag(interp_table(1, :)), true, 16, 14), numerictype(true, 16, 0)));
        fid = fopen(sprintf('data/rtl/input/%s_weight_data_fixed.%d.bin', operation_type, size(table_data_real_fixed, 2)), 'wb');
        for idx = 1:size(table_data_real_fixed, 2)
            fwrite(fid, table_data_real_fixed(idx), 'int16');
            fwrite(fid, table_data_imag_fixed(idx), 'int16');
        end
        fclose(fid);


        % Save the nonuniform data
        nonuniform_data_real = single(real(nonuniform_data));
        nonuniform_data_imag = single(imag(nonuniform_data));
        fid = fopen(sprintf('data/rtl/input/%s_nonuniform_data.%d.bin', operation_type, size(nonuniform_data_real, 2)), 'wb');
        for idx = 1:size(nonuniform_data_real, 2)
            fwrite(fid, nonuniform_data_real(idx), 'single');
            fwrite(fid, nonuniform_data_imag(idx), 'single');
        end
        fclose(fid);


        % Save the nonuniform coordinates
        nonuniform_x_coords = uint32(reinterpretcast(fi(nonuniform_coord(1, :), false, 32, 22), numerictype(false, 32, 0)));
        nonuniform_y_coords = uint32(reinterpretcast(fi(nonuniform_coord(2, :), false, 32, 22), numerictype(false, 32, 0)));
        fid = fopen(sprintf('data/rtl/input/%s_nonuniform_coords.%d.bin', operation_type, size(nonuniform_x_coords, 2)), 'wb');
        for idx = 1:size(nonuniform_x_coords, 2)
            fwrite(fid, nonuniform_x_coords(idx), 'uint32');
            fwrite(fid, nonuniform_y_coords(idx), 'uint32');
        end
        fclose(fid);


        % Save the uniform grid
        uniform_data_tiled_real = single(real(grid_data_tiled));
        uniform_data_tiled_imag = single(imag(grid_data_tiled));
        fid = fopen(sprintf('data/rtl/input/%s_uniform_data_tiled.%d.bin', operation_type, size(uniform_data_tiled_real, 1) * size(uniform_data_tiled_real, 2) * size(uniform_data_tiled_real, 3)), 'wb');
        for x = 1:size(uniform_data_tiled_real, 1)
            for y = 1:size(uniform_data_tiled_real, 2)
                fid2 = fopen(sprintf('data/rtl/input/%s_uniform_data_tiled_col_x%d_y%d.%d.bin', operation_type, x-1, y-1, size(uniform_data_tiled_real, 3)), 'wb');
                for z = 1:size(uniform_data_tiled_real, 3)
                    fwrite(fid, uniform_data_tiled_real(x, y, z), 'single');
                    fwrite(fid, uniform_data_tiled_imag(x, y, z), 'single');
                    
                    fwrite(fid2, uniform_data_tiled_real(x, y, z), 'single');
                    fwrite(fid2, uniform_data_tiled_imag(x, y, z), 'single');
                end
                fclose(fid2);
            end
        end
        fclose(fid);

        fprintf('Regridding Data Dump Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
    case 'gridding'
        tic
        % Save the weights
        table_data_real_fixed = int16(reinterpretcast(fi(real(interp_table(1, :)), true, 16, 14), numerictype(true, 16, 0)));
        table_data_imag_fixed = int16(reinterpretcast(fi(imag(interp_table(1, :)), true, 16, 14), numerictype(true, 16, 0)));
        fid = fopen(sprintf('data/rtl/input/%s_weight_data_fixed.%d.bin', operation_type, size(table_data_real_fixed, 2)), 'wb');
        for idx = 1:size(table_data_real_fixed, 2)
            fwrite(fid, table_data_real_fixed(idx), 'int16');
            fwrite(fid, table_data_imag_fixed(idx), 'int16');
        end
        fclose(fid);


        % Save the nonuniform data
        nonuniform_data_real = single(real(nonuniform_data));
        nonuniform_data_imag = single(imag(nonuniform_data));
        fid = fopen(sprintf('data/rtl/input/%s_nonuniform_data.%d.bin', operation_type, size(nonuniform_data_real, 2)), 'wb');
        for idx = 1:size(nonuniform_data_real, 2)
            fwrite(fid, nonuniform_data_real(idx), 'single');
            fwrite(fid, nonuniform_data_imag(idx), 'single');
        end
        fclose(fid);


        % Save the nonuniform coordinates
        nonuniform_x_coords = uint32(reinterpretcast(fi(nonuniform_coord(1, :), false, 32, 22), numerictype(false, 32, 0)));
        nonuniform_y_coords = uint32(reinterpretcast(fi(nonuniform_coord(2, :), false, 32, 22), numerictype(false, 32, 0)));
        fid = fopen(sprintf('data/rtl/input/%s_nonuniform_coords.%d.bin', operation_type, size(nonuniform_x_coords, 2)), 'wb');
        for idx = 1:size(nonuniform_x_coords, 2)
            fwrite(fid, nonuniform_x_coords(idx), 'uint32');
            fwrite(fid, nonuniform_y_coords(idx), 'uint32');
        end
        fclose(fid);

        fprintf('Gridding Data Dump Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
    case 'ifft2'
        tic
        % Save the weights
        load(sprintf('w_arr_descending_bitrevorder_N=%d.mat', x_fft_len))
        table_data_real_fixed = int16(reinterpretcast(fi(real(w_arr), true, 16, 14), numerictype(true, 16, 0)));
        table_data_imag_fixed = int16(reinterpretcast(fi(imag(w_arr), true, 16, 14), numerictype(true, 16, 0)));
        fid = fopen(sprintf('data/rtl/input/%s_weight_data_fixed.%d.bin', operation_type, size(table_data_real_fixed, 2)), 'wb');
        for idx = 1:size(table_data_real_fixed, 2)
            fwrite(fid, table_data_real_fixed(idx), 'int16');
            fwrite(fid, table_data_imag_fixed(idx), 'int16');
        end
        fclose(fid);


        % Save the uniform grid
        uniform_data_tiled_real = single(real(input_data.matlab_gridded_data));
        uniform_data_tiled_imag = single(imag(input_data.matlab_gridded_data));
        fid = fopen(sprintf('data/rtl/input/%s_uniform_data_tiled.%d.bin', operation_type, size(uniform_data_tiled_real, 1) * size(uniform_data_tiled_real, 2) * size(uniform_data_tiled_real, 3)), 'wb');
        for x = 1:size(uniform_data_tiled_real, 1)
            for y = 1:size(uniform_data_tiled_real, 2)
                for z = 1:size(uniform_data_tiled_real, 3)
                    fwrite(fid, uniform_data_tiled_real(x, y, z), 'single');
                    fwrite(fid, uniform_data_tiled_imag(x, y, z), 'single');
                end
            end
        end
        fclose(fid);


        % Save the uniform grid
        uniform_data_tiled_real = single(real(input_data.matlab_gridded_data));
        uniform_data_tiled_imag = single(imag(input_data.matlab_gridded_data));
        for x = 1:size(uniform_data_tiled_real, 1)
            for y = 1:size(uniform_data_tiled_real, 2)
                fid = fopen(sprintf('data/rtl/output/final_output_x%d_y%d.out', x-1, y-1), 'w');
                for z = 1:size(uniform_data_tiled_real, 3)
%                     fwrite(fid, uniform_data_tiled_real(x, y, z), 'single');
%                     fwrite(fid, uniform_data_tiled_imag(x, y, z), 'single');
                    fprintf(fid, '%e', uniform_data_tiled_real(x, y, z));
                    if(uniform_data_tiled_imag(x, y, z) >= 0)
                        fprintf(fid, '+%e\n', uniform_data_tiled_imag(x, y, z));
                    else
                        fprintf(fid, '%e\n', uniform_data_tiled_imag(x, y, z));
                    end
                end
                fclose(fid);
            end
        end

        fprintf('IFFT2 Data Dump Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
    otherwise
        error('Unidentified fi mode');
end

end
