%%%%%%%%%% Initial Setup  %%%%%%%%%%
clear
run('set_paths.m')
run('irt/setup.m')

%%%%%%%%%% Python Interpolation %%%%%%%%%%
if(strcmp(computer('arch'), 'win64'))
    system(['"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\python.exe" kbnufft_forw_interp.py']);
    system(['"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\python.exe" kbnufft_adj_interp.py']);
elseif(strcmp(computer('arch'), 'glnxa64'))
    system(['python3 kbnufft_forw_interp.py']);
    system(['python3 kbnufft_adj_interp.py']);
else
    fprintf('Error: only Windows and Linux are supported.\n');
    return;
end

%%%%%%%%%% Load Python Interpolation %%%%%%%%%%
load(sprintf('python_griddat.mat'));
load(sprintf('python_kdat.mat'));

%%%%%%%%%% Matlab Interpolation %%%%%%%%%%
tic
nufft_type = 'adj'; % must be 'forw' or 'adj'

if(strcmp(nufft_type, 'forw'))
    input_filename = 'run_interp_forw_var.mat';
else
    input_filename = 'run_interp_adj_var.mat';
    % input_filename = 'matlab_griddat_adj.mat';
    % python_kdat = pythonish_interp_forw(input_filename, nufft_type, 'double', 16);
end

% matlab_data = pythonish_interp_forw(input_filename, nufft_type, 'double', 16);
% matlab_data = pythonish_interp_adj(input_filename, nufft_type, 'double', 16);
% matlab_data = pythonish_interp(input_filename, nufft_type, 'fixed', 32);

matlab_data = hardware_sim_adj(input_filename);
% matlab_data = hardware_interp(input_filename, nufft_type);
% matlab_data = hardware_interp_fixed(input_filename, nufft_type, 'fixed', 32);
% matlab_data = hardware_interp_fixed_parfor(input_filename, nufft_type, 'fixed', 32);
% nrms(matlab_data, matlab_data_parfor)
matlab_data = reshape(matlab_data, 1, 768*768);
% save(sprintf('matlab_data_double.mat'), 'matlab_data');

% save(sprintf('matlab_data_fixed_32b_32b.mat'), 'matlab_data');
toc
% datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF')

matlab_data = reshape(matlab_data, 768, 768);
tic
asdf = ifft2(matlab_data);
toc

%%%%%%%%%% Print Error Percentages %%%%%%%%%%
if(strcmp(nufft_type, 'forw'))
    nrms_error    = nrms(python_kdat, matlab_data)*100;
    max_perc_diff = (max(abs(matlab_data(:)-python_kdat(:))) / max(abs(python_kdat(:))))*100;
else
    nrms_error    = nrms(python_griddat, matlab_data)*100;
    max_perc_diff = (max(abs(matlab_data(:)-python_griddat(:))) / max(abs(python_griddat(:))))*100;
end
fprintf('\n\nNormalized Root Mean Square Error: %f%%\n', nrms_error)
fprintf('Maximum [Pixel] Percent Difference: %f%%\n\n\n', max_perc_diff)
