%%%%%%%%%% Initial Setup  %%%%%%%%%%
clear
run('set_paths.m')
run('irt/setup.m')
% delete(fullfile('data/gpu/', '*.mat'))
% delete(fullfile('data/gpu/', '*.data'))


%%%%%%%%%% Compile Custom Datatype Binaries %%%%%%%%%%
mex -client engine cpp/pthread_nonatomic_posit_vector_functions.cpp -Ilib -g lib/libbfp.a  -lpthread -outdir cpp/bin
mex -client engine cpp/pthread_nonatomic_customfloat_vector_functions.cpp -Ilib -g lib/libbfp.a  -lpthread -outdir cpp/bin
mex -client engine cpp/pthread_nonatomic_customfloat_16b_weights_vector_functions_v3.cpp -Ilib -g lib/libbfp.a  -lpthread -outdir cpp/bin
system('nvcc gpu/kernel_floats_nufft.cu -o gpu/bin/kernel_floats_nufft');
mex -client engine gpu/binary_to_mat.cpp -Ilib -g lib/libbfp.a  -lpthread -outdir gpu/bin
mex -client engine cpp/pthread_nonatomic_floats_vector_functions.cpp -Ilib -g lib/libbfp.a  -lpthread -outdir cpp/bin


%%%%%%%%%% Python Interpolation %%%%%%%%%%
if(strcmp(computer('arch'), 'win64'))
    system('"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\python.exe" kbnufft_forw_interp_FFT.py');
    system('"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\python.exe" kbnufft_adj_interp_IFFT.py');
elseif(strcmp(computer('arch'), 'glnxa64'))
    system('python3 kbnufft_forw_interp_FFT.py');
    system('python3 kbnufft_adj_interp_IFFT.py');
else
    fprintf('Error: only Windows and Linux are supported.\n');
    return;
end


%%%%%%%%%% NuFFT Direction %%%%%%%%%%
nufft_type_list = ["forw", "adj"]; % can be "forw", "adj", or both (string list)
quant_mode_list = ["cpu_float"];
fixed_bit_width = 32; % set the number of bits for fixed-point
roundingmode = 'nearest'; % this currently only affects floats/custom floats


for quant_mode_idx = 1:size(quant_mode_list, 2)
    for nufft_type_idx = 1:size(nufft_type_list, 2)
        fprintf('\n\n%s\n', datetime('now'))
        fprintf('Quant Mode: %s, NuFFT Direction: %s\n\n\n', quant_mode_list(quant_mode_idx), nufft_type_list(nufft_type_idx))
        
        %%%%%%%%%% Fixed Point Setup %%%%%%%%%%
        fi_cfg.nufft_type = nufft_type_list(nufft_type_idx); % set interp mode ("forw" or "adj")
        fi_cfg.quant_mode = quant_mode_list(quant_mode_idx); % set quantization mode ("double", "single", "fixed")
        fi_cfg.bit_width = fixed_bit_width; % set the number of bits for fixed-point
        fi_cfg.roundmode = roundingmode;
        fi_cfg = fixed_point_cfg(fi_cfg);


        %%%%%%%%%% Hardware Setup %%%%%%%%%%
        config.hardware.x_accel_dim    = 8;
        config.hardware.y_accel_dim    = 8;
        config.hardware.x_mem_dim      = 1024;
        config.hardware.y_mem_dim      = 1024;
        config.hardware.x_mem_tile_dim = config.hardware.x_mem_dim / config.hardware.x_accel_dim;
        config.hardware.y_mem_tile_dim = config.hardware.y_mem_dim / config.hardware.y_accel_dim;


        %%%%%%%%%% NuFFT %%%%%%%%%%
        if(strcmp(nufft_type_list(nufft_type_idx), 'forw'))
            load('run_interp_forw_var_FFT.mat', 'griddat', 'dims', 'table_mat', 'numpoints', 'Jlist', 'L', 'tm', 'kdat');
            load('forw_x_pre_scaling_x_post.mat');

            input_data.nonuniform_coord = flipud(tm);
            input_data.nonuniform_data  = kdat;
            input_data.table            = table_mat;
            input_data.table_oversamp   = double(L);
            input_data.table_x_dim      = double(numpoints(1));
            input_data.table_y_dim      = double(numpoints(2));
            input_data.griddat          = griddat;

            config.image.x_grid_dim     = double(grid_size(1));
            config.image.y_grid_dim     = double(grid_size(2));
            config.image.x_tile_dim     = config.image.x_grid_dim / config.hardware.x_accel_dim;
            config.image.y_tile_dim     = config.image.y_grid_dim / config.hardware.y_accel_dim;
            config.image.x_img_dim      = double(im_size(1));
            config.image.y_img_dim      = double(im_size(2));
            config.image.x_img_tile_dim = config.image.x_img_dim / config.hardware.x_accel_dim;
            config.image.y_img_tile_dim = config.image.y_img_dim / config.hardware.y_accel_dim;
            config.image.x_fft_len      = 2^nextpow2(config.image.x_grid_dim);
            config.image.y_fft_len      = 2^nextpow2(config.image.y_grid_dim);

            clear griddat dims table_mat numpoints Jlist L tm kdat forw_x_scaled


            %%% Forward NuFFT Step #1: Preapodize %%%
            matlab_fft_data = forw_x_pre_scaling.' .* forw_scaling_coef.'; % transpose forw data because Python is row-major


            %%% Pad, bit-reverse, and pack matrix to tiled format %%%
            matlab_fft_data = matrix_to_tile(matlab_fft_data, config);


            %%% Forward NuFFT Step #2: FFT %%%
            switch quant_mode_list(quant_mode_idx)
                case 'double'
        %             matlab_fft_data = FFT2_IFFT2_DIT_R2_TILED(matlab_fft_data, nufft_type_list(nufft_type_idx), config);
                    % matlab_fft_data = hardware_FFT2_IFFT2_DIT_R2_TILED(matlab_fft_data, nufft_type_list(nufft_type_idx), config);
                    matlab_fft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_parfor(matlab_fft_data, nufft_type_list(nufft_type_idx), config);
                case 'gpu_float'
                    matlab_fft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_parfor(matlab_fft_data, nufft_type_list(nufft_type_idx), config);
                case 'cpu_float'
                    matlab_fft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_parfor(matlab_fft_data, nufft_type_list(nufft_type_idx), config);
                case 'cpp_posit'
                    % Store the output back into the struct
                    input_data.matlab_gridded_data = matlab_fft_data;

                    % Dump the required data to files for consumption by the C++ program
                    fi_cfg.quant_mode = 'fixedfft'; % set quantization mode since the FFT is a different type
                    cpp_data_dump('fft2', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);

                    % Call the C++ program
                    system('./cpp/bin/pthread_nonatomic_posit_vector_functions fft2 data/cpp/fft2/weight_data_real.256.data data/cpp/fft2/weight_data_imag.256.data data/cpp/fft2/nonuniform_data_real.103680.data data/cpp/fft2/nonuniform_data_imag.103680.data data/cpp/fft2/nonuniform_x_coord.103680.data data/cpp/fft2/nonuniform_y_coord.103680.data data/cpp/fft2/uniform_data_tiled_real.1048576.data data/cpp/fft2/uniform_data_tiled_imag.1048576.data');

                    load('data/cpp/tmp/posit_uniform_data.mat')
                    posit_uniform_data = complex(uniform_data_tiled_real, uniform_data_tiled_imag);
                    posit_uniform_data = reshape(posit_uniform_data, [16384 8 8]);
                    matlab_fft_data = zeros(8, 8, 16384);
                    for x = 1:8
                        for y = 1:8
                            matlab_fft_data(x,y,:) = posit_uniform_data(:,x,y);
                        end
                    end
                case 'cpp_customfloat'
                    % Store the output back into the struct
                    input_data.matlab_gridded_data = matlab_fft_data;

                    % Dump the required data to files for consumption by the C++ program
                    fi_cfg.quant_mode = 'fixedfft'; % set quantization mode since the FFT is a different type
                    cpp_data_dump('fft2', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    % rtl_data_dump('fft2', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);

                    % Call the C++ program
%                     system('./pthread_nonatomic_customfloat_vector_functions fft2 data/cpp/fft2/weight_data_real.256.data data/cpp/fft2/weight_data_imag.256.data data/cpp/fft2/nonuniform_data_real.103680.data data/cpp/fft2/nonuniform_data_imag.103680.data data/cpp/fft2/nonuniform_x_coord.103680.data data/cpp/fft2/nonuniform_y_coord.103680.data data/cpp/fft2/uniform_data_tiled_real.1048576.data data/cpp/fft2/uniform_data_tiled_imag.1048576.data');
                    system('./cpp/bin/pthread_nonatomic_customfloat_16b_weights_vector_functions_v3 fft2 data/cpp/fft2/weight_data_real_fixed.256.data data/cpp/fft2/weight_data_imag_fixed.256.data data/cpp/fft2/nonuniform_data_real.103680.data data/cpp/fft2/nonuniform_data_imag.103680.data data/cpp/fft2/nonuniform_x_coord.103680.data data/cpp/fft2/nonuniform_y_coord.103680.data data/cpp/fft2/uniform_data_tiled_real.1048576.data data/cpp/fft2/uniform_data_tiled_imag.1048576.data');

                    load('data/cpp/tmp/customfloat_uniform_data.mat')
                    posit_uniform_data = complex(uniform_data_tiled_real, uniform_data_tiled_imag);
                    posit_uniform_data = reshape(posit_uniform_data, [16384 8 8]);
                    matlab_fft_data = zeros(8, 8, 16384);
                    for x = 1:8
                        for y = 1:8
                            matlab_fft_data(x,y,:) = posit_uniform_data(:,x,y);
                        end
                    end
                case 'fixed'
                    fi_cfg = rmfield(fi_cfg,'sample_frac_length');
                    fi_cfg.quant_mode = 'fixedfft'; % set quantization mode since the FFT is a different type
                    fi_cfg = fixed_point_cfg(fi_cfg);
                    
                    % matlab_fft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_fixed(matlab_fft_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    matlab_fft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_fixed_parfor(matlab_fft_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
%                     matlab_fft_data = double(hardware_FFT2_IFFT2_DIT_R2_TILED_fi_parfor(matlab_fft_data, nufft_type_list(nufft_type_idx), fi_cfg, config));
                otherwise
                    % matlab_fft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_fixed(matlab_fft_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    matlab_fft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_fixed_parfor(matlab_fft_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
            end


            %%% Unpack arrays so they can be checked vs the Python output %%%
            matlab_fft_data = tile_to_matrix(matlab_fft_data, config);
            matlab_fft_data = matlab_fft_data(1:config.image.x_grid_dim, 1:config.image.x_grid_dim);
            matlab_fft_data = bitrevorder(bitrevorder(matlab_fft_data).').';

            % Store the output back into the struct
            input_data.griddat = matlab_fft_data; % Transpose since Python data is rotated (row-vs-col major)


            %%% Forward NuFFT Step #3: Regridding %%%
            switch quant_mode_list(quant_mode_idx)
                case 'double'
                    matlab_regridded_data = hardware_interp_FFT_IFFT(input_data, nufft_type_list(nufft_type_idx), config);
                case 'gpu_float'
                    % Dump the required data to files for consumption by the GPU program
                    gpu_data_dump('regridding', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    
                    % Call the CUDA program
                    system('./gpu/bin/kernel_floats_nufft regridding data/gpu/regridding/weight_data_real.193.data data/gpu/regridding/weight_data_imag.193.data data/gpu/regridding/nonuniform_data_real.103680.data data/gpu/regridding/nonuniform_data_imag.103680.data data/gpu/regridding/nonuniform_x_coord.103680.data data/gpu/regridding/nonuniform_y_coord.103680.data data/gpu/regridding/uniform_data_tiled_real.1048576.data data/gpu/regridding/uniform_data_tiled_imag.1048576.data');

                    % Call the C++ program to convert the data to .mat
                    system('./gpu/bin/binary_to_mat data/tmp/gpu_nonuniform_data_arr_real.bin data/tmp/gpu_nonuniform_data_arr_imag.bin data/tmp/gpu_uniform_data_arr_real.bin data/tmp/gpu_uniform_data_arr_imag.bin');

                    load('data/gpu/gpu_nonuniform_data.mat')
                    matlab_regridded_data = complex(nonuniform_data_real, nonuniform_data_imag);
                    matlab_regridded_data = matlab_regridded_data.';
                case 'cpu_float'
                    % Dump the required data to files for consumption by the C++ program
                    cpp_data_dump('regridding', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
%                     rtl_data_dump('regridding', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);

                    % Call the C++ program
%                     system('./pthread_nonatomic_customfloat_vector_functions regridding data/cpp/regridding/weight_data_real.37249.data data/cpp/regridding/weight_data_imag.37249.data data/cpp/regridding/nonuniform_data_real.103680.data data/cpp/regridding/nonuniform_data_imag.103680.data data/cpp/regridding/nonuniform_x_coord.103680.data data/cpp/regridding/nonuniform_y_coord.103680.data data/cpp/regridding/uniform_data_tiled_real.1048576.data data/cpp/regridding/uniform_data_tiled_imag.1048576.data');
                    system('./cpp/bin/pthread_nonatomic_floats_vector_functions regridding data/cpp/regridding/weight_data_real.193.data data/cpp/regridding/weight_data_imag.193.data data/cpp/regridding/nonuniform_data_real.103680.data data/cpp/regridding/nonuniform_data_imag.103680.data data/cpp/regridding/nonuniform_x_coord.103680.data data/cpp/regridding/nonuniform_y_coord.103680.data data/cpp/regridding/uniform_data_tiled_real.1048576.data data/cpp/regridding/uniform_data_tiled_imag.1048576.data');

                    % Read in the C++ output filessystem('./gpu/bin/kernel_floats_nufft regridding data/gpu/regridding/weight_data_real.193.data data/gpu/regridding/weight_data_imag.193.data data/gpu/regridding/nonuniform_data_real.103680.data data/gpu/regridding/nonuniform_data_imag.103680.data data/gpu/regridding/nonuniform_x_coord.103680.data data/gpu/regridding/nonuniform_y_coord.103680.data data/gpu/regridding/uniform_data_tiled_real.1048576.data data/gpu/regridding/uniform_data_tiled_imag.1048576.data');
                    load('data/cpp/tmp/cpu_nonuniform_data.mat')
                    matlab_regridded_data = complex(nonuniform_data_real, nonuniform_data_imag);
                    matlab_regridded_data = matlab_regridded_data.';
                case 'cpp_posit'
                    % Dump the required data to files for consumption by the C++ program
                    cpp_data_dump('regridding', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);

                    % Call the C++ program
                    system('./cpp/bin/pthread_nonatomic_posit_vector_functions regridding data/cpp/regridding/weight_data_real.37249.data data/cpp/regridding/weight_data_imag.37249.data data/cpp/regridding/nonuniform_data_real.103680.data data/cpp/regridding/nonuniform_data_imag.103680.data data/cpp/regridding/nonuniform_x_coord.103680.data data/cpp/regridding/nonuniform_y_coord.103680.data data/cpp/regridding/uniform_data_tiled_real.1048576.data data/cpp/regridding/uniform_data_tiled_imag.1048576.data');

                    % Read in the C++ output files
                    load('data/cpp/tmp/posit_nonuniform_data.mat')
                    matlab_regridded_data = complex(nonuniform_data_real, nonuniform_data_imag);
                    matlab_regridded_data = matlab_regridded_data.';
                case 'cpp_customfloat'
                    % Dump the required data to files for consumption by the C++ program
                    cpp_data_dump('regridding', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
%                     rtl_data_dump('regridding', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);

                    % Call the C++ program
%                     system('./pthread_nonatomic_customfloat_vector_functions regridding data/cpp/regridding/weight_data_real.37249.data data/cpp/regridding/weight_data_imag.37249.data data/cpp/regridding/nonuniform_data_real.103680.data data/cpp/regridding/nonuniform_data_imag.103680.data data/cpp/regridding/nonuniform_x_coord.103680.data data/cpp/regridding/nonuniform_y_coord.103680.data data/cpp/regridding/uniform_data_tiled_real.1048576.data data/cpp/regridding/uniform_data_tiled_imag.1048576.data');
                    system('./cpp/bin/pthread_nonatomic_customfloat_16b_weights_vector_functions_v3 regridding data/cpp/regridding/weight_data_real_fixed.193.data data/cpp/regridding/weight_data_imag_fixed.193.data data/cpp/regridding/nonuniform_data_real.103680.data data/cpp/regridding/nonuniform_data_imag.103680.data data/cpp/regridding/nonuniform_x_coord.103680.data data/cpp/regridding/nonuniform_y_coord.103680.data data/cpp/regridding/uniform_data_tiled_real.1048576.data data/cpp/regridding/uniform_data_tiled_imag.1048576.data');

                    % Read in the C++ output filessystem('./gpu/bin/kernel_floats_nufft regridding data/gpu/regridding/weight_data_real.193.data data/gpu/regridding/weight_data_imag.193.data data/gpu/regridding/nonuniform_data_real.103680.data data/gpu/regridding/nonuniform_data_imag.103680.data data/gpu/regridding/nonuniform_x_coord.103680.data data/gpu/regridding/nonuniform_y_coord.103680.data data/gpu/regridding/uniform_data_tiled_real.1048576.data data/gpu/regridding/uniform_data_tiled_imag.1048576.data');
                    load('data/cpp/tmp/customfloat_nonuniform_data.mat')
                    matlab_regridded_data = complex(nonuniform_data_real, nonuniform_data_imag);
                    matlab_regridded_data = matlab_regridded_data.';
                case 'fixed'
                    fi_cfg.quant_mode = 'fixed';
                    fi_cfg = fixed_point_cfg(fi_cfg);
                    
                    matlab_regridded_data = hardware_interp_FFT_IFFT_fixed(input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
%                     matlab_regridded_data = hardware_interp_FFT_IFFT_fixed_parfor(input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                otherwise
                    % matlab_regridded_data = hardware_interp_FFT_IFFT_fixed(input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    matlab_regridded_data = hardware_interp_FFT_IFFT_fixed_parfor(input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
            end


            %%% Load Python data, calculate error percentages, and print %%%
            load(sprintf('python_kdat_FFT.mat'));
            fft_nrms_error = nrms(forw_x_fft.', matlab_fft_data)*100; % transpose forw data because Python is row-major
            nrms_error    = nrms(python_kdat, matlab_regridded_data)*100;
            max_perc_diff = (max(abs(matlab_regridded_data(:)-python_kdat(:))) / max(abs(python_kdat(:))))*100;
            fprintf('\n\nFFT Normalized Root Mean Square Error: %f%%\n', fft_nrms_error)
            fprintf('\n\nRegridding Normalized Root Mean Square Error: %f%%\n', nrms_error)
            fprintf('\n\nRegridding Maximum [Point] Percent Difference: %f%%\n', max_perc_diff)
        else
            load('run_interp_adj_var_IFFT.mat', 'griddat', 'dims', 'table_mat', 'numpoints', 'Jlist', 'L', 'tm', 'kdat');
            load('adj_x_pre_scaling_x_post.mat');

            input_data.nonuniform_coord = flipud(tm);
            input_data.nonuniform_data  = kdat;
            input_data.table            = table_mat;
            input_data.table_oversamp   = double(L);
            input_data.table_x_dim      = double(numpoints(1));
            input_data.table_y_dim      = double(numpoints(2));
            input_data.griddat          = griddat;

            config.image.x_grid_dim     = double(grid_size(1));
            config.image.y_grid_dim     = double(grid_size(2));
            config.image.x_tile_dim     = config.image.x_grid_dim / config.hardware.x_accel_dim;
            config.image.y_tile_dim     = config.image.y_grid_dim / config.hardware.y_accel_dim;
            config.image.x_img_dim      = double(im_size(1));
            config.image.y_img_dim      = double(im_size(2));
            config.image.x_img_tile_dim = config.image.x_img_dim / config.hardware.x_accel_dim;
            config.image.y_img_tile_dim = config.image.y_img_dim / config.hardware.y_accel_dim;
            config.image.x_fft_len      = 2^nextpow2(config.image.x_grid_dim);
            config.image.y_fft_len      = 2^nextpow2(config.image.y_grid_dim);

            clear griddat dims table_mat numpoints Jlist L tm kdat adj_x_ifft


            %%% Adjoint NuFFT Step #1: Gridding %%%
            switch quant_mode_list(quant_mode_idx)
                case 'double'
                    matlab_gridded_data = hardware_interp_FFT_IFFT(input_data, nufft_type_list(nufft_type_idx), config);
                case 'gpu_float'
                    % Dump the required data to files for consumption by the GPU program
                    gpu_data_dump('gridding', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    
                    % Call the CUDA program
                    system('./gpu/bin/kernel_floats_nufft gridding data/gpu/gridding/weight_data_real.193.data data/gpu/gridding/weight_data_imag.193.data data/gpu/gridding/nonuniform_data_real.460800.data data/gpu/gridding/nonuniform_data_imag.460800.data data/gpu/gridding/nonuniform_x_coord.460800.data data/gpu/gridding/nonuniform_y_coord.460800.data data/gpu/gridding/uniform_data_tiled_real.1048576.data data/gpu/gridding/uniform_data_tiled_imag.1048576.data');

                    % Call the C++ program to convert the data to .mat
                    system('./gpu/bin/binary_to_mat data/tmp/gpu_nonuniform_data_arr_real.bin data/tmp/gpu_nonuniform_data_arr_imag.bin data/tmp/gpu_uniform_data_arr_real.bin data/tmp/gpu_uniform_data_arr_imag.bin');

                    load('data/gpu/gpu_uniform_data.mat')
                    gpu_uniform_data = complex(uniform_data_tiled_real, uniform_data_tiled_imag);
                    gpu_uniform_data = reshape(gpu_uniform_data, [16384 8 8]);
                    matlab_gridded_data = zeros(8, 8, 16384);
                    for x = 1:8
                        for y = 1:8
                            matlab_gridded_data(x,y,:) = gpu_uniform_data(:,x,y);
                        end
                    end
                case 'cpu_float'
                    % Dump the required data to files for consumption by the C++ program
                    cpp_data_dump('gridding', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    % rtl_data_dump('gridding', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);

                    % Call the C++ program
%                     system('./pthread_nonatomic_customfloat_vector_functions gridding data/cpp/gridding/weight_data_real.37249.data data/cpp/gridding/weight_data_imag.37249.data data/cpp/gridding/nonuniform_data_real.460800.data data/cpp/gridding/nonuniform_data_imag.460800.data data/cpp/gridding/nonuniform_x_coord.460800.data data/cpp/gridding/nonuniform_y_coord.460800.data data/cpp/gridding/uniform_data_tiled_real.1048576.data data/cpp/gridding/uniform_data_tiled_imag.1048576.data');
                    system('./cpp/bin/pthread_nonatomic_floats_vector_functions gridding data/cpp/gridding/weight_data_real.193.data data/cpp/gridding/weight_data_imag.193.data data/cpp/gridding/nonuniform_data_real.460800.data data/cpp/gridding/nonuniform_data_imag.460800.data data/cpp/gridding/nonuniform_x_coord.460800.data data/cpp/gridding/nonuniform_y_coord.460800.data data/cpp/gridding/uniform_data_tiled_real.1048576.data data/cpp/gridding/uniform_data_tiled_imag.1048576.data');

                    % Read in the C++ output files
                    load('data/cpp/tmp/cpu_uniform_data.mat')
                    posit_uniform_data = complex(uniform_data_tiled_real, uniform_data_tiled_imag);
                    posit_uniform_data = reshape(posit_uniform_data, [16384 8 8]);
                    matlab_gridded_data = zeros(8, 8, 16384);
                    for x = 1:8
                        for y = 1:8
                            matlab_gridded_data(x,y,:) = posit_uniform_data(:,x,y);
                        end
                    end

                    % Store the output back into the struct
                    input_data.matlab_gridded_data = matlab_gridded_data;
                case 'cpp_posit'
                    % Dump the required data to files for consumption by the C++ program
                    cpp_data_dump('gridding', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);

                    % Call the C++ program
                    system('./cpp/bin/pthread_nonatomic_posit_vector_functions gridding data/cpp/gridding/weight_data_real.37249.data data/cpp/gridding/weight_data_imag.37249.data data/cpp/gridding/nonuniform_data_real.460800.data data/cpp/gridding/nonuniform_data_imag.460800.data data/cpp/gridding/nonuniform_x_coord.460800.data data/cpp/gridding/nonuniform_y_coord.460800.data data/cpp/gridding/uniform_data_tiled_real.1048576.data data/cpp/gridding/uniform_data_tiled_imag.1048576.data');

                    % Read in the C++ output files
                    load('data/cpp/tmp/posit_uniform_data.mat')
                    posit_uniform_data = complex(uniform_data_tiled_real, uniform_data_tiled_imag);
                    posit_uniform_data = reshape(posit_uniform_data, [16384 8 8]);
                    matlab_gridded_data = zeros(8, 8, 16384);
                    for x = 1:8
                        for y = 1:8
                            matlab_gridded_data(x,y,:) = posit_uniform_data(:,x,y);
                        end
                    end

                    % Store the output back into the struct
                    input_data.matlab_gridded_data = matlab_gridded_data;
                case 'cpp_customfloat'
                    % Dump the required data to files for consumption by the C++ program
                    cpp_data_dump('gridding', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    % rtl_data_dump('gridding', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);

                    % Call the C++ program
%                     system('./pthread_nonatomic_customfloat_vector_functions gridding data/cpp/gridding/weight_data_real.37249.data data/cpp/gridding/weight_data_imag.37249.data data/cpp/gridding/nonuniform_data_real.460800.data data/cpp/gridding/nonuniform_data_imag.460800.data data/cpp/gridding/nonuniform_x_coord.460800.data data/cpp/gridding/nonuniform_y_coord.460800.data data/cpp/gridding/uniform_data_tiled_real.1048576.data data/cpp/gridding/uniform_data_tiled_imag.1048576.data');
                    system('./cpp/bin/pthread_nonatomic_customfloat_16b_weights_vector_functions_v3 gridding data/cpp/gridding/weight_data_real_fixed.193.data data/cpp/gridding/weight_data_imag_fixed.193.data data/cpp/gridding/nonuniform_data_real.460800.data data/cpp/gridding/nonuniform_data_imag.460800.data data/cpp/gridding/nonuniform_x_coord.460800.data data/cpp/gridding/nonuniform_y_coord.460800.data data/cpp/gridding/uniform_data_tiled_real.1048576.data data/cpp/gridding/uniform_data_tiled_imag.1048576.data');

                    % Read in the C++ output files
                    load('data/cpp/tmp/customfloat_uniform_data.mat')
                    posit_uniform_data = complex(uniform_data_tiled_real, uniform_data_tiled_imag);
                    posit_uniform_data = reshape(posit_uniform_data, [16384 8 8]);
                    matlab_gridded_data = zeros(8, 8, 16384);
                    for x = 1:8
                        for y = 1:8
                            matlab_gridded_data(x,y,:) = posit_uniform_data(:,x,y);
                        end
                    end

                    % Store the output back into the struct
                    input_data.matlab_gridded_data = matlab_gridded_data;
                case 'fixed'
                    fi_cfg.quant_mode = 'fixed';
                    fi_cfg = fixed_point_cfg(fi_cfg);
                    
                    % matlab_gridded_data = hardware_interp_FFT_IFFT_fixed(input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    matlab_gridded_data = hardware_interp_FFT_IFFT_fixed_parfor(input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    % matlab_gridded_data = hardware_interp_FFT_IFFT_fi(input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    % matlab_gridded_data2 = hardware_interp_FFT_IFFT_fixed_TEST(input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                otherwise
                    % matlab_gridded_data = hardware_interp_FFT_IFFT_fixed(input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    matlab_gridded_data = hardware_interp_FFT_IFFT_fixed_parfor(input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
            end


            %%% Adjoint NuFFT Step #2: IFFT %%%
            switch quant_mode_list(quant_mode_idx)
                case 'double'
                    % matlab_ifft_data = hardware_FFT2_IFFT2_DIT_R2_TILED(matlab_gridded_data, nufft_type_list(nufft_type_idx), config);
                    matlab_ifft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_parfor(matlab_gridded_data, nufft_type_list(nufft_type_idx), config);
                case 'gpu_float'
                    matlab_ifft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_parfor(matlab_gridded_data, nufft_type_list(nufft_type_idx), config);
                case 'cpu_float'
                    matlab_ifft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_parfor(matlab_gridded_data, nufft_type_list(nufft_type_idx), config);
                case 'cpp_posit'
                    % Dump the required data to files for consumption by the C++ program
                    fi_cfg.quant_mode = 'fixedfft'; % set quantization mode since the FFT is a different type
                    cpp_data_dump('ifft2', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);

                    % Call the C++ program
                    system('./cpp/bin/pthread_nonatomic_posit_vector_functions ifft2 data/cpp/ifft2/weight_data_real.512.data data/cpp/ifft2/weight_data_imag.512.data data/cpp/ifft2/nonuniform_data_real.460800.data data/cpp/ifft2/nonuniform_data_imag.460800.data data/cpp/ifft2/nonuniform_x_coord.460800.data data/cpp/ifft2/nonuniform_y_coord.460800.data data/cpp/ifft2/uniform_data_tiled_real.1048576.data data/cpp/ifft2/uniform_data_tiled_imag.1048576.data');

                    load('data/cpp/tmp/posit_uniform_data.mat')
                    posit_uniform_data = complex(uniform_data_tiled_real, uniform_data_tiled_imag);
                    posit_uniform_data = reshape(posit_uniform_data, [16384 8 8]);
                    matlab_ifft_data = zeros(8, 8, 16384);
                    for x = 1:8
                        for y = 1:8
                            matlab_ifft_data(x,y,:) = posit_uniform_data(:,x,y);
                        end
                    end
                case 'cpp_customfloat'
                    % Dump the required data to files for consumption by the C++ program
                    fi_cfg.quant_mode = 'fixedfft'; % set quantization mode since the FFT is a different type
                    cpp_data_dump('ifft2', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
%                     rtl_data_dump('ifft2', input_data, nufft_type_list(nufft_type_idx), fi_cfg, config);

                    % Call the C++ program
%                     system('./pthread_nonatomic_customfloat_vector_functions ifft2 data/cpp/ifft2/weight_data_real.512.data data/cpp/ifft2/weight_data_imag.512.data data/cpp/ifft2/nonuniform_data_real.460800.data data/cpp/ifft2/nonuniform_data_imag.460800.data data/cpp/ifft2/nonuniform_x_coord.460800.data data/cpp/ifft2/nonuniform_y_coord.460800.data data/cpp/ifft2/uniform_data_tiled_real.1048576.data data/cpp/ifft2/uniform_data_tiled_imag.1048576.data');
                    system('./cpp/bin/pthread_nonatomic_customfloat_16b_weights_vector_functions_v3 ifft2 data/cpp/ifft2/weight_data_real_fixed.512.data data/cpp/ifft2/weight_data_imag_fixed.512.data data/cpp/ifft2/nonuniform_data_real.460800.data data/cpp/ifft2/nonuniform_data_imag.460800.data data/cpp/ifft2/nonuniform_x_coord.460800.data data/cpp/ifft2/nonuniform_y_coord.460800.data data/cpp/ifft2/uniform_data_tiled_real.1048576.data data/cpp/ifft2/uniform_data_tiled_imag.1048576.data');

                    load('data/cpp/tmp/customfloat_uniform_data.mat')
                    posit_uniform_data = complex(uniform_data_tiled_real, uniform_data_tiled_imag);
                    posit_uniform_data = reshape(posit_uniform_data, [16384 8 8]);
                    matlab_ifft_data = zeros(8, 8, 16384);
                    for x = 1:8
                        for y = 1:8
                            matlab_ifft_data(x,y,:) = posit_uniform_data(:,x,y);
                        end
                    end
                case 'fixed'
                    fi_cfg = rmfield(fi_cfg,'sample_frac_length');
                    fi_cfg.quant_mode = 'fixedfft';
                    fi_cfg = fixed_point_cfg(fi_cfg);
                    
                    % matlab_ifft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_fixed(matlab_gridded_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    matlab_ifft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_fixed_parfor(matlab_gridded_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
%                     matlab_ifft_data = double(hardware_FFT2_IFFT2_DIT_R2_TILED_fi_parfor(matlab_gridded_data, nufft_type_list(nufft_type_idx), fi_cfg, config));
                otherwise
                    % matlab_ifft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_fixed(matlab_gridded_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
                    matlab_ifft_data = hardware_FFT2_IFFT2_DIT_R2_TILED_fixed_parfor(matlab_gridded_data, nufft_type_list(nufft_type_idx), fi_cfg, config);
            end


            %%% Unpack both arrays so they can be checked vs the Python output %%%
            matlab_gridded_data = tile_to_matrix(matlab_gridded_data, config);
            matlab_ifft_data = tile_to_matrix(matlab_ifft_data, config);
            %%% Bit-reverse the hardware matrix along all columns, then rows %%%
            matlab_ifft_data = bitrevorder(bitrevorder(matlab_ifft_data).').';
            %%% Rearrange data extract final image from oversampled grid %%%
            matlab_ifft_data = matlab_ifft_data(1:config.image.x_img_dim, 1:config.image.y_img_dim);


            %%% Adjoint NuFFT Step #3: Deapodize %%%
            matlab_ifft_data = (matlab_ifft_data .* adj_scaling_coef.'); % transpose adj data because Python is row-major


            %%% Resize the grid data since we unpacked the entire memory array %%%
            matlab_gridded_data = matlab_gridded_data(1:config.image.x_grid_dim, 1:config.image.y_grid_dim);


            %%% Load Python data, calculate error percentages, and print %%%
            load(sprintf('python_griddat_IFFT.mat'));
            nrms_error    = nrms(python_griddat, matlab_gridded_data)*100;
            max_perc_diff = (max(abs(matlab_gridded_data(:)-python_griddat(:))) / max(abs(python_griddat(:))))*100;
            ifft_nrms_error = nrms(adj_x_scaled.', matlab_ifft_data)*100; % transpose adj data because Python is row-major
            fprintf('\n\nGridding Normalized Root Mean Square Error: %f%%\n', nrms_error)
            fprintf('\n\nGridding Maximum [Point] Percent Difference: %f%%\n', max_perc_diff)
            fprintf('\n\nIFFT Normalized Root Mean Square Error: %f%%\n', ifft_nrms_error)
        end
    end
end
