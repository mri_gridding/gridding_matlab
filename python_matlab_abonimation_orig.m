%%%%%%%%%% Initial Setup  %%%%%%%%%%
clear
run('set_paths.m')
run('irt/setup.m')

%%%%%%%%%% Python Interpolation %%%%%%%%%%
if(strcmp(computer('arch'), 'win64'))
	system(['"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\python.exe" kbnufft_generate_griddata_for_matlab.py']);
elseif(strcmp(computer('arch'), 'glnxa64'))
	system(['python3 kbnufft_generate_griddata_for_matlab.py']);
else
    fprintf('Error: only Windows and Linux are supported.\n');
    return;
end

%%%%%%%%%% Matlab Interpolation %%%%%%%%%%
% tic
matlab_griddat = pythonish_interp_adj('run_interp_adj_var.mat', 'adj', 'double', 16);
% matlab_griddat = run_hardware_interp('run_interp_adj_var.mat', 'adj', 'double', 16);
% save(sprintf('matlab_griddat.mat'), 'matlab_griddat');
% datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF')

%%%%%%%%%% Load Python Interpolation %%%%%%%%%%
load(sprintf('python_griddat.mat'));

%%%%%%%%%% Compare Interpolation Results %%%%%%%%%%
% nrms(python_griddat, matlab_griddat)
% nrms(griddat, matlab_griddat)

%%%%%%%%%% Print Error Percentages %%%%%%%%%%
nrms_error = nrms(python_griddat, matlab_griddat)*100;
max_perc_diff = (max(abs(matlab_griddat(:)-python_griddat(:))) / max(abs(python_griddat(:))))*100;
fprintf('\n\nNormalized Root Mean Square Error: %f%%\n', nrms_error)
fprintf('Maximum [Pixel] Percent Difference: %f%%\n\n\n', max_perc_diff)
