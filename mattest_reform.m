mex -client engine /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/pthread_nonatomic_posits_vector.cpp -I/home/westbl/Research/MRI/SIMULATORS/gridding_gpu/bfp/lib -I/home/westbl/Research/MRI/SIMULATORS/gridding_gpu/bfp/test -g /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/bfp/lib/libbfp.a  -lpthread

% delete('cpu_gridding_posits.mat');
% delete('cpu_ifft2_posits.mat');
% 
% system('./pthread_nonatomic_posits_vector /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/input_x_coord_1_460800_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/input_y_coord_1_460800_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/input_data_real_1_460800_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/input_data_imag_1_460800_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/table_data_real_37249_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/table_data_imag_37249_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/twiddle_data_real_512_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/twiddle_data_imag_512_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/fft_input_data_real_8_8_16384_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/fft_input_data_imag_8_8_16384_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/regridding_input_data_real_8_8_16384_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/regridding_input_data_imag_8_8_16384_fixed.data');
% 
% load('cpu_gridding_posits.mat')
% load('mattest_matlab_gridding_1024.mat')
% griddat = complex(gridding_real, gridding_imag);
% griddat = reshape(griddat, [16384 8 8]);
% posit_griddat = zeros(8, 8, 16384);
% 
% load('cpu_ifft2_posits.mat')
% load('mattest_matlab_ifft2_1024.mat')
% ifft2 = complex(ifft2_real, ifft2_imag);
% ifft2 = reshape(ifft2, [16384 8 8]);
% posit_ifft2 = zeros(8, 8, 16384);
% for x = 1:8
%     for y = 1:8
%         posit_griddat(x,y,:) = griddat(:,x,y);
%         posit_ifft2(x,y,:) = ifft2(:,x,y);
%     end
% end
% nrms(matlab_gridding_data, posit_griddat)
% nrms(matlab_ifft2_data, posit_ifft2)



delete('cpu_fft2_posits.mat');
delete('cpu_regridding_posits.mat');

system('./pthread_nonatomic_posits_vector /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/input_x_coord_1_103680_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/input_y_coord_1_103680_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/input_data_real_1_103680_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/input_data_imag_1_103680_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/regridding_table_data_real_37249_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/regridding_table_data_imag_37249_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/twiddle_data_real_256_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/twiddle_data_imag_256_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/fft_input_data_real_8_8_16384_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/fft_input_data_imag_8_8_16384_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/regridding_input_data_real_8_8_16384_fixed.data /home/westbl/Research/MRI/SIMULATORS/gridding_gpu/doubles/regridding_input_data_imag_8_8_16384_fixed.data');

load('cpu_fft2_posits.mat')
load('mattest_matlab_fft2_1024.mat')
fft2 = complex(fft2_real, fft2_imag);
fft2 = reshape(fft2, [16384 8 8]);
posit_fft2 = zeros(8, 8, 16384);

load('cpu_regridding_posits.mat')
load('mattest_matlab_regridding_1024.mat')
regridding = complex(regridding_real, regridding_imag);
posit_regridding = regridding.';
for x = 1:8
    for y = 1:8
        posit_fft2(x,y,:) = fft2(:,x,y);
    end
end
nrms(matlab_fft2_data, posit_fft2)
nrms(matlab_regridding_data, posit_regridding)
