element_range_start = 1;
element_range_end = 320013504;
input_x_coord = input_coord(1, element_range_start:element_range_end);
input_x_coord_fixed_point = fi(input_x_coord, false, 32, 22);
input_x_coord_fixed_point = reinterpretcast(input_x_coord_fixed_point, numerictype(false, 32, 0));
input_x_coord_fixed_point = uint32(input_x_coord_fixed_point);
fid = fopen(sprintf('data/input_x_coord_%d_%d.data', element_range_start, element_range_end), 'w');
fwrite(fid, swapbytes(input_x_coord_fixed_point), 'uint32');
fclose(fid);
input_y_coord = input_coord(2, element_range_start:element_range_end);
input_y_coord_fixed_point = fi(input_y_coord, false, 32, 22);
input_y_coord_fixed_point = reinterpretcast(input_y_coord_fixed_point, numerictype(false, 32, 0));
input_y_coord_fixed_point = uint32(input_y_coord_fixed_point);
fid = fopen(sprintf('data/input_y_coord_%d_%d.data', element_range_start, element_range_end), 'w');
fwrite(fid, swapbytes(input_y_coord_fixed_point), 'uint32');
fclose(fid);
input_z_coord = input_coord(3, element_range_start:element_range_end);
input_z_coord_fixed_point = fi(input_z_coord, false, 32, 22);
input_z_coord_fixed_point = reinterpretcast(input_z_coord_fixed_point, numerictype(false, 32, 0));
input_z_coord_fixed_point = uint32(input_z_coord_fixed_point);
fid = fopen(sprintf('data/input_z_coord_%d_%d.data', element_range_start, element_range_end), 'w');
fwrite(fid, swapbytes(input_z_coord_fixed_point), 'uint32');
fclose(fid);
input_data_real = real(input_data(element_range_start:element_range_end));
input_data_real_fixed_point = fi(input_data_real, true, 32, 40);
input_data_real_fixed_point = reinterpretcast(input_data_real_fixed_point, numerictype(true, 32, 0));
input_data_real_fixed_point = int32(input_data_real_fixed_point);
fid = fopen(sprintf('data/input_data_real_%d_%d.data', element_range_start, element_range_end), 'w');
fwrite(fid, swapbytes(input_data_real_fixed_point), 'int32');
fclose(fid);
input_data_imag = imag(input_data(element_range_start:element_range_end));
input_data_imag_fixed_point = fi(input_data_imag, true, 32, 40);
input_data_imag_fixed_point = reinterpretcast(input_data_imag_fixed_point, numerictype(true, 32, 0));
input_data_imag_fixed_point = int32(input_data_imag_fixed_point);
fid = fopen(sprintf('data/input_data_imag_%d_%d.data', element_range_start, element_range_end), 'w');
fwrite(fid, swapbytes(input_data_imag_fixed_point), 'int32');
fclose(fid);



% table_data_real = real(table_mat(1, :));
% table_data_real_fixed_point = fi(table_data_real, true, 16, 14);
% table_data_real_fixed_point = reinterpretcast(table_data_real_fixed_point, numerictype(true, 16, 0));
% table_data_real_fixed_point = int16(table_data_real_fixed_point);
% fid = fopen(sprintf('data/table_data_real_%d.data', size(table_mat, 2)), 'w');
% fwrite(fid, swapbytes(table_data_real_fixed_point), 'int16');
% fclose(fid);
% 
% table_data_imag = imag(table_mat(1, :));
% table_data_imag_fixed_point = fi(table_data_imag, true, 16, 14);
% table_data_imag_fixed_point = reinterpretcast(table_data_imag_fixed_point, numerictype(true, 16, 0));
% table_data_imag_fixed_point = int16(table_data_imag_fixed_point);
% fid = fopen(sprintf('data/table_data_imag_%d.data', size(table_mat, 2)), 'w');
% fwrite(fid, swapbytes(table_data_imag_fixed_point), 'int16');
% fclose(fid);



for z_slice = z_slice_start:z_slice_end-1
    for x_idx = 0:x_accel_dim-1
        for y_idx = 0:y_accel_dim-1
            fileID = fopen(sprintf('data/final_output_zslice%d_x%d_y%d.out', z_slice, x_idx, y_idx), 'w');
            for z_idx = 0:((x_grid_dim*y_grid_dim)/(x_accel_dim*y_accel_dim))-1
                result = grid_data(z_slice+1, x_idx+1, y_idx+1, z_idx+1);
                result_real = fi(real(result), true, SAMPLE_BIT_WIDTH, SAMPLE_FRAC_WIDTH+6);
                result_imag = fi(imag(result), true, SAMPLE_BIT_WIDTH, SAMPLE_FRAC_WIDTH+6);
                fprintf(fileID,'%s', result_real.bin);
                fprintf(fileID,'%s\n', result_imag.bin);
            end
            fclose(fileID);
        end
    end
end
