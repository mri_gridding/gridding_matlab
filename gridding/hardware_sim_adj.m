function [ hardware_interp ] = hardware_sim_adj(python_filename)
%   Detailed explanation goes here
load(python_filename, 'kdat', 'dims', 'table_mat', 'numpoints', 'Jlist', 'L', 'tm');
% kdat --- complex magnitude; one complex value for each tm coordinate
% dims --- dimensions of the output grid
% table_mat --- interpolation table; each dim (x or y) has one row
% numpoints --- size of the interpolation window
% Jlist --- interpolation window indices for numpoints*numpoints elements
% L --- table oversampling factor (L table points between every grid point)
% tm --- coordinates for the kdata samples
% griddat --- output grid arranged as a 1D vector

%%%%%%%%%% Assign Inputs %%%%%%%%%%
input_coord = flipud(tm); % flip the coordinates to match Python output
minmax(input_coord)
input_data = kdat;
minmax(input_data)
interp_table = table_mat;
minmax(interp_table)

table_oversamp = L;

num_input_points = size(input_coord, 2);

x_grid_dim = int64(dims(1));
y_grid_dim = int64(dims(2));

x_accel_dim = 8;
y_accel_dim = 8;

x_tile_dim = x_grid_dim / x_accel_dim;
y_tile_dim = y_grid_dim / y_accel_dim;

table_x_dim = nthroot(size(Jlist, 2), size(Jlist, 1));
table_y_dim = nthroot(size(Jlist, 2), size(Jlist, 1));

grid_data = zeros(x_accel_dim, y_accel_dim, 16384);
% grid_data_2D = zeros(x_grid_dim, y_grid_dim);

grid_data_tiled = zeros(x_grid_dim, y_grid_dim);

% load(sprintf('python_griddat.mat'));
% python_griddat_square = rot90(reshape(python_griddat, 768, 768));

clear kdat dims table_mat numpoints Jlist L tm


%%%%%%%%%% Interpolate Data %%%%%%%%%%
CHECK_ACCEL_X = 5;
CHECK_ACCEL_Y = 5;
fid = fopen(sprintf('pipline_x%d_y%d_addresses_readable.data', CHECK_ACCEL_X, CHECK_ACCEL_Y), 'w');
tic
% Process the input array one point at a time, updating target grid points
for input_point_idx = 1:num_input_points
% for input_point_idx = 27:27
    % Extract the current point being processed
    input_point_coord = input_coord(:, input_point_idx);
    input_point_data = input_data(:, input_point_idx);
    
    % Split the complex value into its real and imaginary components
    input_point_data_real = real(input_point_data);
    input_point_data_imag = imag(input_point_data);

    % Get the quotient and remainder to determine grid and tile location
    x_base_tile_idx = fix(input_point_coord(1)/x_accel_dim); % x virtual tile index
    y_base_tile_idx = fix(input_point_coord(2)/y_accel_dim); % y virtual tile index

    % Extract the "relative" coordinates (within a tile)
    x_coord = rem(input_point_coord(1), x_accel_dim);
    y_coord = rem(input_point_coord(2), y_accel_dim);

    % Loop over accelerator pipelines to process input point
    for x_accel_idx = 0:x_accel_dim-1
        % Determine if this pipeline is affected by this source point
        % If x_coord is within [x_accel_idx,x_accel_idx+table_x_dim) of the x_accel_idx, it does
        x_distance = rem(x_accel_dim+x_coord-x_accel_idx, x_accel_dim); % rem is implemented as an upper truncation
        if(x_distance < table_x_dim)
            for y_accel_idx = 0:y_accel_dim-1
                % If y_coord is within [y_accel_idx,y_accel_idx+table_y_dim) of the y_accel_idx, it does
                y_distance = rem(y_accel_dim+y_coord-y_accel_idx, y_accel_dim); % rem is implemented as an upper truncation
                if(y_distance < table_y_dim)
                    % This point affects this pipeline!

                    % Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
                    if(x_coord < x_accel_idx)
                        % A wrap occured!
                        if(x_base_tile_idx == 0)
                            x_tile_idx = x_tile_dim - 1;
                        else
                            x_tile_idx = x_base_tile_idx - 1;
                        end
                    else
                        x_tile_idx = x_base_tile_idx;
                    end

                    % Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
                    if(y_coord < y_accel_idx)
                        % A wrap occured!
                        if(y_base_tile_idx == 0)
                            y_tile_idx = y_tile_dim - 1;
                        else
                            y_tile_idx = y_base_tile_idx - 1;
                        end
                    else
                        y_tile_idx = y_base_tile_idx;
                    end

                    % Calculate the tile index ("depth" in the pipeline's data array)
                    tile_idx = y_tile_idx * x_tile_dim + x_tile_idx; % 1D global virtual tile index

                    % Calculate the table index
                    x_table_idx = floor((x_distance * double(table_oversamp(1))) + 0.5); % *L (a power of 2) is implemented as a left shift
                    y_table_idx = floor((y_distance * double(table_oversamp(2))) + 0.5); % *L (a power of 2) is implemented as a left shift

                    % Extract the interp table values
                    x_table_val = interp_table(1, x_table_idx+1); % +1 because Matlab indexing starts at 1
                    y_table_val = interp_table(2, y_table_idx+1); % +1 because Matlab indexing starts at 1

                    % Split the complex value into its real and imaginary components
                    x_table_val_real = real(x_table_val);
                    x_table_val_imag = imag(x_table_val);
                    y_table_val_real = real(y_table_val);
                    y_table_val_imag = imag(y_table_val);
                    
                    % Calculate 2D table value (product of X and Y values)
                    k1 = x_table_val_real + x_table_val_imag;
                    k2 = y_table_val_imag - y_table_val_real;
                    k3 = y_table_val_real + y_table_val_imag;
                    k4 = y_table_val_real * k1;
                    k5 = x_table_val_real * k2;
                    k6 = x_table_val_imag * k3;
                    R1 = k4 - k6;
                    I1 = k4 + k5;

                    % Calculate interpolation result; conjugate of interp table value incorporated into this calculation directly by flipping sign for I1
                    k7 = R1 - I1;
                    k8 = input_point_data_imag - input_point_data_real;
                    k9 = input_point_data_real + input_point_data_imag;
                    k10 = input_point_data_real * k7;
                    k11 = R1 * k8;
                    k12 = I1 * k9;
                    R2 = k10 + k12;
                    I2 = k10 + k11;
                    interp_result = complex(R2, I2);
                    
                    if(x_accel_idx == CHECK_ACCEL_X && y_accel_idx == CHECK_ACCEL_Y)
                        fprintf(fid, '%d\n', tile_idx);
                    end

                    grid_data(x_accel_idx+1, y_accel_idx+1, tile_idx+1) = interp_result + grid_data(x_accel_idx+1, y_accel_idx+1, tile_idx+1); % +1 because Matlab indexing starts at 1

%                     grid_data_2D(x_tile_idx*x_accel_dim + x_accel_idx+1, y_tile_idx*y_accel_dim + y_accel_idx+1) = interp_result + grid_data_2D(x_tile_idx*x_accel_dim + x_accel_idx+1, y_tile_idx*y_accel_dim + y_accel_idx+1); % for debugging
                end
            end
        end
    end
end
toc
fclose(fid);


%%%%%%%%%% Check GPU data %%%%%%%%%%
% fid_real = fopen('/home/westbl/fjoule_mnt/home/westbl/mri_gpu/gpu_griddat_real.bin', 'rb');
% gpu_r = fread(fid_real,768*768,'double');
% fid_imag = fopen('/home/westbl/fjoule_mnt/home/westbl/mri_gpu/gpu_griddat_imag.bin', 'rb');
% gpu_i = fread(fid_imag,768*768,'double');
% gpu_complex = complex(gpu_r, gpu_i);
% gpu_complex = reshape(gpu_complex, 8, 8, 9216);
% nrms(grid_data, gpu_complex)


%%%%%%%%%% Unpack Grid %%%%%%%%%%
tic
for i = 0:x_tile_dim-1
    for j = 0:y_tile_dim-1
        x_min = i*x_accel_dim +1;
        x_max = (i+1)*x_accel_dim;
        y_min = j*y_accel_dim +1;
        y_max = (j+1)*y_accel_dim;
        tile_num = x_tile_dim*j+i +1;
        grid_data_tiled(x_min:x_max, y_min:y_max) = grid_data(:,:,tile_num);
    end
end
toc


%%%%%%%%%% Shift and Output Grid %%%%%%%%%%
% hardware_interp = circshift(grid_data_2D, [3 3]);
hardware_interp = circshift(grid_data_tiled, [3 3]);

end
