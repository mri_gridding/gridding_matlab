% function griddat = run_interp_adj(kdat, dims, table_mat, numpoints, Jlist, L, tm, griddat)
function [ griddat ] = run_hardware_interp_adj(python_filename, num_type, bit_width)
%   Detailed explanation goes here
load(python_filename, 'kdat', 'dims', 'table_mat', 'numpoints', 'Jlist', 'L', 'tm', 'griddat');
% kdat --- complex magnitude; one complex value for each tm coordinate
% dims --- dimensions of the output grid
% table_mat --- interpolation table; each dim (x or y) has one row
% numpoints --- size of the interpolation window
% Jlist --- interpolation window indices for numpoints*numpoints elements
% L --- table oversampling factor (L table points between every grid point)
% tm --- coordinates for the kdata samples
% griddat --- output grid arranged as a 1D vector

%%%%%%%%%% Fixed Point Setup %%%%%%%%%%
fi_cfg.mode = num_type;
fi_cfg.type = bit_width; % set bit width
fi_cfg = fixed_point_cfg(fi_cfg);

% assign size variables for later use
M = size(tm, 2);
ndims = size(tm, 1);
nJ = size(Jlist, 2);

% center of tables
centers = floor(numpoints.*L/2);

% offset from k-space to first coef loc (top left corner of 2D window)
kofflist = 1 + floor(tm - ((double(numpoints')) / 2.0));

% do a bit of type management - ints for faster index comps
coef = complex(ones(M,1))';
arr_ind = zeros([M 1],'int64')';
dims = int64(dims);
kofflist = int64(kofflist);
Jlist = int64(Jlist);

%%%%%%%%%% Quantize Inputs %%%%%%%%%%
tm = quantize(fi_cfg.input_coord, tm);
kdat = quantize(fi_cfg.input_data, kdat);
table_mat = quantize(fi_cfg.interp_table, table_mat);

tm = tm + double(numpoints'/2); % offset by numpoints/2 to shift the origin to the left

% loop over offsets and take advantage of numpy broadcasting
for Jind = 1:nJ
    % get the grid indices for this iteration (window index); one index
    % per window per point ((0,0) through (numpoints-1,numpoints-1))
    cur_grid_ind = kofflist + Jlist(:, Jind); % mod(cur_grid_ind, 32) is the affected pipeline index
    
    % find exact distance between current grid index and the point location
    tm_rem32 = rem(tm, 32);
    cur_grid_ind_rem32 = rem(double(cur_grid_ind), 32);
    cur_dist = rem(tm_rem32-cur_grid_ind_rem32+32,32); %+32 to avoid negatives
    
%     cur_dist = tm - double(cur_grid_ind);
    
    % convert distance to table_oversampling_factor units for table indexing;
    % these are used to directly index into the interp table
    % CAN this be implemented as a left shift/N-zero right pad if L is 2^N?
    cur_dist_ind = int64(floor((cur_dist .* double(L'))+1+0.5)); % +1 for matlab indexing, +0.5 for rounding

    % reset the coefficients and array indices for this window index
    coef(:) = 1.0 + 0j;
    arr_ind(:) = 1;

    for d = 1:ndims
        % read the interp table values; add the table center's values to
        % the distance to account for the kofflist shift
%         table_vals = table_mat(d, curdistind(d, :)+double(centers(d))+1);
        table_vals = table_mat(d, cur_dist_ind(d, :));
        coef = coef .* conj(table_vals);
        x_table_coords = cur_dist_ind(1, :);
        y_table_coords = cur_dist_ind(2, :);
        x_tab_vals = table_mat(1, cur_dist_ind(1, :));
        y_tab_vals = table_mat(2, cur_dist_ind(2, :));
        table_val_2d = conj(x_tab_vals .* y_tab_vals);
        interp_vals2 = conj(coef.*kdat);
        if(d == ndims)
            % handle wrapping in the last dimension (calculate 1D offset)
            arr_ind = arr_ind + mod(cur_grid_ind(d, :), dims(d));
        else
            % handle wrapping in the d-th dimension (calculate 1D offset)
            arr_ind = arr_ind + (mod(cur_grid_ind(d, :), dims(d)) * prod(dims(d+1:end)));
        end
    end

    % avoid write collisions by using accumarray
    tmp = accumarray(arr_ind', conj(coef.*kdat))';
    griddat(1:size(tmp,2)) = griddat(1:size(tmp,2)) + tmp;
end

%%%%%%%%%% Quantize Output %%%%%%%%%%
griddat = quantize(fi_cfg.output_data, griddat);

end
