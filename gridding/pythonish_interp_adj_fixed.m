% function griddat = run_interp_adj(kdat, dims, table_mat, numpoints, Jlist, L, tm, griddat)
function [ griddat ] = pythonish_interp_adj_fixed(python_filename, interp_mode, fi_cfg)
%   Detailed explanation goes here
load(python_filename, 'kdat', 'dims', 'table_mat', 'numpoints', 'Jlist', 'L', 'tm', 'griddat');
% kdat --- complex magnitude; one complex value for each tm coordinate
% dims --- dimensions of the output grid
% table_mat --- interpolation table; each dim (x or y) has one row
% numpoints --- size of the interpolation window
% Jlist --- interpolation window indices for numpoints*numpoints elements
% L --- table oversampling factor (L table points between every grid point)
% tm --- coordinates for the kdata samples
% griddat --- output grid arranged as a 1D vector

% assign size variables for later use
M = quantize(fi_cfg.generic, size(tm, 2));
ndims = quantize(fi_cfg.generic, size(tm, 1));
nJ = quantize(fi_cfg.generic, size(Jlist, 2));


%%%%%%%%%% Restrict Inputs %%%%%%%%%%
% num_elem = 66592;
% new_dim = 64;
% element_range_start = 1;
% element_range_end = num_elem;
% oversamp_fact = 2;
% new_grid_dim = new_dim * oversamp_fact;
% if(num_elem <= 460800)
%     random_idx = randperm(M, num_elem);
% else
%     rand_first = randperm(M, 460800);
%     rand_second = randi(M, 1, num_elem-460800);
%     random_idx = [rand_first rand_second];
% end
% griddat = zeros(1,new_grid_dim*new_grid_dim);
% if(new_grid_dim <= 768)
%     tm = mod(tm(:, random_idx), new_grid_dim);
% else
%     tm = tm(:, random_idx)*(1024.0/768.0);
% end
% kdat = kdat(random_idx);
% M = num_elem;
% dims = [new_grid_dim new_grid_dim];

% center of tables
centers = quantize(fi_cfg.generic, double(floor(numpoints.*L/2)));

% offset from k-space to first coef loc (top left corner of 2D window)
kofflist = quantize(fi_cfg.generic, 1 + floor(tm - ((double(numpoints')) / 2.0)));

% do a bit of type management - ints for faster index comps
coef = quantize(fi_cfg.generic, complex(ones(M,1))');
arr_ind = quantize(fi_cfg.generic, zeros([M 1])');
dims = quantize(fi_cfg.generic, double(dims));
kofflist = quantize(fi_cfg.generic, kofflist);
Jlist = quantize(fi_cfg.generic, double(Jlist));
L = quantize(fi_cfg.generic, double(L));
numpoints = quantize(fi_cfg.generic, double(numpoints));

%%%%%%%%%% Quantize Inputs %%%%%%%%%%
tm = quantize(fi_cfg.input_coord, tm);
kdat = quantize(fi_cfg.input_data, kdat);
table_mat = quantize(fi_cfg.interp_table, table_mat);

tic
% loop over offsets and take advantage of numpy broadcasting
for Jind = 1:nJ
    % get the grid indices for this iteration (window index) - one index
    % per window per point ((0,0) through (numpoints-1,numpoints-1))
    curgridind = quantize(fi_cfg.generic, kofflist + Jlist(:, Jind));
    
    % find the distance from the intput point to the current grid point in
    % table_oversampling_factor units (distance*table_oversampling_factor)
    curdistind = quantize(fi_cfg.generic, round((tm - double(curgridind)) .* double(L')));

    % reset the coefficients and array indices for this window index
    coef(:) = 1.0 + 0j;
    arr_ind(:) = 1;

    for d = 1:ndims
        % read the interp table values; add the table center's values to
        % the distance to account for the kofflist shift
        table_vals = quantize(fi_cfg.generic, table_mat(d, curdistind(d, :)+double(centers(d))+1));
        coef = quantize(fi_cfg.generic, coef .* conj(table_vals));
        if(d == ndims)
            % handle wrapping in the last dimension (calculate 1D offset)
            arr_ind = quantize(fi_cfg.generic, arr_ind + mod(curgridind(d, :), dims(d)));
        else
            % handle wrapping in the d-th dimension (calculate 1D offset)
            arr_ind = quantize(fi_cfg.generic, arr_ind + (mod(curgridind(d, :), dims(d)) * prod(dims(d+1:end))));
        end
    end

    % avoid write collisions by using accumarray
    tmp = accumarray(arr_ind', conj(coef.*kdat))';
    griddat(1:size(tmp,2)) = quantize(fi_cfg.generic, griddat(1:size(tmp,2)) + tmp);
end
toc
fprintf('\nPythonish_Interp (ms): %f\n\n', toc * 1000)

% gpu_data_gen

%%%%%%%%%% Quantize Output %%%%%%%%%%
% griddat = quantize(fi_cfg.output_data, griddat);
griddat = quantize(fi_cfg.generic, griddat);

end
