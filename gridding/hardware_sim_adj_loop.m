function [ hardware_interp ] = hardware_sim_adj_loop(python_filename, interp_mode, num_type, bit_width, test_val1, test_val2)
%   Detailed explanation goes here
load(python_filename, 'kdat', 'dims', 'table_mat', 'numpoints', 'Jlist', 'L', 'tm');
% kdat --- complex magnitude; one complex value for each tm coordinate
% dims --- dimensions of the output grid
% table_mat --- interpolation table; each dim (x or y) has one row
% numpoints --- size of the interpolation window
% Jlist --- interpolation window indices for numpoints*numpoints elements
% L --- table oversampling factor (L table points between every grid point)
% tm --- coordinates for the kdata samples
% griddat --- output grid arranged as a 1D vector

%%%%%%%%%% Fixed Point Setup %%%%%%%%%%
fi_cfg.mode = num_type;
fi_cfg.type = bit_width; % set bit width
fi_cfg.interp_mode = interp_mode; % set interp mode ('forw' or 'adj')
fi_cfg.test_val1 = test_val1;
fi_cfg.test_val2 = test_val2;
fi_cfg = fixed_point_cfg_loop(fi_cfg);

M = size(tm, 2);
ndims = size(tm, 1);
nJ = size(Jlist, 2);

% center of tables
centers = floor(numpoints.*table_oversamp/2);
% offset from k-space to first coef loc
kofflist = 1 + floor(tm - (double(numpoints')) / 2.0);

% do a bit of type management - ints for faster index comps
% curgridind = zeros(size(tm),'int64');
% curdistind = zeros(size(tm),'int64');
coef = complex(ones(M,1))';
arr_ind = zeros([M 1],'int64')';
dims = int64(dims);
kofflist = int64(kofflist);
Jlist = int64(Jlist);

%%%%%%%%%% Quantize Inputs %%%%%%%%%%
tm = quantize(fi_cfg.input_coord, tm);
minmax(tm)
kdat = quantize(fi_cfg.input_data, kdat);
minmax(kdat)
interp_table = quantize(fi_cfg.interp_table, table_mat);
minmax(interp_table)

table_oversamp = L;


x_grid_dim = dims(1);
y_grid_dim = dims(2);

x_accel_dim = 32;
y_accel_dim = 32;

x_tile_dim = x_grid_dim / x_accel_dim;
y_tile_dim = y_grid_dim / y_accel_dim;

table_x_dim = sqrt(nJ);
table_y_dim = sqrt(nJ);

num_input_points = M;
input_coord = tm;
input_data = kdat;

% input_coord(1,:) = input_coord(1,:) + (table_x_dim/2);
% input_coord(2,:) = input_coord(2,:) + (table_y_dim/2);

grid_data = zeros(x_accel_dim, y_accel_dim, ((x_grid_dim*y_grid_dim)/(x_accel_dim*y_accel_dim)));
% grid_data_2D = zeros(x_grid_dim, y_grid_dim);

grid_data_tiled = zeros(x_grid_dim, y_grid_dim);

% halftab_x = table_x_dim/2;
% halftab_y = table_y_dim/2;

% load(sprintf('python_griddat.mat'));
% python_griddat_square = rot90(reshape(python_griddat, 768, 768));

clear kdat dims table_mat numpoints Jlist L tm


%%%%%%%%%% Interpolate Data %%%%%%%%%%
% Process the input array one point at a time, updating target grid points
for input_point_idx = 1:num_input_points
% for input_point_idx = 1:50
    % Extract the current point being processed
    input_point_coord = input_coord(:, input_point_idx);
    input_point_data = input_data(:, input_point_idx);

    % Get the quotient and remainder to determine grid and tile location
    x_base_tile_idx = fix(input_point_coord(1)/x_accel_dim); % x virtual tile index
    y_base_tile_idx = fix(input_point_coord(2)/y_accel_dim); % y virtual tile index
    
    x_coord = mod(input_point_coord(1), x_accel_dim);
    y_coord = mod(input_point_coord(2), y_accel_dim);
    
%     x_wrap = false;
    
%     y_wrap = false;

    % Calculate the pipeline tile index (which x_accel_dim*y_accel_dim block)
%     grid_depth_idx = (y_base_tile_idx*32) + x_base_tile_idx +1; % +1 because Matlab arrays start at index=1

    % Loop over accelerator pipelines to process input point
    for x_accel_idx = 0:x_accel_dim-1
        % Determine if this pipeline is affected by this source point
        % If x_coord is within [x_accel_idx,x_accel_idx+table_x_dim) of the x_accel_idx, it does
        x_distance = rem(x_accel_dim+x_coord-x_accel_idx, x_accel_dim); % rem is implemented as an upper truncation
        if(x_distance < table_x_dim)
            % This point affects this pipeline!
            
            % Determine if a wrap occured; if x_coord < x_accel_idx, a wrap occured
            if(x_coord < x_accel_idx)
                % A wrap occured!
                x_tile_idx = x_base_tile_idx - 1;
                if(x_tile_idx < 0)
                    x_tile_idx = x_tile_dim-1;
                end
                
%                 x_wrap = true; % used for debugging
            else
                x_tile_idx = x_base_tile_idx;
            end
            
            x_table_idx = floor((x_distance * double(table_oversamp(1))) + 0.5); % *L (a power of 2) is implemented as a left shift

            for y_accel_idx = 0:y_accel_dim-1
                % Determine if this pipeline is affected by this source point
                % If y_coord is within [y_accel_idx,y_accel_idx+table_y_dim) of the y_accel_idx, it does
                y_distance = rem(y_accel_dim+y_coord-y_accel_idx, y_accel_dim); % rem is implemented as an upper truncation
                if(y_distance < table_y_dim)
                    % This point affects this pipeline!

                    % Determine if a wrap occured; if y_coord < y_accel_idx, a wrap occured
                    if(y_coord < y_accel_idx)
                        % A wrap occured!
                        y_tile_idx = y_base_tile_idx - 1;
                        if(y_tile_idx < 0)
                            y_tile_idx = y_tile_dim-1;
                        end

%                         y_wrap = true; % used for debugging
                    else
                        y_tile_idx = y_base_tile_idx;
                    end

                    tile_idx = y_tile_idx * x_tile_dim + x_tile_idx; % 1D global virtual tile index

                    y_table_idx = floor((y_distance * double(table_oversamp(2))) + 0.5); % *L (a power of 2) is implemented as a left shift

                    % Now we know that this pipeline is affected and where
                    % wraps occur; do some work!
                    x_table_val = interp_table(1, x_table_idx+1); % +1 because Matlab indexing starts at 1
                    y_table_val = interp_table(2, y_table_idx+1); % +1 because Matlab indexing starts at 1
%                     x_table_val = quantize(fi_cfg.interp_table, interp_table(1, x_table_idx+1)); % +1 because Matlab indexing starts at 1
%                     y_table_val = quantize(fi_cfg.interp_table, interp_table(2, y_table_idx+1)); % +1 because Matlab indexing starts at 1
%                     table_val = conj(x_table_val * y_table_val);
                    table_val = quantize(fi_cfg.interp_table_comb, conj(x_table_val * y_table_val));

                    interp_result = (table_val * input_point_data);
%                     interp_result = quantize(fi_cfg.interp_result, (table_val * input_point_data));
%                     if(interp_result ~= 0)
%                         fprintf('%d\n', tile_idx)
%                     end

                    grid_data(x_accel_idx+1, y_accel_idx+1, tile_idx+1) = interp_result + grid_data(x_accel_idx+1, y_accel_idx+1, tile_idx+1); % +1 because Matlab indexing starts at 1
%                     grid_data(x_accel_idx+1, y_accel_idx+1, tile_idx+1) = quantize(fi_cfg.output_data, interp_result + grid_data(x_accel_idx+1, y_accel_idx+1, tile_idx+1)); % +1 because Matlab indexing starts at 1
%                     grid_data_2D(x_tile_idx*x_accel_dim + x_accel_idx+1, y_tile_idx*y_accel_dim + y_accel_idx+1) = interp_result + grid_data_2D(x_tile_idx*x_accel_dim + x_accel_idx+1, y_tile_idx*y_accel_dim + y_accel_idx+1); % for debugging
%                     asdf = circshift(grid_data_2D,[3 3]);
%                     if(interp_result ~= 0)
% %                         interp_result
%                     end
%                     if(x_distance > 5 && y_distance > 5)
%                     end

    %                 fprintf('\n\nx_accel_idx: %d, x_coord: %f,  x_distance: %f, x_wrap: %d\n', x_accel_idx, x_coord, x_distance, x_wrap)
    %                 fprintf('y_accel_idx: %d, y_coord: %f,  y_distance: %f, y_wrap: %d\n', y_accel_idx, y_coord, y_distance, y_wrap)
                end
    %             fprintf('\n\nx_accel_idx: %d, x_coord: %f,  x_distance: %f, x_wrap: %d\n', x_accel_idx, x_coord, x_distance, x_wrap)
%                 fprintf('y_accel_idx: %d, y_coord: %f,  y_distance: %f, y_wrap: %d\n', y_accel_idx, y_coord, y_distance, y_wrap)
            end
        end
    end
end

%%%%%%%%%% Unpack Output %%%%%%%%%%
for i = 0:x_tile_dim-1
    for j = 0:y_tile_dim-1
        x_min = i*x_accel_dim +1;
        x_max = (i+1)*x_accel_dim;
        y_min = j*y_accel_dim +1;
        y_max = (j+1)*y_accel_dim;
        tile_num = x_tile_dim*j+i +1;
        grid_data_tiled(x_min:x_max, y_min:y_max) = grid_data(:,:,tile_num);
    end
end



%%%%%%%%%% Quantize Output %%%%%%%%%%
% hardware_interp = quantize(fi_cfg.output_data, grid_data);
% isequal(grid_data_tiled, grid_data_2D)
% nrms(grid_data_tiled, grid_data_2D)
grid_data_tiled = circshift(flipud(grid_data_tiled),[-3 3]);
% nrms(grid_data_tiled, python_griddat_square)
% hardware_interp = quantize(fi_cfg.output_data, reshape(grid_data_2D, 1, 589824));
% hardware_interp = reshape(grid_data_tiled, 1, 589824);
% hardware_interp = quantize(fi_cfg.output_data, reshape(grid_data_tiled, 1, 589824));
hardware_interp = quantize(fi_cfg.output_data, grid_data_tiled);

end
