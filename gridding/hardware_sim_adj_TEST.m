function [ hardware_interp ] = hardware_sim_adj_TEST(python_filename, interp_mode, fixed_mode, bit_width)
%   Detailed explanation goes here
load(python_filename, 'kdat', 'dims', 'table_mat', 'numpoints', 'Jlist', 'L', 'tm', 'griddat');
% kdat --- complex magnitude; one complex value for each tm coordinate
% dims --- dimensions of the output grid
% table_mat --- interpolation table; each dim (x or y) has one row
% numpoints --- size of the interpolation window
% Jlist --- interpolation window indices for numpoints*numpoints elements
% L --- table oversampling factor (L table points between every grid point)
% tm --- coordinates for the kdata samples
% griddat --- output grid arranged as a 1D vector

%%%%%%%%%% Fixed Point Setup %%%%%%%%%%
fi_cfg.mode = fixed_mode;
fi_cfg.interp_mode = interp_mode; % set interp mode ('forw' or 'adj')
fi_cfg = fixed_point_cfg(fi_cfg);

%%%%%%%%%% Assign Inputs %%%%%%%%%%
input_coord = quantize(fi_cfg.input_coord, flipud(tm)); % flip the coordinates to match Python output
minmax(input_coord)
input_data = quantize(fi_cfg.input_data, kdat);
minmax(input_data)
interp_table = quantize(fi_cfg.interp_table, table_mat);
minmax(interp_table)

table_oversamp = L;

num_input_points = size(input_coord, 2);

x_grid_dim = int64(dims(1));
y_grid_dim = int64(dims(2));

x_accel_dim = 32;
y_accel_dim = 32;

x_tile_dim = x_grid_dim / x_accel_dim;
y_tile_dim = y_grid_dim / y_accel_dim;

table_x_dim = sqrt(size(Jlist, 2));
table_y_dim = sqrt(size(Jlist, 2));

grid_data = zeros(x_accel_dim, y_accel_dim, ((x_grid_dim*y_grid_dim)/(x_accel_dim*y_accel_dim)));
% grid_data_2D = zeros(x_grid_dim, y_grid_dim);

grid_data_tiled = zeros(x_grid_dim, y_grid_dim);

% load(sprintf('python_griddat.mat'));
% python_griddat_square = rot90(reshape(python_griddat, 768, 768));

clear kdat dims table_mat numpoints Jlist L tm


%%%%%%%%%% Interpolate Data %%%%%%%%%%
% Process the input array one point at a time, updating target grid points
for input_point_idx = 1:num_input_points
% for input_point_idx = 1:50
    % Extract the current point being processed
    input_point_coord = input_coord(:, input_point_idx);
    input_point_data = input_data(:, input_point_idx);

    % Get the quotient and remainder to determine grid and tile location
    x_base_tile_idx = fix(input_point_coord(1)/x_accel_dim); % x virtual tile index
    y_base_tile_idx = fix(input_point_coord(2)/y_accel_dim); % y virtual tile index

    x_coord = mod(input_point_coord(1), x_accel_dim);
    y_coord = mod(input_point_coord(2), y_accel_dim);

    % Loop over accelerator pipelines to process input point
    for x_accel_idx = 0:x_accel_dim-1
        % Determine if this pipeline is affected by this source point
        % If x_coord is within [x_accel_idx,x_accel_idx+table_x_dim) of the x_accel_idx, it does
        x_distance = rem(x_accel_dim+x_coord-x_accel_idx, x_accel_dim); % rem is implemented as an upper truncation
        if(x_distance < table_x_dim)
            for y_accel_idx = 0:y_accel_dim-1
                % If y_coord is within [y_accel_idx,y_accel_idx+table_y_dim) of the y_accel_idx, it does
                y_distance = rem(y_accel_dim+y_coord-y_accel_idx, y_accel_dim); % rem is implemented as an upper truncation
                if(y_distance < table_y_dim)
                    % This point affects this pipeline!

                    % Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
                    if(x_coord < x_accel_idx)
                        % A wrap occured!
                        x_tile_idx = x_base_tile_idx - 1;
                        if(x_tile_idx < 0)
                            x_tile_idx = x_tile_dim-1;
                        end
                    else
                        x_tile_idx = x_base_tile_idx;
                    end

                    % Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
                    if(y_coord < y_accel_idx)
                        % A wrap occured!
                        y_tile_idx = y_base_tile_idx - 1;
                        if(y_tile_idx < 0)
                            y_tile_idx = y_tile_dim-1;
                        end
                    else
                        y_tile_idx = y_base_tile_idx;
                    end

                    x_table_idx = floor((x_distance * double(table_oversamp(1))) + 0.5); % *L (a power of 2) is implemented as a left shift

                    tile_idx = y_tile_idx * x_tile_dim + x_tile_idx; % 1D global virtual tile index

                    y_table_idx = floor((y_distance * double(table_oversamp(2))) + 0.5); % *L (a power of 2) is implemented as a left shift

                    % Now we know that this pipeline is affected and where
                    % wraps occur; do some work!
                    x_table_val = interp_table(1, x_table_idx+1); % +1 because Matlab indexing starts at 1
                    y_table_val = interp_table(2, y_table_idx+1); % +1 because Matlab indexing starts at 1

                    % Calculate 2D table value (product of X and Y values)
                    k1 = real(x_table_val) + imag(x_table_val);
                    k2 = imag(y_table_val) - real(y_table_val);
                    k3 = real(y_table_val) + imag(y_table_val);
                    k4 = real(y_table_val) * k1;
                    k5 = real(x_table_val) * k2;
                    k6 = imag(x_table_val) * k3;
                    R1 = k4 - k6;
                    I1 = k4 + k5;
                    table_val = complex(R1, I1);
                    table_val_conj = conj(table_val);

                    % Calculate interpolation result
                    k7 = real(table_val_conj) + imag(table_val_conj);
                    k8 = imag(input_point_data) - real(input_point_data);
                    k9 = real(input_point_data) + imag(input_point_data);
                    k10 = real(input_point_data) * k7;
                    k11 = real(table_val_conj) * k8;
                    k12 = imag(table_val_conj) * k9;
                    R2 = k10 - k12;
                    I2 = k10 + k11;
                    interp_result = complex(R2, I2);

                    grid_data(x_accel_idx+1, y_accel_idx+1, tile_idx+1) = interp_result + grid_data(x_accel_idx+1, y_accel_idx+1, tile_idx+1); % +1 because Matlab indexing starts at 1

%                     grid_data_2D(x_tile_idx*x_accel_dim + x_accel_idx+1, y_tile_idx*y_accel_dim + y_accel_idx+1) = interp_result + grid_data_2D(x_tile_idx*x_accel_dim + x_accel_idx+1, y_tile_idx*y_accel_dim + y_accel_idx+1); % for debugging
                end
            end
        end
    end
end

%%%%%%%%%% Unpack Grid %%%%%%%%%%
for i = 0:x_tile_dim-1
    for j = 0:y_tile_dim-1
        x_min = i*x_accel_dim +1;
        x_max = (i+1)*x_accel_dim;
        y_min = j*y_accel_dim +1;
        y_max = (j+1)*y_accel_dim;
        tile_num = x_tile_dim*j+i +1;
        grid_data_tiled(x_min:x_max, y_min:y_max) = grid_data(:,:,tile_num);
    end
end



%%%%%%%%%% Shift and Output Grid %%%%%%%%%%
% hardware_interp = circshift(grid_data_2D, [3 3]);
hardware_interp = circshift(grid_data_tiled, [3 3]);

end
