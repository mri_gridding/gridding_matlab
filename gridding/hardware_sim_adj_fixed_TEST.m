function [ hardware_interp ] = hardware_sim_adj_fixed_TEST(python_filename, interp_mode, fi_cfg)
%   Detailed explanation goes here
load(python_filename, 'kdat', 'dims', 'table_mat', 'numpoints', 'Jlist', 'L', 'tm');
% kdat --- complex magnitude; one complex value for each tm coordinate
% dims --- dimensions of the output grid
% table_mat --- interpolation table; each dim (x or y) has one row
% numpoints --- size of the interpolation window
% Jlist --- interpolation window indices for numpoints*numpoints elements
% L --- table oversampling factor (L table points between every grid point)
% tm --- coordinates for the kdata samples
% griddat --- output grid arranged as a 1D vector

print_output = true


%%%%%%%%%% Constants Setup %%%%%%%%%%
SAMPLE_BIT_WIDTH        = 32;
SAMPLE_FRAC_WIDTH       = 30;
SAMPLE_COORD_BIT_WIDTH  = 32;
SAMPLE_COORD_FRAC_WIDTH = 22;
TABLE_BIT_WIDTH = 16;
TABLE_FRAC_WIDTH = 14;

x_grid_dim = double(dims(1));
y_grid_dim = double(dims(2));

X_ACCEL_DIM = 8;
Y_ACCEL_DIM = 8;

X_TILE_DIM = x_grid_dim / X_ACCEL_DIM;
Y_TILE_DIM = y_grid_dim / Y_ACCEL_DIM;

X_WINDOW_DIM = sqrt(size(Jlist, 2));
Y_WINDOW_DIM = sqrt(size(Jlist, 2));

x_accel_dim = X_ACCEL_DIM;
y_accel_dim = Y_ACCEL_DIM;

element_range_start = 1;
element_range_end = 460800;


%%%%%%%%%% Fixed Point Setup %%%%%%%%%%
fi_cfg.x_grid_dim = x_grid_dim;
fi_cfg.y_grid_dim = y_grid_dim;
fi_cfg.x_accel_dim = X_ACCEL_DIM;
fi_cfg.y_accel_dim = Y_ACCEL_DIM;
fi_cfg.x_tile_dim = X_TILE_DIM;
fi_cfg.y_tile_dim = Y_TILE_DIM;
fi_cfg = fixed_point_cfg(fi_cfg);


%%%%%%%%%% Quantize Inputs %%%%%%%%%%
input_coord = quantize(fi_cfg.input_coord, flipud(tm)); % flip the coordinates to match Python output
minmax(input_coord)
input_data = quantize(fi_cfg.input_data, kdat);
minmax(input_data)
interp_table = quantize(fi_cfg.interp_table, table_mat);
minmax(interp_table)

table_oversamp = L;

num_input_points = size(input_coord, 2);

grid_data = zeros(X_ACCEL_DIM, Y_ACCEL_DIM, ((x_grid_dim*y_grid_dim)/(X_ACCEL_DIM*Y_ACCEL_DIM)));
% grid_data_2D = zeros(x_grid_dim, y_grid_dim);

grid_data_tiled = zeros(x_grid_dim, y_grid_dim);

% load(sprintf('python_griddat.mat'));
% python_griddat_square = rot90(reshape(python_griddat, 768, 768));

clear kdat dims table_mat numpoints Jlist L tm

x_accel_idx_sim = 5;
y_accel_idx_sim = 5;


%%%%%%%%%% Interpolate Data %%%%%%%%%%
% Process the input array one point at a time, updating target grid points
fileID1 = fopen(sprintf('data/x_base_tile_idx.out'),'w');
fileID2 = fopen(sprintf('data/y_base_tile_idx.out'),'w');
fileID3 = fopen(sprintf('data/x_coord.out'),'w');
fileID4 = fopen(sprintf('data/y_coord.out'),'w');

fileID5 = fopen(sprintf('data/x_distance.out'),'w');
fileID6 = fopen(sprintf('data/y_distance.out'),'w');
fileID7 = fopen(sprintf('data/x_tile_idx.out'),'w');
fileID8 = fopen(sprintf('data/y_tile_idx.out'),'w');
% 
fileID9 = fopen(sprintf('data/xy_tile_idx.out'),'w');
fileID10 = fopen(sprintf('data/x_table_idx.out'),'w');
fileID11 = fopen(sprintf('data/y_table_idx.out'),'w');
fileID12 = fopen(sprintf('data/random.out'),'w');

fileID13 = fopen(sprintf('data/k1.out'),'w');
fileID14 = fopen(sprintf('data/k2.out'),'w');
fileID15 = fopen(sprintf('data/k3.out'),'w');
fileID16 = fopen(sprintf('data/k4.out'),'w');
fileID17 = fopen(sprintf('data/k5.out'),'w');
fileID18 = fopen(sprintf('data/k6.out'),'w');
fileID19 = fopen(sprintf('data/R1.out'),'w');
fileID20 = fopen(sprintf('data/I1.out'),'w');

fileID21 = fopen(sprintf('data/k7.out'),'w');
fileID22 = fopen(sprintf('data/k8.out'),'w');
fileID23 = fopen(sprintf('data/k9.out'),'w');
fileID24 = fopen(sprintf('data/k10.out'),'w');
fileID25 = fopen(sprintf('data/k11.out'),'w');
fileID26 = fopen(sprintf('data/k12.out'),'w');
fileID27 = fopen(sprintf('data/R2.out'),'w');
fileID28 = fopen(sprintf('data/I2.out'),'w');
% for input_point_idx = 1:num_input_points
for input_point_idx = element_range_start:element_range_end
    % Extract the current point being processed
    input_point_coord = input_coord(:, input_point_idx);
    input_point_data = input_data(:, input_point_idx);
    
    % Split the complex value into its real and imaginary components
    input_point_data_real = real(input_point_data);
    input_point_data_imag = imag(input_point_data);

    % Get the quotient and remainder to determine grid and tile location
    x_base_tile_idx = fix(input_point_coord(1)/X_ACCEL_DIM); % x virtual tile index
    temp = fi(x_base_tile_idx, false, SAMPLE_COORD_BIT_WIDTH-SAMPLE_COORD_FRAC_WIDTH-log2(X_ACCEL_DIM), 0);
    fprintf(fileID1,'%s\n', temp.bin);
    y_base_tile_idx = fix(input_point_coord(2)/Y_ACCEL_DIM); % y virtual tile index
    temp = fi(y_base_tile_idx, false, SAMPLE_COORD_BIT_WIDTH-SAMPLE_COORD_FRAC_WIDTH-log2(Y_ACCEL_DIM), 0);
    fprintf(fileID2,'%s\n', temp.bin);

    % Extract the "relative" coordinates (within a tile)
    x_coord = rem(input_point_coord(1), X_ACCEL_DIM);
    temp = fi(x_coord, false, log2(X_ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH, 22);
    fprintf(fileID3,'%s\n', temp.bin);
    y_coord = rem(input_point_coord(2), Y_ACCEL_DIM);
    temp = fi(y_coord, false, log2(Y_ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH, 22);
    fprintf(fileID4,'%s\n', temp.bin);

    % Loop over accelerator pipelines to process input point
    for x_accel_idx = 0:X_ACCEL_DIM-1
        % Determine if this pipeline is affected by this source point
        % If x_coord is within [x_accel_idx,x_accel_idx+X_WINDOW_DIM) of the x_accel_idx, it does
        x_distance = rem(X_ACCEL_DIM+x_coord-x_accel_idx, X_ACCEL_DIM); % rem is implemented as an upper truncation
        if(x_accel_idx == x_accel_idx_sim)
            temp = fi(x_distance, false, log2(X_ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH, 22);
            fprintf(fileID5,'%s\n', temp.bin);
            y_accel_idx_fake = 0;
            y_distance_fake = rem(Y_ACCEL_DIM+y_coord-y_accel_idx_fake, Y_ACCEL_DIM);
            temp = fi(y_distance_fake, false, log2(Y_ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH, 22);
            fprintf(fileID6,'%s\n', temp.bin);
        end
        if(x_distance < X_WINDOW_DIM)
%             if(x_accel_idx == x_accel_idx_sim)
%                 fprintf(fileID12,'x\n');
%             end
            for y_accel_idx = 0:Y_ACCEL_DIM-1
                % If y_coord is within [y_accel_idx,y_accel_idx+Y_WINDOW_DIM) of the y_accel_idx, it does
                y_distance = rem(Y_ACCEL_DIM+y_coord-y_accel_idx, Y_ACCEL_DIM); % rem is implemented as an upper truncation
%                 fprintf(fileID6,'%s\n', fi(y_distance, false, log2(Y_ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH, 22).bin);
                if(y_distance < Y_WINDOW_DIM)
                    % This point affects this pipeline!

                    % Determine if a wrap occured in the x-dim; if x_coord < x_accel_idx, a wrap occured
                    if(x_coord < x_accel_idx)
                        % A wrap occured!
                        if(x_base_tile_idx == 0)
                            x_tile_idx = X_TILE_DIM - 1;
                        else
                            x_tile_idx = x_base_tile_idx - 1;
                        end
                    else
                        x_tile_idx = x_base_tile_idx;
                    end

                    % Determine if a wrap occured in the y-dim; if y_coord < y_accel_idx, a wrap occured
                    if(y_coord < y_accel_idx)
                        % A wrap occured!
                        if(y_base_tile_idx == 0)
                            y_tile_idx = Y_TILE_DIM - 1;
                        else
                            y_tile_idx = y_base_tile_idx - 1;
                        end
                    else
                        y_tile_idx = y_base_tile_idx;
                    end

                    % Calculate the combined tile index ("depth" in the pipeline's data array)
                    xy_tile_idx = y_tile_idx * X_TILE_DIM + x_tile_idx; % 1D global virtual tile index

                    % Calculate the table index
                    x_table_idx = floor((x_distance * double(table_oversamp(1))) + 0.5); % *L (a power of 2) is implemented as a left shift
                    y_table_idx = floor((y_distance * double(table_oversamp(2))) + 0.5); % *L (a power of 2) is implemented as a left shift

                    % Extract the interp table values
                    x_table_val = interp_table(1, x_table_idx+1); % +1 because Matlab indexing starts at 1
                    y_table_val = interp_table(2, y_table_idx+1); % +1 because Matlab indexing starts at 1

                    % Split the complex value into its real and imaginary components
                    x_table_val_real = real(x_table_val);
                    x_table_val_imag = imag(x_table_val);
                    y_table_val_real = real(y_table_val);
                    y_table_val_imag = imag(y_table_val);

                    % Calculate 2D table value (product of X and Y values)
                    k1 = quantize(fi_cfg.k1, x_table_val_real + x_table_val_imag);
                    k2 = quantize(fi_cfg.k2, y_table_val_imag - y_table_val_real);
                    k3 = quantize(fi_cfg.k3, y_table_val_real + y_table_val_imag);
                    k4 = quantize(fi_cfg.k4, y_table_val_real * k1);
                    k5 = quantize(fi_cfg.k5, x_table_val_real * k2);
                    k6 = quantize(fi_cfg.k6, x_table_val_imag * k3);
                    R1 = quantize(fi_cfg.R1, k4 - k6);
                    I1 = quantize(fi_cfg.I1, k4 + k5);

                    % Calculate interpolation result; conjugate of interp table value incorporated into this calculation directly by flipping sign for I1
                    k7 = quantize(fi_cfg.k7, R1 - I1);
                    k8 = quantize(fi_cfg.k8, input_point_data_imag - input_point_data_real);
                    k9 = quantize(fi_cfg.k9, input_point_data_real + input_point_data_imag);
                    k10 = quantize(fi_cfg.k10, input_point_data_real * k7);
                    k11 = quantize(fi_cfg.k11, R1 * k8);
                    k12 = quantize(fi_cfg.k12, I1 * k9);
                    R2 = quantize(fi_cfg.R2, k10 + k12);
                    I2 = quantize(fi_cfg.I2, k10 + k11);
                    interp_result = complex(R2, I2);

                    % grid_data(x_accel_idx+1, y_accel_idx+1, tile_idx+1) = interp_result + grid_data(x_accel_idx+1, y_accel_idx+1, tile_idx+1); % +1 because Matlab indexing starts at 1
                    old_val = grid_data(x_accel_idx+1, y_accel_idx+1, xy_tile_idx+1);
                    accum_result = quantize(fi_cfg.interp_accum, interp_result + old_val); % +1 because Matlab indexing starts at 1
                    grid_data(x_accel_idx+1, y_accel_idx+1, xy_tile_idx+1) = accum_result; % +1 because Matlab indexing starts at 1

%                     grid_data_2D(x_tile_idx*X_ACCEL_DIM + x_accel_idx+1, y_tile_idx*Y_ACCEL_DIM + y_accel_idx+1) = interp_result + grid_data_2D(x_tile_idx*X_ACCEL_DIM + x_accel_idx+1, y_tile_idx*Y_ACCEL_DIM + y_accel_idx+1); % for debugging
                    if((x_accel_idx == x_accel_idx_sim) && (y_accel_idx == y_accel_idx_sim))
%                         fprintf(fileID12,'y\n');
                        temp = fi(x_tile_idx, false, ceil(log2(X_TILE_DIM)), 0);
                        fprintf(fileID7,'%s\n', temp.bin);
                        temp = fi(y_tile_idx, false, ceil(log2(Y_TILE_DIM)), 0);
                        fprintf(fileID8,'%s\n', temp.bin);
                        
                        temp = fi(xy_tile_idx, false, ceil(log2(X_TILE_DIM))+ceil(log2(Y_TILE_DIM)), 0);
                        fprintf(fileID9,'%s\n', temp.bin);
                        
                        temp = fi(x_table_idx, false, ceil(log2(double(table_oversamp(1)*X_ACCEL_DIM))), 0);
                        fprintf(fileID10,'%s\n', temp.bin);
                        temp = fi(y_table_idx, false, ceil(log2(double(table_oversamp(2)*Y_ACCEL_DIM))), 0);
                        fprintf(fileID11,'%s\n', temp.bin);
                        
                        
%                         fprintf(fileID12,'%s\n', fi(x_table_idx, false, ceil(log2(double(L(1)*X_ACCEL_DIM))), 0).bin);
%                         fprintf(fileID12,'%s\n', fi(real(x_table_val).bin);
%                         fprintf(fileID12,'%s\n', fi(imag(x_table_val).bin);
%                         fprintf(fileID12,'%s\n', fi(real(y_table_val).bin);
%                         fprintf(fileID12,'%s\n', fi(imag(y_table_val).bin);


                        temp = fi(k1, true, TABLE_BIT_WIDTH, TABLE_FRAC_WIDTH);
                        fprintf(fileID13,'%s\n', temp.bin);
                        temp = fi(k2, true, TABLE_BIT_WIDTH, TABLE_FRAC_WIDTH);
                        fprintf(fileID14,'%s\n', temp.bin);
                        temp = fi(k3, true, TABLE_BIT_WIDTH, TABLE_FRAC_WIDTH);
                        fprintf(fileID15,'%s\n', temp.bin);
                        temp = fi(k4, true, TABLE_BIT_WIDTH, TABLE_FRAC_WIDTH);
                        fprintf(fileID16,'%s\n', temp.bin);
                        temp = fi(k5, true, TABLE_BIT_WIDTH, TABLE_FRAC_WIDTH);
                        fprintf(fileID17,'%s\n', temp.bin);
                        temp = fi(k6, true, TABLE_BIT_WIDTH, TABLE_FRAC_WIDTH);
                        fprintf(fileID18,'%s\n', temp.bin);
                        temp = fi(R1, true, TABLE_BIT_WIDTH, TABLE_FRAC_WIDTH);
                        fprintf(fileID19,'%s\n', temp.bin);
                        temp = fi(I1, true, TABLE_BIT_WIDTH, TABLE_FRAC_WIDTH);
                        fprintf(fileID20,'%s\n', temp.bin);
                        
%                         fprintf(fileID12,'%s\n', fi(R1, true, TABLE_BIT_WIDTH, TABLE_FRAC_WIDTH).bin);
%                         fprintf(fileID12,'%s\n', fi(I1, true, TABLE_BIT_WIDTH, TABLE_FRAC_WIDTH).bin);
                        
                        % fprintf(fileID12,'%s\n', fi(input_point_data_real, true, 32, 40).bin);
%                         fprintf(fileID12,'%s\n', fi(input_point_data_imag, true, 32, 40).bin);
%                         fprintf(fileID12,'%s\n', fi(k7, true, 32, 30).bin);
                        % fprintf(fileID12,'%s\n', transpose(dec2bin(uint32(xy_tile_idx), ceil(log2((x_grid_dim*y_grid_dim)/(X_ACCEL_DIM*Y_ACCEL_DIM))))));
                        % fprintf(fileID12,'%s', fi(R2, true, 32, 36).bin);
                        % fprintf(fileID12,'%s\n', fi(I2, true, 32, 36).bin);
                        % fprintf(fileID12,'%s', fi(real(interp_result), true, 32, 36).bin);
                        % fprintf(fileID12,'%s\n', fi(imag(interp_result), true, 32, 36).bin);
                        % fprintf(fileID12,'%s', fi(real(old_val), true, 32, 36).bin);
                        % fprintf(fileID12,'%s\n', fi(imag(old_val), true, 32, 36).bin);
                        % fprintf(fileID12,'%s', fi(real(accum_result), true, 32, 36).bin);
                        % fprintf(fileID12,'%s\n', fi(imag(accum_result), true, 32, 36).bin);


                        temp = fi(k7, true, 32, 30);
                        fprintf(fileID21,'%s\n', temp.bin);
                        temp = fi(k8, true, 32, 30);
                        fprintf(fileID22,'%s\n', temp.bin);
                        temp = fi(k9, true, 32, 30);
                        fprintf(fileID23,'%s\n', temp.bin);
                        temp = fi(k10, true, SAMPLE_BIT_WIDTH, SAMPLE_FRAC_WIDTH+6);
                        fprintf(fileID24,'%s\n', temp.bin);
                        temp = fi(k11, true, SAMPLE_BIT_WIDTH, SAMPLE_FRAC_WIDTH+6);
                        fprintf(fileID25,'%s\n', temp.bin);
                        temp = fi(k12, true, SAMPLE_BIT_WIDTH, SAMPLE_FRAC_WIDTH+6);
                        fprintf(fileID26,'%s\n', temp.bin);
                        temp = fi(R2, true, SAMPLE_BIT_WIDTH, SAMPLE_FRAC_WIDTH+6);
                        fprintf(fileID27,'%s\n', temp.bin);
                        temp = fi(I2, true, SAMPLE_BIT_WIDTH, SAMPLE_FRAC_WIDTH+6);
                        fprintf(fileID28,'%s\n', temp.bin);
                    end
                end
            end
        end
    end
end

% for idx = 0:((x_grid_dim*y_grid_dim)/(X_ACCEL_DIM*Y_ACCEL_DIM))-1
%     result = grid_data(x_accel_idx_sim+1, y_accel_idx_sim+1, idx+1);
%     fprintf(fileID12,'%s', fi(real(result), true, 32, 36).bin);
%     fprintf(fileID12,'%s\n', fi(imag(result), true, 32, 36).bin);
% end


script


%%%%%%%%%% Print Output %%%%%%%%%%
if(print_output)
%     x_idx = 5;
%     y_idx = 5;
    fileID = fopen(sprintf('data/final_output_ALL.out'),'w');
    for x_idx = 0:x_accel_dim-1
        for y_idx = 0:y_accel_dim-1
            for z_idx = 0:((x_grid_dim*y_grid_dim)/(x_accel_dim*y_accel_dim))-1
                result = grid_data(x_idx+1, y_idx+1, z_idx+1);
                result_real = fi(real(result), true, 32, 36);
                result_imag = fi(imag(result), true, 32, 36);
                fprintf(fileID,'%s', result_real.bin);
                fprintf(fileID,'%s\n', result_imag.bin);
            end
        end
    end
    fclose(fileID);
end

%%%%%%%%%% Unpack Grid %%%%%%%%%%
for i = 0:X_TILE_DIM-1
    for j = 0:Y_TILE_DIM-1
        x_min = i*X_ACCEL_DIM +1;
        x_max = (i+1)*X_ACCEL_DIM;
        y_min = j*Y_ACCEL_DIM +1;
        y_max = (j+1)*Y_ACCEL_DIM;
        tile_num = X_TILE_DIM*j+i +1;
        grid_data_tiled(x_min:x_max, y_min:y_max) = grid_data(:,:,tile_num);
    end
end

fclose(fileID1);
fclose(fileID2);
fclose(fileID3);
fclose(fileID4);
fclose(fileID5);
fclose(fileID6);
fclose(fileID7);
fclose(fileID8);
fclose(fileID9);
fclose(fileID10);
fclose(fileID11);
fclose(fileID12);
fclose(fileID13);
fclose(fileID14);
fclose(fileID15);
fclose(fileID16);
fclose(fileID17);
fclose(fileID18);
fclose(fileID19);
fclose(fileID20);
fclose(fileID21);
fclose(fileID22);
fclose(fileID23);
fclose(fileID24);
fclose(fileID25);
fclose(fileID26);
fclose(fileID27);
fclose(fileID28);

%%%%%%%%%% Quantize Output %%%%%%%%%%
% hardware_interp = circshift(grid_data_2D, [3 3]);
hardware_interp = circshift(grid_data_tiled, [3 3]);

end
