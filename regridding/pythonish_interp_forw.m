% function kdat = run_interp(griddat, dims, table, numpoints, Jlist, L, tm, kdat)
function [ kdat ] = pythonish_interp_forw(python_filename, interp_mode, num_type, bit_width)
%   Detailed explanation goes here
load(python_filename, 'griddat', 'dims', 'table_mat', 'numpoints', 'Jlist', 'L', 'tm');
% griddat --- output grid arranged as a 1D vector
% dims --- dimensions of the output grid
% table_mat --- interpolation table; each dim (x or y) has one row
% numpoints --- size of the interpolation window
% Jlist --- interpolation window indices for numpoints*numpoints elements
% L --- table oversampling factor (L table points between every grid point)
% tm --- coordinates for the kdata samples
% kdat --- complex magnitude; one complex value for each tm coordinate

%%%%%%%%%% Fixed Point Setup %%%%%%%%%%
fi_cfg.mode = num_type;
fi_cfg.type = bit_width; % set bit width
fi_cfg.interp_mode = interp_mode; % set interp mode ('forw' or 'adj')
fi_cfg = fixed_point_cfg(fi_cfg);


% assign size variables for later use
M = size(tm, 2);
ndims = size(tm, 1);
nJ = size(Jlist, 2);

% center of tables
centers = floor(numpoints.*L/2);

% offset from k-space to first coef loc (top left corner of 2D window)
kofflist = 1 + floor(tm - ((double(numpoints')) / 2.0));

% do a bit of type management - ints for faster index comps
coef = complex(ones(M,1))';
arr_ind = zeros([M 1],'int64')';
dims = int64(dims);
kofflist = int64(kofflist);
Jlist = int64(Jlist);
kdat = complex(zeros(1, size(tm, 2)), 0);

if(size(griddat,1) ~= 1)
    griddat = reshape(griddat, 1, dims(1)*dims(2)); % Reshape into a vector to match Python output
end

%%%%%%%%%% Quantize Inputs %%%%%%%%%%
% tm = quantize(fi_cfg.input_coord, tm);
% kdat = quantize(fi_cfg.input_data, kdat);
% table_mat = quantize(fi_cfg.interp_table, table_mat);

tic
% loop over offsets and take advantage of numpy broadcasting
for Jind = 1:nJ
    % get the grid indices for this iteration (window index) - one index
    % per window per point ((0,0) through (numpoints-1,numpoints-1))
    curgridind = kofflist + Jlist(:, Jind);
    
    % find the distance from the intput point to the current grid point in
    % table_oversampling_factor units (distance*table_oversampling_factor)
    curdistind = int64(round((tm - double(curgridind)) .* double(L')));

    % reset the coefficients and array indices for this window index
    coef(:) = 1.0 + 0j;
    arr_ind(:) = 1;

    for d = 1:ndims
        % read the interp table values; add the table center's values to
        % the distance to account for the kofflist shift
        table_vals = table_mat(d, curdistind(d, :)+double(centers(d))+1);
        coef = coef .* table_vals;
        if(d == ndims)
            % handle wrapping in the last dimension (calculate 1D offset)
            arr_ind = arr_ind + mod(curgridind(d, :), dims(d));
        else
            % handle wrapping in the d-th dimension (calculate 1D offset)
            arr_ind = arr_ind + (mod(curgridind(d, :), dims(d)) * prod(dims(d+1:end)));
        end
    end

    % no danger of collisions for forward op
    kdat = kdat + coef .* griddat(arr_ind);
end
toc
fprintf('\nPythonish_Interp (ms): %f\n\n', toc * 1000)

% gpu_data_gen

%%%%%%%%%% Quantize Output %%%%%%%%%%
% griddat = quantize(fi_cfg.output_data, griddat);
kdat = kdat;

end
