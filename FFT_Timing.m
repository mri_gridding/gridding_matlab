iterations = 100;
N = 384;
sigma = 2;
data = rand(N*sigma, N*sigma);

lowest = 5000000000;
average = 0;
for idx = 1:iterations
    tic
    qwerty = ifft2(data);
    time = toc;
    average = average + time;
    if(time < lowest)
        lowest = time;
    end
end
fprintf('\nFFT Average (s): %f\n\n', average/iterations)
fprintf('\nFFT Lowest (s): %f\n\n', lowest)