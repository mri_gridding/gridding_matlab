%%%%%%%%%% Initial Setup  %%%%%%%%%%
clear
run('set_paths.m')
run('irt/setup.m')

%%%%%%%%%% Python Interpolation %%%%%%%%%%
if(strcmp(computer('arch'), 'win64'))
    system('"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\python.exe" kbnufft_forw_interp_FFT.py');
    system('"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\python.exe" kbnufft_adj_interp_IFFT.py');
elseif(strcmp(computer('arch'), 'glnxa64'))
    system('python3 kbnufft_forw_interp.py');
    system('python3 kbnufft_adj_interp_IFFT.py');
else
    fprintf('Error: only Windows and Linux are supported.\n');
    return;
end

%%%%%%%%%% Load Python Interpolation %%%%%%%%%%
load(sprintf('python_griddat_IFFT.mat'));
load(sprintf('python_kdat.mat'));

%%%%%%%%%% Matlab Interpolation %%%%%%%%%%
interp_mode = 'adj'; % must be 'forw' or 'adj'

if(strcmp(interp_mode, 'forw'))
    input_filename = 'run_interp_forw_var.mat';
    
    load('forw_x_pre_scaling_x_post.mat')
    
%     matlab_fft_data = matlab_fft_data(1:size(forw_scaling_coef, 1), 1:size(forw_scaling_coef, 2))';
%     matlab_fft_data = conj(matlab_fft_data .* forw_scaling_coef);
% 
%     fft_nrms_error = nrms(forw_x_scaled, matlab_fft_data)*100;
    
%     [matlab_data, matlab_fft_data] = hardware_interp_FFT(input_filename, interp_mode);
    
    nrms_error    = nrms(python_kdat, matlab_data)*100;
    max_perc_diff = (max(abs(matlab_data(:)-python_kdat(:))) / max(abs(python_kdat(:))))*100;
    
%     fprintf('\n\nFFT Normalized Root Mean Square Error: %f%%\n', fft_nrms_error)
    fprintf('\n\nGrid Normalized Root Mean Square Error: %f%%\n', nrms_error)
    fprintf('Grid Maximum [Point] Percent Difference: %f%%\n\n\n', max_perc_diff)
else
    input_filename = 'run_interp_adj_var_IFFT.mat';
%     [matlab_data, matlab_ifft_data] = hardware_interp_IFFT(input_filename, interp_mode);
    [matlab_data, hardware_config] = hardware_interp(input_filename, interp_mode);

    matlab_ifft_data = IFFT2_DIT_R2_TILED(matlab_data, hardware_config);

    % Unpack both arrays so they can be checked vs the Python output
    matlab_data = tile_to_matrix(matlab_data, hardware_config);
    matlab_ifft_data = tile_to_matrix(matlab_ifft_data, hardware_config);
    
    % Bit-reverse the matrix along all columns and rows
    matlab_ifft_data = bitrevorder(bitrevorder(matlab_ifft_data')');
    
    % input_filename = 'matlab_griddat_adj.mat';
    % python_kdat = pythonish_interp_forw(input_filename, interp_mode, 'double', 16);
    
    load('adj_x_pre_scaling_x_post.mat')
    
    matlab_ifft_data = matlab_ifft_data(1:size(adj_scaling_coef, 1), 1:size(adj_scaling_coef, 2))';
    matlab_ifft_data = conj(matlab_ifft_data .* adj_scaling_coef);
    
    ifft_nrms_error = nrms(adj_x_scaled, matlab_ifft_data)*100;
    
    % Resize the grid data since we unpacked the entire memory array
    matlab_data = matlab_data(1:hardware_config.x_grid_dim, 1:hardware_config.y_grid_dim);
    
    nrms_error    = nrms(python_griddat, matlab_data)*100;
    max_perc_diff = (max(abs(matlab_data(:)-python_griddat(:))) / max(abs(python_griddat(:))))*100;
    
    fprintf('\n\nGrid Normalized Root Mean Square Error: %f%%\n', nrms_error)
    fprintf('\n\nGrid Maximum [Point] Percent Difference: %f%%\n', max_perc_diff)
    fprintf('\n\nIFFT Normalized Root Mean Square Error: %f%%\n', ifft_nrms_error)
end

% matlab_data = pythonish_interp_forw(input_filename, interp_mode, 'double', 16);
% matlab_data = pythonish_interp_adj(input_filename, interp_mode, 'double', 16);
% matlab_data = pythonish_interp(input_filename, interp_mode, 'double', 32);

% matlab_data = hardware_interp(input_filename, interp_mode);
% matlab_data = hardware_interp_fixed(input_filename, interp_mode, 'double', 32);
% matlab_data = reshape(matlab_data, 1, 768*768);
% save(sprintf('matlab_data_double.mat'), 'matlab_data');

% save(sprintf('matlab_data.mat'), 'matlab_data');


%%%%%%%%%% Print Error Percentages %%%%%%%%%%
% if(strcmp(interp_mode, 'forw'))
%     nrms_error    = nrms(python_kdat, matlab_data)*100;
%     max_perc_diff = (max(abs(matlab_data(:)-python_kdat(:))) / max(abs(python_kdat(:))))*100;
% else
%     nrms_error    = nrms(python_griddat, matlab_data)*100;
%     max_perc_diff = (max(abs(matlab_data(:)-python_griddat(:))) / max(abs(python_griddat(:))))*100;
% end
% fprintf('\n\nGrid Normalized Root Mean Square Error: %f%%\n', nrms_error)
% fprintf('Grid Maximum [Point] Percent Difference: %f%%\n\n\n', max_perc_diff)


%%%%%%%%%% Perform IFFT %%%%%%%%%%
% if(strcmp(interp_mode, 'forw') && exist('matlab_fft_data', 'var'))
%     load('forw_x_pre_scaling_x_post.mat')
%     matlab_fft_data = matlab_fft_data(1:size(forw_scaling_coef, 1), 1:size(forw_scaling_coef, 2))';
%     matlab_fft_data = conj(matlab_fft_data .* forw_scaling_coef);
%     
%     fft_nrms_error = nrms(forw_x_scaled, matlab_fft_data)*100;
%     fprintf('\n\nIFFT Normalized Root Mean Square Error: %f%%\n', fft_nrms_error)
% elseif(strcmp(interp_mode, 'adj') && exist('matlab_ifft_data', 'var'))
%     load('adj_x_pre_scaling_x_post.mat')
% %     if(size(matlab_data, 1) == 1)
% %         matlab_data = reshape(matlab_data, 768, 768);
% %     end
% %     matlab_data = IFFT2_DIT_R2(matlab_data);
% %     matlab_data = IFFT2_DIT_R2_TILED(matlab_data, 768, 768, 1024, 1024);
% %     matlab_data = IFFT2_DIT_R2_TILE_UNPACK(matlab_data);
% %     matlab_data = matlab_data.*(1024*1024); % removed by removing /1024^2 in IFFT2_DIT_R2_TILED
%     matlab_ifft_data = matlab_ifft_data(1:size(adj_scaling_coef, 1), 1:size(adj_scaling_coef, 2))';
%     matlab_ifft_data = conj(matlab_ifft_data .* adj_scaling_coef);
%     
%     ifft_nrms_error = nrms(adj_x_scaled, matlab_ifft_data)*100;
%     fprintf('\n\nIFFT Normalized Root Mean Square Error: %f%%\n', ifft_nrms_error)
% end
