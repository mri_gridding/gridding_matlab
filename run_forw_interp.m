%%%%%%%%%% Initial Setup  %%%%%%%%%%
clear
run('set_paths.m')
run('irt/setup.m')

%%%%%%%%%% Python Interpolation %%%%%%%%%%
if(strcmp(computer('arch'), 'win64'))
	system(['"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\python.exe" kbnufft_forw_interp.py']);
elseif(strcmp(computer('arch'), 'glnxa64'))
	system(['python3 kbnufft_forw_interp.py']);
else
    fprintf('Error: only Windows and Linux are supported.\n');
    return;
end

%%%%%%%%%% Load Python Interpolation %%%%%%%%%%
load(sprintf('python_kdat.mat'));

%%%%%%%%%% Matlab Interpolation %%%%%%%%%%
tic
input_filename = 'run_interp_forw_var.mat';
% input_filename = 'matlab_griddat_adj.mat';
% python_kdat = pythonish_interp_forw(input_filename, 'adj', 'double', 16);

% matlab_kdat = pythonish_interp_forw(input_filename, 'adj', 'double', 16);
matlab_kdat = hardware_sim_forw(input_filename);

% save(sprintf('matlab_kdat.mat'), 'matlab_kdat');
% python_kdat = reshape(python_kdat, 768, 768);
datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF')

%%%%%%%%%% Compare Interpolation Results %%%%%%%%%%
% nrms(python_kdat, matlab_kdat)
% nrms(kdat, matlab_kdat)

%%%%%%%%%% Print Error Percentages %%%%%%%%%%
nrms_error = nrms(python_kdat, matlab_kdat)*100;
max_perc_diff = (max(abs(matlab_kdat(:)-python_kdat(:))) / max(abs(python_kdat(:))))*100;
% isequal(python_kdat, matlab_kdat)
% nrms(python_kdat, matlab_kdat)
fprintf('\n\nNormalized Root Mean Square Error: %f%%\n', nrms_error)
fprintf('Maximum [Pixel] Percent Difference: %f%%\n\n\n', max_perc_diff)
