%%%%%%%%%% Initial Setup  %%%%%%%%%%
clear
run('set_paths.m')
run('irt/setup.m')

%%%%%%%%%% Python Interpolation %%%%%%%%%%
if(strcmp(computer('arch'), 'win64'))
	system(['"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\python.exe" kbnufft_adj_interp.py']);
elseif(strcmp(computer('arch'), 'glnxa64'))
	system(['python3 kbnufft_adj_interp.py']);
else
    fprintf('Error: only Windows and Linux are supported.\n');
    return;
end

%%%%%%%%%% Load Python Interpolation %%%%%%%%%%
load(sprintf('python_griddat.mat'));
% load(sprintf('data\python_griddat_oversamp_1024_Feb5.mat'));
python_griddat = reshape(python_griddat, 768, 768); % Reshape into a square and rotate to match hardware output

%%%%%%%%%% Matlab Interpolation %%%%%%%%%%
input_filename = 'run_interp_adj_var.mat';
% tic
matlab_griddat = pythonish_interp_adj(input_filename, 'adj', 'double', 16);
% datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF')

% matlab_griddat = hardware_sim_adj(input_filename);
% matlab_griddat = hardware_sim_adj_TEST(input_filename, 'single_fixed_input_coord_interp_table');
% matlab_griddat = hardware_sim_adj_fixed(input_filename, 'adj', 'fixed', 32);
% matlab_griddat = hardware_sim_adj_fixed_TEST(input_filename, 'adj', 'fixed', 32);

% load('adj_x_pre_scaling_x_post.mat');
asdf1 = reshape(matlab_griddat, 768, 768);
tic
matlab_ifft_data = ifft2(asdf1);
% matlab_ifft_data = ifft2(asdf1, 384, 384);
% matlab_ifft_data = ifft2(asdf1, 512, 512);

%%% Adjoint NuFFT Step #3: Deapodize %%%
% matlab_ifft_data = (matlab_ifft_data .* adj_scaling_coef); % transpose adj data because Python is row-major
datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF')
% ifft_nrms_error = nrms(adj_x_scaled.', matlab_ifft_data)*100 % transpose adj data because Python is row-major

save(sprintf('matlab_griddat.mat'), 'matlab_griddat');
% python_griddat = reshape(python_griddat, 768, 768);

%%%%%%%%%% Compare Interpolation Results %%%%%%%%%%
% nrms(python_griddat, matlab_griddat)
% nrms(griddat, matlab_griddat)

%%%%%%%%%% Print Error Percentages %%%%%%%%%%
% minmax(python_griddat)
% minmax(matlab_griddat)
% python_griddat_square = reshape(python_griddat, 768, 768);
% matlab_griddat_square = reshape(matlab_griddat, 768, 768);
% find(reshape(python_griddat_square, 768, 768)~=0)
% python_griddat_square(find(reshape(python_griddat_square, 768, 768)~=0))'
% find(reshape(matlab_griddat_square, 768, 768))
% matlab_griddat_square(find(reshape(matlab_griddat_square, 768, 768)~=0))'
nrms_error = nrms(python_griddat, matlab_griddat)*100;
max_perc_diff = (max(abs(matlab_griddat(:)-python_griddat(:))) / max(abs(python_griddat(:))))*100;
% isequal(python_griddat, matlab_griddat)
% nrms(python_griddat, matlab_griddat)
fprintf('\n\nNormalized Root Mean Square Error: %f%%\n', nrms_error)
fprintf('Maximum [Pixel] Percent Difference: %f%%\n\n\n', max_perc_diff)
