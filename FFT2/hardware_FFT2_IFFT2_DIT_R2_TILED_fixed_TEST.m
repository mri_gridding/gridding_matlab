% 2D Radix 2 Decimation in Time; for any length of N*M (RADIX 2 DIT)
% Input         : Normal Order
% Output        : Bit-reversed
% Unpack/resort : unpack(data_matrix) -> bitrevorder(bitrevorder(data_matrix.').')
% Author        : NinjaNife

function [ data_matrix ] = hardware_FFT2_IFFT2_DIT_R2_TILED_fixed_TEST( data_matrix, nufft_type, fi_cfg, config )

%%%%%%%%%% Assign Inputs %%%%%%%%%%
x_accel_dim    = config.hardware.x_accel_dim;
y_accel_dim    = config.hardware.y_accel_dim;
x_mem_dim      = config.hardware.x_mem_dim;
y_mem_dim      = config.hardware.y_mem_dim;
x_mem_tile_dim = config.hardware.x_mem_tile_dim;
y_mem_tile_dim = config.hardware.y_mem_tile_dim;

x_grid_dim     = config.image.x_grid_dim;
y_grid_dim     = config.image.y_grid_dim;
x_tile_dim     = config.image.x_tile_dim;
y_tile_dim     = config.image.y_tile_dim;
x_img_dim      = config.image.x_img_dim;
y_img_dim      = config.image.y_img_dim;
x_img_tile_dim = config.image.x_img_tile_dim;
y_img_tile_dim = config.image.y_img_tile_dim;
x_fft_len      = config.image.x_fft_len;
y_fft_len      = config.image.y_fft_len;

data_matrix = quantize(fi_cfg.ifft_input, data_matrix);

% Load the bit-reversed array of twiddle values
load(sprintf('w_arr_descending_bitrevorder_N=%d.mat', y_fft_len))
w_arr = quantize(fi_cfg.interp_table, w_arr);

% Do an FFT/IFFT of each "col" (cols are stacked)
tic
start_val = 0;
step_val = 1;
end_val = y_fft_len/x_accel_dim;
offset = y_fft_len/2/x_accel_dim;
comb_pipe_offset = y_fft_len/2;
w_mask = int32(0);
bit_shift_offset = log2(1024)-log2(y_fft_len);
num_stages = log2(y_fft_len);

for stage=0:num_stages-1 % stages of transformation
    % Loop over accelerator pipelines to process input point
    for x_accel_idx = 0:x_accel_dim-1
        for y_accel_idx = 0:y_accel_dim-1
            if(stage < num_stages-3)
                % Loop over all columns before going to next stage
                for set_idx = 0:x_fft_len/x_accel_dim-1
                    set_offset = set_idx * x_mem_tile_dim;

                    iter1 = start_val;
                    iter2 = start_val+offset;
                    saved_val = iter2;

                    while(iter2 < end_val)
                        tile_idx_1 = int32(set_offset+iter1);
                        tile_idx_2 = int32(set_offset+iter2);

                        % tile_idx_1[6:(7-stage)] for stage > 0; else 0 (6:7 is zero bits)
                        w_addr = bitshift(bitand(tile_idx_1, w_mask), -(7-(stage+bit_shift_offset)));
                        
                        if(strcmp(nufft_type, 'forw'))
                            val1 = data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1);                          % value at first address
                            val2 = data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_2+1);                          % value at second address
                        else
                            val1 = swap_complex_components(data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1)); % value at first address
                            val2 = swap_complex_components(data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_2+1)); % value at second address
                        end

                        a1=val1+val2.*w_arr(w_addr+1); % 1-st part of the "butterfly" creating operation
                        b1=val1-val2.*w_arr(w_addr+1); % 2-nd part of the "butterfly" creating operation

                        if(strcmp(nufft_type, 'forw'))
                            data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1) = a1;                          % saving computation of the 1-st part
                            data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_2+1) = b1;                          % saving computation of the 2-nd part
                        else
                            data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1) = swap_complex_components(a1); % saving computation of the 1-st part
                            data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_2+1) = swap_complex_components(b1); % saving computation of the 2-nd part
                        end

                        if(iter1+step_val == saved_val)
                            iter1 = iter1+step_val+offset;
                            iter2 = iter2+step_val+offset;
                            saved_val = iter2;
                        else
                            iter1 = iter1+step_val;
                            iter2 = iter2+step_val;
                        end
                    end
                end
            else
                if(bitget(x_accel_idx, num_stages-stage) == 0) % num_stages instead of num_stages-1 because the first index is 1 in Matlab, not 0
                    % Loop over all columns before going to next stage
                    for set_idx = 0:x_fft_len/x_accel_dim-1
                        set_offset = set_idx * x_mem_tile_dim;

                        iter1 = start_val;
                        iter2 = start_val+offset;
                        saved_val = iter2;

                        while(iter2 < end_val)

                            tile_idx_1 = int32(set_offset+iter1);
                            tile_idx_2 = int32(set_offset+iter2);

                            % Stage 7: tile_idx_1[6:(7-stage)]. Stage 8: Concatenate tile_idx_1[6:0] and x_accel_idx[2]. Stage 9: Concatenate tile_idx_1[6:0] and x_accel_idx[2:1]
                            w_addr = bitshift(bitand(tile_idx_1, w_mask), -(7-(stage+bit_shift_offset))); % tile_idx_1[6:0] shifted to the left (for stage > 7)
                            append_bits = bitshift(bitand(int32(x_accel_idx), 6), -comb_pipe_offset); % x_accel_idx[2:1] shifted to the right
                            w_addr = bitor(w_addr, append_bits); % [tile_idx_1[6:0] x_accel_idx[2:1]]

                            if(strcmp(nufft_type, 'forw'))
                                val1 = data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1);                                           % value at first address
                                val2 = data_matrix(x_accel_idx+comb_pipe_offset+1, y_accel_idx+1, tile_idx_2+1);                          % value at second address
                            else
                                val1 = swap_complex_components(data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1));                  % value at first address
                                val2 = swap_complex_components(data_matrix(x_accel_idx+comb_pipe_offset+1, y_accel_idx+1, tile_idx_2+1)); % value at second address
                            end

                            a1=val1+val2.*w_arr(w_addr+1); % 1-st part of the "butterfly" creating operation
                            b1=val1-val2.*w_arr(w_addr+1); % 2-nd part of the "butterfly" creating operation
                            
                            if(strcmp(nufft_type, 'forw'))
                                data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1) = a1;                                           % saving computation of the 1-st part
                                data_matrix(x_accel_idx+comb_pipe_offset+1, y_accel_idx+1, tile_idx_2+1) = b1;                          % saving computation of the 2-nd part
                            else
                                data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1) = swap_complex_components(a1);                  % saving computation of the 1-st part
                                data_matrix(x_accel_idx+comb_pipe_offset+1, y_accel_idx+1, tile_idx_2+1) = swap_complex_components(b1); % saving computation of the 2-nd part
                            end

                            if(iter1+step_val == saved_val)
                                iter1 = iter1+step_val+offset;
                                iter2 = iter2+step_val+offset;
                                saved_val = iter2;
                            else
                                iter1 = iter1+step_val;
                                iter2 = iter2+step_val;
                            end
                        end
                    end
                end
            end
        end
    end
    offset = fix(offset / 2);
    comb_pipe_offset = fix(comb_pipe_offset / 2);
    w_mask = bitset(w_mask, max(7-(stage+bit_shift_offset), 1));
end


% Do an FFT/IFFT of each "row" (rows are stacked)
start_val = 0;
step_val = 128;
end_val = x_fft_len*2*y_accel_dim;
offset = x_fft_len*y_accel_dim;
comb_pipe_offset = x_fft_len/2;
w_mask = int32(0);
bit_shift_offset = log2(1024)-log2(x_fft_len);
num_stages = log2(x_fft_len);

for stage=0:num_stages-1 % stages of transformation
    % Loop over accelerator pipelines to process input point
    for x_accel_idx = 0:x_accel_dim-1
        for y_accel_idx = 0:y_accel_dim-1
            if(stage < num_stages-3)
                % Loop over all columns before going to next stage
                for set_idx = 0:y_fft_len/y_accel_dim-1

                    iter1 = start_val;
                    iter2 = start_val+offset;
                    saved_val = iter2;

                    while(iter2 < end_val)
                        tile_idx_1 = int32(set_idx+iter1);
                        tile_idx_2 = int32(set_idx+iter2);

                        % tile_idx_1[13:(14-stage)] for stage > 0; else 0 (13:14 is zero bits)
                        w_addr = bitshift(bitand(tile_idx_1, w_mask), -(14-(stage+bit_shift_offset)));
                        
                        if(strcmp(nufft_type, 'forw'))
                            val1 = data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1);                          % value at first address
                            val2 = data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_2+1);                          % value at second address
                        else
                            val1 = swap_complex_components(data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1)); % value at first address
                            val2 = swap_complex_components(data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_2+1)); % value at second address
                        end

                        a1=val1+val2.*w_arr(w_addr+1); % 1-st part of the "butterfly" creating operation
                        b1=val1-val2.*w_arr(w_addr+1); % 2-nd part of the "butterfly" creating operation

                        if(strcmp(nufft_type, 'forw'))
                            data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1) = a1;                          % saving computation of the 1-st part
                            data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_2+1) = b1;                          % saving computation of the 2-nd part
                        else
                            data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1) = swap_complex_components(a1); % saving computation of the 1-st part
                            data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_2+1) = swap_complex_components(b1); % saving computation of the 2-nd part
                        end

                        if(iter1+step_val == saved_val)
                            iter1 = iter1+step_val+offset;
                            iter2 = iter2+step_val+offset;
                            saved_val = iter2;
                        else
                            iter1 = iter1+step_val;
                            iter2 = iter2+step_val;
                        end
                    end
                end
            else
                if(bitget(y_accel_idx, num_stages-stage) == 0) % num_stages instead of num_stages-1 because the first index is 1 in Matlab, not 0
                    % Loop over all columns before going to next stage
                    for set_idx = 0:y_fft_len/y_accel_dim-1

                        iter1 = start_val;
                        iter2 = start_val;
                        saved_val = iter2;
                        
                        while(iter2 < end_val)

                            tile_idx_1 = int32(set_idx+iter1);
                            tile_idx_2 = int32(set_idx+iter2);

                            % Stage 7: tile_idx_1[13:(14-stage)]. Stage 8: Concatenate tile_idx_1[13:7] and y_accel_idx[2]. Stage 9: Concatenate tile_idx_1[13:7] and y_accel_idx[2:1]
                            w_addr = bitshift(bitshift(bitand(tile_idx_1, w_mask), -max(14-(stage+bit_shift_offset), 7)), mod(-(14-(stage+bit_shift_offset)),7)); % tile_idx_1[13:7] shifted to the left (for stage > 7)
                            append_bits = bitshift(bitand(int32(y_accel_idx), 6), -comb_pipe_offset); % y_accel_idx[2:1] shifted to the right
                            w_addr = bitor(w_addr, append_bits); % [tile_idx_1[13:7] y_accel_idx[2:1]]

                            if(strcmp(nufft_type, 'forw'))
                                val1 = data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1);                                           % value at first address
                                val2 = data_matrix(x_accel_idx+1, y_accel_idx+comb_pipe_offset+1, tile_idx_2+1);                          % value at second address
                            else
                                val1 = swap_complex_components(data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1));                  % value at first address
                                val2 = swap_complex_components(data_matrix(x_accel_idx+1, y_accel_idx+comb_pipe_offset+1, tile_idx_2+1)); % value at second address
                            end

                            a1=val1+val2.*w_arr(w_addr+1); % 1-st part of the "butterfly" creating operation
                            b1=val1-val2.*w_arr(w_addr+1); % 2-nd part of the "butterfly" creating operation
                            
                            if(strcmp(nufft_type, 'forw'))
                                data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1) = a1;                                           % saving computation of the 1-st part
                                data_matrix(x_accel_idx+1, y_accel_idx+comb_pipe_offset+1, tile_idx_2+1) = b1;                          % saving computation of the 2-nd part
                            else
                                data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1) = swap_complex_components(a1);                  % saving computation of the 1-st part
                                data_matrix(x_accel_idx+1, y_accel_idx+comb_pipe_offset+1, tile_idx_2+1) = swap_complex_components(b1); % saving computation of the 2-nd part
                            end

                            if(iter1+step_val == saved_val)
                                iter1 = iter1+step_val;
                                iter2 = iter2+step_val;
                                saved_val = iter2;
                            else
                                iter1 = iter1+step_val;
                                iter2 = iter2+step_val;
                            end
                        end
                    end
                end
            end
        end
    end
    offset = fix(offset / 2);
    comb_pipe_offset = fix(comb_pipe_offset / 2);
    w_mask = bitset(w_mask, max(14-(stage+bit_shift_offset), 1));
end
if(strcmp(nufft_type, 'forw'))
    fprintf('2D FFT Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
else
    fprintf('2D IFFT Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
end

end
