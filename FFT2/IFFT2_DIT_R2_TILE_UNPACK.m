                                       % 2D Radix 2 Decimation in Time
                                       % Input : Normal Order (Input Bits reversed)
                                       % Output: Normal Order
                                       % For the any length of N*M (RADIX 2 DIT)
                                       % 1D done by Nevin Alex Jacob
                                       % Modified by NinjaNife
                                       % example : IFFT2_DIT_R2([1 2; 3 4])

function [ data_matrix ] = IFFT2_DIT_R2_TILE_UNPACK( data_matrix_tiled ) % entry point to the function of FFT decimation in time
x_accel_dim    = 8;
y_accel_dim    = 8;
x_mem_dim      = 1024;
y_mem_dim      = 1024;
x_mem_tile_dim = x_mem_dim / x_accel_dim;
y_mem_tile_dim = y_mem_dim / y_accel_dim;
x_grid_dim     = 1024;
y_grid_dim     = 1024;
x_tile_dim     = x_grid_dim / x_accel_dim;
y_tile_dim     = y_grid_dim / y_accel_dim;
data_matrix      = zeros(x_grid_dim, y_grid_dim);
for i = 0:x_tile_dim-1
    for j = 0:y_tile_dim-1
        x_min = i*x_accel_dim +1;
        x_max = (i+1)*x_accel_dim;
        y_min = j*y_accel_dim +1;
        y_max = (j+1)*y_accel_dim;
        tile_num = x_mem_tile_dim*j+i +1;
        data_matrix(x_min:x_max, y_min:y_max) = data_matrix_tiled(:,:,tile_num);
    end
end

data_matrix = bitrevorder(bitrevorder(data_matrix')');

end
