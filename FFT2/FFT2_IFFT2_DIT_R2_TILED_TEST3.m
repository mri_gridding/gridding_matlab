% 2D Radix 2 Decimation in Time; for any length of N*M (RADIX 2 DIT)
% Input         : Normal Order
% Output        : Bit-reversed
% Unpack/resort : unpack(data_matrix) -> bitrevorder(bitrevorder(data_matrix.').')
% Author        : NinjaNife

function [ data_matrix ] = FFT2_IFFT2_DIT_R2_TILED_TEST3( data_matrix, nufft_type, config )

x_accel_dim    = config.hardware.x_accel_dim;
y_accel_dim    = config.hardware.y_accel_dim;
x_mem_dim      = config.hardware.x_mem_dim;
y_mem_dim      = config.hardware.y_mem_dim;
x_mem_tile_dim = config.hardware.x_mem_tile_dim;
y_mem_tile_dim = config.hardware.y_mem_tile_dim;

x_grid_dim     = config.image.x_grid_dim;
y_grid_dim     = config.image.y_grid_dim;
x_tile_dim     = config.image.x_tile_dim;
y_tile_dim     = config.image.y_tile_dim;
x_img_dim      = config.image.x_img_dim;
y_img_dim      = config.image.y_img_dim;
x_img_tile_dim = config.image.x_img_tile_dim;
y_img_tile_dim = config.image.y_img_tile_dim;
x_fft_len      = config.image.x_fft_len;
y_fft_len      = config.image.y_fft_len;

% z_len = (x_fft_len*y_fft_len)/(size(data_matrix, 1)*size(data_matrix, 2));

% if(z_len > size(data_matrix, 3))
%     data_matrix = padarray(data_matrix, [0 0 (z_len-size(data_matrix, 3))], 0, 'post');
% end

% Do an FFT/IFFT of each "col" (cols are stacked)
tic
N=y_fft_len;                             % computing the array size
S=log2(N);                               % computing the number of conversion stages
addr_list = [0:N-1];        % Create array of addresses in bit-reversed order
for col = 0:y_fft_len-1
    Half=1;                                  % Setting the initial "Half" value
    for stage=1:S                            % stages of transformation
        for index=0:(2^stage):(N-1)          % series of "butterflies" for each stage
            for n=0:(Half-1)                 % creating "butterfly" and saving the results
                pow=(2^(S-stage))*n;         % part of power of the complex multiplier
                w=exp((-1i)*(2*pi)*pow/N);   % complex multiplier

                pos=n+index+1;               % index of the data sample
                addr1 = addr_list(pos);      % bit-reversed address
                addr2 = addr_list(pos+Half); % bit-reversed address

                accel_idx_1 = mod(addr1, 8); % pipeline index/location (bank ID) of first address
                accel_idx_2 = mod(addr2, 8); % pipeline index/location (bank ID) of second address

                accel_idx_y = mod(col, 8);
                y_tile_idx  = fix(col / 8);

                tile_idx_1 = y_tile_idx * x_mem_tile_dim + fix(addr1 / 8); % "depth" of first address
                tile_idx_2 = y_tile_idx * x_mem_tile_dim + fix(addr2 / 8); % "depth" of second address

                if(strcmp(nufft_type, 'forw'))
                    val1 = data_matrix(accel_idx_1+1, accel_idx_y+1, tile_idx_1+1);                          % value at first address
                    val2 = data_matrix(accel_idx_2+1, accel_idx_y+1, tile_idx_2+1);                          % value at second address
                else
                    val1 = swap_complex_components(data_matrix(accel_idx_1+1, accel_idx_y+1, tile_idx_1+1)); % value at first address
                    val2 = swap_complex_components(data_matrix(accel_idx_2+1, accel_idx_y+1, tile_idx_2+1)); % value at second address
                end

                a1=val1+val2.*w; % 1-st part of the "butterfly" creating operation
                b1=val1-val2.*w; % 2-nd part of the "butterfly" creating operation
                
%                 if(stage == S)
%                     a1 = a1./1024;
%                     b1 = b1./1024;
%                 end

                if(strcmp(nufft_type, 'forw'))
                    data_matrix(accel_idx_1+1, accel_idx_y+1, tile_idx_1+1) = a1;                          % saving computation of the 1-st part
                    data_matrix(accel_idx_2+1, accel_idx_y+1, tile_idx_2+1) = b1;                          % saving computation of the 2-nd part
                else
                    data_matrix(accel_idx_1+1, accel_idx_y+1, tile_idx_1+1) = swap_complex_components(a1); % saving computation of the 1-st part
                    data_matrix(accel_idx_2+1, accel_idx_y+1, tile_idx_2+1) = swap_complex_components(b1); % saving computation of the 2-nd part
                end

            end
        end
        Half=2*Half; % computing the next "Half" value
    end
end
% data_matrix = data_matrix ./ 1024; % TODO: remove this and don't swap components in next loop?


% Do an FFT/IFFT of each "row" (rows are stacked)
N=x_fft_len;                             % computing the array size
S=log2(N);                               % computing the number of conversion stages
addr_list = [0:N-1];        % Create array of addresses in bit-reversed order
for row = 0:x_fft_len-1
    Half=1;                                  % Setting the initial "Half" value
    for stage=1:S                            % stages of transformation
        for index=0:(2^stage):(N-1)          % series of "butterflies" for each stage
            for n=0:(Half-1)                 % creating "butterfly" and saving the results
                pow=(2^(S-stage))*n;         % part of power of the complex multiplier
                w=exp((-1i)*(2*pi)*pow/N);   % complex multiplier

                pos=n+index+1;               % index of the data sample
                addr1 = addr_list(pos);      % bit-reversed address
                addr2 = addr_list(pos+Half); % bit-reversed address

                accel_idx_1 = mod(addr1, 8);
                accel_idx_2 = mod(addr2, 8);

                accel_idx_x = mod(row, 8);
                x_tile_idx  = fix(row / 8);

                y_tile_idx1 = fix(addr1 / 8);
                y_tile_idx2 = fix(addr2 / 8);

                tile_idx_1 = y_tile_idx1 * y_mem_tile_dim + x_tile_idx; % "depth" of first address
                tile_idx_2 = y_tile_idx2 * y_mem_tile_dim + x_tile_idx; % "depth" of second address

                if(strcmp(nufft_type, 'forw'))
                    val1 = data_matrix(accel_idx_x+1, accel_idx_1+1, tile_idx_1+1);                          % value at first address
                    val2 = data_matrix(accel_idx_x+1, accel_idx_2+1, tile_idx_2+1);                          % value at second address
                else
                    val1 = swap_complex_components(data_matrix(accel_idx_x+1, accel_idx_1+1, tile_idx_1+1)); % value at first address
                    val2 = swap_complex_components(data_matrix(accel_idx_x+1, accel_idx_2+1, tile_idx_2+1)); % value at second address
                end

                a1=val1+val2.*w; % 1-st part of the "butterfly" creating operation
                b1=val1-val2.*w; % 2-nd part of the "butterfly" creating operation
                
%                 if(stage == S)
%                     a1 = a1./1024;
%                     b1 = b1./1024;
%                 end

                if(strcmp(nufft_type, 'forw'))
                    data_matrix(accel_idx_x+1, accel_idx_1+1, tile_idx_1+1) = a1;                          % saving computation of the 1-st part
                    data_matrix(accel_idx_x+1, accel_idx_2+1, tile_idx_2+1) = b1;                          % saving computation of the 2-nd part
                else
                    data_matrix(accel_idx_x+1, accel_idx_1+1, tile_idx_1+1) = swap_complex_components(a1); % saving computation of the 1-st part
                    data_matrix(accel_idx_x+1, accel_idx_2+1, tile_idx_2+1) = swap_complex_components(b1); % saving computation of the 2-nd part
                end
            end
        end
        Half=2*Half; % computing the next "Half" value
    end
end
% data_matrix = data_matrix ./ (1024*1024); % TODO: remove this and don't swap components in next loop?
if(strcmp(nufft_type, 'forw'))
    fprintf('2D FFT Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
else
    fprintf('2D IFFT Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
end

end
