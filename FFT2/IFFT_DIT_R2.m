                                       % Radix 2 Decimation in Time
                                       % Input : Normal Order (Input Bits reversed)
                                       % Output: Normal Order
                                       % For the any length of N (RADIX 2 DIT)
                                       % Done by Nevin Alex Jacob
                                       % Modified by Nazar Hnydyn
                                       % example : FFT_DIT_R2([1 2 3 4 5 ])

function [ y ] = IFFT_DIT_R2( x )      % entry point to the function of FFT decimation in time
p=nextpow2(length(x));                 % checking the size of the input array
x=swap_complex_components(x); % can also just take the conjugate; x=conj(x)
x=[x zeros(1,(2^p)-length(x))];        % complementing an array of zeros if necessary
N=length(x);                           % computing the array size
S=log2(N);                             % computing the number of conversion stages
Half=1;                                % Seting the initial "Half" value
x=bitrevorder(x);                      % Placing the data samples in bit-reversed order
% fileID1 = fopen(sprintf('data/fft_addr1.out'), 'w');
% fileID2 = fopen(sprintf('data/fft_addr2.out'), 'w');
% fileID3 = fopen(sprintf('data/fft_addr3.out'), 'w');
% fileID4 = fopen(sprintf('data/fft_addr4.out'), 'w');
% fileID5 = fopen(sprintf('data/fft_addr5.out'), 'w');
for stage=1:S                          % stages of transformation
%     fprintf(fileID1,'***Stage %d***\n', stage);
%     fprintf(fileID2,'***Stage %d***\n', stage);
%     fprintf(fileID3,'***Stage %d***\n', stage);
%     fprintf(fileID4,'***Stage %d***\n', stage);
%     fprintf(fileID5,'***Stage %d***\n', stage);
    for index=0:(2^stage):(N-1)        % series of "butterflies" for each stage
        for n=0:(Half-1)               % creating "butterfly" and saving the results
            pos=n+index+1;             % index of the data sample
            pow=(2^(S-stage))*n;       % part of power of the complex multiplier
            w=exp((-1i)*(2*pi)*pow/N); % complex multiplier
            a=x(pos)+x(pos+Half).*w;   % 1-st part of the "butterfly" creating operation
            b=x(pos)-x(pos+Half).*w;   % 2-nd part of the "butterfly" creating operation
            x(pos)=a;                  % saving computation of the 1-st part
            x(pos+Half)=b;             % saving computation of the 2-nd part
%             fprintf(fileID1,'%d\n', pos);
%             fprintf(fileID2,'%d\n', pos+Half);
%             fprintf(fileID3,'%d : %d\n', pos, pos+Half);
%             fprintf(fileID4,'%d : %d\n', mod(pos, 8), mod(pos+Half, 8));
%             fprintf(fileID5,'%d : %d\n', fix(pos/8), fix(pos+Half/8));
        end
    end
Half=2*Half;                           % computing the next "Half" value
end
% fclose(fileID1);
% fclose(fileID2);
% fclose(fileID3);
% fclose(fileID4);
% fclose(fileID5);
x=swap_complex_components(x)./1024; % can also just take the conjugate; x=conj(x)
y=x;                                   % returning the result from function
end