                                       % 2D Radix 2 Decimation in Time
                                       % Input : Normal Order (Input Bits reversed)
                                       % Output: Normal Order
                                       % For the any length of N*M (RADIX 2 DIT)
                                       % 1D done by Nevin Alex Jacob
                                       % Modified by NinjaNife
                                       % example : IFFT2_DIT_R2([1 2; 3 4])

function [ data_matrix1, data_matrix2, data_matrix3, data_matrix4 ] = IFFT2_DIT_R2_TILED_TEST( data_matrix1, data_matrix2, data_matrix3, data_matrix4, x_grid_dim, y_grid_dim, x_img_dim, y_img_dim ) % entry point to the function of FFT decimation in time
x_len = 2^nextpow2(x_grid_dim);
y_len = 2^nextpow2(y_grid_dim);

x_mem_tile_dim = ceil(x_img_dim / 8);
y_mem_tile_dim = ceil(y_img_dim / 8);

z_len = (x_len*y_len)/(size(data_matrix1, 1)*size(data_matrix1, 2));

data_matrix1 = padarray(data_matrix1, [0 0 (z_len-size(data_matrix1, 3))], 0, 'post');

data_matrix2 = padarray(data_matrix2, [0 0 (z_len-size(data_matrix2, 3))], 0, 'post');

x_len2 = 2^nextpow2(size(data_matrix3, 1));
y_len2 = 2^nextpow2(size(data_matrix3, 2));
data_matrix3 = padarray(data_matrix3, [(x_len2-size(data_matrix3, 1)) (y_len2-size(data_matrix3, 2))], 0, 'post');

data_matrix4 = padarray(data_matrix4, [(x_len2-size(data_matrix4, 1)) (y_len2-size(data_matrix4, 2))], 0, 'post');

% Do an IFFT of each "row" (rows are stacked)
for row = 0:y_len-1
    N=x_img_dim;                           % computing the array size
    S=log2(N);                             % computing the number of conversion stages
    Half=1;                                % Setting the initial "Half" value
    addr_list = bitrevorder([0:N-1]);        % Create array of addresses in bit-reversed order

    x_vec = data_matrix3(:, row+1);
    x_vec = swap_complex_components(x_vec);
    x_vec=bitrevorder(x_vec);                      % Placing the data samples in bit-reversed order

    x_vec_v2 = data_matrix4(:, row+1);
    x_vec_v2 = swap_complex_components(x_vec_v2);
    x_vec_v2=bitrevorder(x_vec_v2);                      % Placing the data samples in bit-reversed order
    for stage=1:S                          % stages of transformation
        for index=0:(2^stage):(N-1)        % series of "butterflies" for each stage
            for n=0:(Half-1)               % creating "butterfly" and saving the results
                pow=(2^(S-stage))*n;       % part of power of the complex multiplier
                w=exp((-1i)*(2*pi)*pow/N); % complex multiplier

                pos=n+index+1;             % index of the data sample
                addr1 = addr_list(pos);      % bit-reversed address
                addr2 = addr_list(pos+Half); % bit-reversed address
%                 addr1 = addr_list(pos)-3;      % bit-reversed address
%                 addr2 = addr_list(pos+Half)-3; % bit-reversed address
%                 if(addr1 < 0)
%                     addr1 = mod(addr_list(pos)-3, x_grid_dim-1);
%                 end
%                 if(addr2 < 0)
%                     addr2 = mod(addr_list(pos+Half)-3, x_grid_dim-1);
%                 end

                accel_idx_1 = mod(addr1, 8);
                accel_idx_2 = mod(addr2, 8);

                accel_idx_y = mod(row, 8);
                y_tile_idx = fix(row / 8);

                tile_idx_1 = y_tile_idx * x_mem_tile_dim + fix(addr1 / 8);
                tile_idx_2 = y_tile_idx * x_mem_tile_dim + fix(addr2 / 8);

                val1 = swap_complex_components(data_matrix1(accel_idx_1+1, accel_idx_y+1, tile_idx_1+1));
                val2 = swap_complex_components(data_matrix1(accel_idx_2+1, accel_idx_y+1, tile_idx_2+1));

                a1=val1+val2.*w;   % 1-st part of the "butterfly" creating operation
                b1=val1-val2.*w;   % 2-nd part of the "butterfly" creating operation

                x_vec_pos = x_vec(pos);
                x_vec_posHalf = x_vec(pos+Half);
                a2=x_vec_pos+x_vec_posHalf.*w;   % 1-st part of the "butterfly" creating operation
                b2=x_vec_pos-x_vec_posHalf.*w;   % 2-nd part of the "butterfly" creating operation
                x_vec(pos)=a2;                  % saving computation of the 1-st part
                x_vec(pos+Half)=b2;             % saving computation of the 2-nd part

                if(a1 ~= a2)
                    mismatch = 1;
                end
                if(b1 ~= b2)
                    mismatch = 1;
                end

%                 addr1_v2 = addr_list(pos);      % bit-reversed address
%                 addr2_v2 = addr_list(pos+Half); % bit-reversed address
                addr1_v2 = addr_list(pos)-3;      % bit-reversed address
                addr2_v2 = addr_list(pos+Half)-3; % bit-reversed address
                if(addr1_v2 < 0)
                    addr1_v2 = mod(addr1_v2, x_grid_dim);
                end
                if(addr2_v2 < 0)
                    addr2_v2 = mod(addr2_v2, x_grid_dim);
                end
                accel_idx_1_v2 = mod(addr1_v2, 8);
                accel_idx_2_v2 = mod(addr2_v2, 8);
                if(row-3 < 0)
                    row_v2 = mod(row-3, x_grid_dim);
                end
                accel_idx_y_v2 = mod(row_v2, 8);
                y_tile_idx_v2 = fix((row_v2) / 8);
                tile_idx_1_v2 = y_tile_idx_v2 * x_mem_tile_dim + fix(addr1_v2 / 8);
                tile_idx_2_v2 = y_tile_idx_v2 * x_mem_tile_dim + fix(addr2_v2 / 8);
                val1_v2 = swap_complex_components(data_matrix2(accel_idx_1_v2+1, accel_idx_y_v2+1, tile_idx_1_v2+1));
                val2_v2 = swap_complex_components(data_matrix2(accel_idx_2_v2+1, accel_idx_y_v2+1, tile_idx_2_v2+1));
                a1_v2=val1_v2+val2_v2.*w;   % 1-st part of the "butterfly" creating operation
                b1_v2=val1_v2-val2_v2.*w;   % 2-nd part of the "butterfly" creating operation
                if(val1 ~= val1_v2)
                    mismatch = 1;
                end
                if(val2 ~= val2_v2)
                    mismatch = 1;
                end

                x_vec_v2_pos = x_vec_v2(pos);
                x_vec_v2_posHalf = x_vec_v2(pos+Half);
                a2_v2=x_vec_v2_pos+x_vec_v2_posHalf.*w;   % 1-st part of the "butterfly" creating operation
                b2_v2=x_vec_v2_pos-x_vec_v2_posHalf.*w;   % 2-nd part of the "butterfly" creating operation
                x_vec_v2(pos)=a2_v2;                  % saving computation of the 1-st part
                x_vec_v2(pos+Half)=b2_v2;             % saving computation of the 2-nd part

                if(a1_v2 ~= a2_v2)
                    mismatch = 1;
                end
                if(b1_v2 ~= b2_v2)
                    mismatch = 1;
                end

                data_matrix2(accel_idx_1_v2+1, accel_idx_y_v2+1, tile_idx_1_v2+1) = swap_complex_components(a1_v2);             % saving computation of the 1-st part
                data_matrix2(accel_idx_2_v2+1, accel_idx_y_v2+1, tile_idx_2_v2+1) = swap_complex_components(b1_v2);             % saving computation of the 2-nd part

                data_matrix1(accel_idx_1+1, accel_idx_y+1, tile_idx_1+1) = swap_complex_components(a1);             % saving computation of the 1-st part
                data_matrix1(accel_idx_2+1, accel_idx_y+1, tile_idx_2+1) = swap_complex_components(b1);             % saving computation of the 2-nd part
            end
        end
        Half=2*Half;                    % computing the next "Half" value
    end
    x_vec=swap_complex_components(x_vec)./1024; % can also just take the conjugate; x=conj(x)
    data_matrix3(:, row+1) = x_vec;               % returning the result from function

    x_vec_v2=swap_complex_components(x_vec_v2)./1024; % can also just take the conjugate; x=conj(x)
    data_matrix4(:, row+1) = x_vec_v2;               % returning the result from function
end

% data_matrix = swap_complex_components(data_matrix) ./ 1024; % TODO: remove this and don't swap components in next loop?
data_matrix1 = data_matrix1 ./ 1024; % TODO: remove this and don't swap components in next loop?

data_matrix2 = data_matrix2 ./ 1024; % TODO: remove this and don't swap components in next loop?


% Do an IFFT of each "col" (cols are stacked)
for col = 0:x_len-1
    N=x_img_dim;                           % computing the array size
    S=log2(N);                             % computing the number of conversion stages
    Half=1;                                % Setting the initial "Half" value
    addr_list = bitrevorder([0:N-1]);        % Create array of addresses in bit-reversed order

    x_vec = data_matrix3(col+1, :);
    x_vec = swap_complex_components(x_vec);
    x_vec=bitrevorder(x_vec);                      % Placing the data samples in bit-reversed order

    x_vec_v2 = data_matrix4(col+1, :);
    x_vec_v2 = swap_complex_components(x_vec_v2);
    x_vec_v2=bitrevorder(x_vec_v2);                      % Placing the data samples in bit-reversed order
    for stage=1:S                          % stages of transformation
        for index=0:(2^stage):(N-1)        % series of "butterflies" for each stage
            for n=0:(Half-1)               % creating "butterfly" and saving the results
                pow=(2^(S-stage))*n;       % part of power of the complex multiplier
                w=exp((-1i)*(2*pi)*pow/N); % complex multiplier

                pos=n+index+1;             % index of the data sample
                addr1 = addr_list(pos);      % bit-reversed address
                addr2 = addr_list(pos+Half); % bit-reversed address
%                 addr1 = addr_list(pos)-3;      % bit-reversed address
%                 addr2 = addr_list(pos+Half)-3; % bit-reversed address
%                 if(addr1 < 0)
%                     addr1 = mod(addr_list(pos)-3, y_grid_dim-1);
%                 end
%                 if(addr2 < 0)
%                     addr2 = mod(addr_list(pos+Half)-3, y_grid_dim-1);
%                 end

                accel_idx_1 = mod(addr1, 8);
                accel_idx_2 = mod(addr2, 8);

                accel_idx_x = mod(col, 8);
                x_tile_idx = fix(col / 8);
                
                y_tile_idx1 = fix(addr1/8);
                y_tile_idx2 = fix(addr2/8);

                tile_idx_1 = y_tile_idx1 * y_mem_tile_dim + x_tile_idx;
                tile_idx_2 = y_tile_idx2 * y_mem_tile_dim + x_tile_idx;

                val1 = swap_complex_components(data_matrix1(accel_idx_x+1, accel_idx_1+1, tile_idx_1+1));
                val2 = swap_complex_components(data_matrix1(accel_idx_x+1, accel_idx_2+1, tile_idx_2+1));

                a1=val1+val2.*w;   % 1-st part of the "butterfly" creating operation
                b1=val1-val2.*w;   % 2-nd part of the "butterfly" creating operation

                x_vec_pos = x_vec(pos);
                x_vec_posHalf = x_vec(pos+Half);
                a2=x_vec_pos+x_vec_posHalf.*w;   % 1-st part of the "butterfly" creating operation
                b2=x_vec_pos-x_vec_posHalf.*w;   % 2-nd part of the "butterfly" creating operation
                x_vec(pos)=a2;                  % saving computation of the 1-st part
                x_vec(pos+Half)=b2;             % saving computation of the 2-nd part

                % TODO: make these checks work (they don't because columns
                % are out of order)
%                 if(a1 ~= a2)
%                     mismatch = 1;
%                 end
%                 if(b1 ~= b2)
%                     mismatch = 1;
%                 end

%                 addr1_v2 = addr_list(pos);      % bit-reversed address
%                 addr2_v2 = addr_list(pos+Half); % bit-reversed address
                addr1_v2 = addr_list(pos)-3;      % bit-reversed address
                addr2_v2 = addr_list(pos+Half)-3; % bit-reversed address
                if(addr1_v2 < 0)
                    addr1_v2 = mod(addr1_v2, x_grid_dim);
                end
                if(addr2_v2 < 0)
                    addr2_v2 = mod(addr2_v2, x_grid_dim);
                end
                accel_idx_1_v2 = mod(addr1_v2, 8);
                accel_idx_2_v2 = mod(addr2_v2, 8);
                if(col-3 < 0)
                    col_v2 = mod(col-3, y_grid_dim);
                end
                accel_idx_x_v2 = mod(col_v2, 8);
                x_tile_idx_v2 = fix((col_v2) / 8);
                y_tile_idx1_v2 = fix(addr1_v2/8);
                y_tile_idx2_v2 = fix(addr2_v2/8);
                tile_idx_1_v2 = y_tile_idx1_v2 * y_mem_tile_dim + x_tile_idx_v2;
                tile_idx_2_v2 = y_tile_idx2_v2 * y_mem_tile_dim + x_tile_idx_v2;
                val1_v2 = swap_complex_components(data_matrix2(accel_idx_x_v2+1, accel_idx_1_v2+1, tile_idx_1_v2+1));
                val2_v2 = swap_complex_components(data_matrix2(accel_idx_x_v2+1, accel_idx_2_v2+1, tile_idx_2_v2+1));
                a1_v2=val1_v2+val2_v2.*w;   % 1-st part of the "butterfly" creating operation
                b1_v2=val1_v2-val2_v2.*w;   % 2-nd part of the "butterfly" creating operation
                if(val1 ~= val1_v2)
                    mismatch = 1;
                end
                if(val2 ~= val2_v2)
                    mismatch = 1;
                end

                x_vec_v2_pos = x_vec_v2(pos);
                x_vec_v2_posHalf = x_vec_v2(pos+Half);
                a2_v2=x_vec_v2_pos+x_vec_v2_posHalf.*w;   % 1-st part of the "butterfly" creating operation
                b2_v2=x_vec_v2_pos-x_vec_v2_posHalf.*w;   % 2-nd part of the "butterfly" creating operation
                x_vec_v2(pos)=a2_v2;                  % saving computation of the 1-st part
                x_vec_v2(pos+Half)=b2_v2;             % saving computation of the 2-nd part

                data_matrix2(accel_idx_x_v2+1, accel_idx_1_v2+1, tile_idx_1_v2+1) = swap_complex_components(a1_v2);             % saving computation of the 1-st part
                data_matrix2(accel_idx_x_v2+1, accel_idx_2_v2+1, tile_idx_2_v2+1) = swap_complex_components(b1_v2);             % saving computation of the 2-nd part

                data_matrix1(accel_idx_x+1, accel_idx_1+1, tile_idx_1+1) = swap_complex_components(a1);             % saving computation of the 1-st part
                data_matrix1(accel_idx_x+1, accel_idx_2+1, tile_idx_2+1) = swap_complex_components(b1);             % saving computation of the 2-nd part
            end
        end
        Half=2*Half;                    % computing the next "Half" value
    end
    x_vec=swap_complex_components(x_vec)./1024; % can also just take the conjugate; x=conj(x)
    data_matrix3(col+1, :) = x_vec;               % returning the result from function

    x_vec_v2=swap_complex_components(x_vec_v2)./1024; % can also just take the conjugate; x=conj(x)
    data_matrix4(col+1, :) = x_vec_v2;               % returning the result from function
end

data_matrix1 = data_matrix1 ./ 1024; % TODO: remove this and don't swap components in next loop?

data_matrix2 = data_matrix2 ./ 1024; % TODO: remove this and don't swap components in next loop?

end
