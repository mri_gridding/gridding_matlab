% 2D RADIX 2 Decimation in Time; for any length of N*M (RADIX 2 DIT)
% Input         : Normal Order
% Output        : Bit-reversed
% Unpack/resort : unpack(data_matrix) -> bitrevorder(bitrevorder(data_matrix.').')
% Author        : NinjaNife

function [ data_matrix ] = hardware_FFT2_IFFT2_DIT_R2_TILED_fi_parfor( data_matrix, nufft_type, fi_cfg, config )

%%%%%%%%%% Assign Inputs %%%%%%%%%%
x_accel_dim    = config.hardware.x_accel_dim;
y_accel_dim    = config.hardware.y_accel_dim;
x_mem_dim      = config.hardware.x_mem_dim;
y_mem_dim      = config.hardware.y_mem_dim;
x_mem_tile_dim = config.hardware.x_mem_tile_dim;
y_mem_tile_dim = config.hardware.y_mem_tile_dim;

x_grid_dim     = config.image.x_grid_dim;
y_grid_dim     = config.image.y_grid_dim;
x_tile_dim     = config.image.x_tile_dim;
y_tile_dim     = config.image.y_tile_dim;
x_img_dim      = config.image.x_img_dim;
y_img_dim      = config.image.y_img_dim;
x_img_tile_dim = config.image.x_img_tile_dim;
y_img_tile_dim = config.image.y_img_tile_dim;
x_fft_len      = config.image.x_fft_len;
y_fft_len      = config.image.y_fft_len;

table_bit_width = 16;
table_frac_length = 14;
sample_bit_width = 32;
if(strcmp(nufft_type, 'forw'))
    sample_frac_length = 24; % fraction length > word length means leading 0's
else
    sample_frac_length = 36; % fraction length > word length means leading 0's
end

% Load the bit-reversed array of twiddle values
load(sprintf('w_arr_descending_bitrevorder_N=%d.mat', y_fft_len))
w_arr = fi(w_arr,true,table_bit_width,table_frac_length,'ProductMode','SpecifyPrecision','ProductWordLength',table_frac_length,'ProductFractionLength',table_frac_length,'SumMode','SpecifyPrecision','SumWordLength',table_frac_length,'SumFractionLength',table_frac_length);


tic

% Do an FFT/IFFT of each "row" (rows are stacked)
start_val = 0;
step_val = 128;
end_val = x_fft_len*2*y_accel_dim;
offset = x_fft_len*y_accel_dim;
comb_pipe_offset = x_fft_len/2;
w_mask = int32(0);
bit_shift_offset = log2(1024)-log2(x_fft_len);
num_stages = log2(x_fft_len);

if(strcmp(fi_cfg.nufft_type, 'forw'))
    frac_len_init = 18; % fraction length > word length means leading 0's
                      % 0  1  2  3  4  5  6  7  8
    frac_len_offsets = [0, 1, 0, 1, 0, 1, 1, 0, 1];
else
    frac_len_init = 31; % fraction length > word length means leading 0's
                      % 0  1  2  3  4  5  6  7  8  9
    frac_len_offsets = [0, 1, 0, 1, 0, 1, 0, 0, 1, 0];
end

sample_frac_length = frac_len_init;

for stage=0:num_stages-1 % stages of transformation

    % if(stage < num_stages-3)
    %     if(mod(stage, 2) == 1)
    %         fi_cfg = fixed_point_cfg(fi_cfg);
    %     end
    % else
    %     if(mod(stage, 2) == 0)
    %         fi_cfg = fixed_point_cfg(fi_cfg);
    %     end
    % end
%     sample_frac_length = sample_frac_length - 1;
    sample_frac_length = sample_frac_length - frac_len_offsets(stage+1);
    data_matrix = fi(data_matrix,true,sample_bit_width,sample_frac_length,'RoundMode',fi_cfg.roundmode,'ProductMode','SpecifyPrecision','ProductWordLength',sample_bit_width,'ProductFractionLength',sample_frac_length,'SumMode','SpecifyPrecision','SumWordLength',sample_bit_width,'SumFractionLength',sample_frac_length);

    % Loop over accelerator pipelines to process input point
    parfor x_accel_idx = 0:x_accel_dim-1
        %%%%%%%%%% Parfor-Specific Setup %%%%%%%%%%
        grid_data_slice = data_matrix(x_accel_idx+1, :, :);

        for y_accel_idx = 0:y_accel_dim-1
            if(stage < num_stages-3)
                % Loop over all columns before going to next stage
                for set_idx = 0:y_fft_len/y_accel_dim-1

                    iter1 = start_val+offset;
                    iter2 = start_val;
                    saved_val = iter1;

                    while(iter1 < end_val)
                        tile_idx_1 = int32(set_idx+iter1);
                        tile_idx_2 = int32(set_idx+iter2);

                        if(iter2+step_val == saved_val)
                            iter1 = iter1+step_val+offset;
                            iter2 = iter2+step_val+offset;
                            saved_val = iter1;
                        else
                            iter1 = iter1+step_val;
                            iter2 = iter2+step_val;
                        end

                        % tile_idx_2[13:(14-stage)] for stage > 0; else 0 (13:14 is zero bits)
                        w_addr = bitshift(bitand(tile_idx_2, w_mask), -(14-(stage+bit_shift_offset)));

                        % Get the twiddle factor
                        w = w_arr(w_addr+1);
                        w_real = real(w);
                        w_imag = imag(w);

                        w_real = fi(w_real,true,sample_bit_width,sample_frac_length,'RoundMode',fi_cfg.roundmode,'ProductMode','SpecifyPrecision','ProductWordLength',sample_bit_width,'ProductFractionLength',sample_frac_length,'SumMode','SpecifyPrecision','SumWordLength',sample_bit_width,'SumFractionLength',sample_frac_length);
                        w_imag = fi(w_imag,true,sample_bit_width,sample_frac_length,'RoundMode',fi_cfg.roundmode,'ProductMode','SpecifyPrecision','ProductWordLength',sample_bit_width,'ProductFractionLength',sample_frac_length,'SumMode','SpecifyPrecision','SumWordLength',sample_bit_width,'SumFractionLength',sample_frac_length);

                        % Get the first value, which will be multiplied by the twiddle factor
                        val1 = grid_data_slice(1, y_accel_idx+1, tile_idx_1+1); % value at first address
                        if(~strcmp(nufft_type, 'forw'))
                            val1 = swap_complex_components(val1);
                        end
                        val1_real = real(val1);
                        val1_imag = imag(val1);

                        % Add/Subtract the result from the second value
                        k13 = w_real + w_imag;
                        k14 = val1_imag - val1_real;
                        k15 = val1_real + val1_imag;
                        k16 = val1_real * k13;
                        k17 = w_real * k14;
                        k18 = w_imag * k15;
                        R3  = k16 - k18;
                        I3  = k16 + k17;

                        % Add/Subtract the result from the second value
                        val2 = grid_data_slice(1, y_accel_idx+1, tile_idx_2+1); % value at second address
                        if(~strcmp(nufft_type, 'forw'))
                            val2 = swap_complex_components(val2);
                        end
                        val2_real = real(val2);
                        val2_imag = imag(val2);
                        Ra = val2_real - R3;
                        Ia = val2_imag - I3;
                        Rb = val2_real + R3;
                        Ib = val2_imag + I3;

                        % Form a complex value for storage
                        a = complex(Ra, Ia); % 1st part of the "butterfly" operation
                        b = complex(Rb, Ib); % 2nd part of the "butterfly" operation
                        if(~strcmp(nufft_type, 'forw'))
                            a = swap_complex_components(a);
                            b = swap_complex_components(b);
                        end
                        grid_data_slice(1, y_accel_idx+1, tile_idx_1+1) = a;
                        grid_data_slice(1, y_accel_idx+1, tile_idx_2+1) = b;
                    end
                end
            elseif(bitget(y_accel_idx, ((num_stages-1)-stage)+1) == 0) % +1 because the first index is 1 in Matlab, not 0
                % Loop over all columns before going to next stage
                for set_idx = 0:y_fft_len/y_accel_dim-1

                    iter1 = start_val;
                    iter2 = start_val;
                    saved_val = iter1;

                    while(iter1 < end_val)
                        tile_idx_1 = int32(set_idx+iter1);
                        tile_idx_2 = int32(set_idx+iter2);

                        if(iter2+step_val == saved_val)
                            iter1 = iter1+step_val;
                            iter2 = iter2+step_val;
                            saved_val = iter1;
                        else
                            iter1 = iter1+step_val;
                            iter2 = iter2+step_val;
                        end

                        % Stage 7: tile_idx_2[13:(14-stage)]. Stage 8: Concatenate tile_idx_2[13:7] and y_accel_idx[2]. Stage 9: Concatenate tile_idx_2[13:7] and y_accel_idx[2:1]
                        w_addr = bitshift(bitshift(bitand(tile_idx_2, w_mask), -max(14-(stage+bit_shift_offset), 7)), mod(-(14-(stage+bit_shift_offset)),7)); % tile_idx_2[13:7] shifted to the left (for stage > 7)
                        append_bits = bitshift(bitand(int32(y_accel_idx), 6), -comb_pipe_offset); % y_accel_idx[2:1] shifted to the right
                        w_addr = bitor(w_addr, append_bits); % [tile_idx_2[13:7] y_accel_idx[2:1]]

                        % Get the twiddle factor
                        w = w_arr(w_addr+1);
                        w_real = real(w);
                        w_imag = imag(w);

                        w_real = fi(w_real,true,sample_bit_width,sample_frac_length,'RoundMode',fi_cfg.roundmode,'ProductMode','SpecifyPrecision','ProductWordLength',sample_bit_width,'ProductFractionLength',sample_frac_length,'SumMode','SpecifyPrecision','SumWordLength',sample_bit_width,'SumFractionLength',sample_frac_length);
                        w_imag = fi(w_imag,true,sample_bit_width,sample_frac_length,'RoundMode',fi_cfg.roundmode,'ProductMode','SpecifyPrecision','ProductWordLength',sample_bit_width,'ProductFractionLength',sample_frac_length,'SumMode','SpecifyPrecision','SumWordLength',sample_bit_width,'SumFractionLength',sample_frac_length);

                        % Get the first value, which will be multiplied by the twiddle factor
                        val1 = grid_data_slice(1, y_accel_idx+comb_pipe_offset+1, tile_idx_1+1); % value at first address
                        if(~strcmp(nufft_type, 'forw'))
                            val1 = swap_complex_components(val1);
                        end
                        val1_real = real(val1);
                        val1_imag = imag(val1);

                        % Multiply the first value by the twiddle factor
                        k13 = w_real + w_imag;
                        k14 = val1_imag - val1_real;
                        k15 = val1_real + val1_imag;
                        k16 = val1_real * k13;
                        k17 = w_real * k14;
                        k18 = w_imag * k15;
                        R3  = k16 - k18;
                        I3  = k16 + k17;

                        % Add/Subtract the result from the second value
                        val2 = grid_data_slice(1, y_accel_idx+1, tile_idx_2+1); % value at second address
                        if(~strcmp(nufft_type, 'forw'))
                            val2 = swap_complex_components(val2);
                        end
                        val2_real = real(val2);
                        val2_imag = imag(val2);
                        Ra = val2_real - R3;
                        Ia = val2_imag - I3;
                        Rb = val2_real + R3;
                        Ib = val2_imag + I3;

                        % Form a complex value for storage
                        a = complex(Ra, Ia); % 1st part of the "butterfly" operation
                        b = complex(Rb, Ib); % 2nd part of the "butterfly" operation
                        if(~strcmp(nufft_type, 'forw'))
                            a = swap_complex_components(a);
                            b = swap_complex_components(b);
                        end
                        grid_data_slice(1, y_accel_idx+comb_pipe_offset+1, tile_idx_1+1) = a;
                        grid_data_slice(1, y_accel_idx+1, tile_idx_2+1) = b;
                    end
                end
            end
        end
        data_matrix(x_accel_idx+1, :, :) = grid_data_slice;
    end
    offset = fix(offset / 2);
    comb_pipe_offset = fix(comb_pipe_offset / 2);
    w_mask = bitset(w_mask, max(14-(stage+bit_shift_offset), 1));
end

fprintf('First dimension completed; starting second dimension.\n')

% Do an FFT/IFFT of each "col" (cols are stacked)
start_val = 0;
step_val = 1;
end_val = y_fft_len/x_accel_dim;
offset = y_fft_len/2/x_accel_dim;
comb_pipe_offset = y_fft_len/2;
w_mask = int32(0);
bit_shift_offset = log2(1024)-log2(y_fft_len);
num_stages = log2(y_fft_len);

for stage=0:num_stages-1 % stages of transformation

    % if(stage < num_stages-3)
    %     if(mod(stage, 2) == 1)
    %         fi_cfg = fixed_point_cfg(fi_cfg);
    %     end
    % else
    %     if(mod(stage, 2) == 0)
    %         fi_cfg = fixed_point_cfg(fi_cfg);
    %     end
    % end
%     sample_frac_length = sample_frac_length - 1;
    sample_frac_length = sample_frac_length - frac_len_offsets(stage+1);
    data_matrix = fi(data_matrix,true,sample_bit_width,sample_frac_length,'RoundMode',fi_cfg.roundmode,'ProductMode','SpecifyPrecision','ProductWordLength',sample_bit_width,'ProductFractionLength',sample_frac_length,'SumMode','SpecifyPrecision','SumWordLength',sample_bit_width,'SumFractionLength',sample_frac_length);

    % Loop over accelerator pipelines to process input point
    parfor y_accel_idx = 0:y_accel_dim-1
        %%%%%%%%%% Parfor-Specific Setup %%%%%%%%%%
        grid_data_slice = data_matrix(:, y_accel_idx+1, :);

        for x_accel_idx = 0:x_accel_dim-1
            if(stage < num_stages-3)
                % Loop over all columns before going to next stage
                for set_idx = 0:x_fft_len/x_accel_dim-1
                    set_offset = set_idx * x_mem_tile_dim;

                    iter1 = start_val+offset;
                    iter2 = start_val;
                    saved_val = iter1;

                    while(iter1 < end_val)
                        tile_idx_1 = int32(iter1+set_offset);
                        tile_idx_2 = int32(iter2+set_offset);

                        if(iter2+step_val == saved_val)
                            iter1 = iter1+step_val+offset;
                            iter2 = iter2+step_val+offset;
                            saved_val = iter1;
                        else
                            iter1 = iter1+step_val;
                            iter2 = iter2+step_val;
                        end

                        % tile_idx_2[6:(7-stage)] for stage > 0; else 0 (6:7 is zero bits)
                        w_addr = bitshift(bitand(tile_idx_2, w_mask), -(7-(stage+bit_shift_offset)));

                        % Get the twiddle factor
                        w = w_arr(w_addr+1);
                        w_real = real(w);
                        w_imag = imag(w);

                        w_real = fi(w_real,true,sample_bit_width,sample_frac_length,'RoundMode',fi_cfg.roundmode,'ProductMode','SpecifyPrecision','ProductWordLength',sample_bit_width,'ProductFractionLength',sample_frac_length,'SumMode','SpecifyPrecision','SumWordLength',sample_bit_width,'SumFractionLength',sample_frac_length);
                        w_imag = fi(w_imag,true,sample_bit_width,sample_frac_length,'RoundMode',fi_cfg.roundmode,'ProductMode','SpecifyPrecision','ProductWordLength',sample_bit_width,'ProductFractionLength',sample_frac_length,'SumMode','SpecifyPrecision','SumWordLength',sample_bit_width,'SumFractionLength',sample_frac_length);

                        % Get the first value, which will be multiplied by the twiddle factor
                        val1 = grid_data_slice(x_accel_idx+1, 1, tile_idx_1+1); % value at first address
                        if(~strcmp(nufft_type, 'forw'))
                            val1 = swap_complex_components(val1);
                        end
                        val1_real = real(val1);
                        val1_imag = imag(val1);

                        % Multiply the first value by the twiddle factor
                        k13 = w_real + w_imag;
                        k14 = val1_imag - val1_real;
                        k15 = val1_real + val1_imag;
                        k16 = val1_real * k13;
                        k17 = w_real * k14;
                        k18 = w_imag * k15;
                        R3  = k16 - k18;
                        I3  = k16 + k17;

                        % Add/Subtract the result from the second value
                        val2 = grid_data_slice(x_accel_idx+1, 1, tile_idx_2+1); % value at second address
                        if(~strcmp(nufft_type, 'forw'))
                            val2 = swap_complex_components(val2);
                        end
                        val2_real = real(val2);
                        val2_imag = imag(val2);
                        Ra = val2_real - R3;
                        Ia = val2_imag - I3;
                        Rb = val2_real + R3;
                        Ib = val2_imag + I3;

                        % Form a complex value for storage
                        a = complex(Ra, Ia); % 1st part of the "butterfly" operation
                        b = complex(Rb, Ib); % 2nd part of the "butterfly" operation
                        if(~strcmp(nufft_type, 'forw'))
                            a = swap_complex_components(a);
                            b = swap_complex_components(b);
                        end
                        grid_data_slice(x_accel_idx+1, 1, tile_idx_1+1) = a;
                        grid_data_slice(x_accel_idx+1, 1, tile_idx_2+1) = b;
                    end
                end
            elseif(bitget(x_accel_idx, ((num_stages-1)-stage)+1) == 0) % +1 because the first index is 1 in Matlab, not 0
                % Loop over all columns before going to next stage
                for set_idx = 0:x_fft_len/x_accel_dim-1
                    set_offset = set_idx * x_mem_tile_dim;

                    iter1 = start_val+offset;
                    iter2 = start_val;
                    saved_val = iter1;

                    while(iter1 < end_val)
                        tile_idx_1 = int32(iter1+set_offset);
                        tile_idx_2 = int32(iter2+set_offset);

                        if(iter2+step_val == saved_val)
                            iter1 = iter1+step_val+offset;
                            iter2 = iter2+step_val+offset;
                            saved_val = iter1;
                        else
                            iter1 = iter1+step_val;
                            iter2 = iter2+step_val;
                        end

                        % Stage 7: tile_idx_2[6:(7-stage)]. Stage 8: Concatenate tile_idx_2[6:0] and x_accel_idx[2]. Stage 9: Concatenate tile_idx_2[6:0] and x_accel_idx[2:1]
                        w_addr = bitshift(bitand(tile_idx_2, w_mask), -(7-(stage+bit_shift_offset))); % tile_idx_2[6:0] shifted to the left (for stage > 7)
                        append_bits = bitshift(bitand(int32(x_accel_idx), 6), -comb_pipe_offset); % x_accel_idx[2:1] shifted to the right
                        w_addr = bitor(w_addr, append_bits); % [tile_idx_2[6:0] x_accel_idx[2:1]]

                        % Get the twiddle factor
                        w = w_arr(w_addr+1);
                        w_real = real(w);
                        w_imag = imag(w);

                        w_real = fi(w_real,true,sample_bit_width,sample_frac_length,'RoundMode',fi_cfg.roundmode,'ProductMode','SpecifyPrecision','ProductWordLength',sample_bit_width,'ProductFractionLength',sample_frac_length,'SumMode','SpecifyPrecision','SumWordLength',sample_bit_width,'SumFractionLength',sample_frac_length);
                        w_imag = fi(w_imag,true,sample_bit_width,sample_frac_length,'RoundMode',fi_cfg.roundmode,'ProductMode','SpecifyPrecision','ProductWordLength',sample_bit_width,'ProductFractionLength',sample_frac_length,'SumMode','SpecifyPrecision','SumWordLength',sample_bit_width,'SumFractionLength',sample_frac_length);

                        % Get the first value, which will be multiplied by the twiddle factor
                        val1 = grid_data_slice(x_accel_idx+comb_pipe_offset+1, 1, tile_idx_1+1); % value at first address
                        if(~strcmp(nufft_type, 'forw'))
                            val1 = swap_complex_components(val1);
                        end
                        val1_real = real(val1);
                        val1_imag = imag(val1);

                        % Multiply the first value by the twiddle factor
                        k13 = w_real + w_imag;
                        k14 = val1_imag - val1_real;
                        k15 = val1_real + val1_imag;
                        k16 = val1_real * k13;
                        k17 = w_real * k14;
                        k18 = w_imag * k15;
                        R3  = k16 - k18;
                        I3  = k16 + k17;

                        % Add/Subtract the result from the second value
                        val2 = grid_data_slice(x_accel_idx+1, 1, tile_idx_2+1); % value at second address
                        if(~strcmp(nufft_type, 'forw'))
                            val2 = swap_complex_components(val2);
                        end
                        val2_real = real(val2);
                        val2_imag = imag(val2);
                        Ra = val2_real - R3;
                        Ia = val2_imag - I3;
                        Rb = val2_real + R3;
                        Ib = val2_imag + I3;

                        % Form a complex value for storage
                        a = complex(Ra, Ia); % 1st part of the "butterfly" operation
                        b = complex(Rb, Ib); % 2nd part of the "butterfly" operation
                        if(~strcmp(nufft_type, 'forw'))
                            a = swap_complex_components(a);
                            b = swap_complex_components(b);
                        end
                        grid_data_slice(x_accel_idx+comb_pipe_offset+1, 1, tile_idx_1+1) = a;
                        grid_data_slice(x_accel_idx+1, 1, tile_idx_2+1) = b;
                    end
                end
            end
        end
        data_matrix(:, y_accel_idx+1, :) = grid_data_slice;
    end
    offset = fix(offset / 2);
    comb_pipe_offset = fix(comb_pipe_offset / 2);
    w_mask = bitset(w_mask, max(7-(stage+bit_shift_offset), 1));
end
if(strcmp(nufft_type, 'forw'))
    fprintf('2D FFT Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
else
    fprintf('2D IFFT Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
end

end
