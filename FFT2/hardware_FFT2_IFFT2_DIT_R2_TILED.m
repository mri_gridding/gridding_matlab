% 2D Radix 2 Decimation in Time; for any length of N*M (RADIX 2 DIT)
% Input         : Normal Order
% Output        : Bit-reversed
% Unpack/resort : unpack(data_matrix) -> bitrevorder(bitrevorder(data_matrix.').')
% Author        : NinjaNife

function [ data_matrix ] = hardware_FFT2_IFFT2_DIT_R2_TILED( data_matrix, nufft_type, config )

%%%%%%%%%% Assign Inputs %%%%%%%%%%
x_accel_dim    = config.hardware.x_accel_dim;
y_accel_dim    = config.hardware.y_accel_dim;
x_mem_dim      = config.hardware.x_mem_dim;
y_mem_dim      = config.hardware.y_mem_dim;
x_mem_tile_dim = config.hardware.x_mem_tile_dim;
y_mem_tile_dim = config.hardware.y_mem_tile_dim;

x_grid_dim     = config.image.x_grid_dim;
y_grid_dim     = config.image.y_grid_dim;
x_tile_dim     = config.image.x_tile_dim;
y_tile_dim     = config.image.y_tile_dim;
x_img_dim      = config.image.x_img_dim;
y_img_dim      = config.image.y_img_dim;
x_img_tile_dim = config.image.x_img_tile_dim;
y_img_tile_dim = config.image.y_img_tile_dim;
x_fft_len      = config.image.x_fft_len;
y_fft_len      = config.image.y_fft_len;

% Load the bit-reversed array of twiddle values
load(sprintf('w_arr_descending_bitrevorder_N=%d.mat', y_fft_len))
% load('w_arr_test.mat')

% Do an FFT/IFFT of each "col" (cols are stacked)
tic
offset = y_fft_len/2/x_accel_dim;
comb_pipe_offset = y_fft_len/2;
w_mask = int32(0);
bit_shift_offset = log2(1024)-log2(y_fft_len);
end_val = y_fft_len/x_accel_dim;
num_stages = log2(y_fft_len);
start_val = 0;
step_val = 1;

output_file = fopen(sprintf('random.out'), 'w');

for stage=0:num_stages-1 % stages of transformation
    % Loop over accelerator pipelines to process input point
    for x_accel_idx = 0:x_accel_dim-1
        for y_accel_idx = 0:y_accel_dim-1
            if(stage < num_stages-3)
                % Loop over all columns before going to next stage
                for set_idx = 0:x_fft_len/x_accel_dim-1
                    set_offset = set_idx * x_mem_tile_dim;

                    iter1 = start_val+offset;
                    iter2 = start_val;
                    saved_val = iter1;

                    while(iter1 < end_val)
                        tile_idx_1 = int32(iter1+set_offset);
                        tile_idx_2 = int32(iter2+set_offset);

                        if(iter2+step_val == saved_val)
                            iter1 = iter1+step_val+offset;
                            iter2 = iter2+step_val+offset;
                            saved_val = iter1;
                        else
                            iter1 = iter1+step_val;
                            iter2 = iter2+step_val;
                        end

                        % tile_idx_2[6:(7-stage)] for stage > 0; else 0 (6:7 is zero bits)
                        w_addr = bitshift(bitand(tile_idx_2, w_mask), -(7-(stage+bit_shift_offset)));

                        % Get the twiddle factor
                        w = w_arr(w_addr+1);

                        % Get the first value, which will be multiplied by the twiddle factor
                        val1 = data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1); % value at first address
                        if(~strcmp(nufft_type, 'forw'))
                            val1 = swap_complex_components(val1);
                        end

                        % Add/Subtract the result from the second value
                        val2 = data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_2+1); % value at second address
                        if(~strcmp(nufft_type, 'forw'))
                            val2 = swap_complex_components(val2);
                        end

                        a = val2-val1.*w; % 1st part of the "butterfly" creating operation
                        b = val2+val1.*w; % 2nd part of the "butterfly" creating operation
                        
%                         %fprintf(output_file,"a_real=%f\ta_imag=%f\tb_real=%f\tb_imag=%f\n", real(a), imag(a), real(b), imag(b));
                        % fprintf(output_file,"start_val=%d\tstep_val=%d\tend_val=%d\toffset=%d\tcomb_pipe_offset=%d\tw_mask=%d\tbit_shift_offset=%d\tnum_stages=%d\tstage=%d\tx_accel_idx=%d\ty_accel_idx=%d\tset_idx=%d\tset_offset=%d\titer1=%d\titer2=%d\tsaved_val=%d\ttile_idx_1=%d\ttile_idx_2=%d\tw_addr=%d\n", start_val, step_val, end_val, offset, comb_pipe_offset, w_mask, bit_shift_offset, num_stages, stage, x_accel_idx, y_accel_idx, set_idx, set_offset, iter1, iter2, saved_val, tile_idx_1, tile_idx_2, w_addr);
                        % fprintf(output_file,"w_real=%.5f\tw_imag=%.5f\n", real(w), imag(w));
                        % fprintf(output_file,"stage=%d\tval1_real=%.10f\tval1_imag=%.10f\n", stage, real(val1), imag(val1));
                        % if(stage == 0)
                        %     fprintf(output_file,"stage=%d\tval1_real=%.10f\tval1_imag=%.10f\tval2_real=%.10f\tval2_imag=%.10f\ta_real=%.10f\ta_imag=%.10f\tb_real=%.10f\tb_imag=%.10f\n", stage, real(val1), imag(val1), real(val2), imag(val2), real(a), imag(a), real(b), imag(b));
                        % end

                        if(~strcmp(nufft_type, 'forw'))
                            a = swap_complex_components(a);
                            b = swap_complex_components(b);
                        end
                        data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1) = a;
                        data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_2+1) = b;
                    end
                end
            elseif(bitget(x_accel_idx, ((num_stages-1)-stage)+1) == 1) % num_stages instead of num_stages-1 because the first index is 1 in Matlab, not 0
                % Loop over all columns before going to next stage
                for set_idx = 0:x_fft_len/x_accel_dim-1
                    set_offset = set_idx * x_mem_tile_dim;

                    iter1 = start_val+offset;
                    iter2 = start_val;
                    saved_val = iter1;

                    while(iter1 < end_val)
                        tile_idx_1 = int32(iter1+set_offset);
                        tile_idx_2 = int32(iter2+set_offset);

                        if(iter2+step_val == saved_val)
                            iter1 = iter1+step_val+offset;
                            iter2 = iter2+step_val+offset;
                            saved_val = iter1;
                        else
                            iter1 = iter1+step_val;
                            iter2 = iter2+step_val;
                        end

                        % Stage 7: tile_idx_2[6:(7-stage)]. Stage 8: Concatenate tile_idx_2[6:0] and x_accel_idx[2]. Stage 9: Concatenate tile_idx_2[6:0] and x_accel_idx[2:1]
                        w_addr = bitshift(bitand(tile_idx_2, w_mask), -(7-(stage+bit_shift_offset))); % tile_idx_2[6:0] shifted to the left (for stage > 7)
                        append_bits = bitshift(bitand(int32(x_accel_idx), 6), -comb_pipe_offset); % x_accel_idx[2:1] shifted to the right
                        w_addr = bitor(w_addr, append_bits); % [tile_idx_2[6:0] x_accel_idx[2:1]]

                        % Get the twiddle factor
                        w = w_arr(w_addr+1);

                        % Get the first value, which will be multiplied by the twiddle factor
                        val1 = data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1); % value at first address
                        if(~strcmp(nufft_type, 'forw'))
                            val1 = swap_complex_components(val1);
                        end

                        % Add/Subtract the result from the second value
                        val2 = data_matrix(x_accel_idx-comb_pipe_offset+1, y_accel_idx+1, tile_idx_2+1); % value at second address
                        if(~strcmp(nufft_type, 'forw'))
                            val2 = swap_complex_components(val2);
                        end

                        a = val2-val1.*w; % 1st part of the "butterfly" creating operation
                        b = val2+val1.*w; % 2nd part of the "butterfly" creating operation
                        
%                             %fprintf(output_file,"a_real=%f\ta_imag=%f\tb_real=%f\tb_imag=%f\n", real(a), imag(a), real(b), imag(b));
                        % fprintf(output_file,"start_val=%d\tstep_val=%d\tend_val=%d\toffset=%d\tcomb_pipe_offset=%d\tw_mask=%d\tbit_shift_offset=%d\tnum_stages=%d\tstage=%d\tx_accel_idx=%d\ty_accel_idx=%d\tset_idx=%d\tset_offset=%d\titer1=%d\titer2=%d\tsaved_val=%d\ttile_idx_1=%d\ttile_idx_2=%d\tw_addr=%d\n", start_val, step_val, end_val, offset, comb_pipe_offset, w_mask, bit_shift_offset, num_stages, stage, x_accel_idx, y_accel_idx, set_idx, set_offset, iter1, iter2, saved_val, tile_idx_1, tile_idx_2, w_addr);
                        % fprintf(output_file,"w_real=%.5f\tw_imag=%.5f\n", real(w), imag(w));
                        % fprintf(output_file,"stage=%d\tval1_real=%.10f\tval1_imag=%.10f\n", stage, real(val1), imag(val1));
                        
                        if(~strcmp(nufft_type, 'forw'))
                            a = swap_complex_components(a);
                            b = swap_complex_components(b);
                        end
                        data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1) = a;
                        data_matrix(x_accel_idx-comb_pipe_offset+1, y_accel_idx+1, tile_idx_2+1) = b;
                    end
                end
            end
        end
    end
    offset = fix(offset / 2);
    comb_pipe_offset = fix(comb_pipe_offset / 2);
    w_mask = bitset(w_mask, max(7-(stage+bit_shift_offset), 1));
end


% Do an FFT/IFFT of each "row" (rows are stacked)
offset = x_fft_len*y_accel_dim;
comb_pipe_offset = x_fft_len/2;
w_mask = int32(0);
bit_shift_offset = log2(1024)-log2(x_fft_len);
end_val = x_fft_len*2*y_accel_dim;
num_stages = log2(x_fft_len);
start_val = 0;
step_val = 128;

for stage=0:num_stages-1 % stages of transformation
    % Loop over accelerator pipelines to process input point
    for x_accel_idx = 0:x_accel_dim-1
        for y_accel_idx = 0:y_accel_dim-1
            if(stage < num_stages-3)
                % Loop over all columns before going to next stage
                for set_idx = 0:y_fft_len/y_accel_dim-1
                    iter1 = start_val+offset;
                    iter2 = start_val;
                    saved_val = iter1;

                    while(iter1 < end_val)
                        tile_idx_1 = int32(iter1+set_idx);
                        tile_idx_2 = int32(iter2+set_idx);

                        if(iter2+step_val == saved_val)
                            iter1 = iter1+step_val+offset;
                            iter2 = iter2+step_val+offset;
                            saved_val = iter1;
                        else
                            iter1 = iter1+step_val;
                            iter2 = iter2+step_val;
                        end

                        % tile_idx_2[13:(14-stage)] for stage > 0; else 0 (13:14 is zero bits)
                        w_addr = bitshift(bitand(tile_idx_2, w_mask), -(14-(stage+bit_shift_offset)));

                        % Get the twiddle factor
                        w = w_arr(w_addr+1);

                        % Get the first value, which will be multiplied by the twiddle factor
                        val1 = data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1); % value at first address
                        if(~strcmp(nufft_type, 'forw'))
                            val1 = swap_complex_components(val1);
                        end

                        % Add/Subtract the result from the second value
                        val2 = data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_2+1); % value at second address
                        if(~strcmp(nufft_type, 'forw'))
                            val2 = swap_complex_components(val2);
                        end

                        a = val2-val1.*w; % 1st part of the "butterfly" creating operation
                        b = val2+val1.*w; % 2nd part of the "butterfly" creating operation
                        
%                         %fprintf(output_file,"a_real=%f\ta_imag=%f\tb_real=%f\tb_imag=%f\n", real(a), imag(a), real(b), imag(b));
                        % fprintf(output_file,"start_val=%d\tstep_val=%d\tend_val=%d\toffset=%d\tcomb_pipe_offset=%d\tw_mask=%d\tbit_shift_offset=%d\tnum_stages=%d\tstage=%d\tx_accel_idx=%d\ty_accel_idx=%d\tset_idx=%d\titer1=%d\titer2=%d\tsaved_val=%d\ttile_idx_1=%d\ttile_idx_2=%d\tw_addr=%d\n", start_val, step_val, end_val, offset, comb_pipe_offset, w_mask, bit_shift_offset, num_stages, stage, x_accel_idx, y_accel_idx, set_idx, iter1, iter2, saved_val, tile_idx_1, tile_idx_2, w_addr);
                        % fprintf(output_file,"w_real=%.5f\tw_imag=%.5f\n", real(w), imag(w));
                        % fprintf(output_file,"stage=%d\tval1_real=%.10f\tval1_imag=%.10f\n", stage, real(val1), imag(val1));
                        
                        if(~strcmp(nufft_type, 'forw'))
                            a = swap_complex_components(a);
                            b = swap_complex_components(b);
                        end
                        data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1) = a;
                        data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_2+1) = b;
                    end
                end
            elseif(bitget(y_accel_idx, ((num_stages-1)-stage)+1) == 1) % num_stages instead of num_stages-1 because the first index is 1 in Matlab, not 0
                % Loop over all columns before going to next stage
                for set_idx = 0:y_fft_len/y_accel_dim-1
                    iter1 = start_val;
                    iter2 = start_val;
                    saved_val = iter1;

                    while(iter1 < end_val)
                        tile_idx_1 = int32(iter1+set_idx);
                        tile_idx_2 = int32(iter2+set_idx);

                        if(iter2+step_val == saved_val)
                            iter1 = iter1+step_val;
                            iter2 = iter2+step_val;
                            saved_val = iter1;
                        else
                            iter1 = iter1+step_val;
                            iter2 = iter2+step_val;
                        end

                        % Stage 7: tile_idx_2[13:(14-stage)]. Stage 8: Concatenate tile_idx_2[13:7] and y_accel_idx[2]. Stage 9: Concatenate tile_idx_2[13:7] and y_accel_idx[2:1]
                        w_addr = bitshift(bitshift(bitand(tile_idx_2, w_mask), -max(14-(stage+bit_shift_offset), 7)), mod(-(14-(stage+bit_shift_offset)),7)); % tile_idx_2[13:7] shifted to the left (for stage > 7)
                        append_bits = bitshift(bitand(int32(y_accel_idx), 6), -comb_pipe_offset); % y_accel_idx[2:1] shifted to the right
                        w_addr = bitor(w_addr, append_bits); % [tile_idx_2[13:7] y_accel_idx[2:1]]

                        % Get the twiddle factor
                        w = w_arr(w_addr+1);

                        % Get the first value, which will be multiplied by the twiddle factor
                        val1 = data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1); % value at first address
                        if(~strcmp(nufft_type, 'forw'))
                            val1 = swap_complex_components(val1);
                        end

                        % Add/Subtract the result from the second value
                        val2 = data_matrix(x_accel_idx+1, y_accel_idx-comb_pipe_offset+1, tile_idx_2+1); % value at second address
                        if(~strcmp(nufft_type, 'forw'))
                            val2 = swap_complex_components(val2);
                        end

                        a = val2-val1.*w; % 1st part of the "butterfly" creating operation
                        b = val2+val1.*w; % 2nd part of the "butterfly" creating operation
                        
%                             %fprintf(output_file,"a_real=%f\ta_imag=%f\tb_real=%f\tb_imag=%f\n", real(a), imag(a), real(b), imag(b));
                        % fprintf(output_file,"start_val=%d\tstep_val=%d\tend_val=%d\toffset=%d\tcomb_pipe_offset=%d\tw_mask=%d\tbit_shift_offset=%d\tnum_stages=%d\tstage=%d\tx_accel_idx=%d\ty_accel_idx=%d\tset_idx=%d\titer1=%d\titer2=%d\tsaved_val=%d\ttile_idx_1=%d\ttile_idx_2=%d\tw_addr=%d\n", start_val, step_val, end_val, offset, comb_pipe_offset, w_mask, bit_shift_offset, num_stages, stage, x_accel_idx, y_accel_idx, set_idx, iter1, iter2, saved_val, tile_idx_1, tile_idx_2, w_addr);
                        % fprintf(output_file,"w_real=%.5f\tw_imag=%.5f\n", real(w), imag(w));
                        % fprintf(output_file,"stage=%d\tval1_real=%.10f\tval1_imag=%.10f\n", stage, real(val1), imag(val1));
                        
                        if(~strcmp(nufft_type, 'forw'))
                            a = swap_complex_components(a);
                            b = swap_complex_components(b);
                        end
                        data_matrix(x_accel_idx+1, y_accel_idx+1, tile_idx_1+1) = a;
                        data_matrix(x_accel_idx+1, y_accel_idx-comb_pipe_offset+1, tile_idx_2+1) = b;
                    end
                end
            end
        end
    end
    offset = fix(offset / 2);
    comb_pipe_offset = fix(comb_pipe_offset / 2);
    w_mask = bitset(w_mask, max(14-(stage+bit_shift_offset), 1));
end
fclose(output_file);
if(strcmp(nufft_type, 'forw'))
    fprintf('2D FFT Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
else
    fprintf('2D IFFT Time: %s\n', datestr(toc/(24*60*60), 'DD:HH:MM:SS.FFF'))
end

end
